<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class AddAutogradeToExercisesTable extends Migration
{    /*** Run the migrations.
     ** @return void
     */
    public function up()
    {     Schema::table('exercises', function (Blueprint $table) {
            $table->integer('autograde')->nullable();// autograde (0 or 1)
        });
    }
    /*** Reverse the migrations.
     *@return void */

            public function down()
    {        Schema::table('exercises', function (Blueprint $table) {
         });
    }
}
