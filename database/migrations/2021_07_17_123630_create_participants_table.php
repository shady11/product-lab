<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();

            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('patronymic')->nullable();

            $table->string('image')->nullable();

            $table->string('birth_date')->nullable();
            $table->boolean('gender')->default(0);
            $table->string('phone')->nullable();

            $table->string('login')->nullable();
            $table->string('email');
            $table->string('password')->nullable();

            $table->foreignId('region_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('team_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();

            $table->string('activity')->nullable();
            $table->text('socio_economic_status')->nullable();
            $table->text('type')->nullable();
            $table->text('goal')->nullable();
            $table->text('motivation')->nullable();

            $table->boolean('participated')->default(0);
            $table->string('ready')->nullable();

            $table->integer('grade')->default(0);

            $table->boolean('status')->default(0);

            $table->string('token');

            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
