<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->id();

            $table->string('name')->nullable();//наименование задания
            $table->text('description')->nullable();//описания задания
            $table->string('link')->nullable();// ссылка
            $table->integer('type')->nullable();// загрузка
            $table->string('deadline')->nullable();//дата срок выполнения
            $table->integer('grade')->nullable();//оценка баллы

            $table->foreignId('user_id')->nullable()->constrained()->cascadeOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
