<?php

use App\Models\Grade;
use App\Models\MailMessage;
use App\Models\Sdg;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

use App\Models\Participant;
use App\Models\Region;
use App\Models\User;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');


        User::truncate();

        // test user - admin
        $user = User::create([

            'email' => 'test@gmail.com',
            'login' => 'test@gmail.com',
            'password' => bcrypt('123qwe'),

            'name' => 'Test',
            'lastname' => 'Testov',
            'patronymic' => 'Testovich',

            'status' => 1
        ]);

        $role = Role::create(['name' => 'Администратор']);
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);

        // test user - mentor
        $mentor = User::create([

            'email' => 'mentor@gmail.com',
            'login' => 'mentor@gmail.com',
            'password' => bcrypt('123qwe'),

            'name' => 'Ментор',
            'lastname' => 'Менторов',
            'patronymic' => 'Менторович',

            'status' => 1
        ]);

        $role_mentor = Role::create(['name' => 'Ментор']);
        $mentor_permissions = [
            'teams-index',
            'teams-create',
            'teams-update',
            'teams-delete',

            'participants-index',
            'participants-create',
            'participants-update',
            'participants-delete',

            'topics-index',
            'topics-create',
            'topics-update',
            'topics-delete',

            'materials-index',
            'materials-create',
            'materials-update',
            'materials-delete',

            'exercises-index',
            'exercises-create',
            'exercises-update',
            'exercises-delete',

            'events-index',
        ];
        $permissions_mentor = Permission::whereIn('name', $mentor_permissions)->pluck('id','id')->all();
        $role_mentor->syncPermissions($permissions_mentor);
        $mentor->assignRole([$role_mentor->id]);

        Region::truncate();
        Region::create(['name' => 'Бишкек','name_ru' => 'Бишкек']);
        Region::create(['name' => 'Беловодск','name_ru' => 'Беловодск']);
        Region::create(['name' => 'Талас','name_ru' => 'Талас']);
        Region::create(['name' => 'Угут','name_ru' => 'Угут']);
        Region::create(['name' => 'Каракол','name_ru' => 'Каракол']);
        Region::create(['name' => 'Жалал-Абад','name_ru' => 'Жалал-Абад']);
        Region::create(['name' => 'Сузак','name_ru' => 'Сузак']);
        Region::create(['name' => 'Ош','name_ru' => 'Ош']);
        Region::create(['name' => 'Гулистан','name_ru' => 'Гулистан']);
        Region::create(['name' => 'Кызыл-Кия','name_ru' => 'Кызыл-Кия']);
        Region::create(['name' => 'Нарын','name_ru' => 'Нарын']);
        Region::create(['name' => 'Кара-Буура','name_ru' => 'Кара-Буура']);
        Region::create(['name' => 'Баткен','name_ru' => 'Баткен']);
        Region::create(['name' => 'Кашкар Кыштак','name_ru' => 'Кашкар Кыштак']);

        Sdg::truncate();
        Sdg::create(['name' => 'Цель устойчивого развития 1','name_ru' => 'Цель устойчивого развития 1']);
        Sdg::create(['name' => 'Цель устойчивого развития 2','name_ru' => 'Цель устойчивого развития 2']);

        MailMessage::truncate();
        MailMessage::create([
            'code' => 'participant_passed',
            'name' => 'Поздравительное письмо',
            'header' => 'Поздравляем!',
            'content' => 'Вы успешно прошли отбор на Молодежный конкурс социальных инноваций "UStart"! Для регистрации на конкурсе нажмите на кнопку ниже',
            'link' => 'Если кнопка не работает, вставьте ссылку ниже в браузер:',
            'footer' => 'С уважением, Product Lab.',
        ]);
        MailMessage::create([
            'code' => 'participant_failed',
            'name' => 'Мотивационное письмо',
            'header' => 'Здравствуйте!',
            'content' => 'К сожалению вы не прошли отбор на Молодежный конкурс социальных инноваций "UStart". Но вы можете принять участие в наших следующих проектах!',
            'link' => '',
            'footer' => 'С уважением, Product Lab.',
        ]);

        Grade::truncate();
        Grade::create([
            'code' => 'apply_grade',
            'name' => 'Балл за прохождение отбора',
            'content' => '60',
        ]);

        // test participant
        Participant::truncate();
        Participant::create([

            'name' => 'Test 2',
            'lastname' => 'Testov 2',
            'patronymic' => 'Testovich 2',

            'login' => 'test2',
            'email' => 'test2@gmail.com',
            'password' => bcrypt('123qwe'),

            'region_id' => 1,
            'status' => 1,
            'grade' => 0,
            'token' => Str::random(64)
        ]);
        Participant::create([

            'name' => 'Test 3',
            'lastname' => 'Testov 3',
            'patronymic' => 'Testovich 3',

            'login' => 'test3',
            'email' => 'test3@gmail.com',
            'password' => bcrypt('123qwe'),

            'region_id' => 2,
            'status' => 1,
            'grade' => 0,
            'token' => Str::random(64)
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
