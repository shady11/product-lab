<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'roles-index',
            'roles-create',
            'roles-update',
            'roles-delete',

            'permissions-index',
            'permissions-create',
            'permissions-update',
            'permissions-delete',

            'users-index',
            'users-create',
            'users-update',
            'users-delete',

            'grades-index',
            'grades-create',
            'grades-update',
            'grades-delete',

            'mail-messages-index',
            'mail-messages-create',
            'mail-messages-update',
            'mail-messages-delete',

            'teams-index',
            'teams-create',
            'teams-update',
            'teams-delete',

            'participants-index',
            'participants-create',
            'participants-update',
            'participants-delete',

            'topics-index',
            'topics-create',
            'topics-update',
            'topics-delete',

            'materials-index',
            'materials-create',
            'materials-update',
            'materials-delete',

            'exercises-index',
            'exercises-create',
            'exercises-update',
            'exercises-delete',

            'regions-index',
            'regions-create',
            'regions-update',
            'regions-delete',

            'sdg-index',
            'sdg-create',
            'sdg-update',
            'sdg-delete',

            'events-index',
            'report-index',

            'news-index',
            'news-create',
            'news-update',
            'news-delete',

            'feedbacks-index',
            'feedbacks-create',
            'feedbacks-update',
            'feedbacks-delete',

            'mentors-index',
            'mentors-create',
            'mentors-update',
            'mentors-delete'
        ];

        foreach ($permissions as $row) {
            Permission::create(['name' => $row]);
        }
    }
}
