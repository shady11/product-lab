<?php

use Illuminate\Support\Facades\Route;

Route::prefix(LaravelLocalization::setLocale().'/participant')->middleware('localeSessionRedirect', 'localizationRedirect', 'localeViewPath')->group(function () {

    /// Auth routes
    Route::name('participant.login')->get('login','Auth\LoginController@showLoginForm');
    Route::name('participant.login.submit')->post('login', 'Auth\LoginController@login');
    Route::name('participant.logout')->get('logout', 'Auth\LoginController@logout');
    Route::name('participant.register')->get('register/{token}','Auth\LoginController@showRegisterForm');
    Route::name('participant.register.submit')->post('register', 'Auth\LoginController@register');
    Route::name('participant.forget-password')->get('forget-password','Auth\LoginController@showForgetPasswordForm');
    Route::name('participant.forget-password.submit')->post('forget-password', 'Auth\LoginController@forgetPassword');
    Route::name('participant.check-email')->get('check-email','Auth\LoginController@checkEmail');
    Route::name('participant.reset-password')->get('reset-password/{token}','Auth\LoginController@showResetPasswordForm');
    Route::name('participant.reset-password.submit')->post('reset-password', 'Auth\LoginController@resetPassword');

    /// General
    Route::name('participant.index')->get('', ['uses' => 'IndexController@index']);

    Route::name('participant.profile')->get('profile', ['uses' => 'IndexController@profile']);
    Route::name('participant.profile.update')->post('profile/update', ['uses' => 'IndexController@profileUpdate']);

    Route::name('events.index')->get('events', ['uses' => 'IndexController@events']);
    Route::name('events.data')->get('events/data', ['uses' => 'IndexController@eventsData']);
    Route::name('events.data.view')->get('events/data/view/{id}', ['uses' => 'IndexController@viewEvent']);

    // Resource Routes
    Route::resource('teams', 'TeamController');
    Route::resource('exercises', 'ExerciseController');
    Route::resource('topics', 'TopicController');
    Route::resource('materials', 'MaterialController');

    // Delete Routes
    Route::name('teams.delete')->get('teams/{team}/delete', ['uses' => 'TeamController@destroy']);
    Route::name('exercises.delete')->get('exercises/{exercise}/delete', ['uses' => 'ExerciseController@destroy']);

    // Team Routes
    Route::name('teams.delete_user')->get('teams/{team}/delete/{participant}', ['uses' => 'TeamController@delete_user']);
    Route::name('teams.particiantAdd')->post('teams/{team}/add/participant', ['uses' => 'TeamController@particiantAdd']);
    Route::name('team.enter')->get('team/enter', ['uses' => 'TeamController@enter']);
    Route::name('team.enter.submit')->post('team/enter', ['uses' => 'TeamController@enterSubmit']);
    Route::name('teams.info')->post('teams/info', ['uses' => 'TeamController@info']);
    Route::name('teams.exit')->get('teams/{team}/exit', ['uses' => 'TeamController@teamExit']);
    Route::name('team.profile')->get('team/profile', ['uses' => 'TeamController@teamProfile']);
    Route::name('team.set_leader')->get('team/{team}/set/leader/{participant}', ['uses' => 'TeamController@setLeader']);

    // Exercises Routes
    Route::name('exercise.participant.file')->post('exercise/{exercise}/participant/file', ['uses' => 'ExerciseController@participantFile']);

});
