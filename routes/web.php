<?php

use Illuminate\Support\Facades\Route;

Route::middleware('localeSessionRedirect', 'localizationRedirect', 'localeViewPath')->prefix(LaravelLocalization::setLocale())->group(function () {

    Route::name('web.index')->get('', 'IndexController@index');
    Route::name('web.news')->get('news', 'IndexController@news');
    Route::name('web.news.single')->get('news/single/{id}', 'IndexController@newsSingle');

    Route::name('participant.apply')->get('apply','IndexController@showApplyForm');
    Route::name('participant.apply.submit')->post('apply', 'IndexController@apply');

    Route::name('web.login')->get('login', 'IndexController@login');
    Route::name('web.login.submit')->post('login', 'IndexController@loginSubmit');

});
