<?php

use Illuminate\Support\Facades\Route;

Route::prefix(LaravelLocalization::setLocale().'/admin')
    ->middleware('localeSessionRedirect', 'localizationRedirect', 'localeViewPath')
    ->group(function () {

        Route::group(['as' => 'admin.'], function() {

            Route::name('login')->get('login','Auth\LoginController@showLoginForm');
            Route::name('login.submit')->post('login', 'Auth\LoginController@login');
            Route::name('logout')->get('logout', 'Auth\LoginController@logout');

            Route::name('index')->get('', ['uses' => 'IndexController@index']);

            // applications
            Route::name('applications.index')->get('applications', ['uses' => 'IndexController@applications']);
            Route::name('applications.show')->get('applications/{participant}', ['uses' => 'IndexController@applicationsShow']);
            Route::name('applications.delete')->get('applications/{participant}/delete', ['uses' => 'IndexController@applicationsDelete']);

            // events
            Route::group(['as' => 'events.'], function() {
                Route::name('index')->get('/events', ['uses' => 'EventController@index']);
                Route::name('data')->get('/events/data', ['uses' => 'EventController@data']);
                Route::name('data.add')->post('/events/data/add', ['uses' => 'EventController@add']);
                Route::name('data.view')->get('/events/data/view/{id}', ['uses' => 'EventController@view']);
                Route::name('data.delete')->delete('/events/data/delete/{row}', ['uses' => 'EventController@delete']);
            });

            // Report
            Route::group(['as' => 'report.'], function() {
                Route::name('index')->get('/report', ['uses' => 'ReportController@index']);
                Route::name('ishtapp')->get('/report/ishtapp', ['uses' => 'ReportController@ishtapp']);
                Route::name('ishtapp.users')->get('/report/ishtapp/users', ['uses' => 'ReportController@ishtappUsers']);
                Route::name('ishtapp.companies')->get('/report/ishtapp/companies', ['uses' => 'ReportController@ishtappCompanies']);
                Route::name('ishtapp.opportunities')->get('/report/ishtapp/opportunities', ['uses' => 'ReportController@ishtappOpportunities']);
            });

            /*
             * ADMIN CONTROL
            */

            Route::resources([
                'roles' => 'RoleController',
                'permissions' => 'PermissionController',
                'topics' => 'TopicController',
                'exercises'=>'ExerciseController',
                'users' => 'UserController',
                'participants' => 'ParticipantController',
                'regions' => 'RegionController',
                'sdg' => 'SdgController',
                'teams' => 'TeamController',
                'materials' => 'MaterialController',
                'grades' => 'GradeController',
                'mail_messages' => 'MailMessageController',
            ]);

            // Participant Routes
            Route::name('participants.grade')->post('/participants/{participant}/grade', ['uses' => 'ParticipantController@grade']);

            // Team Routes
            Route::name('teams.delete_user')->get('teams/{team}/delete/{participant}', ['uses' => 'TeamController@delete_user']);
            Route::name('teams.particiantAdd')->post('teams/{team}/add/participant', ['uses' => 'TeamController@particiantAdd']);

            // Exercise Routes
            Route::name('exercise.participant.info')->post('exercise/{exercise}/participant/info', ['uses' => 'ExerciseController@participantInfo']);
            Route::name('exercise.team.info')->post('exercise/{exercise}/team/info', ['uses' => 'ExerciseController@teamInfo']);
            Route::name('exercise.participant.grade')->post('exercise/{exercise}/participant/grade', ['uses' => 'ExerciseController@participantGrade']);
            Route::name('exercise.team.grade')->post('exercise/{exercise}/team/grade', ['uses' => 'ExerciseController@teamGrade']);
            Route::name('exercise.create_')->get('exercise/{topic}/create', ['uses' => 'ExerciseController@create_']);

            // Materials Routes
            Route::name('material.create_')->get('material/{topic}/create', ['uses' => 'MaterialController@create_']);

            // Delete Routes
            Route::name('teams.delete')->get('teams/{team}/delete', ['uses' => 'TeamController@destroy']);
            Route::name('permissions.delete')->get('permissions/{permission}/delete', ['uses' => 'PermissionController@destroy']);
            Route::name('roles.delete')->get('roles/{role}/delete', ['uses' => 'RoleController@destroy']);
            Route::name('users.delete')->get('users/delete/{user}', ['uses' => 'UserController@destroy']);
            Route::name('participants.delete')->get('participants/delete/{participant}', ['uses' => 'ParticipantController@destroy']);
            Route::name('regions.delete')->get('regions/delete/{region}', ['uses' => 'RegionController@destroy']);
            Route::name('sdg.delete')->get('sdg/delete/{sdg}', ['uses' => 'SdgController@destroy']);
            Route::name('exercises.delete')->get('exercise/{exercise}/delete', ['uses' => 'ExerciseController@delete']);
            Route::name('topics.delete')->get('topic/{topic}/delete', ['uses' => 'TopicController@delete']);
            Route::name('materials.delete')->get('material/{material}/delete', ['uses' => 'MaterialController@delete']);
            Route::name('grades.delete')->get('grades/{grade}/delete', ['uses' => 'GradeController@delete']);
            Route::name('mail_messages.delete')->get('mail_messages/{mail_message}/delete', ['uses' => 'MailMessageController@delete']);

            // User [login, password] change
            Route::name('users.changeLogin')->put('users/changeLogin/{user}',  ['uses' => 'UserController@changeLogin']);
            Route::name('users.changePassword')->put('users/changePassword/{user}',  ['uses' => 'UserController@changePassword']);
            /*
             * LANDING PAGE CONTROL
            */
            Route::group(['as' => 'landing.'], function() {
                Route::resources([
                    'news' => 'NewsController',
                    'feedbacks' => 'FeedbackController',
                    'mentors' => 'MentorController'
                ]);

                Route::name('news.delete')->get('news/delete/{news}', ['uses' => 'NewsController@destroy']);
                Route::name('feedbacks.delete')->get('feedbacks/delete/{feedback}', ['uses' => 'FeedbackController@destroy']);
                Route::name('mentors.delete')->get('mentors/delete/{mentor}', ['uses' => 'MentorController@destroy']);
            });
        });
});
