$(document).ready(function () {
	"use strict"; // start of use strict

	/*==============================
	Mobile navigation
	==============================*/
	$('.header__btn').on('click', function() {
		$(this).toggleClass('header__btn--active');
		$('.header__nav').toggleClass('header__nav--active');

		if ( $(window).scrollTop() == 0 ) {
			$('.header').toggleClass('header--active');
		}
	});

	$('.header__nav a[data-scroll]').on('click', function() {
		$('.header__nav').toggleClass('header__nav--active');
		$('.header__btn').toggleClass('header__btn--active');
	});

	/*==============================
	Header
	==============================*/
	$(window).on('scroll', function () {
		if ( $(window).scrollTop() > 0 ) {
			$('.header').addClass('header--active');
		} else {
			$('.header').removeClass('header--active');
		}
	});
	$(window).trigger('scroll');

	/*==============================
	Parallax
	==============================*/
	if ($(window).width() > 1200) {
		if ($('#form__parallax').length) {
			var scene = document.getElementById('form__parallax');
			var parallaxInstance = new Parallax(scene);
		}

		if ($('#hero__dates').length) {
			var scene1 = document.getElementById('hero__dates');
			var parallaxInstance1 = new Parallax(scene1);
		}

		if ($('#hero__about').length) {
			var scene2 = document.getElementById('hero__about');
			var parallaxInstance2 = new Parallax(scene2);
		}

		if ($('#hero__parallax').length) {
			var scene3 = document.getElementById('hero__parallax');
			var parallaxInstance3 = new Parallax(scene3);
		}
	}


});