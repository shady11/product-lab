<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ParticipantPassed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $link, $header, $content, $link_text, $footer)
    {
        $this->email = $email;
        $this->link = $link;
        $this->header = $header;
        $this->content = $content;
        $this->linkt_text = $link_text;
        $this->footer = $footer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.participant-passed')
            ->subject('Добро пожаловать на Молодежный конкурс социальных инноваций "UStart"!')
            ->with([
                'email' => $this->email,
                'link' => $this->link,
                'header' => $this->header,
                'content' => $this->content,
                'link_text' => $this->linkt_text,
                'footer' => $this->footer,
            ]);
    }
}
