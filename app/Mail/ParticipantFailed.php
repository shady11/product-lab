<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ParticipantFailed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $header, $content, $footer)
    {
        $this->email = $email;
        $this->header = $header;
        $this->content = $content;
        $this->footer = $footer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.participant-failed')
            ->subject('Молодежный конкурс социальных инноваций "UStart"!')
            ->with([
                'email' => $this->email,
                'header' => $this->header,
                'content' => $this->content,
                'footer' => $this->footer,
            ]);
    }
}
