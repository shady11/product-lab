<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespaceWeb = 'App\Http\Controllers\Web';
    protected $namespaceAdmin = 'App\Http\Controllers\Admin';
    protected $namespaceParticipant = 'App\Http\Controllers\Participant';

    const HOME_WEB = '/';
    const HOME_ADMIN = '/admin';
    const HOME_PARTICIPANT = '/participant';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapWebRoutes();
        $this->mapAdminRoutes();
        $this->mapParticipantRoutes();
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespaceWeb)
            ->group(base_path('routes/web.php'));
    }

    protected function mapAdminRoutes()
    {
        Route::middleware('admin')
            ->namespace($this->namespaceAdmin)
            ->group(base_path('routes/admin.php'));
    }

    protected function mapParticipantRoutes()
    {
        Route::middleware('participant')
            ->namespace($this->namespaceParticipant)
            ->group(base_path('routes/participant.php'));
    }
}
