<?php

namespace App\Providers;

use App\Models\Participant;
use App\Models\Product;
use App\Observers\ParticipantObserver;
use App\Observers\ProductObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function (View $view) {

            $current_year = date('Y');
            $current_month = date('m');

            $view->with('current_year', $current_year);
            $view->with('current_month', $current_month);

        });
    }
}
