<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Exercise extends Model
{
    use SearchableTrait;

    protected $table = 'exercises';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10
        ]
    ];

    public function topic(){
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function getTopic(){
        $topic = Topic::findOrFail($this->topic_id);
        if ($topic){
            return $topic->name;
        }
        return '';
    }

    // relations
    public function exercise($participant)
    {
        if($participant){
            return ParticipantExercise::where('exercise_id', $this->id)->where('participant_id', $participant)->first();
        }
        return false;
    }

    // relation with team
    public function team($team)
    {
        if($team){
            return TeamExercise::where('exercise_id', $this->id)->where('team_id', $team)->first();
        }
        return false;
    }

    public function status($id){
        $participantExercise = ParticipantExercise::where('participant_id', $id)->where('exercise_id', $this->id)->first();
        if ($participantExercise){
            return $participantExercise->status;
        }
        return false;
    }

    public function status_team($id){
        $teamExercise = TeamExercise::where('team_id', $id)->where('exercise_id', $this->id)->first();
        if ($teamExercise){
            return $teamExercise->status;
        }
        return false;
    }
}
