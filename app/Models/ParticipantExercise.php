<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class ParticipantExercise extends Model
{
    use SearchableTrait;

    protected $table = 'participant_exercises';

    protected $guarded = ['id'];
}
