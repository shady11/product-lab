<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class IshtappUser extends Model
{
    use SearchableTrait;

    protected $connection = 'mysql2';

    protected $table = 'users';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'email' => 9
        ]
    ];

    public function getRegion()
    {
        return $this->belongsTo(IshtappRegion::class, 'region');
    }

    public function getOpportunities()
    {
        return $this->hasMany(IshtappVacancy::class, 'company_id')->where('is_product_lab_vacancy', true);
    }
}
