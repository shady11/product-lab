<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class TeamExercise extends Model
{
    use SearchableTrait;

    protected $table = 'team_exercises';

    protected $guarded = ['id'];
}
