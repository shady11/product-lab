<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class MailMessage extends Model
{
    use SearchableTrait;

    protected $table = 'mail_messages';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'code' => 10,
            'name' => 10,
        ],
    ];
}
