<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Topic extends Model
{
    use SearchableTrait;

    protected $table = 'topics';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'topics.name' => 10
        ]
    ];

    public function exercises()
    {
        return $this->hasMany(Exercise::class, 'topic_id');
    }

    public function materials()
    {
        return $this->hasMany(Material::class, 'topic_id');
    }
}
