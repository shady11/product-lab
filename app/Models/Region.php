<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Region extends Model
{
    use SearchableTrait;

    protected $table = 'regions';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'id' => 10,
            'name' => 10,
        ],
    ];

    public function getTitle($lang)
    {
        if($lang == 'ru') return $this->name_ru;
        return $this->name;
    }

    public function getName($lang)
    {
        if($lang == 'ru') return $this->name_ru;
        return $this->name;
    }

    //relations
    public function participants()
    {
        return $this->hasMany(Participant::class, 'region_id');
    }
    public function teams()
    {
        return $this->hasMany(Team::class, 'region_id');
    }
}
