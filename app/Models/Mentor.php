<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Mentor extends Model
{
    use SearchableTrait;

    protected $guard = 'admin';
    protected $table = 'mentors';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
        ]
    ];

    protected $appends = ['full_name'];

    public function getName()
    {
        return $this->lastname.' '.$this->name;
    }

    public function getFullName()
    {
        return $this->lastname.' '.$this->name.' '.$this->patronymic;
    }
    public function getInitials()
    {
        return $this->name[0];
    }

    // permissions
    public function permissionTo($permission)
    {
        $can = false;

        if($this->sections){
            foreach ($this->sections as $key => $id) {
                $section = Section::find($id);
                if($section->code == $permission) $can = true;
            }
        }

        return $can;
    }

    // attributes
    public function getFullNameAttribute()
    {
        return $this->getFullName();
    }
}
