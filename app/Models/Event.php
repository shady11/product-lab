<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $guarded = ['id'];

    // attributes
    public function getType()
    {
        // 1 - Онлайн, 0 - Оффлайн
        if($this->type == 1){
            return 'Онлайн';
        }else{
            return 'Оффлайн';
        }
    }
    public function getEventDates()
    {
        $date = date('d.m.Y', strtotime($this->start_date));
        if($this->end_date && ($this->start_date != $this->end_date)){
            $date .= '-'.date('d.m.Y', strtotime($this->end_date));
        }
        return $date;
    }
    public function getEventTimes()
    {
        $time = $this->start_time;
        if($this->end_time && ($this->start_time != $this->end_time)){
            $time .= '-'.$this->end_time;
        }
        return $time;
    }

    // relations
    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getRegion()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }
}


