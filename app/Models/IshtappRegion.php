<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class IshtappRegion extends Model
{
    use SearchableTrait;

    protected $connection = 'mysql2';

    protected $table = 'regions';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'nameKg' => 10,
            'nameRu' => 10,
            'nameEn' => 10,
        ]
    ];
}
