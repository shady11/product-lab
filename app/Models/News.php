<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class News extends Model
{
    use SearchableTrait;

    protected $guard = 'admin';
    protected $table = 'news';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'id' => 10,
            'name' => 10,
        ],
    ];

    // get => Carbon::class
    protected $dates = [
        'published_at'
    ];


    public function getTitle($lang)
    {
        if($lang == 'ru') return $this->name_ru;
        return $this->name;
    }

    public function getContent($lang)
    {
        if($lang == 'ru') return $this->content_ru;
        return $this->content;
    }

    public function getPublishedDate($lang = 'ky') {
        $months = (object) [
            'ru' => [ 'Января', 'Февраля', 'Марта',
                'Апреля', 'Мая', 'Июня',
                'Июля',  'Августа', 'Сентября',
                'Октября', 'Ноября', 'Декабря'
            ],
            'ky' => [ 'Январь', 'Февраль', 'Март',
                'Апрель', 'Май', 'Июнь',
                'Июль',  'Август', 'Сентябрь',
                'Октябрь', 'Ноябрь', 'Декабрь'
            ]
        ];

        if($this->published_at){
            $date = $this->published_at->day . '-' .
                $months->$lang[$this->published_at->month - 1] . ', ' .
                $this->published_at->year;
        } else {
            $date = $this->created_at->day . '-' .
                $months->$lang[$this->created_at->month - 1] . ', ' .
                $this->created_at->year;
        }

        return $date;
    }
}
