<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Grade extends Model
{
    use SearchableTrait;

    protected $table = 'grades';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'code' => 10,
            'content' => 10,
        ],
    ];
}
