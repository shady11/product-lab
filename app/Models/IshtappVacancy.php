<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class IshtappVacancy extends Model
{
    use SearchableTrait;

    protected $connection = 'mysql2';

    protected $table = 'vacancies';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'description' => 9
        ]
    ];

    public function getRegion()
    {
        return $this->belongsTo(IshtappRegion::class, 'region_id');
    }

    public function getCompany()
    {
        return $this->belongsTo(IshtappUser::class, 'company_id');
    }

    public function getOpportunity()
    {
        return $this->belongsTo(IshtappOpportunity::class, 'opportunity_id');
    }
}
