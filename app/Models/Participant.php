<?php

namespace App\Models;

use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Nicolaslopezj\Searchable\SearchableTrait;

class Participant extends Authenticatable
{
    use SearchableTrait;

    protected $table = 'participants';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'lastname' => 10,
        ]
    ];

    protected $appends = ['full_name', 'full_name_region'];

    protected $casts = [
        'socio_economic_status' => 'array',
        'type' => 'array',
        'goal' => 'array',
    ];

    // relations
    public function getRegion()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    public function exercises()
    {
        return $this->hasMany(ParticipantExercise::class, 'participant_id');
    }
    public function exercise($exercise)
    {
        if($exercise){
            return ParticipantExercise::where('exercise_id', $exercise)->where('participant_id', $this->id)->first();
        }
        return false;
    }

    public function exerciseItem(){
        return $this->belongsTo(ParticipantExercise::class, 'id', 'participant_id');
    }


    // attributes
    public function getName()
    {
        return $this->lastname.' '.$this->name;
    }
    public function getInitials()
    {
        return $this->lastname[0].''.$this->name[0];
    }
    public function getCreatedAt()
    {
        return date('d-m-Y', strtotime($this->created_at));
    }
    public function getCreatedAtDateTime()
    {
        return date('d-m-Y H:i', strtotime($this->created_at));
    }
    public function getBirthDate()
    {
        if($this->birth_date)
            return date('d-m-Y', strtotime($this->birth_date));
        return '-';
    }
    public function getStatus()
    {
        $status = '';
        if($this->status){
            $status .= '<span class="d-block text-success font-size-sm">верифицирован</span>';
        } else {
            $status .= '<span class="d-block text-muted font-size-sm">не верифицирован</span>';
        }
        if($this->entered_at){
            $status .= '<span class="d-block text-success font-size-sm">в структуре</span>';
        }
        return $status;
    }
    public function getGender()
    {
        return $this->gender ? 'мужской' : 'женский';
    }
    public function getSocioEconomicStatus()
    {
        if($this->socio_economic_status){
            $socio_economic_status = '<ul>';
            foreach ($this->socio_economic_status as $key => $row) {
                $socio_economic_status .= '<li>'.$row.'</li>';
            }
            $socio_economic_status .= '</ul>';
            return $socio_economic_status;
        } else {
            return '-';
        }
    }
    public function getType()
    {
        if($this->type){
            $type = '<ul>';
            foreach ($this->type as $key => $row) {
                $type .= '<li>'.$row.'</li>';
            }
            $type .= '</ul>';
            return $type;
        } else {
            return '-';
        }
    }
    public function getGoal()
    {
        if($this->goal){
            $goal = '<ul>';
            foreach ($this->goal as $key => $row) {
                $goal .= '<li>'.$row.'</li>';
            }
            $goal .= '</ul>';
            return $goal;
        } else {
            return '-';
        }
    }
    public function getParticipated()
    {
        return $this->participated ? 'Да / Ооба' : 'Нет / Жок';
    }
    public function getReady()
    {
        return $this->ready ? 'Да / Ооба' : 'Нет / Жок';
    }
    public function getFullNameAttribute()
    {
        return $this->lastname.' '.$this->name.' '.$this->patronymic;
    }
    public function getFullNameRegionAttribute()
    {
        $lang = app()->getLocale();
        if($this->getRegion){
            return $this->lastname.' '.$this->name.' ('.$this->getRegion->getName($lang).')';
        }
        return $this->lastname.' '.$this->name;
    }

}
