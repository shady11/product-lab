<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use SearchableTrait, HasRoles;

    protected $guard = 'admin';
    protected $table = 'users';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'login' => 9
        ]
    ];

    protected $appends = ['full_name'];

    public function getName()
    {
        return $this->lastname.' '.$this->name;
    }

    public function getFullName()
    {
        return $this->lastname.' '.$this->name.' '.$this->patronymic;
    }
    public function getInitials()
    {
//        return $this->lastname[0].''.$this->name[0];
        return $this->name[0];
    }

    // attributes
    public function getFullNameAttribute()
    {
        return $this->getFullName();
    }
}
