<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Team extends Model
{
    use SearchableTrait;

    protected $table = 'teams';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'teams.name' => 10
        ]
    ];

    protected $casts = [
        'sdgs' => 'array',
    ];

    // relations
    public function region(){
        return $this->belongsTo(Region::class, 'region_id');
    }
    public function sdg(){
        return $this->belongsTo(Sdg::class, 'sdg_id');
    }
    public function mentor(){
        return $this->hasOne(User::class, 'team_id');
    }
    public function leader(){
        return $this->belongsTo(Participant::class, 'leader_id');
    }
    public function participants()
    {
        return $this->hasMany(Participant::class, 'team_id');
    }

    public function exercises()
    {
        return $this->hasMany(TeamExercise::class, 'team_id');
    }

    public function lider(){
        if ($this->leader_id){
            $participant = Participant::findOrFail($this->leader_id);
            if ($participant){
                return $participant->id;
            }
        }
        return null;
    }

    public function exercise($exercise)
    {
        if($exercise){
            return TeamExercise::where('exercise_id', $exercise)->where('team_id', $this->id)->first();
        }
        return false;
    }

    // attributes
    public function sdgsList()
    {
        $lang = app()->getLocale();
        if($this->sdgs){
            $sdgs = '<ul class="list-unstyled">';
            foreach ($this->sdgs as $key => $row) {
                $sdg = Sdg::find($row);
                $sdgs .= '<li>- '.$sdg->getName($lang).'</li>';
            }
            $sdgs .= '</ul>';
            return $sdgs;
        } else {
            return '-';
        }
    }


}
