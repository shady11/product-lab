<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class IshtappOpportunity extends Model
{
    use SearchableTrait;

    protected $connection = 'mysql2';

    protected $table = 'opportunities';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'name_ru' => 9
        ]
    ];
}
