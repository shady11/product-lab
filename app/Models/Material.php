<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Material extends Model
{
    use SearchableTrait;

    protected $table = 'materials';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'materials.name' => 10
        ]
    ];

    public function topic(){
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function getTopic(){
        $topic = Topic::findOrFail($this->topic_id);
        if ($topic){
            return $topic->name;
        }
        return null;
    }

    public function getVideo(){
        if (preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $this->videourl, $matches)){
            if ($matches[0])
                return $matches[0];
        };
        return null;
    }


}
