<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Feedback extends Model
{
    use SearchableTrait;

    protected $guard = 'admin';
    protected $table = 'feedbacks';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'id' => 10,
            'author' => 10,
        ],
    ];

}
