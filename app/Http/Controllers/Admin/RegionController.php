<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\DataTables;

class RegionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:regions-index');
        $this->middleware('permission:regions-create', ['only' => ['create','store']]);
        $this->middleware('permission:regions-update', ['only' => ['edit','update']]);
        $this->middleware('permission:regions-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Регионы';

        if(request()->ajax()) {

            $data = Region::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.regions.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.regions.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.regions.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('name', function ($row) { return $row->name; })
                ->addColumn('name_ru', function ($row) { return $row->name_ru; })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.regions.index', compact('title'));
    }


    public function create()
    {
        $region = new Region();
        $title = 'Создание новости';

        return view('admin.regions.create', compact('region', 'title'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_ru' => ['required'],
            'name' => ['required'],
        ], array(
            'name_ru.required' => 'Поля является объзательным к заполнению',
            'name.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.regions.create')
                ->withErrors($validator)
                ->withInput();
        }

        $region = Region::create($request->all());
        $region->save();

        return redirect()->route('admin.regions.show', $region);
    }

    public function show(Region $region)
    {
        $title = 'Просмотр';
        return view('admin.regions.show', compact('region', 'title'));
    }



    public function edit(Region $region)
    {
        $title = 'Редактировование новости';
        return view('admin.regions.edit', compact('region', 'title'));
    }


    public function update(Request $request, Region $region)
    {
        $this->validate($request, [
            'name_ru' => ['required'],
            'name' => ['required'],
        ], array(
            'name_ru.required' => 'Поля является объзательным к заполнению',
            'name.required' => 'Поля является объзательным к заполнению',
        ));

        $region->update($request->all());

        return redirect()->route('admin.regions.show', $region);
    }

    public function destroy(Region $region)
    {
        $region->delete();
        return redirect()->route('admin.regions.index');
    }
}
