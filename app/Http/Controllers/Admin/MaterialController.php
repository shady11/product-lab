<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MaterialController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:materials-index');
        $this->middleware('permission:materials-create', ['only' => ['create','store']]);
        $this->middleware('permission:materials-update', ['only' => ['edit','update']]);
        $this->middleware('permission:materials-delete', ['only' => ['destroy']]);
    }
    public function create(){
        $title = 'Форма создания материала';
        $row = new Material();
        $topics = Topic::pluck('name', 'id')->toArray();
        return view('admin.materials.create', compact('title', 'row', 'topics'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'description' => ['required'],
//            'link' => ['required'],
            'type' => ['required'],
            'topic_id' => ['required'],
        ], array(
            'name.required' => 'Поля является объзательным к заполнению',
            'description.required' => 'Поля является объзательным к заполнению',
            'type.required' => 'Поля является объзательным к заполнению',
            'topic_id.required' => 'Поля является объзательным к заполнению',
//            'link.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.materials.create')
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->only('name', 'description', 'link', 'videourl', 'file', 'type', 'topic_id');
        $data['user_id'] = auth()->user()->id;
        $material = Material::create($data);
        if($request->hasFile('file')){
            $file = $request->file('file');

            $dir  = 'assets/materials/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $current = date('YmdHs');
            $filename = $material->id.'-'.Str::slug($material->name, '-').'-'.auth()->user()->id.'.'.$current.'-'.$file->getClientOriginalExtension();

            if($file->move(public_path($dir), $filename)){
                $material->file = $dir.$filename;
                $material->save();

                return redirect(route('admin.topics.index'));
            }
        }
        return redirect(route('admin.topics.index'));
    }

    public function show(Material $material){
        $title = 'Материал';
        $row = $material;
        return view('admin.materials.show', compact('title', 'row'));
    }

    public function edit(Material $material){
        $title = 'Форма редактирования материала';
        $row = $material;
        $topics = Topic::pluck('name', 'id')->toArray();
        return view('admin.materials.edit', compact('row', 'topics', 'title'));
    }

    public function update(Request $request, Material $material){
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'description' => ['required'],
            'type' => ['required'],
            'topic_id' => ['required'],
        ], array(
            'name.required' => 'Поля является объзательным к заполнению',
            'description.required' => 'Поля является объзательным к заполнению',
            'type.required' => 'Поля является объзательным к заполнению',
            'topic_id.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.materials.edit', $material)
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->only('name', 'description', 'link', 'videourl', 'file', 'type', 'topic_id');
        $material->update($data);
        if($request->hasFile('file')){
            $file = $request->file('file');

            $dir  = 'assets/materials/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $current = date('YmdHs');
            $filename = $material->id.'-'.Str::slug($material->name, '-').'-'.auth()->user()->id.'.'.$current.'-'.$file->getClientOriginalExtension();

            if($file->move(public_path($dir), $filename)){
                $material->file = $dir.$filename;
                $material->save();

                redirect(route('admin.topics.index'));
            }
        }
        return redirect(route('admin.topics.index'));
    }

    public function delete(Material $material){
        $material->delete();
        return redirect(route('admin.topics.index'));
    }
}
