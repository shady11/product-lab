<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sdg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class SdgController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:sdg-index');
        $this->middleware('permission:sdg-create', ['only' => ['create','store']]);
        $this->middleware('permission:sdg-update', ['only' => ['edit','update']]);
        $this->middleware('permission:sdg-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'ЦУР (Цели Устойчивого Развития)';

        if(request()->ajax()) {

            $data = Sdg::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.sdg.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.sdg.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.sdg.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('name', function ($row) { return $row->name; })
                ->addColumn('name_ru', function ($row) { return $row->name_ru; })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.sdg.index', compact('title'));
    }

    public function create()
    {
        $sdg = new Sdg();
        $title = 'Создание новости';

        return view('admin.sdg.create', compact('sdg', 'title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_ru' => ['required'],
            'name' => ['required'],
        ], array(
            'name_ru.required' => 'Поля является объзательным к заполнению',
            'name.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.sdg.create')
                ->withErrors($validator)
                ->withInput();
        }

        $sdg = Sdg::create($request->all());
        $sdg->save();

        return redirect()->route('admin.sdg.show', $sdg);
    }

    public function show(Sdg $sdg)
    {
        $title = 'Просмотр';
        return view('admin.sdg.show', compact('sdg', 'title'));
    }

    public function edit(Sdg $sdg)
    {
        $title = 'Редактировование новости';
        return view('admin.sdg.edit', compact('sdg', 'title'));
    }

    public function update(Request $request, Sdg $sdg)
    {
        $this->validate($request, [
            'name_ru' => ['required'],
            'name' => ['required'],
        ], array(
            'name_ru.required' => 'Поля является объзательным к заполнению',
            'name.required' => 'Поля является объзательным к заполнению',
        ));

        $sdg->update($request->all());

        return redirect()->route('admin.sdg.show', $sdg);
    }

    public function destroy(Sdg $sdg)
    {
        $sdg->delete();
        return redirect()->route('admin.sdg.index');
    }
}
