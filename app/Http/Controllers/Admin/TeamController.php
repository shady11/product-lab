<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Participant;
use App\Models\Region;
use App\Models\Sdg;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:teams-index');
         $this->middleware('permission:teams-create', ['only' => ['create','store']]);
         $this->middleware('permission:teams-update', ['only' => ['edit','update']]);
         $this->middleware('permission:teams-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Команды';

        if(request()->ajax()){
            $data = Team::query();
            if (request()->search['value']) {
                $data = $data->search(request()->search['value']);
            }
            $data = $data->get();
            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.teams.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.teams.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.teams.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('participants_count', function ($row) {
                    return $row->participants->count();
                })
                ->addColumn('region', function ($row) {
                    return $row->region ? $row->region->name_ru : '-';
                })
                ->addColumn('mentor', function ($row) {
                    return $row->mentor ? $row->mentor->full_name : '-';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.teams.index', compact('title'));
    }

    public function create()
    {
        $lang = app()->getLocale();
        $title = 'Форма создания команды';
        $row = new Team();
        $regions = Region::pluck('name_ru', 'id')->toArray();
        $participants = Participant::where('team_id', null)->pluck('name', 'id')->toArray();
        $participants_selected = null;
        $sdg = $lang == 'ru' ? Sdg::pluck('name_ru', 'id')->toArray() : Sdg::pluck('name', 'id')->toArray();
        $mentors = User::role('Ментор')->get()->pluck('full_name', 'id')->toArray();
        return view('admin.teams.create', compact('title', 'row', 'regions', 'participants', 'participants_selected', 'sdg', 'mentors'));
    }

    public function store(Request $request)
    {
        $data = $request->only('name', 'description', 'region_id', 'sdg_id');
        $request->validate([
            'name'   => 'required',
            'description' => 'required',
            'region_id' => 'required',
            'sdgs' => 'required',
        ]);

        $participants_id = $request['participants'];
        $team = Team::create($data);

        if ($participants_id){
            foreach ($participants_id as $item) {
                $p = Participant::findOrFail($item);
                $p->team_id = $team->id;
                $p->save();
            }
        }

        if ($request->mentor_id){
            $mentor = User::findOrFail($request->mentor_id);
            $mentor->team_id = $team->id;
            $mentor->save();
        }

        return redirect(route('admin.teams.index'));
    }

    public function show(Team $team){
        $i = 1;
        $row = $team;
        $title = 'Команда';
        $participants = Participant::where('team_id', $team->id)->get();

        $participants_select = Participant::where('team_id', null)->orWhere('team_id', $team->id)->pluck('name', 'id')->toArray();
        $participants_selected = $row->participants ? $row->participants->pluck('id')->toArray() : [];

        return view('admin.teams.show', compact('row', 'title', 'participants', 'participants_select', 'participants_selected', 'i'));
    }

    public function edit(Team $team)
    {
        $lang = app()->getLocale();
        $title = 'Редактировать';
        $row = $team;
        $regions = Region::pluck('name_ru', 'id')->toArray();
        $participants = Participant::where('team_id', '=', null)->orWhere('team_id', $team->id)->pluck('name', 'id')->toArray();
        $participants_selected = Participant::where('team_id', $team->id)->orWhere('team_id', $team->id)->pluck('id')->toArray();
        $sdg = $lang == 'ru' ? Sdg::pluck('name_ru', 'id')->toArray() : Sdg::pluck('name', 'id')->toArray();
        $mentors = User::role('Ментор')->get()->pluck('full_name', 'id')->toArray();
        return view('admin.teams.edit', compact('title', 'row', 'regions', 'participants', 'participants_selected', 'sdg', 'mentors'));
    }

    public function update(Request $request, Team $team)
    {
        $data = $request->only('name', 'description', 'region_id', 'sdgs');
        $request->validate([
            'name'   => 'required',
            'description' => 'required',
            'region_id' => 'required',
            'sdgs' => 'required',
        ]);
        $team->update($data);

        $participants_id = $request['participants'];
        $sql = 'UPDATE participants SET team_id = null WHERE team_id = ' . strval($team->id);
        DB::statement($sql);
        if ($participants_id) {
            foreach ($participants_id as $item) {
                $p = Participant::findOrFail($item);
                $p->team_id = $team->id;
                $p->save();
            }
        }

        if ($request->mentor_id && $team->mentor_id != $request->mentor_id){
            if($team->mentor){
                $team->mentor->team_id = null;
                $team->mentor->save();
            }
            $mentor = User::findOrFail($request->mentor_id);
            $mentor->team_id = $team->id;
            $mentor->save();
        }

        return redirect(route('admin.teams.show', $team));
    }

    public function destroy(Team $team)
    {
        $team->delete();
        return redirect(route('admin.teams.index'));
    }

    public function particiantAdd(Request $request, Team $team){
        $participants_id = $request['participants'];
        $sql = 'UPDATE participants SET team_id = null WHERE team_id = ' . strval($team->id);
        DB::statement($sql);
        if ($participants_id) {
            foreach ($participants_id as $item) {
                $p = Participant::findOrFail($item);
                $p->team_id = $team->id;
                $p->save();
            }
        }
        return redirect(route('admin.teams.show', $team));
    }

    public function delete_user(Team $team, Participant $participant)
    {
        $participant->team_id = null;
        $participant->save();
        return redirect(route('admin.teams.show', $team));
    }
}
