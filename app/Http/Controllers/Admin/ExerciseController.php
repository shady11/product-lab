<?php

namespace App\Http\Controllers\Admin;

use App\Models\Exercise;
use App\Models\Participant;
use App\Models\ParticipantExercise;
use App\Models\Region;
use App\Models\Team;
use App\Models\TeamExercise;
use App\Models\Topic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Commands\UpgradeForTeams;


use DB;

class ExerciseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:exercises-index');
        $this->middleware('permission:exercises-create', ['only' => ['create','store']]);
        $this->middleware('permission:exercises-update', ['only' => ['edit','update']]);
        $this->middleware('permission:exercises-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Задания';

        if (request()->ajax()) {
            $data = Exercise::query();
            if (request()->search['value']) {
                $data = $data->search(request()->search['value']);
            }
            $data = $data->get();

            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="' . route('admin.exercises.edit', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Редактировать">
                            <i class="las la-pen fs-4 text-success"></i>
                        </a>
                        <a href="' . route('admin.exercises.delete', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="' . route('admin.exercises.show', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.exercises.index', compact('title'));
    }

    public function create()
    {
        $title = 'Форма создания задания';
        $row = new Exercise();
        $topics = Topic::pluck('name', 'id')->toArray();
        return view('admin.exercises.create', compact('title', 'row', 'topics'));
    }

    public function create_(Topic $topic)
    {
        $title = 'Форма создания задания';
        $row = new Exercise();
        $topics = Topic::pluck('name', 'id')->toArray();
        return view('admin.exercises.create', compact('title', 'row', 'topics', 'topic'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'description' => ['required'],
            'deadline' => ['required'],
            'autograde' => ['required'],
            'type' => ['required'],
            'topic_id' => ['required'],
        ], array(
            'name.required' => 'Поле является объзательным к заполнению',
            'description.required' => 'Поле является объзательным к заполнению',
            'deadline.required' => 'Поле является объзательным к заполнению',
            'type.required' => 'Поле является объзательным к заполнению',
            'topic_id.required' => 'Поле является объзательным к заполнению',
            'autograde.required' => 'Поле является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.exercises.create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->autograde == 2){
            $validator = Validator::make($request->all(), [
                'grade' => ['required'],
                'percent' => ['required']
            ], array(
                'grade.required' => 'Поле является объзательным к заполнению',
                'percent.required' => 'Поле является объзательным к заполнению',
            ));
            if ($validator->fails()) {
                return redirect()->route('admin.exercises.create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $data = $request->only('name', 'description', 'region_id', 'deadline', 'autograde', 'link', 'grade', 'percent', 'type', 'topic_id');
        $data['user_id'] = auth()->user()->id;

        Exercise::create($data);
        return redirect(route('admin.topics.index'));
    }

    public function show(Exercise $exercise)
    {
        $row = $exercise;
        $regions = Region::pluck('name_ru', 'id');
        $participants_id = ParticipantExercise::where('exercise_id', $exercise->id)->pluck('participant_id')->toArray();
        $participants = Participant::whereIn('id', $participants_id)->get();
        $teams_id = TeamExercise::where('exercise_id', $exercise->id)->pluck('team_id')->toArray();
        $teams = Team::whereIn('id', $teams_id)->get();
        $title = 'Просмотр страницы задания';
        return view('admin.exercises.estimate', compact('title', 'row', 'regions', 'participants', 'teams'));
    }

    public function edit(Exercise $exercise)
    {
        $title = 'Форма редактирования задания';
        $row = $exercise;
        $topics = Topic::pluck('name', 'id')->toArray();
        $participants = Participant::all()->pluck('full_name', 'id')->toArray();
        $teams = Team::all()->pluck('name', 'id')->toArray();

        $participants_id = ParticipantExercise::where('exercise_id', $exercise->id)->pluck('participant_id')->toArray();
        $selected_participants = Participant::whereIn('id', $participants_id)->get()->pluck('id')->toArray();
        $teams_id = TeamExercise::where('exercise_id', $exercise->id)->pluck('team_id')->toArray();
        $selected_teams = Team::whereIn('id', $teams_id)->get()->pluck('id')->toArray();
        return view('admin.exercises.edit', compact('title', 'row', 'topics', 'participants', 'selected_participants', 'teams', 'selected_teams'));
    }

    public function update(Request $request, Exercise $exercise)
    {
        $data = $request->only('name', 'description', 'deadline', 'link', 'grade', 'percent', 'autograde', 'type');
        if ($request->autograde != 2){
            $data['grade'] = null;
            $data['percent'] = null;
        }
        $exercise->update($data);
        $row = $exercise;
        return redirect(route('admin.exercises.show', $row));
    }

    public function delete(Exercise $exercise)
    {
        $exercise->delete();
        return redirect(route('admin.topics.index'));
    }

    public function participantInfo(Request $request, Exercise $exercise)
    {
        $lang = app()->getLocale();
        $participant = Participant::find($request->participant_id);

        $participantExercise = ParticipantExercise::where('exercise_id', $exercise->id)->where('participant_id', $participant->id)->first();

        $autograde = null;
        if ($exercise->autograde == 2) {
            $autograde = $exercise->grade;
        }
        $result = array(
            'id' => $participant->id,
            'full_name' => $participant->full_name,
            'grade' => $participantExercise->grade,
            'autograde' => $autograde,
            'file' => $participantExercise->file ? asset($participantExercise->file) : '#',
        );

        return $result;
    }

    public function teamInfo(Request $request, Exercise $exercise)
    {
        $lang = app()->getLocale();
        $team = Team::find($request->team_id);
        $teamExercise = TeamExercise::where('exercise_id', $exercise->id)->where('team_id', $team->id)->first();

        $autograde = null;
        if ($exercise->autograde == 2) {
            $autograde = $exercise->grade;
        }
        $result = array(
            'id' => $team->id,
            'teamName' => $team->name,
            'grade' => $teamExercise->grade,
            'autograde' => $autograde,
            'file' => $teamExercise->file ? asset($teamExercise->file) : '#',
        );

        return $result;
    }

    public function participantGrade(Request $request, Exercise $exercise)
    {
        $lang = app()->getLocale();

        $participant = Participant::findOrFail($request->participant_id);

        $participantExercise = ParticipantExercise::where('exercise_id', $exercise->id)->where('participant_id', $request->participant_id)->first();
        if ($participantExercise->file) {
            $participantExercise->grade = $request->grade;
            $participantExercise->status = 1;
            $participantExercise->save();
            return redirect()->back()->with('exercise-graded', true);
        }

        return redirect()->back()->with('exercise-not-graded', true);
    }

    public function teamGrade(Request $request, Exercise $exercise)
    {
        $lang = app()->getLocale();

        $team = Team::findOrFail($request->team_id);

        $teamExercise = TeamExercise::where('exercise_id', $exercise->id)->where('team_id', $request->team_id)->first();
        if ($teamExercise->file) {
            $teamExercise->grade = $request->grade;
            $teamExercise->status = 1;
            $teamExercise->save();
            return redirect()->back()->with('exercise-graded-team', true);
        }

        return redirect()->back()->with('exercise-not-graded-team', true);
    }

}
