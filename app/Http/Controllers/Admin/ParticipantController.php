<?php

namespace App\Http\Controllers\Admin;

use App\Mail\ParticipantFailed;
use App\Mail\ParticipantPassed;
use App\Models\Grade;
use App\Models\MailMessage;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class ParticipantController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:participants-index');
         $this->middleware('permission:participants-create', ['only' => ['create','store']]);
         $this->middleware('permission:participants-update', ['only' => ['edit','update']]);
         $this->middleware('permission:participants-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Заявки участников';
        $lang = app()->getLocale();

        if(request()->ajax()) {

            $data = Participant::where('status', false)->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.participants.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                        <a href="'.route('admin.participants.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                    ';
                })
                ->addColumn('full_name', function ($row) { return $row->full_name; })
                ->addColumn('region', function ($row) use ($lang) { return $row->region_id ? $row->getRegion->getName($lang) : '-'; })
                ->addColumn('gender', function ($row) { return $row->getGender(); })
                ->addColumn('created_at', function ($row) { return $row->getCreatedAtDateTime(); })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.participants.index', compact('title'));
    }

    public function show(Participant $participant)
    {
        return view('admin.participants.show', compact('participant'));
    }

    public function destroy(Participant $participant)
    {
        $participant->delete();
        return redirect()->route('admin.participants.index');
    }

    public function grade(Request $request, Participant $participant)
    {
        if($request->grade){
            $participant->grade = $participant->grade + $request->grade;
            $participant->save();

            $apply_grade = Grade::where('code', 'apply_grade')->first();

            if($request->grade >= $apply_grade->content) {

                $mail_message = MailMessage::where('code', 'participant_passed')->first();

                Mail::to($participant->email)->send(new ParticipantPassed($request->email, route('participant.register', $participant->token), $mail_message->header, $mail_message->content, $mail_message->link, $mail_message->footer));

                if (Mail::failures()) {
                    return redirect()->route('admin.participants.show', $participant)->with('mail', 'failed');
                } else {
                    $participant->status = true;
                    $participant->save();
                    return redirect()->route('admin.participants.index')->with('status', 'passed');
                }

            } else {

                $mail_message = MailMessage::where('code', 'participant_failed')->first();

                Mail::to($participant->email)->send(new ParticipantFailed($request->email, $mail_message->header, $mail_message->content, $mail_message->footer));

                if (Mail::failures()) {
                    return redirect()->route('admin.participants.show', $participant)->with('mail', 'failed');
                } else {
                    return redirect()->route('admin.participants.index')->with('status', 'failed');
                }
            }
        }

        return redirect()->back();
    }
}
