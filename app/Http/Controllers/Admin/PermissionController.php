<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Spatie\Permission\Models\{Role, Permission};
use Yajra\DataTables\DataTables;

class PermissionController extends Controller {

    function __construct()
    {
         $this->middleware('permission:permissions-index');
         $this->middleware('permission:permissions-create', ['only' => ['create','store']]);
         $this->middleware('permission:permissions-update', ['only' => ['edit','update']]);
         $this->middleware('permission:permissions-delete', ['only' => ['destroy']]);
    }

    public function index() {
        if(request()->ajax()){
            $data = Permission::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return ' <a href="'.route('admin.permissions.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                        <i class="las la-pen fs-3 text-success"></i>
                    </a>
                    <a href="'.route('admin.permissions.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                        <i class="las la-times fs-4 text-danger"></i>
                    </a>
                    <a href="'.route('admin.permissions.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                        <i class="las la-eye fs-4 text-warning"></i>
                    </a>';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.permissions.index');
    }

    public function create() {
        $roles = Role::get();
        return view('admin.permissions.create')->with('roles', $roles);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);

        $name = $request['name'];
        $roles = $request['roles'];
        $cruds = ['index', 'create', 'update', 'delete'];

        foreach ($cruds as $key => $crud) {
            $permission = new Permission();
            $permission->name = $name.'-'.$crud;
            $permission->guard_name = 'admin';
            $permission->save();
            if (!empty($roles)) { //If one or more role is selected
                foreach ($roles as $role) {
                    $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                    $perm = Permission::where('name', '=', $name.'-'.$crud)->first(); //Match input //permission to db record
                    $r->givePermissionTo($perm);
                }
            }
        }

        $permission->save();

        return redirect()->route('admin.permissions.index');
    }

    public function show($id) {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.show', compact('permission'));
    }

    public function edit($id) {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.edit', compact('permission'));
    }

    public function update(Request $request, $id) {

        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('admin.permissions.index')
            ->with('flash_message',
             'Permission'. $permission->name.' updated!');
    }

    public function destroy(Permission $permission){
        $permission->delete();
        return redirect(route('admin.permissions.index'));
    }
}
