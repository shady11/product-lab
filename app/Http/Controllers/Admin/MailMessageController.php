<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MailMessage;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MailMessageController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:mail-messages-index');
        $this->middleware('permission:mail-messages-create', ['only' => ['create','store']]);
        $this->middleware('permission:mail-messages-update', ['only' => ['edit','update']]);
        $this->middleware('permission:mail-messages-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Почтовые сообщения';

        if(request()->ajax()) {

            $data = MailMessage::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.mail_messages.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.mail_messages.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.mail_messages.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.mail_messages.index', compact('title'));
    }

    public function create()
    {
        $mail_message = new MailMessage();
        $title = 'Добавить';

        return view('admin.mail_messages.create', compact('mail_message', 'title'));
    }

    public function store(Request $request)
    {
        MailMessage::create($request->all());
        return redirect()->route('admin.mail_messages.index');
    }

    public function show(MailMessage $mail_message)
    {
        $title = 'Просмотр';
        return view('admin.mail_messages.show', compact('mail_message', 'title'));
    }

    public function edit(MailMessage $mail_message)
    {
        $title = 'Редактировать';
        return view('admin.mail_messages.edit', compact('mail_message', 'title'));
    }

    public function update(Request $request, MailMessage $mail_message)
    {
        $mail_message->update($request->all());
        return redirect()->route('admin.mail_messages.index');
    }

    public function destroy(MailMessage $mail_message)
    {
        $mail_message->delete();
        return redirect()->route('admin.mail_messages.index');
    }
}
