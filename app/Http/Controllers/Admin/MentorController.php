<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\DataTables;

class MentorController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:mentors-index');
         $this->middleware('permission:mentors-create', ['only' => ['create','store']]);
         $this->middleware('permission:mentors-update', ['only' => ['edit','update']]);
         $this->middleware('permission:mentors-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $mentor = new Mentor();
        $title = 'Тренеры/Менторы';

        if(request()->ajax()) {

            $data = Mentor::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.landing.mentors.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.landing.mentors.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.landing.mentors.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('name', function ($row) { return $row->name; })
                ->addColumn('lastname', function ($row) { return $row->lastname; })
                ->addColumn('patronymic', function ($row) { return $row->patronymic; })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.mentors.index', compact('mentor', 'title'));
    }


    public function create()
    {
        $mentor = new Mentor();
        $title = 'Добавление нового тренера/ментора';

        return view('admin.mentors.create', compact('mentor', 'title'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lastname' => ['required'],
            'name' => ['required'],
        ], array(
            'lastname.required' => 'Поля является объзательным к заполнению',
            'name.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.landing.mentors.create')
                ->withErrors($validator)
                ->withInput();
        }

        $mentor = Mentor::create($request->all());

        if($request->file('img')) {

            $file = $request->file('img');

            $path = $file->store('uploads/mentors', 'public');
            $mentor->img = $path;

            $dir  = 'storage/uploads/mentors';
            $dirMin  = 'storage/uploads/mentors/min';

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $filename = Str::slug($mentor->getFullName(), '-').'-'.$mentor->id.'.'.$file->getClientOriginalExtension();
            $filenameMin = Str::slug($mentor->getFullName(), '-').'-'.$mentor->id.'_min.'.$file->getClientOriginalExtension();


            Image::make($file)->save($dir.$filename);
            Image::make($file)->fit(400, 300)->save($dirMin.$filenameMin);

            $mentor->img = $dir.$filename;
            $mentor->img_thumbnail = $dirMin.$filenameMin;
        }
        $mentor->save();

        return redirect()->route('admin.landing.mentors.show', $mentor);
    }

    public function show(Mentor $mentor)
    {
        $title = 'Просмотр';
        return view('admin.mentors.show', compact('mentor', 'title'));
    }



    public function edit(Mentor $mentor)
    {
        $title = 'Редактировать';
        return view('admin.mentors.edit', compact('mentor', 'title'));
    }


    public function update(Request $request, Mentor $mentor)
    {
        $validator = Validator::make($request->all(), [
            'lastname' => ['required'],
            'name' => ['required'],
        ], array(
            'lastname.required' => 'Поля является объзательным к заполнению',
            'name.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.landing.mentors.create')
                ->withErrors($validator)
                ->withInput();
        }

        $mentor->update($request->except('img'));

        if($request->file('img')) {

            if ($mentor->img) {
                @unlink($mentor->img);
            }

            $file = $request->file('img');

            $path = $file->store('uploads/mentors', 'public');
            $mentor->img = $path;

            $dir  = 'storage/uploads/mentors';
            $dirMin  = 'storage/uploads/mentors/min';

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $filename = Str::slug($mentor->getFullName(), '-').'-'.$mentor->id.'.'.$file->getClientOriginalExtension();
            $filenameMin = Str::slug($mentor->getFullName(), '-').'-'.$mentor->id.'_min.'.$file->getClientOriginalExtension();

            Image::make($file)->save($dir.$filename);
            Image::make($file)->fit(400, 300)->save($dirMin.$filenameMin);

            $mentor->img = $dir.$filename;
            $mentor->img_thumbnail = $dirMin.$filenameMin;
        }
        $mentor->save();

        return redirect()->route('admin.landing.mentors.show', $mentor);
    }

    public function destroy(Mentor $mentor)
    {
        if ($mentor->img) {
            @unlink($mentor->img);
        }
        $mentor->delete();

        return redirect()->route('admin.landing.mentors.index');
    }
}
