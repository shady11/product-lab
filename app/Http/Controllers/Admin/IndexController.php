<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use App\Models\Exercise;
use App\Models\Participant;
use App\Models\ParticipantProduct;
use App\Models\Region;
use App\Models\Team;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\DataTables;

class IndexController extends Controller
{
    public function index()
    {
        $title = 'Главная';

        $regions = Region::all();

        $participants = Participant::orderBy('grade', 'desc')->get();
        $participants_exercises = Exercise::where('type', 1)->get();
        $teams = Team::all();
        $teams_exercises = Exercise::where('type', 2)->get();

        return view('admin.index', compact('title', 'regions', 'participants', 'participants_exercises', 'teams', 'teams_exercises'));
    }

    public function profileUpdate(Request $request)
    {
        $participant = auth()->user();

        $participant->login = $request->login;
        $participant->email = $request->login;

        if($request->password){
            $participant->password = bcrypt($request->password);
        }

        $participant->save();

        return redirect()->back();
    }

    public function applications()
    {
        $title = 'Заявки участников';
        $lang = app()->getLocale();

        if(request()->ajax()) {

            $data = Participant::where('status', false)->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.applications.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                        <a href="'.route('admin.applications.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                    ';
                })
                ->addColumn('full_name', function ($row) { return $row->full_name; })
                ->addColumn('region', function ($row) use ($lang) { return $row->region_id ? $row->getRegion->getName($lang) : '-'; })
                ->addColumn('gender', function ($row) { return $row->getGender(); })
                ->addColumn('created_at', function ($row) { return $row->getCreatedAtDateTime(); })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.applications', compact('title'));
    }

    public function applicationsShow(Participant $participant)
    {

    }
}
