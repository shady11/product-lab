<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:users-index');
         $this->middleware('permission:users-create', ['only' => ['create','store']]);
         $this->middleware('permission:users-update', ['only' => ['edit','update']]);
         $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Пользователи';

        if(request()->ajax()) {

            $data = User::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return ' <a href="'.route('admin.users.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                        <i class="las la-pen fs-3 text-success"></i>
                    </a>
                    <a href="'.route('admin.users.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                        <i class="las la-times fs-4 text-danger"></i>
                    </a>
                    <a href="'.route('admin.users.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                        <i class="las la-eye fs-4 text-warning"></i>
                    </a>';
                })
                ->addColumn('role', function ($row) {
                    if(!empty($row->getRoleNames())){
                        return $row->getRoleNames()->first();
                    }else{
                        return '-';
                    }
                })
                ->addColumn('name', function ($row) { return $row->name; })
                ->addColumn('lastname', function ($row) { return $row->lastname; })
                ->addColumn('patronymic', function ($row) { return $row->patronymic; })
                ->addColumn('login', function ($row) { return $row->login; })
                ->addColumn('email', function ($row) { return $row->email; })
                ->rawColumns(['actions', 'role'])
                ->make(true);
        }

        return view('admin.users.index', compact('title'));
    }

    public function create()
    {
        $user = new User();
        $title = 'Добавить';
        $mode = 'create';
        $roles = Role::get()->sortBy('id')->pluck('name','id')->toArray();

        return view('admin.users.create', compact('user', 'title', 'mode', 'roles'));
    }

    public function store(Request $request)
    {
        $userLogins = DB::table('users')->pluck('login');
        $inputLogin = $request->input('login');
        $inputPassword = $request->input('password');


        $validator = Validator::make($request->all(), [
            'login' => ['bail', 'required', function($attribute, $inputLogin, $fail) use ($userLogins) {
                if ($userLogins->contains($inputLogin))   {
                    return  $fail('Такой Логин уже существует');
                }
            }],
            'password' => ['required']
        ], array(
            'login.required' => 'Поля "Логин" является объзательным к заполнению',
            'password.required' => 'Поля "Пароль" является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.users.create')
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::create($request->except('password', 'status-checkbox'));
        $user->password = bcrypt($request->password);
        $user->status = isset($request['status-checkbox']) ? 1 : 0;
        $user->assignRole($request->roles);
        $user->save();

        return redirect()->route('admin.users.show', $user);
    }

    public function show(User $user)
    {
        $title = "Посмотреть";
        return view('admin.users.show', compact( 'user', 'title'));
    }

    public function edit(User $user)
    {
        $title = 'Редактировать';
        $mode = 'edit';
        $roles = Role::get()->sortBy('id')->pluck('name','id')->toArray();
        return view('admin.users.edit', compact('user', 'title', 'mode', 'roles'));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->except('status'));

        DB::table('model_has_roles')->where('model_id', $user->id)->delete();
        $user->assignRole($request->roles);

        $user->status = $request->input('status-checkbox') ? 1 : 0;

        $user->save();

        return redirect()->route('admin.users.show', $user);
    }


    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('admin.users.index');
    }

    public function changeLogin(Request $request, User $user)
    {
        $userLogins = DB::table('users')->pluck('login');

        $inputLogin = $request->input('login');
        $inputPassword = $request->input('confirmloginpassword');


        $validator = $request->validate([
            'login' => ['bail', 'required', function($attribute, $inputLogin, $fail) use ($userLogins) {
                if ($userLogins->contains($inputLogin))   {
                    return  $fail('Такой Логин уже существует');
                }
            }],
            'confirmloginpassword' => ['bail', 'required', function($attribute, $inputPassword, $fail) use ($user) {
                if (!Hash::check($inputPassword, $user->password)) {
                    return  $fail('Пароль некорректен');
                }
            }],

        ], array(
            'login.required' => 'Поле "Новый Логин" является объзательным к заполнению',
            'confirmloginpassword.required' => 'Поле "Пароль" является объзательным к заполнению',
        ));

        $user->login = $inputLogin;
        $user->save();

        $statusType = 'success';
        $statusMsg = 'Логин успешно обновлен';

        return view('admin.users.show', compact('user', 'statusType', 'statusMsg'));
    }


    public function changePassword(Request $request, User $user)
    {
        $inputCurrentPassword = $request->input('currentpassword');
        $inputNewPassword = $request->input('newpassword');
        $inputConfirmPassword = $request->input('confirmpassword');


        $validator = $request->validate([
            'currentpassword' => ['bail', 'required', function($attribute, $inputCurrentPassword, $fail) use ($user) {
                if (!Hash::check($inputCurrentPassword, $user->password)) {
                    return  $fail('Текущий пароль некорректен');
                }
            }],

            'newpassword' =>  ['bail', 'required', function($attribute, $inputNewPassword, $fail) use ($inputConfirmPassword) {
                if ($inputNewPassword !== $inputConfirmPassword) {
                    return  $fail('Новые пароли не совпадает с друг другом');
                }
             }],

            'confirmpassword' =>  ['bail', 'required', function($attribute, $inputNewPassword, $fail) use ($user) {
                if (Hash::check($inputNewPassword, $user->password)) {
                    return  $fail('Новый пароль совпадает со старым паролем');
                }
            }],

        ], array(
            'currentpassword.required' => 'Поля является объзательным к заполнению',
            'newpassword.required' => 'Поля является объзательным к заполнению',
            'confirmpassword.required' => 'Поля является объзательным к заполнению',
        ));

        $user->password = bcrypt($inputNewPassword);
        $user->save();

        $statusType = 'success';
        $statusMsg = 'Пароль успешно обновлен';

        return view('admin.users.show', compact('user','statusType', 'statusMsg'));

    }

}
