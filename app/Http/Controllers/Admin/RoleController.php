<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\{Role, Permission};
use DB;

class RoleController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:roles-index');
         $this->middleware('permission:roles-create', ['only' => ['create','store']]);
         $this->middleware('permission:roles-update', ['only' => ['edit','update']]);
         $this->middleware('permission:roles-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $data = Role::get();
        if(request()->ajax()){
            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return ' <a href="'.route('admin.roles.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                        <i class="las la-pen fs-3 text-success"></i>
                    </a>
                    <a href="'.route('admin.roles.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                        <i class="las la-times fs-4 text-danger"></i>
                    </a>
                    <a href="'.route('admin.roles.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                        <i class="las la-eye fs-4 text-warning"></i>
                    </a>';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.roles.index');
    }

    public function create()
    {
        $permission = Permission::get();
        return view('admin.roles.create',compact('permission'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create([
            'guard_name' => 'admin',
            'name' => $request->name,
        ]);
        $role->syncPermissions($request->input('permission'));

        return redirect()->route('admin.roles.index')
                        ->with('success','Role created successfully');
    }

    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();


        return view('admin.roles.show',compact('role','rolePermissions'));
    }

    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        return view('admin.roles.edit',compact('role','permission','rolePermissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return redirect()->route('admin.roles.index')->with('success','Role updated successfully');
    }

    public function destroy(Role $role){
        $role->delete();
        return redirect(route('admin.roles.index'));
    }
}
