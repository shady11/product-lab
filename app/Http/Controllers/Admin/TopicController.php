<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exercise;
use App\Models\Material;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:topics-index');
        $this->middleware('permission:topics-create', ['only' => ['create','store']]);
        $this->middleware('permission:topics-update', ['only' => ['edit','update']]);
        $this->middleware('permission:topics-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Все темы';

//        if(request()->ajax()){
//            $data = Topic::query();
//            if (request()->search['value']) {
//                $data = $data->search(request()->search['value']);
//            }
//            $data = $data->get();
//            return datatables()->of($data)
//                ->addIndexColumn()
//                ->addColumn('actions', function ($row) {
//                    return '
//                        <a href="'.route('admin.topics.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
//                            <i class="las la-pen fs-3 text-success"></i>
//                        </a>
//                        <a href="'.route('admin.topics.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
//                            <i class="las la-times fs-4 text-danger"></i>
//                        </a>
//                        <a href="'.route('admin.topics.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
//                            <i class="las la-eye fs-4 text-warning"></i>
//                        </a>
//                    ';
//                })
//                ->rawColumns(['actions'])
//                ->make(true);
//        }
        $topics = Topic::all();
        return view('admin.topics.index', compact('title', 'topics'));
    }

    public function create(){
        $lang = app()->getLocale();
        $title = 'Форма создания темы';
        $row = new Topic();
        return view('admin.topics.create', compact('lang', 'title', 'row'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'dateactive' => ['required'],
        ], array(
            'name.required' => 'Поля является объзательным к заполнению',
            'dateactive.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.topics.create')
                ->withErrors($validator)
                ->withInput();
        }
        $topic = Topic::create($request->all());
        $topic->user_id = auth()->user()->id;
        $topic->save();
        return redirect()->route('admin.topics.index');
    }

    public function show(Topic $topic){
        $row = $topic;
        $title = 'Show';
        $materials = Material::all();
        $exercises = Exercise::all();
        return view('admin.topics.show', compact('row', 'title', 'materials', 'exercises'));
    }

    public function edit(Topic $topic){
        $row = $topic;
        $title = 'Форма создания темы';
        return view('admin.topics.edit', compact('row', 'title'));
    }

    public function update(Request $request, Topic $topic){
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'description' => ['required'],
            'dateactive' => ['required'],
        ], array(
            'name.required' => 'Поля является объзательным к заполнению',
            'description.required' => 'Поля является объзательным к заполнению',
            'dateactive.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.topics.create')
                ->withErrors($validator)
                ->withInput();
        }
        $topic->update($request->all());
        return redirect()->route('admin.topics.index');
    }

    public function delete(Topic $topic){
        $topic->delete();
        return redirect(route('admin.topics.index'));
    }

}
