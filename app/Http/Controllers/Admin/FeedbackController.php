<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\DataTables;

class FeedbackController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:feedbacks-index');
         $this->middleware('permission:feedbacks-create', ['only' => ['create','store']]);
         $this->middleware('permission:feedbacks-update', ['only' => ['edit','update']]);
         $this->middleware('permission:feedbacks-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $feedback = new Feedback();
        $title = 'Список отзывов';

        if(request()->ajax()) {

            $data = Feedback::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.landing.feedbacks.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.landing.feedbacks.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.landing.feedbacks.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('author', function ($row) { return $row->author; })
                ->addColumn('activity', function ($row) { return $row->activity; })
                ->addColumn('created_at', function ($row) { return $row->created_at; })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.feedbacks.index', compact('feedback', 'title'));
    }

    public function create()
    {
        $feedback = new Feedback();
        $title = 'Создание нового отзыва';

        return view('admin.feedbacks.create', compact('feedback', 'title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'author' => ['required'],
            'content' => ['required'],
        ], array(
            'author.required' => 'Поля является объзательным к заполнению',
            'content.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.landing.feedbacks.create')
                ->withErrors($validator)
                ->withInput();
        }

        $feedback = Feedback::create($request->all());

        if($request->file('avatar')) {

            $file = $request->file('avatar');

            $path = $file->store('uploads/feedbacks/', 'public');
            $feedback->avatar = $path;

            $dir  = 'storage/uploads/feedbacks/';

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $filename = Str::slug($feedback->author, '-').'-'.$feedback->id.'.'.$file->getClientOriginalExtension();

            Image::make($file)->fit(400, 300)->save($dir.$filename);
            $feedback->avatar = $dir.$filename;
        }
        $feedback->save();

        return redirect()->route('admin.landing.feedbacks.show', $feedback);
    }

    public function show(Feedback $feedback)
    {
        $title = 'Просмотр';
        return view('admin.feedbacks.show', compact('feedback', 'title'));
    }

    public function edit(Feedback $feedback)
    {
        $title = 'Редактировование новости';
        return view('admin.feedbacks.edit', compact('feedback', 'title'));
    }

    public function update(Request $request, Feedback $feedback)
    {
        $validator = Validator::make($request->all(), [
            'author' => ['required'],
            'content' => ['required'],
        ], array(
            'author.required' => 'Поля является объзательным к заполнению',
            'content.required' => 'Поля является объзательным к заполнению',
        ));

        if ($validator->fails()) {
            return redirect()->route('admin.landing.feedbacks.create')
                ->withErrors($validator)
                ->withInput();
        }

        $feedback->update($request->except('avatar'));

        if($request->file('avatar')) {

            $file = $request->file('avatar');

            $path = $file->store('uploads/feedbacks/', 'public');
            $feedback->avatar = $path;

            $dir = 'storage/uploads/feedbacks/';

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $filename = Str::slug($feedback->author, '-').'-'.$feedback->id.'.'.$file->getClientOriginalExtension();

            Image::make($file)->save($dir.$filename);
            $feedback->avatar = $dir.$filename;
        }
        $feedback->save();

        return redirect()->route('admin.landing.feedbacks.show', $feedback);
    }

    public function destroy(Feedback $feedback)
    {
        if ($feedback->avatar) {
            @unlink($feedback->avatar);
        }
        $feedback->delete();

        return redirect()->route('admin.landing.feedbacks.index');
    }
}
