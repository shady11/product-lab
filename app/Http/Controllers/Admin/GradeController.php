<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class GradeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:grades-index');
        $this->middleware('permission:grades-create', ['only' => ['create','store']]);
        $this->middleware('permission:grades-update', ['only' => ['edit','update']]);
        $this->middleware('permission:grades-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $title = 'Баллы';

        if(request()->ajax()) {

            $data = Grade::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('admin.grades.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.grades.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                    ';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.grades.index', compact('title'));
    }

    public function create()
    {
        $grade = new Grade();
        $title = 'Добавить';

        return view('admin.grades.create', compact('grade', 'title'));
    }

    public function store(Request $request)
    {
        Grade::create($request->all());
        return redirect()->route('admin.grades.index');
    }

    public function show(Grade $grade)
    {
        $title = 'Просмотр';
        return view('admin.grades.show', compact('grade', 'title'));
    }

    public function edit(Grade $grade)
    {
        $title = 'Редактировать';
        return view('admin.grades.edit', compact('grade', 'title'));
    }

    public function update(Request $request, Grade $grade)
    {
        $grade->update($request->all());
        return redirect()->route('admin.grades.index');
    }

    public function destroy(Grade $grade)
    {
        $grade->delete();
        return redirect()->route('admin.grades.index');
    }
}
