<?php
namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\Region;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Str;

class EventController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:events-index');
    }
    public function index()
    {
        $regions = Region::pluck('name_ru', 'id')->toArray();
        return view('admin.events.index', compact('regions'));
    }

    public function data(Request $request)
    {
        $dateStart = Carbon::parse($request->start)->format('Y-m-d');
        $dateEnd = Carbon::parse($request->end)->format('Y-m-d');

        $start = (!empty($request->start)) ? $dateStart : Carbon::now()->startOfMonth();
        $end = (!empty($request->end)) ? $dateEnd : Carbon::now()->endOfMonth();

        $data = Event::query();
        $data = $data->whereBetween('start_date', [$start, $end]);
        $rows = $data->get();

        $array = [];
        foreach ($rows as $row) {
            $start_date_time = new DateTime($row->start_date." ".$row->start_time);
            $end_date_time = new DateTime($row->start_date." ".$row->end_time);
            $array[] = [
                'id' => $row->id,
                'event_id' => $row->id,
                'title' => $row->title,
                'description' => _(Str::of($row->description)->limit(60, ' ...')),

                "start" => $start_date_time->format('Y-m-d H:i:s'),
                "end" => $end_date_time->format('Y-m-d H:i:s'),

                "start_time" => $row->start_time,
                "end_time" => $row->end_time,
            ];
        }
        return response()->json($array, 200);
    }

    public function add(Request $request)
    {
        $divide = explode(" — ", $request->date);
        $start_date = $end_date = $divide[0];
        if(count($divide) > 1) $end_date = $divide[1];

        Event::create([
            "title" => $request->title,
            "description" => $request->description,
            "location" => $request->location,
            "type" => $request->type,
        "autograde"=> $request->autograde,
            "start_date" => $start_date,
            "end_date" => $end_date,
            "start_time" => $request->start_time,
            "end_time" => $request->end_time,
            "user_id" => auth()->user()->id,
            "region_id" => $request->region_id,
            "created_at" => Carbon::now('Asia/Bishkek'),
            "updated_at" => Carbon::now('Asia/Bishkek'),
        ]);

        return redirect()->back();
    }

    public function view($id)
    {
        $row = Event::findOrFail($id);
        return view('admin.events.view', compact('row'));
    }

    public function delete(Event $row)
    {
        $row->delete();
        return back();
    }
}
