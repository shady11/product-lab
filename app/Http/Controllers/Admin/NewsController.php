<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

class NewsController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:news-index');
         $this->middleware('permission:news-create', ['only' => ['create','store']]);
         $this->middleware('permission:news-update', ['only' => ['edit','update']]);
         $this->middleware('permission:news-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $title = 'Список новостей';

        if(request()->ajax()) {

            $data = News::all();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                     return '
                        <a href="'.route('admin.landing.news.edit', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="'.route('admin.landing.news.delete', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="'.route('admin.landing.news.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                  })
                ->addColumn('name', function ($row) { return $row->name; })
                ->addColumn('name_ru', function ($row) { return $row->name_ru; })
                ->addColumn('created_at', function ($row) { return $row->created_at; })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.news.index', compact('title'));
    }


    public function create()
    {
        $news = new News();
        $title = 'Создание новости';

        return view('admin.news.create', compact('news', 'title'));
    }


    public function store(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'name_ru' => ['required'],
//            'name' => ['required'],
//            'published_at' => ['required']
//        ], array(
//            'name_ru.required' => 'Поля является объзательным к заполнению',
//            'name.required' => 'Поля является объзательным к заполнению',
//            'published_at.required' => 'Поля является объзательным к заполнению',
//        ));
//
//        if ($validator->fails()) {
//            return redirect()->route('admin.landing.news.create')
//                ->withErrors($validator)
//                ->withInput();
//        }

        $news = News::create($request->except('img', 'img_remove'));

        if($request->file('img')) {

            $file = $request->file('img');

            $path = $file->store('uploads/news/', 'public');
            $news->img = $path;

            $dir  = 'storage/uploads/news/';

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $filename = Str::slug($news->name_ru, '-').'-'.$news->id.'.'.$file->getClientOriginalExtension();
            $filenameMin = Str::slug($news->name_ru, '-').'-'.$news->id.'_min.'.$file->getClientOriginalExtension();

            Image::make($file)->fit(800, 600)->save($dir.$filename);
            Image::make($file)->fit(400, 300)->save($dir.$filenameMin);

            $news->img = $dir.$filename;
            $news->img_thumbnail = $dir.$filenameMin;
        }
        $news->save();

        return redirect()->route('admin.landing.news.show', $news);
    }

    public function show(News $news)
    {
        $title = 'Просмотр';
        return view('admin.news.show', compact('news', 'title'));
    }



    public function edit(News $news)
    {
        $title = 'Редактировование новости';
        return view('admin.news.edit', compact('news', 'title'));
    }


    public function update(Request $request, News $news)
    {
//        $validator = Validator::make($request->all(), [
//            'name_ru' => ['required'],
//            'name' => ['required'],
//            'published_at' => ['required']
//        ], array(
//            'name_ru.required' => 'Поля является объзательным к заполнению',
//            'name.required' => 'Поля является объзательным к заполнению',
//            'published_at.required' => 'Поля является объзательным к заполнению',
//        ));
//
//        if ($validator->fails()) {
//            return redirect()->route('admin.news.create')
//                ->withErrors($validator)
//                ->withInput();
//        }

        $news->update($request->except('img', 'img_remove'));

        if($request->file('img')) {

            if ($news->img) {
                @unlink($news->img);
            }

            $file = $request->file('img');

            $path = $file->store('uploads/news/', 'public');
            $news->img = $path;

            $dir  = 'storage/uploads/news/';

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $filename = Str::slug($news->name_ru, '-').'-'.$news->id.'.'.$file->getClientOriginalExtension();
            $filenameMin = Str::slug($news->name_ru, '-').'-'.$news->id.'_min.'.$file->getClientOriginalExtension();

            Image::make($file)->fit(800, 600)->save($dir.$filename);
            Image::make($file)->fit(400, 300)->save($dir.$filenameMin);

            $news->img = $dir.$filename;
            $news->img_thumbnail = $dir.$filenameMin;
        }
        $news->save();

        return redirect()->route('admin.landing.news.show', $news);
    }

    public function destroy(News $news)
    {
        if ($news->img) {
            @unlink($news->img);
        }

        $news->delete();

        return redirect()->route('admin.landing.news.index');
    }
}
