<?php
namespace App\Http\Controllers\Admin;

use App\Models\Exercise;
use App\Models\IshtappUser;
use App\Models\IshtappVacancy;
use App\Models\Participant;
use App\Models\ParticipantExercise;
use App\Models\Region;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:report-index');
    }

    public function index()
    {
        $regions = Region::get()->sortBy('name_ru')->pluck('name_ru', 'id')->toArray();
        $participants = Participant::where('status', true)->get()->sortBy('full_name')->pluck('full_name', 'id')->toArray();
        $teams = Team::get()->sortBy('name')->pluck('name', 'id')->toArray();

        $exercises = Exercise::query();
        if(request()->period){
            $divide = explode(" - ",request()->period);
            $start = $divide[0];
            $end = $divide[1];
            $date_start = Carbon::create($start)->format('d-m-Y');
            $date_end = Carbon::create($end)->format('d-m-Y');
            $exercises = $exercises->whereBetween('deadline', [$date_start, $date_end]);
        }
        $exercises = $exercises->get();

        $data = Participant::query();
        $data = $data->where('status', true);
        if(request()->participant){
            $data = $data->where('id', request()->participant);
        }
        if(request()->region){
            $data = $data->where('region_id', request()->region);
        }
        if(request()->team){
            $data = $data->where('team_id', request()->team);
        }
        $data = $data->get();

        return view('admin.report.index', compact('regions', 'participants', 'teams', 'exercises', 'data'));
    }

    public function ishtapp()
    {
        $ishtapp_users = IshtappUser::where('type', 'USER')->where('is_product_lab_user', true)->orderBy('created_at', 'desc')->get();
        $ishtapp_companies = IshtappUser::where('type', 'COMPANY')->join('vacancies', 'users.id', '=', 'vacancies.company_id')->where('vacancies.is_product_lab_vacancy', true) ->select('users.*')->groupBy('users.id')->orderBy('created_at', 'desc')->get();
        $ishtapp_vacancies = IshtappVacancy::where('is_product_lab_vacancy', true)->orderBy('created_at', 'desc')->get();
        return view('admin.report.ishtapp', compact('ishtapp_companies', 'ishtapp_users', 'ishtapp_vacancies'));
    }

    public function ishtappUsers()
    {
        $title = 'ishtapp - Пользователи';

        if(request()->ajax()) {

            $data = IshtappUser::where('type', 'USER')->where('is_product_lab_user', true)->orderBy('created_at', 'desc')->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('birth_date', function ($row) { return date('d-m-Y', strtotime($row->birth_date)); })
                ->addColumn('region', function ($row) { return $row->getRegion ? $row->getRegion->nameRu : '-'; })
                ->make(true);
        }

        return view('admin.report.ishtapp-users', compact('title'));
    }

    public function ishtappCompanies()
    {
        $title = 'ishtapp - Компании';

        if(request()->ajax()) {

//            $data = IshtappUser::where('type', 'COMPANY')
//                ->leftJoin('vacancies', 'users.id', '=', 'vacancies.company_id')
//                ->where('vacancies.is_product_lab_vacancy', true)
//                ->select('users.*')
//                ->orderBy('created_at', 'desc')
//                ->get();

            $data = IshtappUser::where('type', 'COMPANY')
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('vacancies')
                        ->whereColumn('vacancies.company_id', 'users.id')
                        ->where('vacancies.is_product_lab_vacancy', true);
                })
                ->orderBy('created_at', 'desc')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('region', function ($row) { return $row->getRegion ? $row->getRegion->nameRu : '-'; })
                ->addColumn('opportunities', function ($row) { return $row->getOpportunities->count(); })
                ->make(true);
        }

        return view('admin.report.ishtapp-companies', compact('title'));
    }

    public function ishtappOpportunities()
    {
        $title = 'ishtapp - Возможности';

        if(request()->ajax()) {

            $data = IshtappVacancy::where('is_product_lab_vacancy', true)->orderBy('created_at', 'desc')->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('company', function ($row) { return $row->getCompany ? $row->getCompany->name : '-'; })
                ->addColumn('opportunity', function ($row) { return $row->getOpportunity ? $row->getOpportunity->name_ru : '-'; })
                ->addColumn('region', function ($row) { return $row->getRegion ? $row->getRegion->nameRu : '-'; })
                ->make(true);
        }

        return view('admin.report.ishtapp-opportunities', compact('title'));
    }
}
