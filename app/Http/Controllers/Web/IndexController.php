<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Participant;
use App\Models\Region;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class IndexController extends Controller
{
    public function index()
    {
        $lang = App::getLocale();

        if ($lang == 'ru') {
            $news = News::whereNotNull('name_ru')
                ->orWhereNotNull('content_ru')
                ->orderBy('id', 'desc')
                ->take(3)
                ->get();
        } else if ($lang == 'ky') {
            $news = News::whereNotNull('name')
                    ->orWhereNotNull('content')
                    ->orderBy('id', 'desc')
                    ->take(3)
                    ->get();
        }

        return view('web.index', compact('news', 'lang'));
    }
    public function news()
    {
        $lang = App::getLocale();

        if ($lang == 'ru') {
            $news = News::whereNotNull('name_ru')
                ->orWhereNotNull('content_ru')
                ->orderBy('id', 'desc')
                ->get();
        } else if ($lang == 'ky') {
            $news = News::whereNotNull('name')
                ->orWhereNotNull('content')
                ->orderBy('id', 'desc')
                ->get();
        }

        return view('web.news', compact('news', 'lang'));
    }
    public function newsSingle($id)
    {
        $lang = App::getLocale();
        $news = News::find($id);

        return view('web.news-single', compact('news', 'lang'));
    }

    public function showApplyForm()
    {
        $lang = app()->getLocale();

        $regions = Region::all();

        return view('participant.auth.apply', compact('lang', 'regions'));
    }

    public function apply(Request $request)
    {
        $request->validate([
            'lastname' => 'required',
            'name' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:participants',
            'activity' => 'required',
            'region_id' => 'required',
            'socio_economic_status' => 'required',
            'type' => 'required',
            'participated' => 'required',
            'motivation' => 'required',
        ]);

        $token = Str::random(64);

        $participant = Participant::create($request->except('socio_economic_status_other_checked', 'socio_economic_status_other', 'type_other_checked', 'type_other', 'goal_other_checked', 'goal_other') + [
                'token' => $token
            ]);

        if($request->socio_economic_status_other) {
            $participant->socio_economic_status = array_merge($request->socio_economic_status, [$request->socio_economic_status_other]);
        }

        if($request->type_other) {
            $participant->type = array_merge($request->type, [$request->type_other]);
        }

        if($request->goal_other) {
            $participant->goal = array_merge($request->goal, [$request->goal_other]);
        }

        return redirect()->back()->with('applied', true)->withInput(['token' => $token]);
    }

    public function login()
    {
        if(auth()->guard('participant')->check()) {
            return redirect()->route('participant.index');
        }
        if(auth()->guard('admin')->check()) {
            return redirect()->route('index');
        }
        return view('web.login');
    }

    public function loginSubmit(Request $request)
    {
        $this->validate($request, [
            'login'   => 'required',
            'password' => 'required'
        ]);

        $participant = Participant::where('login', $request->login)->first();
        if($participant){
            if($participant->status) {
                if (Auth::guard('participant')->attempt(['login' => $request->login, 'password' => $request->password])) {
                    return redirect()->route('participant.index')->with('logged', true);
                }
            }
            return redirect()->back()->with('msg', 'Логин или пароль указаны неверно');
        } else {
            $admin = User::where('email', $request->login)->first();
            if($admin){
                if (Auth::guard('admin')->attempt(['email' => $request->login, 'password' => $request->password])) {
                    return redirect()->route('admin.index');
                }
            }
            return redirect()->back()->with('msg', 'Логин или пароль указаны неверно');
        }
        return redirect()->back()->with('msg', 'Такого пользователя нет в системе!');
    }
}
