<?php

namespace App\Http\Controllers\Participant;

use App\Models\Branch;
use App\Models\Event;
use App\Models\Exercise;
use App\Models\Participant;
use App\Models\ParticipantBalance;
use App\Models\ParticipantProduct;
use App\Models\Product;
use App\Models\Region;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class IndexController extends Controller
{
    public function index()
    {
        $title = 'Главная';

        $events = Event::orderBy('start_date', 'asc')->where('region_id', auth()->user()->region_id)->take(5)->get();

        $last_exercises = Exercise::orderBy('deadline', 'asc')->take(3)->get();
        $exercises = Exercise::orderBy('deadline', 'asc')->get();

        return view('participant.index', compact('title', 'events', 'exercises', 'last_exercises'));
    }
    public function events()
    {
        $title = 'Мероприятия';
        return view('participant.events.index', compact('title'));
    }
    public function eventsData(Request $request)
    {
        $dateStart = Carbon::parse($request->start)->format('Y-m-d');
        $dateEnd = Carbon::parse($request->end)->format('Y-m-d');

        $start = (!empty($request->start)) ? $dateStart : Carbon::now()->startOfMonth();
        $end = (!empty($request->end)) ? $dateEnd : Carbon::now()->endOfMonth();

        $data = Event::query();
        $data = $data->whereBetween('start_date', [$start, $end])->where('region_id', auth()->user()->region_id);
        $rows = $data->get();

        $array = [];
        foreach ($rows as $row) {
            $start_date_time = new DateTime($row->start_date." ".$row->start_time);
            $end_date_time = new DateTime($row->start_date." ".$row->end_time);
            $array[] = [
                'id' => $row->id,
                'event_id' => $row->id,
                'title' => $row->title,
                'description' => _(Str::of($row->description)->limit(60, ' ...')),

                "start" => $start_date_time->format('Y-m-d H:i:s'),
                "end" => $end_date_time->format('Y-m-d H:i:s'),

                "start_time" => $row->start_time,
                "end_time" => $row->end_time,
            ];
        }
        return response()->json($array, 200);
    }
    public function viewEvent($id)
    {
        $row = Event::findOrFail($id);
        return view('participant.events.view', compact('row'));
    }
    public function profile()
    {
        $title = 'Профиль';

        $regions = Region::pluck('name_ru', 'id');

        $events = Event::orderBy('start_date', 'asc')->get();
        $exercises = Exercise::orderBy('deadline', 'asc')->get();

        return view('participant.profile', compact('title', 'regions', 'events', 'exercises'));
    }

    public function profileUpdate(Request $request)
    {

        $participant = auth()->user();

        $participant->login = $request->login;
        $participant->email = $request->email;
        $participant->name = $request->name;
        $participant->lastname = $request->lastname;

        if($request->password){
            $participant->password = bcrypt($request->password);
        }

        if($request->avatar_remove){
            if($participant->image && file_exists(asset($participant->image))) @unlink($participant->image);

            $participant->image = null;
        }

        if($request->file('avatar')){

            if($participant->image && file_exists(asset($participant->image))) @unlink($participant->image);

            $file = $request->file('avatar');

            $dir  = 'assets/participants/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $name = Str::slug($participant->full_name, '-').'.jpg';

            Image::make($file)->fit(480, 480)->save($dir.$name);

            $participant->image = $dir.$name;
        }

        $participant->save();

        return redirect()->back()->with('profile-updated', true);
    }

}
