<?php

namespace App\Http\Controllers\Participant;

use App\Http\Controllers\Controller;
use App\Models\Exercise;
use App\Models\Participant;
use App\Models\Region;
use App\Models\Sdg;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use function PHPUnit\Framework\never;
use Validator;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:participant');
    }

    public function index()
    {
        $title = 'Команды';

        if (request()->ajax()) {
            $data = Team::query();
            if (request()->search['value']) {
                $data = $data->search(request()->search['value']);
            }
            $data = $data->get();
            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    if ($row->participant_id == auth()->user()->id){
                        return '
                        <a href="' . route('teams.edit', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-success" title="Редактировать">
                            <i class="las la-pen fs-3 text-success"></i>
                        </a>
                        <a href="' . route('teams.delete', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                            <i class="las la-times fs-4 text-danger"></i>
                        </a>
                        <a href="' . route('teams.show', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                    }
                    return '
                        <a href="' . route('teams.show', $row) . '" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('participants_count', function ($row) {
                    return $row->participants->count();
                })
                ->addColumn('region', function ($row) {
                    return $row->region ? $row->region->name_ru : '-';
                })
                ->addColumn('mentor', function ($row) {
                    return $row->mentor ? $row->mentor->full_name : '-';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('participant.teams.index', compact('title'));
    }

    public function create()
    {
        $lang = app()->getLocale();
        $title = 'Форма создания команды';
        $row = new Team();
        $regions = Region::pluck('name_ru', 'id')->toArray();
        $participants = Participant::where('team_id', '=', null)->where('id','<>',auth()->user()->id)->get()->pluck('full_name_region', 'id')->toArray();
        $participants_selected = null;
        $sdg = $lang == 'ru' ? Sdg::pluck('name_ru', 'id')->toArray() : Sdg::pluck('name', 'id')->toArray();
        $mentors = User::role('Ментор')->get()->pluck('full_name', 'id')->toArray();
        return view('participant.teams.create', compact('title', 'row', 'regions', 'participants', 'participants_selected', 'sdg', 'mentors'));
    }

    public function store(Request $request)
    {
        $data = $request->only('name', 'description', 'region_id', 'sdgs');
        $request->validate([
            'name'   => 'required',
            'description' => 'required',
            'region_id' => 'required',
            'sdgs' => 'required',
        ]);

        $participants_id = $request['participants'];
        $data['participant_id'] = auth()->user()->id;
        $data['leader_id'] = auth()->user()->id;
        $team = Team::create($data);

        if ($participants_id){
            foreach ($participants_id as $item) {
                $p = Participant::findOrFail($item);
                $p->team_id = $team->id;
                $p->save();
            }
        }

        if ($request->mentor_id){
            $mentor = User::findOrFail($request->mentor_id);
            $mentor->team_id = $team->id;
            $mentor->save();
        }

        if ($team) {
            $participant = auth()->user();
            $participant->team_id = $team->id;
            $participant->save();
        }

        if($request->avatar_remove){
            if($team->image && file_exists(asset($team->image))) @unlink($team->image);

            $team->image = null;
        }

        if($request->file('avatar')){

            if($team->image && file_exists(asset($team->image))) @unlink($team->image);

            $file = $request->file('avatar');

            $dir  = 'assets/teams/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $name = Str::slug($team->name, '-').'.jpg';

            Image::make($file)->fit(480, 480)->save($dir.$name);

            $team->image = $dir.$name;
        }

        $team->save();

        return redirect(route('team.profile'))->with('team-stored', true);
    }

    public function show(Team $team)
    {
        $i = 1;
        $row = $team;
        $title = 'Команда';
        $participants = Participant::where('team_id', $team->id)->get();

        $participants_select = Participant::where('team_id', null)->orWhere('team_id', $team->id)->where('id','<>',auth()->user()->id)->pluck('name', 'id')->toArray();
        $participants_selected = $row->participants ? $row->participants->where('id','<>',auth()->user()->id)->pluck('id')->toArray() : [];

        return view('participant.teams.show', compact('row', 'title', 'participants', 'participants_select', 'participants_selected', 'i'));
    }

    public function particiantAdd(Request $request, Team $team){
        $participants_id = $request['participants'];
        $sql = 'UPDATE participants SET team_id = null WHERE team_id = ' . strval($team->id) . ' and id != ' . strval(auth()->user()->id);
        DB::statement($sql);
        if ($participants_id) {
            foreach ($participants_id as $item) {
                $p = Participant::findOrFail($item);
                $p->team_id = $team->id;
                $p->save();
            }
        }
        return redirect(route('team.profile'));
    }

    public function edit(Team $team)
    {
        $lang = app()->getLocale();
        $title = 'Редактировать';
        $row = $team;
        $regions = Region::pluck('name_ru', 'id')->toArray();
        $participants = Participant::where('team_id', '=', null)->orWhere('team_id', $team->id)->where('id','<>',auth()->user()->id)->pluck('name', 'id')->toArray();
        $participants_selected = Participant::where('team_id', $team->id)->orWhere('team_id', $team->id)->pluck('id')->toArray();
        $sdg = $lang == 'ru' ? Sdg::pluck('name_ru', 'id')->toArray() : Sdg::pluck('name', 'id')->toArray();
        $mentors = User::role('Ментор')->get()->pluck('full_name', 'id')->toArray();
        return view('participant.teams.edit', compact('title', 'row', 'regions', 'participants', 'participants_selected', 'sdg', 'mentors'));
    }

    public function update(Request $request, Team $team)
    {
        $data = $request->only('name', 'description', 'region_id', 'sdgs');
        $request->validate([
            'name'   => 'required',
            'description' => 'required',
            'region_id' => 'required',
            'sdgs' => 'required',
        ]);

        $participants_id = $request['participants'];
        $data['participant_id'] = auth()->user()->id;
        $team->update($data);
        $sql = 'UPDATE participants SET team_id = null WHERE team_id = ' . strval($team->id) . ' and id != ' . strval(auth()->user()->id);
        DB::statement($sql);
        if ($participants_id) {
            foreach ($participants_id as $item) {
                $p = Participant::findOrFail($item);
                $p->team_id = $team->id;
                $p->save();
            }
        }

        if ($request->mentor_id && $team->mentor_id != $request->mentor_id){
            if($team->mentor){
                $team->mentor->team_id = null;
                $team->mentor->save();
            }
            $mentor = User::findOrFail($request->mentor_id);
            $mentor->team_id = $team->id;
            $mentor->save();
        }

        if($request->avatar_remove){
            if($team->image && file_exists(asset($team->image))) @unlink($team->image);

            $team->image = null;
        }

        if($request->file('avatar')){

            if($team->image && file_exists(asset($team->image))) @unlink($team->image);

            $file = $request->file('avatar');

            $dir  = 'assets/teams/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $name = Str::slug($team->name, '-').'.jpg';

            Image::make($file)->fit(480, 480)->save($dir.$name);

            $team->image = $dir.$name;
        }

        $team->save();

        return redirect(route('team.profile'))->with('team-updated', true);
    }

    public function destroy(Team $team)
    {
        $team->delete();
        return redirect(route('teams.index'))->with('team-deleted', true);
    }

    public function teamProfile()
    {
        $title = 'Профиль команды';

        if(auth()->user()->team){
            $team = auth()->user()->team;
            $exercises = Exercise::where('type', '2')->get();
            $participants_select = Participant::where('team_id', null)->orWhere('team_id', $team->id)->where('id','<>',auth()->user()->id)->pluck('name', 'id')->toArray();
            $participants_selected = $team->participants ? $team->participants->where('id','<>',auth()->user()->id)->pluck('id')->toArray() : [];
        } else {
            $team = null;
            $exercises = [];
            $participants_select = Participant::where('team_id', null)->where('id','<>',auth()->user()->id)->pluck('name', 'id')->toArray();
            $participants_selected = [];
        }

        return view('participant.teams.profile', compact('title', 'team', 'participants_select', 'participants_selected', 'exercises'));
    }

    public function delete_user(Team $team, Participant $participant)
    {
        $participant->team_id = null;
        $participant->save();
        return redirect(route('team.profile'));
    }

    public function enter()
    {
        $title = 'Вступить в команду';
        $teams = Team::pluck('name', 'id')->toArray();
        $regions = Region::pluck('name_ru', 'id')->toArray();
        return view('participant.teams.enter-team', compact('title', 'teams', 'regions'));
    }

    public function enterSubmit(Request $request)
    {
        $team = Team::find($request->team_id);
        if ($team) {
            $participant = auth()->user();
            $participant->team_id = $team->id;
            $participant->save();
        }
        return redirect()->route('teams.index');
    }

    public function info(Request $request)
    {
        $lang = app()->getLocale();
        $team = Team::find($request->team_id);

        $result = array(
            'description' => $team->description,
            'sdg' => $team->sdg ? $team->sdg->getName($lang) : '-',
            'mentor' => $team->mentor ? $team->mentor->full_name : '-'
        );

        if ($team->participants) {
            foreach ($team->participants as $key => $participant) {
                $result['participants'][$key] = [
                    'key' => $key + 1,
                    'full_name' => $participant->full_name,
                    'phone' => $participant->phone ? $participant->phone : '',
                    'email' => $participant->email ? $participant->email : ''
                ];
            }
        } else {
            $result['participants'] = [];
        }

        return $result;
    }

    public function teamExit(Team $team)
    {
        $participant = auth()->user();
        if($participant->id == $team->participant_id){
            $team->delete();
        }
        $participant->team_id = null;
        $participant->save();
        return redirect()->route('teams.index');
    }

    public function setLeader(Team $team, Participant $participant)
    {
        $team->leader_id = $participant->id;
        $team->save();
        return redirect(route('team.profile'));
    }
}
