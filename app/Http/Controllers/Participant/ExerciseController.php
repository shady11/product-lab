<?php
namespace App\Http\Controllers\Participant;

use App\Models\Exercise;
use App\Models\ParticipantExercise;
use App\Models\Region;
use App\Models\Team;
use App\Models\TeamExercise;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ExerciseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:participant');
    }

    public function index()
    {
        $title = 'Задания';

        if (request()->ajax()) {
            $exercises_id = auth()->user()->exercises->pluck('exercise_id');
            $data = Exercise::query();
//            $data = $data->whereIn('id', $exercises_id);
            if (request()->search['value']) {
                $data = $data->search(request()->search['value']);
            }
            $data = $data->get();
            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('actions', function ($row) {
                    return '
                        <a href="'.route('exercises.show', $row).'" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                            <i class="las la-eye fs-4 text-warning"></i>
                        </a>
                    ';
                })
                ->addColumn('participant_grade', function ($row) {
                    return $row->exercise(auth()->user()->id) ? $row->exercise(auth()->user()->id)->grade : '-';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('participant.exercises.index', compact('title'));
    }

    public function show(Exercise $exercise)
    {
        $title = 'Страница задания';
        return view('participant.exercises.show', compact('exercise', 'title'));
    }

    public function participantFile(Request $request, Exercise $exercise)
    {
        if($request->hasFile('file')){
            $file = $request->file('file');

            $dir  = 'assets/exercises/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            if ($request->type == 1){
                $filename = $exercise->id.'-'.Str::slug($exercise->name, '-').'-'.auth()->user()->id.'.'.$file->getClientOriginalExtension();
            }else{
                $filename = $exercise->id.'-'.Str::slug($exercise->name, '-').'team'.'-'.auth()->user()->team->leader_id.'.'.$file->getClientOriginalExtension();
            }

            if($file->move(public_path($dir), $filename)){
                if ($request->type == 1){
                    $participantExercise = ParticipantExercise::where('exercise_id', $exercise->id)->where('participant_id', auth()->user()->id)->first();
                    if (!$participantExercise){
                        $participantExercise = ParticipantExercise::create([
                            'exercise_id' => $exercise->id,
                            'participant_id' => auth()->user()->id
                        ]);
                    }
                    $participantExercise->file = $dir.$filename;
                    if($exercise->autograde != 1){
                        $participantExercise->grade = $exercise->percent * $exercise->grade / 100;
                    }
                    $participantExercise->save();
                }else{
                    $teamExercise = TeamExercise::where('exercise_id', $exercise->id)->where('team_id', auth()->user()->team->id)->first();
                    if (!$teamExercise){
                        $teamExercise = TeamExercise::create([
                            'exercise_id' => $exercise->id,
                            'team_id' => auth()->user()->team->id
                        ]);
                    }
                    $teamExercise->file = $dir.$filename;
                    if($exercise->autograde != 1){
                        $teamExercise->grade = $exercise->percent * $exercise->grade / 100;
                    }
                    $teamExercise->save();
                }



                return redirect()->back()->with('exercise-file-success', true);
            }
        }
        return redirect()->back()->with('exercise-file-failure', true);
    }
}
