<?php

namespace App\Http\Controllers\Participant\Auth;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Participant;
use App\Models\ParticipantBalance;
use App\Models\Region;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME_PARTICIPANT;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:participant')->except('logout');
    }

    public function showLoginForm()
    {
        return view('participant.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'login'   => 'required',
            'password' => 'required'
        ]);

        $participant = Participant::where('login', $request->login)->first();
        if($participant->status) {
            if (Auth::guard('participant')->attempt(['login' => $request->login, 'password' => $request->password])) {
                return redirect()->route('participant.index')->with('logged', true);
            }
        }
        return redirect()->back()->with('msg', 'Логин или пароль указаны неверно');
    }

    public function showRegisterForm($token)
    {
        $participant = DB::table('participants')
            ->where([
                'token' => $token
            ])
            ->first();

        if(!$participant){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        return view('participant.auth.register', ['email' => $participant->email]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'login'   => 'required',
            'email' => 'required',
            'password' => 'required|min:6',
        ]);

        $participant = Participant::where('email', $request->email)->first();
        $participant->password = bcrypt($request->password);
        $participant->login = $request->login;
        $participant->status = 1;
        $participant->save();

        return redirect()->route('participant.login')->with('registered', true);
    }

    public function showApplyForm()
    {
        $lang = app()->getLocale();

        $regions = Region::all();

        return view('participant.auth.apply', compact('lang', 'regions'));
    }

    public function apply(Request $request)
    {
        $request->validate([
            'lastname' => 'required',
            'name' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:participants',
            'activity' => 'required',
            'region_id' => 'required',
            'socio_economic_status' => 'required',
            'type' => 'required',
            'participated' => 'required',
            'motivation' => 'required',
        ]);

        $token = Str::random(64);

        $participant = Participant::create($request->except('socio_economic_status_other_checked', 'socio_economic_status_other', 'type_other_checked', 'type_other', 'goal_other_checked', 'goal_other') + [
            'token' => $token
        ]);

        if($request->socio_economic_status_other) {
            $participant->socio_economic_status = array_merge($request->socio_economic_status, [$request->socio_economic_status_other]);
        }

        if($request->type_other) {
            $participant->type = array_merge($request->type, [$request->type_other]);
        }

        if($request->goal_other) {
            $participant->goal = array_merge($request->goal, [$request->goal_other]);
        }

        return redirect()->back()->with('applied', true)->withInput(['token' => $token]);
    }

    public function showForgetPasswordForm()
    {
        return view('participant.auth.forget-password');
    }

    public function forgetPassword(Request $request)
    {
        $validator = $this->validate($request, [
            'email' => 'required|email|exists:participants',
        ]);

        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        Mail::send('participant.email.forget-password', ['token' => $token, 'link' => route('participant.reset-password', $token)], function($message) use($request){
            $message->to($request->email);
            $message->subject('Сброс пароля');
        });

        return redirect()->route('participant.check-email')->withInput(['email' => $request->email]);
    }

    public function checkEmail()
    {
        return view('participant.auth.check-email');
    }

    public function showResetPasswordForm($token)
    {
        return view('participant.auth.reset-password', compact('token'));
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|min:6',
        ]);

        $updatePassword = DB::table('password_resets')
            ->where([
                'token' => $request->token
            ])
            ->first();

        if(!$updatePassword){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $participant = Participant::where('email', $updatePassword->email)
            ->update(['password' => bcrypt($request->password)]);

        DB::table('password_resets')->where(['email'=> $updatePassword->email])->delete();

        return view('participant.auth.password-changed');
    }

    public function logout()
    {
        Auth::guard('participant')->logout();
        return redirect('/participant');
    }
}
