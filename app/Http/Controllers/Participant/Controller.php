<?php

namespace App\Http\Controllers\Participant;

class Controller extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        $this->middleware('auth:participant');
    }
}
