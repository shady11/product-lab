<?php

namespace App\Http\Controllers\Participant;

use App\Models\Exercise;
use App\Models\Material;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    public function index()
    {
        $title = 'Все темы';
        $current_date = date('Y-m-d');
        $topics = Topic::where('dateactive', '<=', $current_date)->orderBy('created_at', 'desc')->get();
        return view('participant.topics.index', compact('title', 'topics'));
    }


    public function show(Topic $topic){
        $row = $topic;
        $title = 'Show';
        $materials = Material::all();
        $exercises = Exercise::all();
        return view('admin.topics.show', compact('row', 'title', 'materials', 'exercises'));
    }

}
