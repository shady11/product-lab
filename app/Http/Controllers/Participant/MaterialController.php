<?php

namespace App\Http\Controllers\Participant;

use App\Models\Exercise;
use App\Models\Material;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MaterialController extends Controller
{
    public function show(Material $material){
        $title = 'Материал';
        $row = $material;
        $materials = Material::where('topic_id', $material->topic_id)->where('id', '<>', $material->id)->get();
        $exercises = Exercise::where('topic_id', $material->topic_id)->get();
        return view('participant.materials.show', compact('title', 'row', 'materials', 'exercises'));
    }
}
