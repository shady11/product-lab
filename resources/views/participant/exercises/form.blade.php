<div class="card-body">
    <div class="row fv-row mb-9">
        {!! Form::label('name', 'Наименование задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('name'))
                <div class="error-message">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('description', 'Описание задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '6', 'disabled']) !!}
            @if($errors->has('description'))
                <div class="error-message">{{ $errors->first('description') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('link', 'Ccылка на задание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('link', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('link'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('deadline', 'Срок выполнения', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('deadline', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('deadline'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('grade', 'Баллы за выполнение задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('grade', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('grade'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
{{--    @if($exercise->exercise(auth()->user()->id)->file)--}}
    @if(isset($exercise->exercise(auth()->user()->id)->file))
    <div class="row fv-row mb-9">
        {!! Form::label('file', 'Загруженное задание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            <div class="d-flex align-items-center mb-6">
                <!--begin::Symbol-->
                <div class="symbol symbol-50px me-3">
                    <div class="symbol-label bg-light-primary">
                        <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm002.svg-->
                        <span class="svg-icon svg-icon-1 svg-icon-success">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M21 10H13V11C13 11.6 12.6 12 12 12C11.4 12 11 11.6 11 11V10H3C2.4 10 2 10.4 2 11V13H22V11C22 10.4 21.6 10 21 10Z" fill="black"></path>
                                <path opacity="0.3" d="M12 12C11.4 12 11 11.6 11 11V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V11C13 11.6 12.6 12 12 12Z" fill="black"></path>
                                <path opacity="0.3" d="M18.1 21H5.9C5.4 21 4.9 20.6 4.8 20.1L3 13H21L19.2 20.1C19.1 20.6 18.6 21 18.1 21ZM13 18V15C13 14.4 12.6 14 12 14C11.4 14 11 14.4 11 15V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18ZM17 18V15C17 14.4 16.6 14 16 14C15.4 14 15 14.4 15 15V18C15 18.6 15.4 19 16 19C16.6 19 17 18.6 17 18ZM9 18V15C9 14.4 8.6 14 8 14C7.4 14 7 14.4 7 15V18C7 18.6 7.4 19 8 19C8.6 19 9 18.6 9 18Z" fill="black"></path>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                </div>
                <!--end::Symbol-->
                <!--begin::Title-->
                <div>
                    <a href="{{asset($exercise->exercise(auth()->user()->id)->file)}}" class="fs-4 fw-bolder">
                        {{explode('/', $exercise->exercise(auth()->user()->id)->file)[count(explode('/', $exercise->exercise(auth()->user()->id)->file))-1]}}
                    </a>
                </div>
                <!--end::Title-->
            </div>
        </div>
    </div>
    @endif

    @if(!isset($exercise->exercise(auth()->user()->id)->grade))
    <div class="row fv-row mb-9">
        {!! Form::label('file', 'Загрузка задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::file('file', ['class' => 'form-control']) !!}
        </div>
    </div>
    @else
        <div class="row fv-row mb-9">
            {!! Form::label('participant_grade', 'Оценка за задание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9">
                {!! Form::text('participant_grade', $exercise->exercise(auth()->user()->id)->grade, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
    @endif
</div>
 <div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            @if(!isset($exercise->exercise(auth()->user()->id)->grade))
                <button type="submit" class="btn btn-success mr-2">Отправить задание</button>
            @endif
                <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад
        </div>
    </div>
</div>



