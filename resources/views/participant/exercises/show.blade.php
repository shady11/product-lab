@extends('participant.default')

@section('title', $title)

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card h-100 mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h3 class="fw-bolder">
                            Задание
                        </h3>
                    </div>
                    <!--begin::Card title-->
                    <div class="card-toolbar">
                        <span class="fw-boldest text-gray-600 text-uppercase">{{$exercise->getTopic()}}</span>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body">
                    <!--begin::Section-->
                    <div class="mb-10">
                        <!--begin::Details-->
                        <div class="d-flex flex-wrap py-5">
                            <!--begin::Row-->
                            <div class="flex-equal me-5">
                                <!--begin::Details-->
                                <table class="table align-middle fs-4 fw-bold gs-0 gy-2 gx-2">
                                    <tbody>
                                    <!--begin::Row-->
                                    <tr>
                                        <td class="fw-bolder fs-6 text-uppercase text-gray-500 min-w-250px w-250px">
                                            Заголовок задания:
                                        </td>
                                        <td class="text-gray-800 fs-5 min-w-200px">
                                            {{$exercise->name}}
                                        </td>
                                    </tr>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <tr>
                                        <td class="fw-bolder fs-6 text-uppercase text-gray-500 min-w-250px w-250px">Срок
                                            выполнения:
                                        </td>
                                        <td class="text-gray-800 min-w-200px">
                                            {{$exercise->deadline}}
                                        </td>
                                    </tr>
                                    <!--end::Row-->
                                    @if($exercise->grade)
                                        <!--begin::Row-->
                                        <tr>
                                            <td class="fw-bolder fs-6 text-uppercase text-gray-500 min-w-250px w-250px">
                                                Баллы за выполнение задания:
                                            </td>
                                            <td class="text-gray-800 min-w-200px">
                                                {{$exercise->grade}}
                                            </td>
                                        </tr>
                                        <!--end::Row-->
                                    @endif
                                    @if(auth()->user()->exercise($exercise->id))
                                        <!--begin::Row-->
                                        <tr>
                                            <td class="fw-bolder fs-6 text-uppercase text-gray-500 min-w-250px w-250px">
                                                Оценка:
                                            </td>
                                            <td class="text-gray-800 min-w-200px">
                                                {{auth()->user()->exercise($exercise->id)->grade}}
                                            </td>
                                        </tr>
                                        <!--end::Row-->
                                    @endif
                                    </tbody>
                                </table>
                                <!--end::Details-->
                            </div>
                            <!--end::Row-->
                        </div>
                        <!--end::Row-->
                        <div class="fs-5 fw-bold text-gray-600 mb-12">
                            {!! $exercise->description !!}
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Card body-->
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-6">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h3 class="fw-bolder">
                            Файл
                        </h3>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                @if(isset($exercise->exercise(auth()->user()->id)->file) or isset($exercise->team(auth()->user()->team->id)->file))
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-6">
                            <!--begin::Symbol-->
                            <div class="symbol symbol-50px me-3">
                                <div class="symbol-label bg-light-primary">
                                    <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm002.svg-->
                                    <span class="svg-icon svg-icon-1 svg-icon-success">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none">
                                                <path
                                                    d="M21 10H13V11C13 11.6 12.6 12 12 12C11.4 12 11 11.6 11 11V10H3C2.4 10 2 10.4 2 11V13H22V11C22 10.4 21.6 10 21 10Z"
                                                    fill="black"></path>
                                                <path opacity="0.3"
                                                      d="M12 12C11.4 12 11 11.6 11 11V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V11C13 11.6 12.6 12 12 12Z"
                                                      fill="black"></path>
                                                <path opacity="0.3"
                                                      d="M18.1 21H5.9C5.4 21 4.9 20.6 4.8 20.1L3 13H21L19.2 20.1C19.1 20.6 18.6 21 18.1 21ZM13 18V15C13 14.4 12.6 14 12 14C11.4 14 11 14.4 11 15V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18ZM17 18V15C17 14.4 16.6 14 16 14C15.4 14 15 14.4 15 15V18C15 18.6 15.4 19 16 19C16.6 19 17 18.6 17 18ZM9 18V15C9 14.4 8.6 14 8 14C7.4 14 7 14.4 7 15V18C7 18.6 7.4 19 8 19C8.6 19 9 18.6 9 18Z"
                                                      fill="black"></path>
                                            </svg>
                                        </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Symbol-->
                            <!--begin::Title-->
                            @if($exercise->type == 1)
                                <div>
                                    <a href="{{asset($exercise->exercise(auth()->user()->id)->file)}}"
                                       class="fs-5 fw-bolder">
                                        {{explode('/', $exercise->exercise(auth()->user()->id)->file)[count(explode('/', $exercise->exercise(auth()->user()->id)->file))-1]}}
                                    </a>
                                </div>
                            @else
                                <div>
                                    <a href="{{asset($exercise->team(auth()->user()->team->id)->file)}}"
                                       class="fs-5 fw-bolder">
                                        {{explode('/', $exercise->team(auth()->user()->team->id)->file)[count(explode('/', $exercise->team(auth()->user()->team->id)->file))-1]}}
                                    </a>
                                </div>
                        @endif
                        <!--end::Title-->
                        </div>
                    </div>
                @else
                    @if($exercise->type == 2 and auth()->user()->team->leader_id != auth()->user()->id)
                        <div class="card-body">
                            <div class="d-flex align-items-center mb-6">
                                <!--begin::Symbol-->
                                <div class="symbol symbol-50px me-3">
                                    <div class="symbol-label bg-light-danger">
                                        <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm002.svg-->
                                        <span class="svg-icon svg-icon-1 svg-icon-danger">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </div>
                                </div>
                                <!--end::Symbol-->
                                <!--begin::Title-->
                                <div>
                                    Вы не можете отправить задание
                                </div>
                                <!--end::Title-->
                            </div>
                        </div>
                    @else
                        {!! Form::model($exercise, ['route' => ['exercise.participant.file', $exercise],'enctype' => 'multipart/form-data']) !!}
                        <div class="card-body pt-12">
                            {!! Form::file('file', ['class' => 'form-control form-control-sm']) !!}
                            <div class="d-flex justify-content-end mt-6">
                                @if($exercise->type == 2)
                                    <input type="text" value="2" name="type" hidden>
                                @else
                                    <input type="text" value="1" name="type" hidden>
                                @endif
                                @if(!isset($exercise->exercise(auth()->user()->id)->grade))
                                    <button type="submit" class="btn btn-sm btn-success me-2">Отправить задание</button>
                                @endif
                                <button type="reset" onclick="window.history.back();" class="btn btn-sm btn-secondary">
                                    Назад
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                @endif
            @endif
            <!--end::Card body-->

            </div>
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h3 class="fw-bolder">
                            Материалы по теме
                        </h3>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body">
                    @foreach($exercise->topic->materials as $material)
                        @if(!$loop->first)
                            <div class="separator separator-dashed mb-4"></div> @endif
                    <!--begin::Item-->
                        <div class="d-flex align-items-sm-center @if(!$loop->last) mb-4 @endif">
                            <!--begin::Symbol-->
                            <div class="symbol symbol-50px me-3">
                                <div class="symbol-label bg-light-info">
                                    <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm010.svg-->
                                    <span class="svg-icon svg-icon-2x svg-icon-info">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M16.0077 19.2901L12.9293 17.5311C12.3487 17.1993 11.6407 17.1796 11.0426 17.4787L6.89443 19.5528C5.56462 20.2177 4 19.2507 4 17.7639V5C4 3.89543 4.89543 3 6 3H17C18.1046 3 19 3.89543 19 5V17.5536C19 19.0893 17.341 20.052 16.0077 19.2901Z"
                                                fill="black"/>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Symbol-->
                            <!--begin::Section-->
                            <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                                <div class="flex-grow-1 me-2">
                                    <a href="{{ route('materials.show', $material) }}"
                                       class="text-gray-800 text-hover-info fs-6 fw-bolder">
                                        {{ $material->name }}
                                    </a>
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Item-->
                    @endforeach
                </div>
                <!--end::Card body-->
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        @if(session('exercise-file-success'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Файл успешно загружен!</div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif
        @if(session('exercise-file-failure'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Не удалось загрузить файл!</div>",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif

    </script>@endsection
