@extends('participant.default')
@section('title', $title)
@section('pre_styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body py-4 position-relative">
            <!--begin::Datatable-->
            <table id="dataTable" class="table align-middle table-row-dashed fs-6 gy-5">
                <thead>
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th class="w-50px">№</th>
                    <th>Наименование задания</th>
                    <th>Ссылка на задание</th>
                    <th>Срок выполнения</th>
                    <th>Баллы за задание</th>
                    <th>Оценка</th>
                    <th class="text-end min-w-250px">Действия</th>
                </tr>
                </thead>
                <tbody class="text-gray-600 fw-bold">
                </tbody>
            </table>
            <!--end::Datatable-->
        </div>
        <!--end::Card body-->
    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
    <script>
        let table = $('#dataTable').DataTable({
            // dom: 'Btrlip',
            dom: 'tr'+
                '<"row"<"col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"li><"col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end dataTables_pager"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('exercises.index') }}',
                data: function(d) {
                }
            },
            columns: [
                { data: 'id'},
                { data: 'name'},
                { data: 'link'},
                { data: 'deadline'},
                { data: 'grade'},
                { data: 'participant_grade'},
                { data: 'actions', className: 'text-end'},
            ],
            order: [[ 0, "desc" ]],
            pageLength: 20,
            lengthMenu: ['20', '30', '40', '50', '100'],
            language: {
                "url": "{{asset('assets/js/russian.json')}}"
            },
        });
        // Search Datatable
        const filterSearch = $('#search_handler');
        filterSearch.keyup(function (e) {
            table.search(e.target.value).draw();
        });
    </script>
@endsection
