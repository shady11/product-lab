@extends('participant.default')

@section('title', $title)

@section('content')

    @if(auth()->user()->team_id)
        <div class="d-flex flex-column flex-lg-row">
            <!--begin::Sidebar-->
            <div class="flex-column flex-lg-row-auto w-lg-300px w-xl-400px mb-10">
                <!--begin::Card-->
                <div class="card mb-5 mb-xl-8">
                    <!--begin::Card body-->
                    <div class="card-body">
                        <!--begin::Summary-->
                        <!--begin::User Info-->
                        <div class="d-flex flex-center flex-column py-5">
                            <!--begin::Avatar-->
                            <div class="symbol symbol-100px symbol-circle mb-7">
                                @if($team->image)
                                    <img src="{{asset($team->image)}}" alt="{{$team->name}}">
                                @else
                                    <img src="{{asset('assets/media/avatars/blank.png')}}" alt="{{$team->name}}">
                                @endif
                            </div>
                            <!--end::Avatar-->
                            <!--begin::Name-->
                            <div class="fs-3 text-gray-800 fw-bolder mb-3">{{ $team->name }}</div>
                            <!--end::Name-->
                        </div>
                        <!--end::User Info-->
                        <!--end::Summary-->
                        <!--begin::Details content-->
                        <div>
                            <div class="pb-5 fs-6">
                                <!--begin::Details item-->
                                <div class="fw-boldest mt-5">Название</div>
                                <div class="text-gray-600">{{ $team->name }}</div>
                                <!--begin::Details item-->
                                <div class="fw-boldest mt-5">Описание</div>
                                <div class="text-gray-600">{{ $team->description }}</div>
                                <!--begin::Details item-->
                                <div class="fw-boldest mt-5">Регион</div>
                                <div class="text-gray-600">{{ $team->region ? $team->region->getName(app()->getLocale()) : '-' }}</div>
                                <!--begin::Details item-->
                                <div class="fw-boldest mt-5">Лидер</div>
                                <div class="text-gray-600">{{ $team->leader ? $team->leader->full_name : '-' }}</div>
                                <!--begin::Details item-->
                                <div class="fw-boldest mt-5">Ментор</div>
                                <div class="text-gray-600">{{ $team->mentor ? $team->mentor->full_name : '-' }}</div>
                                <!--begin::Details item-->
                                <div class="fw-boldest mt-5">Цели устойчивого развития</div>
                                <div class="text-gray-600">{!! $team->sdgs ? $team->sdgsList() : '-' !!}</div>
                            </div>
                        </div>
                        <!--end::Details content-->
                        @if(auth()->user()->id == $team->leader_id)
                            <!--begin::Details toggle-->
                            <div class="d-flex flex-stack fs-4 pt-6">
                                <span>
                                    <a href="{{ route('teams.edit', $team) }}" class="btn btn-sm btn-light-primary">
                                        Редактировать
                                    </a>
                                </span>
                                <span>
                                    <a href="{{ route('teams.delete', $team) }}" class="btn btn-sm btn-danger">
                                        Удалить
                                    </a>
                                </span>
                            </div>
                            <!--end::Details toggle-->
                        @elseif(auth()->user()->team_id == $team->id)
                            <!--begin::Details toggle-->
                            <div class="d-flex flex-stack fs-4 py-6">
                                <span>
                                    <a href="{{ route('teams.exit', $team) }}" class="btn btn-sm btn-danger">
                                        Выйти из команды
                                    </a>
                                </span>
                            </div>
                            <!--end::Details toggle-->
                        @endif
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Sidebar-->
            <!--begin::Content-->
            <div class="flex-lg-row-fluid ms-lg-15">

                <div class="card mb-6 mb-xl-9">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <h3 class="card-title">Состав команды</h3>
                        @if(auth()->user()->id == $team->leader_id)
                            <div class="card-toolbar">
                                <a href="#" class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#kt_modal_update_details">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
                                    <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1"  transform="rotate(-90 11.364 20.364)" fill="black"></rect>
                                                <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>
                                            </svg>
                                        </span>
                                    <!--end::Svg Icon-->
                                    Добавить участников
                                </a>
                            </div>
                        @endif
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pb-5">
                        <!--begin::Table wrapper-->
                        <div class="table-responsive">
                        @if($team->participants)
                            <!--begin::Table-->
                                <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                                    <!--begin::Table head-->
                                    <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                                    <!--begin::Table row-->
                                    <tr class="text-start text-muted text-uppercase gs-0">
                                        <th class="w-75px"></th>
                                        <th>ФИО</th>
                                        <th>Телефон</th>
                                        <th>Email адресс</th>
                                        @if(auth()->user()->id == $team->leader_id || auth()->user()->id != $team->participant_id)
                                            <th class="min-w-125px text-end">Действия</th>
                                        @endif
                                    </tr>
                                    <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fs-6 fw-bold">
                                    @foreach($team->participants as $key=>$p)
                                        <tr>
                                            <td>
                                                <div class="symbol symbol-50px">
                                                    @if($p->image)
                                                        <img src="{{asset($p->image)}}" class="img-fluid" alt="" />
                                                    @else
                                                        <img src="{{asset('assets/media/svg/avatars/blank.svg')}}" class="img-fluid" alt="" />
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    {{ $p->name }} {{ $p->lastname }}
                                                    @if(auth()->user()->team->leader_id == $p->id)
                                                        <div class="rating ms-2">
                                                            <div class="rating-label me-1 checked">
                                                                <i class="bi bi-star-fill fs-6s"></i>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                @if(auth()->user()->team->leader_id == $p->id)
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Лидер команды</span>
                                                @endif
                                            </td>
                                            <td>{{ $p->phone }}</td>
                                            <td>{{ $p->email }}</td>
                                            @if(auth()->user()->id == $team->leader_id and auth()->user()->team->leader_id != $p->id)
                                                <td class="text-end">
                                                    <a href="{{ route('team.set_leader', [$team, $p]) }}" class="btn btn-sm btn-light-primary" title="Сделать лидером">
                                                        Сделать лидером
                                                    </a>
                                                    <a href="{{ route('teams.delete_user', [$team, $p->id]) }}" class="btn btn-sm btn-light-danger" title="Удалить">
                                                        Удалить
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            @endif
                        </div>
                        <!--end::Table wrapper-->
                    </div>

                    <!--end::Card body-->
                </div>

                <div class="card mb-6 mb-xl-9">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <h3 class="card-title">Задания команды</h3>
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body">
                        <!--begin::Table wrapper-->
                        <div class="table-responsive">
                        @if($exercises->count()>0)
                            <!--begin::Table-->
                                <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                                    <!--begin::Table head-->
                                    <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                                    <!--begin::Table row-->
                                    <tr class="text-start text-muted text-uppercase gs-0">
                                        <th class="w-40px">№</th>
                                        <th>Задание</th>
                                        <th>Срок выполнения</th>
                                        <th class="text-end">Действие</th>
                                    </tr>
                                    <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fs-6 fw-bold text-gray-600">
                                    @foreach($exercises as $key=>$e)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $e->name }}</td>
                                            <td>{{ $e->deadline }}</td>
                                            <td class="text-end">
                                                <a href="{{ route('exercises.show', $e )}}" class="btn btn-sm btn-light-info" title="Сделать лидер">
                                                    Посмотреть задание
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                        @else
                            <div class="text-center py-9">
                                Нет заданий
                            </div>
                        @endif
                        </div>
                        <!--end::Table wrapper-->
                    </div>

                    <!--end::Card body-->
                </div>

            </div>
            <!--end::Content-->
        </div>

        <!--begin::Modal-->
        <div class="modal fade" id="kt_modal_update_details" tabindex="-1" aria-hidden="true">
            <!--begin::Modal dialog-->
            <div class="modal-dialog modal-dialog-centered mw-650px">
                <!--begin::Modal content-->
                <div class="modal-content">
                    <!--begin::Form-->
                    <form class="form" action="{{route('teams.particiantAdd', $team)}}" id="kt_modal_update_user_form"
                          method="POST" enctype="multipart/form-data">
                    @csrf
                    <!--begin::Modal header-->
                        <div class="modal-header px-lg-12" id="kt_modal_update_user_header">
                            <!--begin::Modal title-->
                            <h2 class="fw-bolder">Добавить участников</h2>
                            <!--end::Modal title-->
                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none">
                                    <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                          transform="rotate(-45 6 17.3137)" fill="black"/>
                                    <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                          transform="rotate(45 7.41422 6)" fill="black"/>
                                </svg>
                            </span>
                            </div>
                            <!--end::Close-->
                        </div>
                        <!--end::Modal header-->
                        <!--begin::Modal body-->
                        <div class="modal-body py-10 px-lg-12">
                            <!--begin::Scroll-->
                            <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_update_user_scroll"
                                 data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}"
                                 data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_user_header"
                                 data-kt-scroll-wrappers="#kt_modal_update_user_scroll" data-kt-scroll-offset="300px">
                                <!--begin::User form-->
                                <div id="kt_modal_update_user_user_info">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-bold mb-2">
                                            <span>Участники</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        {!! Form::select('participants', $participants_select, $participants_selected,['class' => 'form-select', 'data-control'=>'select2', 'multiple'=>'multiple','name'=>'participants[]', 'data-placeholder'=>'Выбрать']) !!}
                                        @if($errors->has('participants'))
                                            <div class="error-message">{{ $errors->first('participants') }}</div>
                                    @endif
                                    <!--end::Input-->
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                            </div>
                            <!--end::User form-->
                        </div>
                        <!--end::Scroll-->
                        <!--end::Modal body-->
                        <!--begin::Modal footer-->
                        <div class="modal-footer flex-center">
                            <!--begin::Button-->
                            <button type="button" class="btn btn-sm btn-light" data-bs-dismiss="modal">Отмена</button>
                            <!--end::Button-->
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-users-modal-action="submit">Сохранить</button>
                            <!--end::Button-->
                        </div>
                        <!--end::Modal footer-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Modal - Update user details-->
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-4">
                <div class="card bg-primary card-md-stretch">
                    <!--begin::Body-->
                    <div class="card-body d-flex flex-column justify-content-center py-12">
                        <!--begin::Heading-->
                        <div class="m-0">
                            <!--begin::Title-->
                            <h1 class="fw-normal text-white text-center lh-lg mb-9">
                                <span class="d-block fs-2">Еще нет команды?</span>
                                <span class="d-block fw-boldest">Создай новую или вступай в существующую</span>
                            </h1>
                            <!--end::Title-->
                            <!--begin::Illustration-->
                            <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center card-rounded-bottom h-200px mh-200px my-5 mb-lg-12">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com014.svg-->
                                <span class="svg-icon svg-icon-light svg-icon-fluid"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black"/>
<rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black"/>
<path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black"/>
<rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black"/>
</svg></span>
                                <!--end::Svg Icon-->
                            </div>
                            <!--end::Illustration-->
                        </div>
                        <!--end::Heading-->
                        <!--begin::Links-->
                        <div class="text-center">
                            <!--begin::Link-->
                            <a href="{{route('teams.create')}}" class="btn btn-sm btn-success fw-boldest me-2">Создать команду</a>
                            <!--end::Link-->
                            <!--begin::Link-->
                            <a href="{{route('team.enter')}}" class="btn btn-sm btn-success fw-boldest">Вступить в команду</a>
                            <!--end::Link-->
                        </div>
                        <!--end::Links-->
                    </div>
                    <!--end::Body-->
                </div>
            </div>
        </div>
    @endif

@endsection

@section('scripts')

    <script>

        $("#datepicker_date").flatpickr({
            dateFormat: "d-m-Y",
        });

        @if(session('team-stored'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Команда успешно создана!</div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif

        @if(session('team-updated'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Изменения сохранены!</div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif

    </script>

@endsection
