@extends('participant.default')

@section('title', $title)

@section('content')

    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <h3>{{ $title }}</h3>
                </div>
                <!--end::Search-->
            </div>
            <!--begin::Card title-->
        </div>
        <!--end::Card header-->

        {!! Form::open(['route' => 'team.enter.submit','enctype' => 'multipart/form-data', 'method' => 'POST']) !!}

        <div class="card-body">
            <div class="row fv-row mb-9">
                {!! Form::label('team_id', 'Команда', ['class' => 'col-lg-3 py-4 text-gray-600 text-right']) !!}
                <div class="col-lg-5">
                    {!! Form::select('team_id', [null=>'- выбрать -']+$teams, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
                </div>
            </div>
            <div class="row fv-row mb-9">
                {!! Form::label('description', 'Описание команды', ['class' => 'col-lg-3 py-4 text-gray-600 text-right']) !!}
                <div class="col-lg-5">
                    <div class="py-4 fw-bolder" id="teamDescription">
                    </div>
                </div>
            </div>
            <div class="row fv-row mb-9">
                {!! Form::label('sdg_id', 'Цель устойчивого развития', ['class' => 'col-lg-3 py-4 text-gray-600 text-right']) !!}
                <div class="col-lg-5">
                    <div class="py-4 fw-bolder" id="teamSdg">
                    </div>
                </div>
            </div>
            <div class="row fv-row mb-9">
                {!! Form::label('mentor_id', 'Ментор', ['class' => 'col-lg-3 py-4 text-gray-600 text-right']) !!}
                <div class="col-lg-5">
                    <div class="py-4 fw-bolder" id="teamMentor">
                    </div>
                </div>
            </div>
            <div class="row fv-row mb-9">
                {!! Form::label('participants', 'Список участников', ['class' => 'col-lg-3 py-4 text-gray-600 text-right']) !!}
                <div class="col-lg-6">
                    <div class="py-4 fw-bolder" id="teamParticipants">
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button id="submitBtn" type="submit" class="btn btn-success mr-2" disabled>Вступить</button>
                    <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад</button>
                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection

@section('scripts')
    <script>
        $('select[name=team_id]').on('change', function () {

            if($(this).val()){
                $('#submitBtn').removeAttr('disabled');
            }

            $.ajax({
                url: "{{route('teams.info')}}",
                cache: false,
                method: 'POST',
                data: {
                    'team_id': $(this).val()
                }
            }).done(function(data) {
                $('#teamDescription').html(data['description']);
                $('#teamSdg').html(data['sdg']);
                $('#teamMentor').html(data['mentor']);
                console.log(data['participants'])

                if(data['participants'].length > 0){
                    for(let i=0; i<data['participants'].length; i++ ){
                        $('#teamParticipants').html(`
                        <table class="table table-row-bordered gs-4 gy-4 gx-4" >
                            <thead>
                            <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-300">
                                <th>№</th>
                                <th>Ф.И.О. участника</th>
                                <th>Контактные данные</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>`+data['participants'][i]['key']+`</td>
                                    <td>`+data['participants'][i]['full_name']+`</td>
                                    <td>`+data['participants'][i]['email']+`<br>`+data['participants'][i]['phone']+`</td>
                                </tr>
                            </tbody>
                        </table>`);
                    }
                }
            });
        });
    </script>
@endsection
