<div class="card-body">
    <!--begin::Input group-->
    <div class="row fv-row mb-9">
        {!! Form::label('image', 'Фото', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <!--begin::Image input wrapper-->
        <div class="col-lg-5">
            <!--begin::Image input-->
            <div class="image-input @if($row->image) image-input-outline @else image-input-empty @endif" data-kt-image-input="true" style="background-image: url('{{asset('assets/media/svg/avatars/blank.svg')}}')">
                <!--begin::Preview existing avatar-->
                <div class="image-input-wrapper w-200px h-200px" @if($row->image) style="background-image: url({{asset($row->image)}})" @endif></div>
                <!--end::Preview existing avatar-->
                <!--begin::Label-->
                <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Изменить">
                    <i class="las la-pen fs-7"></i>
                    <!--begin::Inputs-->
                    <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                    <input type="hidden" name="avatar_remove" />
                    <!--end::Inputs-->
                </label>
                <!--end::Label-->
                <!--begin::Cancel-->
                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Отменить">
                    <i class="bi bi-x fs-2"></i>
                </span>
                <!--end::Cancel-->
                <!--begin::Remove-->
                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Удалить">
                    <i class="bi bi-x fs-2"></i>
                </span>
                <!--end::Remove-->
            </div>
            <!--end::Image input-->
            <!--begin::Hint-->
            <div class="form-text">Допустимые разрешения: png, jpg, jpeg</div>
            <!--end::Hint-->
        </div>
        <!--end::Image input wrapper-->
    </div>
    <!--end::Input group-->
    <div class="row fv-row mb-9">
        {!! Form::label('name', 'Наименование команды', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if($errors->has('name'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('description', 'Описание команды', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '3']) !!}
            @if($errors->has('description'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('description') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('sdgs', 'Цель устойчивого развития', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('sdgs[]', $sdg, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -', 'multiple']) !!}
            @if($errors->has('sdgs'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('sdgs') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('region_id', 'Регион', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('region_id', [null=>'- выбрать -']+$regions, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
            @if($errors->has('region_id'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('region_id') }}</div>
            @endif
        </div>
    </div>
    <div class="row mb-9">
        {!! Form::label('participants', 'Участники', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('participants', $participants, $participants_selected,['class' => 'form-select', 'data-control'=>'select2', 'multiple'=>'multiple','name'=>'participants[]', 'data-placeholder'=>'- выбрать -']) !!}
            @if($errors->has('participants'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('participants') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('mentor_id', 'Ментор', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('mentor_id', [null=>'- выбрать -']+$mentors, $row->mentor ? $row->mentor->id : null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
            @if($errors->has('mentor_id'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('mentor') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success mr-2">Сохранить</button>
            <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад</button>
        </div>
    </div>
</div>
