<div class="card-body">
    <div class="row fv-row mb-9">
        {!! Form::label('name', 'Наименование задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('name'))
                <div class="error-message">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('description', 'Описание задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '6', 'disabled']) !!}
            @if($errors->has('description'))
                <div class="error-message">{{ $errors->first('description') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('link', 'Ccылка на задание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('link', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('link'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('link', 'Ссылка на материал', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('link', null, ['class' => 'form-control', 'disabled']) !!}
            @if($errors->has('link'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('file', 'Файл', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            <a class="current-attachment" href="{{ asset($row->file) }}">{{ $row->file }}</a>&nbsp;<a href="#" class="current-attachment"><span class="glyphicon glyphicon-remove-circle remove-attachment"></span></a>
        </div>
    </div>

</div>
 <div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад</button>
        </div>
    </div>
</div>



