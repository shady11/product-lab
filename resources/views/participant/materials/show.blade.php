@extends('participant.default')

@section('title', $title)

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card h-100 mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h3 class="fw-bolder">
                            Материал
                        </h3>
                    </div>
                    <!--begin::Card title-->
                    <div class="card-toolbar">
                        <span class="fw-boldest text-gray-600 text-uppercase">{{$row->getTopic()}}</span>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body">
                    <!--begin::Layout-->
                    <div class="d-flex flex-column flex-xl-row">
                        <!--begin::Content-->
                        <div class="flex-lg-row-fluid me-xl-15 mb-20 mb-xl-0">
                            <!--begin::Ticket view-->
                            <div class="mb-0">
                                <!--begin::Heading-->
                                <div class="d-flex align-items-center mb-9">
                                    <!--begin::Icon-->
                                    <!--begin::Svg Icon | path: icons/duotune/files/fil008.svg-->
                                    <span class="svg-icon svg-icon-3x svg-icon-info ms-n2 me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path d="M16.0077 19.2901L12.9293 17.5311C12.3487 17.1993 11.6407 17.1796 11.0426 17.4787L6.89443 19.5528C5.56462 20.2177 4 19.2507 4 17.7639V5C4 3.89543 4.89543 3 6 3H17C18.1046 3 19 3.89543 19 5V17.5536C19 19.0893 17.341 20.052 16.0077 19.2901Z" fill="black"/>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <!--begin::Title-->
                                        <h1 class="text-gray-800 fw-bold">{{$row->name}}</h1>
                                        <!--end::Title-->
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Details-->
                                <div class="mb-9">
                                    <!--begin::Description-->
                                    <div class="mt-9 fs-5 fw-normal text-gray-800">
                                        @if($row->videourl)
                                            <!--begin::Container-->
                                            <div class="overlay mt-8">
                                                <!--begin::Video-->
                                                <iframe width="700" height="400"
                                                        src="https://www.youtube.com/embed/{{ $row->getVideo() }}">
                                                </iframe>
                                                <!--end::Vodeo-->
                                            </div>
                                            <!--end::Container-->
                                        @endif
                                    </div>
                                    <!--end::Description-->
                                    <!--begin::Description-->
                                    <div class="mt-9 fs-5 fw-normal text-gray-800">
                                        {!! $row->description !!}
                                    </div>
                                    <!--end::Description-->
                                    @if($row->file)
                                        <div class="mt-9 fs-5 fw-normal text-gray-800">
                                            <div class="d-flex align-items-center me-2">
                                                <!--begin::Symbol-->
                                                <div class="symbol symbol-50px me-3">
                                                    <div class="symbol-label bg-light-info">
                                                        <!--begin::Svg Icon | path: icons/duotune/art/art007.svg-->
                                                        <span class="svg-icon svg-icon-1 svg-icon-info">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                      d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM11.7 17.7L16 14C16.4 13.6 16.4 12.9 16 12.5C15.6 12.1 15.4 12.6 15 13L11 16L9 15C8.6 14.6 8.4 14.1 8 14.5C7.6 14.9 8.1 15.6 8.5 16L10.3 17.7C10.5 17.9 10.8 18 11 18C11.2 18 11.5 17.9 11.7 17.7Z"
                                                                      fill="black"/>
                                                                <path
                                                                    d="M10.4343 15.4343L9.25 14.25C8.83579 13.8358 8.16421 13.8358 7.75 14.25C7.33579 14.6642 7.33579 15.3358 7.75 15.75L10.2929 18.2929C10.6834 18.6834 11.3166 18.6834 11.7071 18.2929L16.25 13.75C16.6642 13.3358 16.6642 12.6642 16.25 12.25C15.8358 11.8358 15.1642 11.8358 14.75 12.25L11.5657 15.4343C11.2533 15.7467 10.7467 15.7467 10.4343 15.4343Z"
                                                                    fill="black"/>
                                                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </div>
                                                </div>
                                                <!--end::Symbol-->
                                                <!--begin::Title-->
                                                <div>
                                                    <a href="{{ asset($row->file) }}" target="_blank" class="fs-4 text-dark fw-bolder">Посмотреть файл</a>
                                                </div>
                                                <!--end::Title-->
                                            </div>
                                        </div>
                                    @endif
                                    @if($row->link)
                                        <div class="mt-9 fs-5 fw-normal text-gray-800">
                                            <div class="d-flex align-items-center me-2">
                                                <!--begin::Symbol-->
                                                <div class="symbol symbol-50px me-3">
                                                    <div class="symbol-label bg-light-info">
                                                        <!--begin::Svg Icon | path: icons/duotune/art/art007.svg-->
                                                        <span class="svg-icon svg-icon-1 svg-icon-info">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3" d="M18.4 5.59998C18.7766 5.9772 18.9881 6.48846 18.9881 7.02148C18.9881 7.55451 18.7766 8.06577 18.4 8.44299L14.843 12C14.466 12.377 13.9547 12.5887 13.4215 12.5887C12.8883 12.5887 12.377 12.377 12 12C11.623 11.623 11.4112 11.1117 11.4112 10.5785C11.4112 10.0453 11.623 9.53399 12 9.15698L15.553 5.604C15.9302 5.22741 16.4415 5.01587 16.9745 5.01587C17.5075 5.01587 18.0188 5.22741 18.396 5.604L18.4 5.59998ZM20.528 3.47205C20.0614 3.00535 19.5074 2.63503 18.8977 2.38245C18.288 2.12987 17.6344 1.99988 16.9745 1.99988C16.3145 1.99988 15.661 2.12987 15.0513 2.38245C14.4416 2.63503 13.8876 3.00535 13.421 3.47205L9.86801 7.02502C9.40136 7.49168 9.03118 8.04568 8.77863 8.6554C8.52608 9.26511 8.39609 9.91855 8.39609 10.5785C8.39609 11.2384 8.52608 11.8919 8.77863 12.5016C9.03118 13.1113 9.40136 13.6653 9.86801 14.132C10.3347 14.5986 10.8886 14.9688 11.4984 15.2213C12.1081 15.4739 12.7616 15.6039 13.4215 15.6039C14.0815 15.6039 14.7349 15.4739 15.3446 15.2213C15.9543 14.9688 16.5084 14.5986 16.975 14.132L20.528 10.579C20.9947 10.1124 21.3649 9.55844 21.6175 8.94873C21.8701 8.33902 22.0001 7.68547 22.0001 7.02551C22.0001 6.36555 21.8701 5.71201 21.6175 5.10229C21.3649 4.49258 20.9947 3.93867 20.528 3.47205Z" fill="black"/>
<path d="M14.132 9.86804C13.6421 9.37931 13.0561 8.99749 12.411 8.74695L12 9.15698C11.6234 9.53421 11.4119 10.0455 11.4119 10.5785C11.4119 11.1115 11.6234 11.6228 12 12C12.3766 12.3772 12.5881 12.8885 12.5881 13.4215C12.5881 13.9545 12.3766 14.4658 12 14.843L8.44699 18.396C8.06999 18.773 7.55868 18.9849 7.02551 18.9849C6.49235 18.9849 5.98101 18.773 5.604 18.396C5.227 18.019 5.0152 17.5077 5.0152 16.9745C5.0152 16.4413 5.227 15.93 5.604 15.553L8.74701 12.411C8.28705 11.233 8.28705 9.92498 8.74701 8.74695C8.10159 8.99737 7.5152 9.37919 7.02499 9.86804L3.47198 13.421C2.52954 14.3635 2.00009 15.6417 2.00009 16.9745C2.00009 18.3073 2.52957 19.5855 3.47202 20.528C4.41446 21.4704 5.69269 21.9999 7.02551 21.9999C8.35833 21.9999 9.63656 21.4704 10.579 20.528L14.132 16.975C14.5987 16.5084 14.9689 15.9544 15.2215 15.3447C15.4741 14.735 15.6041 14.0815 15.6041 13.4215C15.6041 12.7615 15.4741 12.108 15.2215 11.4983C14.9689 10.8886 14.5987 10.3347 14.132 9.86804Z" fill="black"/>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </div>
                                                </div>
                                                <!--end::Symbol-->
                                                <!--begin::Title-->
                                                <div>
                                                    <a href="{{ asset($row->link) }}" target="_blank" class="fs-4 text-primary fw-bolder">{{$row->link}}</a>
                                                </div>
                                                <!--end::Title-->
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <!--end::Details-->
                            </div>
                            <!--end::Ticket view-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Layout-->
                </div>
                <!--end::Card body-->
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h3 class="fw-bolder">
                            Задания по теме
                        </h3>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body">
                    @foreach($row->topic->exercises as $exercise)
                        @if(!$loop->first) <div class="separator separator-dashed mb-4"></div> @endif
                    <!--begin::Item-->
                        <div class="d-flex align-items-sm-center @if(!$loop->last) mb-4 @endif">
                            <!--begin::Symbol-->
                            <div class="symbol symbol-50px me-3">
                                <div class="symbol-label bg-light-primary">
                                    <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm010.svg-->
                                    <span class="svg-icon svg-icon-2x svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3"
                                                  d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                                  fill="black"/>
                                            <rect x="7" y="17" width="6" height="2" rx="1" fill="black"/>
                                            <rect x="7" y="12" width="10" height="2" rx="1" fill="black"/>
                                            <rect x="7" y="7" width="6" height="2" rx="1" fill="black"/>
                                            <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Symbol-->
                            <!--begin::Section-->
                            <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                                <div class="flex-grow-1 me-2">
                                    <a href="{{ route('exercises.show', $exercise) }}" class="text-gray-800 text-hover-info fs-6 fw-bolder">
                                        {{ $exercise->name }}
                                    </a>
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Item-->
                    @endforeach
                </div>
                <!--end::Card body-->
            </div>
        </div>
    </div>


@endsection

@section('scripts')

    <script>

        @if(session('exercise-file-success'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Файл успешно загружен!</div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif
        @if(session('exercise-file-failure'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Не удалось загрузить файл!</div>",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif

    </script>@endsection
