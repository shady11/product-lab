@extends('participant.default')

@section('title', $title)

@section('content')
    <div class="row g-5 g-md-8">
        <div class="col-md-4">

            @if(auth()->user()->team_id)

                <div class="card shadow-sm card-md-stretch">
                    <!--begin::Body-->
                    <div class="card-body p-lg-9">
                        <div class="d-flex align-items-center position-relative">
                            <div class="symbol symbol-60px">
                                <div class="symbol-label bg-light-primary border border-2 border-primary overflow-hidden">
                                    @if(auth()->user()->team->image)
                                        <img class="img-fluid" src="{{asset(auth()->user()->team->image)}}" alt="{{auth()->user()->team->name}}">
                                    @else
                                        <span class="svg-icon svg-icon-primary svg-icon-2hx">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M8.70001 6C8.10001 5.7 7.39999 5.40001 6.79999 5.10001C7.79999 4.00001 8.90001 3 10.1 2.2C10.7 2.1 11.4 2 12 2C12.7 2 13.3 2.1 13.9 2.2C12 3.2 10.2 4.5 8.70001 6ZM12 8.39999C13.9 6.59999 16.2 5.30001 18.7 4.60001C18.1 4.00001 17.4 3.6 16.7 3.2C14.4 4.1 12.2 5.40001 10.5 7.10001C11 7.50001 11.5 7.89999 12 8.39999Z" fill="black"/>
                                                <path d="M7 20C7 20.2 7 20.4 7 20.6C6.2 20.1 5.49999 19.6 4.89999 19C4.59999 18 4.00001 17.2 3.20001 16.6C2.80001 15.8 2.49999 15 2.29999 14.1C4.99999 14.7 7 17.1 7 20ZM10.6 9.89995C8.70001 8.09995 6.39999 6.89996 3.79999 6.29996C3.39999 6.89996 2.99999 7.49995 2.79999 8.19995C5.39999 8.59995 7.7 9.79996 9.5 11.6C9.8 10.9 10.2 10.3999 10.6 9.89995ZM2.20001 10.1C2.10001 10.7 2 11.4 2 12C2 12 2 12 2 12.1C4.3 12.4 6.40001 13.7 7.60001 15.6C7.80001 14.8 8.09999 14.0999 8.39999 13.3999C6.89999 11.5999 4.70001 10.4 2.20001 10.1ZM11 20C11 14 15.4 8.99996 21.2 8.09996C20.9 7.39996 20.6 6.79995 20.2 6.19995C13.8 7.49995 9 13.0999 9 19.8999C9 20.3999 9.00001 21 9.10001 21.5C9.80001 21.7 10.5 21.7999 11.2 21.8999C11.1 21.2999 11 20.7 11 20ZM19.1 19C19.4 18 20 17.2 20.8 16.6C21.2 15.8 21.5 15 21.7 14.1C19 14.7 16.9 17.1 16.9 20C16.9 20.2 16.9 20.4 16.9 20.6C17.8 20.2 18.5 19.6 19.1 19ZM15 20C15 15.9 18.1 12.6 22 12.1C22 12.1 22 12.1 22 12C22 11.3 21.9 10.7 21.8 10.1C16.8 10.7 13 14.9 13 20C13 20.7 13.1 21.2999 13.2 21.8999C13.9 21.7999 14.5 21.7 15.2 21.5C15.1 21 15 20.5 15 20Z" fill="black"/>
                                            </svg>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="ms-6">
                                <a href="{{route('team.profile')}}" class="text-gray-900 text-hover-primary fw-boldest lh-1 fs-4 stretched-link">
                                    {{auth()->user()->team->name}}
                                </a>
                            </div>
                        </div>

                        <div class="d-flex align-items-center flex-column my-9 w-100">
                            <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                <span class="fw-boldest fs-6 text-dark">
                                    <span class="text-muted me-2">Участники:</span>
                                    {{auth()->user()->team->participants->count()}} / 10
                                </span>
                                <span class="fw-bolder fs-6 text-gray-400">{{auth()->user()->team->participants->count()/10*100}}%</span>
                            </div>
                            <div class="h-8px mx-3 w-100 bg-light-primary rounded">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{auth()->user()->team->participants->count()/10*100}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>

                        <!--begin::Table container-->
                        <div class="table-responsive scroll h-300px">
                            <!--begin::Table-->
                            <table class="table table-row-dashed align-middle gs-0 gy-4 my-0">
                                <!--begin::Table body-->
                                <tbody>
                                @foreach(auth()->user()->team->participants as $row)
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50px me-3">
                                                    @if($row->image)
                                                        <img src="{{asset($row->image)}}" class="" alt="" />
                                                    @else
                                                        <img src="{{asset('assets/media/svg/avatars/blank.svg')}}" class="" alt="" />
                                                    @endif
                                                </div>
                                                <div class="d-flex justify-content-start flex-column">
                                                    <div class="d-flex text-dark fw-bolder mb-1 fs-6">
                                                        {{$row->getName()}}
                                                        @if(auth()->user()->team->leader_id == $row->id)
                                                            <div class="rating ms-2">
                                                                <div class="rating-label me-1 checked">
                                                                    <i class="bi bi-star-fill fs-6s"></i>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    @if(auth()->user()->team->leader_id == $row->id)
                                                        <span class="text-muted fw-bold text-muted d-block fs-7">Лидер команды</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Body-->
                </div>

            @else

                <div class="card bg-primary card-md-stretch">
                    <!--begin::Body-->
                    <div class="card-body d-flex flex-column justify-content-center py-12">
                        <!--begin::Heading-->
                        <div class="m-0">
                            <!--begin::Title-->
                            <h1 class="fw-normal text-white text-center lh-lg mb-9">
                                <span class="d-block fs-2">Еще нет команды?</span>
                                <span class="d-block fw-boldest">Создай новую или вступай в существующую</span>
                            </h1>
                            <!--end::Title-->
                            <!--begin::Illustration-->
                            <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center card-rounded-bottom h-200px mh-200px my-5 mb-lg-12">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com014.svg-->
                                <span class="svg-icon svg-icon-light svg-icon-fluid"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black"/>
<rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black"/>
<path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black"/>
<rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black"/>
</svg></span>
                                <!--end::Svg Icon-->
                            </div>
                            <!--end::Illustration-->
                        </div>
                        <!--end::Heading-->
                        <!--begin::Links-->
                        <div class="text-center">
                            <!--begin::Link-->
                            <a href="{{route('teams.create')}}" class="btn btn-sm btn-success fw-boldest me-2">Создать команду</a>
                            <!--end::Link-->
                            <!--begin::Link-->
                            <a href="{{route('team.enter')}}" class="btn btn-sm btn-success fw-boldest">Вступить в команду</a>
                            <!--end::Link-->
                        </div>
                        <!--end::Links-->
                    </div>
                    <!--end::Body-->
                </div>

            @endif
        </div>

        <div class="col-md-4">
            <div class="card shadow-sm card-md-stretch-50 mb-md-8">
                <!--begin::Body-->
                <div class="card-body pt-6 d-flex flex-column flex-center">
                    @if($exercises->count() > 0)
                        <div class="mixed-widget-4-chart d-flex flex-center" data-kt-chart-color="success" style="height: 200px"></div>
                        <p class="fw-bold fs-4 text-gray-600 mb-6" style="position: absolute; bottom: 0;">
                            Выполнено {{auth()->user()->exercises->whereNotNull('grade')->count()}} из {{$exercises->count()}} заданий
                        </p>
                    @else
                        <div class="d-flex align-items-center justify-content-center h-100 text-gray-500">
                            Заданий пока нет.
                        </div>
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <div class="card shadow-sm card-md-stretch-50 mb-md-8">
                <!--begin::Body-->
                <div class="card-body pt-6">
                    <!--begin::Carousel-->
                    <div id="kt_security_recent_alerts" class="carousel carousel-custom carousel-stretch slide" data-bs-ride="carousel" data-bs-interval="8000">
                        <!--begin::Heading-->
                        <div class="d-flex flex-stack align-items-center flex-wrap">
                            <h4 class="fw-bold mb-0 pe-2">Предстоящие задания</h4>
                            <!--begin::Carousel Indicators-->
                            <ol class="p-0 m-0 carousel-indicators carousel-indicators-dots">
                                @foreach($last_exercises as $key => $exercise)
                                    <li data-bs-target="#kt_security_recent_alerts" data-bs-slide-to="{{$key}}" class="ms-1 @if($loop->first) active @endif" @if($loop->first) aria-current="true" @endif></li>
                                @endforeach
                            </ol>
                            <!--end::Carousel Indicators-->
                        </div>
                        <!--end::Heading-->
                        @if($exercises->count() > 0)
                            <!--begin::Carousel inner-->
                            <div class="carousel-inner pt-4">
                                @foreach($last_exercises as $exercise)
                                    <!--begin::Item-->
                                    <div class="carousel-item @if($loop->first) active @endif">
                                        <!--begin::Wrapper-->
                                        <div class="carousel-wrapper">
                                            <!--begin::Description-->
                                            <div class="d-flex flex-column flex-grow-1">
                                                <a href="#" class="fs-5 fw-bolder text-primary text-hover-primary">
                                                    {{$exercise->name}}
                                                </a>
                                                <p class="text-gray-600 fs-6 fw-bold pt-3 mb-0">
                                                    {!! Str::of(strip_tags($exercise->description))->limit(80, ' ...') !!}
                                                </p>
                                            </div>
                                            <!--end::Description-->
                                            <!--begin::Summary-->
                                            <div class="d-flex flex-stack pt-4">
                                                <span class="badge badge-light-primary fs-7 fw-bolder me-2">{{$exercise->deadline}}</span>
                                                <a href="{{route('exercises.show', $exercise)}}" class="btn btn-sm btn-primary">Перейти</a>
                                            </div>
                                            <!--end::Summary-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Item-->
                                @endforeach
                            </div>
                            <!--end::Carousel inner-->
                        @else
                            <div class="d-flex align-items-center justify-content-center h-100 text-gray-500">
                                Предстоящих заданий нет.
                            </div>
                        @endif
                    </div>
                    <!--end::Carousel-->
                </div>
                <!--end::Body-->
            </div>
        </div>

        <div class="col-md-4">
            <div class="card bg-primary shadow-sm card-md-stretch-50 mb-md-8">
                <!--begin::Body-->
                <div class="card-body d-flex flex-column justify-content-center">
                    <!--begin::Svg Icon | path: icons/duotune/graphs/gra007.svg-->
                    <span class="svg-icon svg-icon-white svg-icon-5x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M3 2H10C10.6 2 11 2.4 11 3V10C11 10.6 10.6 11 10 11H3C2.4 11 2 10.6 2 10V3C2 2.4 2.4 2 3 2Z" fill="black"/>
                            <path opacity="0.3" d="M14 2H21C21.6 2 22 2.4 22 3V10C22 10.6 21.6 11 21 11H14C13.4 11 13 10.6 13 10V3C13 2.4 13.4 2 14 2Z" fill="black"/>
                            <path opacity="0.3" d="M3 13H10C10.6 13 11 13.4 11 14V21C11 21.6 10.6 22 10 22H3C2.4 22 2 21.6 2 21V14C2 13.4 2.4 13 3 13Z" fill="black"/>
                            <path opacity="0.3" d="M14 13H21C21.6 13 22 13.4 22 14V21C22 21.6 21.6 22 21 22H14C13.4 22 13 21.6 13 21V14C13 13.4 13.4 13 14 13Z" fill="black"/>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <div class="text-white fw-boldest fs-3x mb-2 mt-5">
                        @if(auth()->user()->team && auth()->user()->team->exercises)
                            {{auth()->user()->team->exercises->sum('grade')}}
                        @else
                            0
                        @endif
                    </div>
                    <div class="fw-bold text-white fs-4">Командные UStart коины</div>
                </div>
                <!--end::Body-->
            </div>
            <div class="card bg-primary shadow-sm card-md-stretch-50 mb-md-8">
                <!--begin::Body-->
                <div class="card-body d-flex flex-column justify-content-center">
                    <!--begin::Svg Icon | path: icons/duotune/graphs/gra007.svg-->
                    <span class="svg-icon svg-icon-white svg-icon-5x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 7C10.3 7 9 8.3 9 10C9 11.7 10.3 13 12 13C13.7 13 15 11.7 15 10C15 8.3 13.7 7 12 7Z" fill="black"/>
                            <path d="M12 22C14.6 22 17 21 18.7 19.4C17.9 16.9 15.2 15 12 15C8.8 15 6.09999 16.9 5.29999 19.4C6.99999 21 9.4 22 12 22Z" fill="black"/>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <div class="text-white fw-boldest fs-3x mb-2 mt-5">
                        @if(auth()->user()->exercises)
                            {{auth()->user()->grade + auth()->user()->exercises->sum('grade')}}
                        @else
                            {{auth()->user()->grade}}
                        @endif
                    </div>
                    <div class="fw-bold text-white fs-4">Индивидуальные UStart коины</div>
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
    <div class="row g-5 g-md-8">
        <div class="col-md-4">
            <!--begin::List Widget 5-->
            <div class="card h-md-100 shadow-sm card-md-stretch">
                <!--begin::Header-->
                <div class="card-header align-items-center border-0 mt-4">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="fw-bolder mb-2 text-dark">Лента</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <!--begin::Timeline-->
                    <div class="timeline-label">
                        <!--begin::Item-->
                        <div class="timeline-item">
                            <!--begin::Label-->
                            <div class="timeline-label fw-bolder text-gray-500 fs-7">21.02.22 10:00</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <div class="symbol symbol-30px">
                                    <span class="symbol-label bg-light-primary">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                        <span class="svg-icon svg-icon-primary svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="black"></path>
                                                <rect x="7" y="17" width="6" height="2" rx="1" fill="black"></rect>
                                                <rect x="7" y="12" width="10" height="2" rx="1" fill="black"></rect>
                                                <rect x="7" y="7" width="6" height="2" rx="1" fill="black"></rect>
                                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Content-->
                            <div class="timeline-content d-flex ps-3">
                                <span class="fw-bolder text-gray-500 fs-6">
                                    Добавлено задание:
                                    <span class="text-primary">Название задания</span>
                                </span>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="timeline-item">
                            <!--begin::Label-->
                            <div class="timeline-label fw-bolder text-gray-500 fs-7">21.02.22 09:15</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <div class="symbol symbol-30px">
                                    <span class="symbol-label bg-light-info">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                        <span class="svg-icon svg-icon-info svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path d="M16.0077 19.2901L12.9293 17.5311C12.3487 17.1993 11.6407 17.1796 11.0426 17.4787L6.89443 19.5528C5.56462 20.2177 4 19.2507 4 17.7639V5C4 3.89543 4.89543 3 6 3H17C18.1046 3 19 3.89543 19 5V17.5536C19 19.0893 17.341 20.052 16.0077 19.2901Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Content-->
                            <div class="timeline-content d-flex">
                                <span class="fw-bolder text-gray-500 fs-6 ps-3">
                                    Добавлен материал:
                                    <span class="text-info">Название материала</span>
                                </span>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="timeline-item">
                            <!--begin::Label-->
                            <div class="timeline-label fw-bolder text-gray-500 fs-7">20.02.22 17:20</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <div class="symbol symbol-30px">
                                    <span class="symbol-label bg-light-success">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                        <span class="svg-icon svg-icon-success svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black"/>
                                                <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black"/>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Content-->
                            <div class="timeline-content d-flex">
                                <span class="fw-bolder text-gray-500 fs-6 ps-3">
                                    Участник
                                    <span class="text-success">Testov 3 Test 3</span>
                                    вступил в команду
                                </span>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="timeline-item">
                            <!--begin::Label-->
                            <div class="timeline-label fw-bolder text-gray-500 fs-7">21.02.22 09:15</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <div class="symbol symbol-30px">
                                    <span class="symbol-label bg-light-info">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                        <span class="svg-icon svg-icon-info svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path d="M16.0077 19.2901L12.9293 17.5311C12.3487 17.1993 11.6407 17.1796 11.0426 17.4787L6.89443 19.5528C5.56462 20.2177 4 19.2507 4 17.7639V5C4 3.89543 4.89543 3 6 3H17C18.1046 3 19 3.89543 19 5V17.5536C19 19.0893 17.341 20.052 16.0077 19.2901Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Content-->
                            <div class="timeline-content d-flex">
                                <span class="fw-bolder text-gray-500 fs-6 ps-3">
                                    Добавлен материал:
                                    <span class="text-info">Название материала</span>
                                </span>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="timeline-item">
                            <!--begin::Label-->
                            <div class="timeline-label fw-bolder text-gray-500 fs-7">21.02.22 10:00</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <div class="symbol symbol-30px">
                                    <span class="symbol-label bg-light-primary">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                        <span class="svg-icon svg-icon-primary svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="black"></path>
                                                <rect x="7" y="17" width="6" height="2" rx="1" fill="black"></rect>
                                                <rect x="7" y="12" width="10" height="2" rx="1" fill="black"></rect>
                                                <rect x="7" y="7" width="6" height="2" rx="1" fill="black"></rect>
                                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Content-->
                            <div class="timeline-content d-flex ps-3">
                                <span class="fw-bolder text-gray-500 fs-6">
                                    Добавлено задание:
                                    <span class="text-primary">Название задания</span>
                                </span>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="timeline-item">
                            <!--begin::Label-->
                            <div class="timeline-label fw-bolder text-gray-500 fs-7">20.02.22 17:20</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <div class="symbol symbol-30px">
                                    <span class="symbol-label bg-light-success">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                        <span class="svg-icon svg-icon-success svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black"/>
                                                <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black"/>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Content-->
                            <div class="timeline-content d-flex">
                                <span class="fw-bolder text-gray-500 fs-6 ps-3">
                                    Участник
                                    <span class="text-success">Testov 3 Test 3</span>
                                    вступил в команду
                                </span>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Item-->
                    </div>
                    <!--end::Timeline-->
                </div>
                <!--end: Card Body-->
            </div>
            <!--end: List Widget 5-->
        </div>
        <div class="col-xl-8">
            <!--begin::Card-->
            <div class="card h-md-100 shadow-sm card-md-stretch">
                <!--begin::Header-->
                <div class="card-header align-items-center border-0 mt-4">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="fw-bolder mb-2 text-dark">Предстоящие мероприятия</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    @if($events->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-row-dashed align-middle fs-6 gy-4 dataTable no-footer">
                                <tbody>
                                    @foreach($events as $event)
                                        <tr class="odd">
                                            <td class="min-w-175px">
                                                <div class="position-relative ps-6 pe-3 py-2">
                                                    <div class="position-absolute start-0 top-0 w-4px h-100 rounded-2 @if($event->type) bg-success @else bg-primary @endif"></div>
                                                    <a href="#" class="mb-1 text-dark text-hover-primary fw-bolder">
                                                        {{$event->title}}
                                                    </a>
                                                    <div class="fs-7 text-muted fw-bolder">Created on 24 Dec 21</div>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="badge @if($event->type) badge-light-success @else badge-light-primary @endif">{{$event->getType()}}</span>
                                            </td>
                                            <td class="min-w-125px">
                                                <div class="d-flex align-items-center">
                                                    <span class="svg-icon svg-icon-2 @if($event->type) svg-icon-success @else svg-icon-primary @endif me-3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="black"></path>
                                                            <path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="black"></path>
                                                        </svg>
                                                    </span>
                                                    <div class="mt-1 fs-7 fw-bolder text-muted">
                                                        {{$event->getRegion->name_ru}}<br>
                                                        {{$event->location}}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="min-w-150px">
                                                <div class="d-flex align-items-center">
                                                    <span class="svg-icon svg-icon-2 @if($event->type) svg-icon-success @else svg-icon-primary @endif me-3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black"/>
                                                            <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black"/>
                                                            <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black"/>
                                                        </svg>
                                                    </span>
                                                    <div class="fs-7 fw-bolder text-muted">
                                                        {{$event->getEventDates()}}
                                                        <br>
                                                        {{$event->getEventTimes()}}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-end">
                                                <button type="button" class="btn btn-icon btn-sm btn-light btn-active-primary w-25px h-25px">
                                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr001.svg-->
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path d="M14.4 11H3C2.4 11 2 11.4 2 12C2 12.6 2.4 13 3 13H14.4V11Z" fill="black"></path>
                                                            <path opacity="0.3" d="M14.4 20V4L21.7 11.3C22.1 11.7 22.1 12.3 21.7 12.7L14.4 20Z" fill="black"></path>
                                                        </svg>
                                                    </span>
                                                    <!--end::Svg Icon-->
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="d-flex align-items-center justify-content-center h-100 text-gray-500">
                            Предстоящих мероприятий нет.
                        </div>
                    @endif
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        @if($exercises)
            $(document).ready(function () {
                exercises();
            });

            function exercises() {
                var e = document.querySelectorAll(".mixed-widget-4-chart");
                [].slice.call(e).map((function(e) {
                    var t = parseInt(KTUtil.css(e, "height"));
                    if (e) {
                        var a = e.getAttribute("data-kt-chart-color"),
                            o = KTUtil.getCssVariableValue("--bs-" + a),
                            s = KTUtil.getCssVariableValue("--bs-light-" + a),
                            r = KTUtil.getCssVariableValue("--bs-gray-700");
                        new ApexCharts(e, {
                            series: [{{round(auth()->user()->exercises->whereNotNull('grade')->count() / $exercises->count() * 100, 1)}}],
                            chart: {
                                fontFamily: "inherit",
                                height: t,
                                type: "radialBar"
                            },
                            plotOptions: {
                                radialBar: {
                                    hollow: {
                                        margin: 0,
                                        size: "65%"
                                    },
                                    dataLabels: {
                                        showOn: "always",
                                        name: {
                                            show: !1,
                                            fontWeight: "700"
                                        },
                                        value: {
                                            color: r,
                                            fontSize: "30px",
                                            fontWeight: "700",
                                            offsetY: 12,
                                            show: !0,
                                            formatter: function(e) {
                                                return e + "%"
                                            }
                                        }
                                    },
                                    track: {
                                        background: s,
                                        strokeWidth: "100%"
                                    }
                                }
                            },
                            colors: [o],
                            stroke: {
                                lineCap: "round"
                            },
                            labels: ["Progress"]
                        }).render()
                    }
                }))
            }
        @endif
    </script>

@endsection
