@extends('participant.default')

@section('title', 'Мероприятия')

@section('styles')
    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar4.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar_theme.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h2 class="card-title fw-bolder">Мероприятия</h2>
    </div>
    <div class="card-body">
        <div id="kt_calendar_app"></div>
    </div>
</div>

<div class="modal fade" id="event" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">

        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar4.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar_ru.js') }}"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let calendarEl = document.getElementById('kt_calendar_app');
            let calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                themeSystem: 'bootstrap',
                header: {
                    left: 'prev, next today, printButton',
                    center: 'title',
                    right: 'dayGridMonth, timeGridWeek, listWeek'
                },
                height: 800,
                aspectRatio: 3,
                nowIndicator: true,
                defaultView: 'timeGridWeek',
                displayEventTime: true,
                locale: 'ru',
                timezone: 'Asia/Bishkek',
                timeFormat: 'HH:mm',
                axisFormat: 'hh:mm',
                contentHeight: "auto",
                editable: false,
                eventLimit: 4,
                navLinks: true,
                forceEventDuration: true,
                eventDurationEditable: false,
                events: {
                    url: "{!! route('events.data') !!}",
                    method: 'GET',
                    error: function() {
                        alert('Ошибка!');
                    }
                },
                eventTimeFormat: {
                    hour12: false,
                    hour: '2-digit',
                    minute: '2-digit'
                },
                eventRender: function(info) {
                    let element = $(info.el);
                    element.find('.fc-content').append('<div class="fc-description">'+ info.event.extendedProps.description + '</div>');
                    element.attr('event_id', info.event.id);

                    console.log(info.event);
                    $(info.el).popover({
                        title: info.event.title,
                        trigger : 'hover',
                        container: "body",
                        boundary: "window",
                        placement: "auto",
                        dismiss: !0,
                        html: !0,
                        content: '<div class="fw-bolder mb-2">' + info.event.extendedProps.description + '</div><div class="fs-7"><span class="fw-bold">Время начала:</span> ' + info.event.extendedProps.start_time + '</div><div class="fs-7 mb-4"><span class="fw-bold">Время завершения:</span> ' + info.event.extendedProps.end_time + '</div>'
                    }).popover('show');
                },
                eventClick:  function(info) {
                    let url = 'events/data/view/'+info.event.id;
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        cache: false,
                        type: 'GET',
                        url: url,
                        success: function (data) {
                            $('#event .modal-content').html(data);
                            $('#event').modal('show');
                        },
                    });
                }
            });
            calendar.render();

        });
    </script>

<script src="{{asset('assets/plugins/custom/form-validation/AutoFocus.min.js')}}"></script>"
<script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>"
<script>

    $("#date").flatpickr({
        dateFormat: "Y-m-d",
        defaultDate: "{{date('Y-m-d')}}",
        mode: "range",
        locale: "ru",
    });

    $("#start_time, #end_time").flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        allowInput: true
    });

    const form = document.getElementById('add_event_form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                'title': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'type': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'date': {validators: {notEmpty: {message: 'Обязательное поле'}}}
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap5({
                    rowSelector: '.fv-row',
                    eleInvalidClass: '',
                    eleValidClass: ''
                }),
                autoFocus: new FormValidation.plugins.AutoFocus(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>
@endsection
