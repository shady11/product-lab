<!--begin::Header-->
<div id="kt_header" class="header">
    <!--begin::Header top-->
    <div class="header-top d-flex align-items-stretch flex-grow-1">
        <!--begin::Container-->
        <div class="d-flex container-xxl w-100">
            <!--begin::Wrapper-->
            <div class="d-flex flex-stack align-items-stretch w-100">
                <!--begin::Brand-->
                <div class="d-flex align-items-center align-items-lg-stretch me-5">
                    <!--begin::Heaeder navs toggle-->
                    <button
                        class="d-lg-none btn btn-icon btn-color-white bg-hover-white bg-hover-opacity-10 w-35px h-35px h-md-40px w-md-40px ms-n2 me-2"
                        id="kt_header_navs_toggle">
                        <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none">
                                <path
                                    d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
                                    fill="black"/>
                                <path opacity="0.3"
                                      d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                                      fill="black"/>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </button>
                    <!--end::Heaeder navs toggle-->
                    <!--begin::Logo-->
                    <a href="{{route('participant.index')}}" class="d-flex align-items-center">
                        <img alt="Logo" src="{{asset('assets/logo.png')}}" class="h-25px h-lg-50px"/>
                    </a>
                    <!--end::Logo-->
                    <div class="align-self-end" id="kt_brand_tabs">
                        <!--begin::Header tabs-->
                        <div class="header-tabs overflow-auto mx-4 ms-lg-10 mb-5 mb-lg-0" id="kt_header_tabs"
                             data-kt-swapper="true" data-kt-swapper-mode="prepend"
                             data-kt-swapper-parent="{default: '#kt_header_navs_wrapper', lg: '#kt_brand_tabs'}">
                            <ul class="nav flex-nowrap">
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'participant') ? 'active' : ''}}" href="{{route('participant.index')}}">Главная</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'team') ? 'active' : ''}}" href="{{route('team.profile')}}">Команда</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'topics') || str_contains(Route::currentRouteName(), 'exercises') || str_contains(Route::currentRouteName(), 'materials') ? 'active' : ''}}" href="{{route('topics.index')}}">Обучение</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'events') ? 'active' : ''}}" href="{{route('events.index')}}">Мероприятия</a>
                                </li>
                            </ul>
                        </div>
                        <!--end::Header tabs-->
                    </div>
                </div>
                <!--end::Brand-->
                <!--begin::Topbar-->
                <div class="d-flex align-items-center flex-shrink-0">
                    <!--begin::Chat-->
                    <div class="d-flex align-items-center ms-1">
                        <!--begin::Menu wrapper-->
                        <div
                            class="btn btn-icon btn-color-white bg-hover-white bg-hover-opacity-10 w-35px h-35px h-md-40px w-md-40px position-relative"
                            id="kt_drawer_chat_toggle">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com012.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none">
                                    <path opacity="0.3"
                                          d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z"
                                          fill="black"/>
                                    <rect x="6" y="12" width="7" height="2" rx="1" fill="black"/>
                                    <rect x="6" y="7" width="12" height="2" rx="1" fill="black"/>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Chat-->
                    <!--begin::Quick links-->
                    <div class="d-flex align-items-center ms-1">
                        <!--begin::Menu wrapper-->
                        <div
                            class="btn btn-icon btn-color-white bg-hover-white bg-hover-opacity-10 w-35px h-35px h-md-40px w-md-40px"
                            data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                            data-kt-menu-placement="bottom-end">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none">
                                    <path opacity="0.3"
                                          d="M12 22C13.6569 22 15 20.6569 15 19C15 17.3431 13.6569 16 12 16C10.3431 16 9 17.3431 9 19C9 20.6569 10.3431 22 12 22Z"
                                          fill="black"/>
<path
    d="M19 15V18C19 18.6 18.6 19 18 19H6C5.4 19 5 18.6 5 18V15C6.1 15 7 14.1 7 13V10C7 7.6 8.7 5.6 11 5.1V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V5.1C15.3 5.6 17 7.6 17 10V13C17 14.1 17.9 15 19 15ZM11 10C11 9.4 11.4 9 12 9C12.6 9 13 8.6 13 8C13 7.4 12.6 7 12 7C10.3 7 9 8.3 9 10C9 10.6 9.4 11 10 11C10.6 11 11 10.6 11 10Z"
    fill="black"/>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>

                    <!--begin::User-->
                    <div class="d-flex align-items-center ms-1" id="kt_header_user_menu_toggle">
                        <!--begin::User info-->
                        <div
                            class="btn btn-flex align-items-center bg-hover-white bg-hover-opacity-10 py-2 px-2 px-md-3"
                            data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                            data-kt-menu-placement="bottom-end">
                            <!--begin::Name-->
                            <div
                                class="d-none d-md-flex flex-column align-items-end justify-content-center me-2 me-md-4">
                                <span class="text-white fs-6 fw-bolder lh-1">
                                    @if(auth()->user()->name) {{auth()->user()->getName()}} @else {{auth()->user()->login}} @endif
                                </span>
                            </div>
                            <!--end::Name-->
                            <!--begin::Symbol-->
                            <div class="symbol symbol-30px symbol-md-40px">
                                <img
                                    src="@if(auth()->user()->image) {{asset(auth()->user()->image)}} @else {{asset('assets/media/svg/avatars/blank.svg')}} @endif"
                                    alt="image"/>
                            </div>
                            <!--end::Symbol-->
                        </div>
                        <!--end::User info-->
                        <!--begin::User account menu-->
                        <div
                            class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                            data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <div class="menu-content d-flex align-items-center px-3">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-50px me-5">
                                        <img alt="Logo"
                                             src="@if(auth()->user()->image) {{asset(auth()->user()->image)}} @else {{asset('assets/media/svg/avatars/blank.svg')}} @endif"/>
                                    </div>
                                    <!--end::Avatar-->
                                    <!--begin::Username-->
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder d-flex align-items-center fs-5">
                                            @if(auth()->user()->name) {{auth()->user()->getName()}} @else {{auth()->user()->login}} @endif
                                        </div>
                                        <div class="fw-bold text-muted fs-7">
                                            {{--                                            {{auth()->user()->role}}--}}
                                            Участник
                                        </div>
                                    </div>
                                    <!--end::Username-->
                                </div>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu separator-->
                            <div class="separator my-2"></div>
                            <!--end::Menu separator-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5">
                                <a href="{{route('participant.profile')}}" class="menu-link px-5">Мой профиль</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu separator-->
                            <div class="separator my-2"></div>
                            <!--end::Menu separator-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5" data-kt-menu-trigger="hover"
                                 data-kt-menu-placement="left-start">
                                <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}"
                                   class="menu-link px-5">
                                    <span class="menu-title position-relative">
                                        Язык
                                        <span
                                            class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">
                                            Русский
                                            <img class="w-15px h-15px rounded-1 ms-2"
                                                 src="{{asset('assets/media/flags/russia.svg')}}" alt=""/>
                                        </span>
                                    </span>
                                </a>
                                <!--begin::Menu sub-->
                                <div class="menu-sub menu-sub-dropdown w-175px py-4">
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}"
                                           class="menu-link d-flex px-5 active">
                                            <span class="symbol symbol-20px me-4">
                                                <img class="rounded-1" src="{{asset('assets/media/flags/russia.svg')}}"
                                                     alt=""/>
                                            </span>
                                            Русский
                                        </a>
                                    </div>
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a href="{{ LaravelLocalization::getLocalizedURL('ky', null, [], true) }}"
                                           class="menu-link d-flex px-5">
                                            <span class="symbol symbol-20px me-4">
                                                <img class="rounded-1"
                                                     src="{{asset('assets/media/flags/kyrgyzstan.svg')}}" alt=""/>
                                            </span>
                                            Кыргызча
                                        </a>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu sub-->
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5 my-1">
                                <a href="#" class="menu-link px-5">Настройки</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5">
                                <a href="{{route('participant.logout')}}" class="menu-link px-5">Выйти</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::User account menu-->
                    </div>
                    <!--end::User -->
                </div>
                <!--end::Topbar-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header top-->

    <!--begin::Header navs-->
    <div class="header-navs d-flex align-items-stretch flex-stack h-lg-70px w-100 py-5 py-lg-0" id="kt_header_navs"
         data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}"
         data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
         data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_header_navs_toggle" data-kt-swapper="true"
         data-kt-swapper-mode="append" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header'}">
        <!--begin::Container-->
        <div class="d-lg-flex container-xxl w-100">
            <!--begin::Wrapper-->
            <div class="d-lg-flex flex-column justify-content-lg-center w-100" id="kt_header_navs_wrapper">
                <!--begin::Header tab content-->
                <div class="tab-content" data-kt-scroll="true" data-kt-scroll-activate="{default: true, lg: false}"
                     data-kt-scroll-height="auto" data-kt-scroll-offset="70px">
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'team') ? 'active show' : ''}}"
                         id="kt_header_navs_tab_2">
                        <!--begin::Menu wrapper-->
                        <div class="header-menu flex-column align-items-stretch flex-lg-row">
                            <!--begin::Menu-->
                            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                @if(!auth()->user()->team)
                                    <div
                                        class="menu-item {{str_contains(Route::currentRouteName(), 'teams.create') ? 'here show' : ''}} me-lg-1">
                                        <a href="{{route('teams.create')}}" class="menu-link py-3">
                                            <span class="menu-title">Создать команду</span>
                                        </a>
                                    </div>
                                    <div
                                        class="menu-item {{str_contains(Route::currentRouteName(), 'teams.enter') ? 'here show' : ''}} me-lg-1">
                                        <a href="{{route('team.enter')}}" class="menu-link py-3">
                                            <span class="menu-title">Вступить в команду</span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Tab panel-->
                    <!--begin::Tab panel-->

                    <!--end::Tab panel-->
                </div>
                <!--end::Header tab content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header navs-->
</div>
<!--end::Header-->
