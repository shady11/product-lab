<!DOCTYPE html>

<html lang="en">
<!--begin::Head-->
<head>
    <base href="/">
    <title>Product Lab</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Favicons -->
    <link rel="icon" href="{{asset('favicon.ico')}}">
    <!--begin::Fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/app.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="bg-light">
<!--begin::Main-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root">
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
        <!--begin::Content-->
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
            <!--begin::Logo-->
            <a href="#" class="mb-12">
                <img alt="Logo" src="{{asset('beta/img/logo--dark.png')}}" class="h-80px" />
            </a>
            <!--end::Logo-->

            <div id="kt_content_container" class="container-xxl">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <!--begin:: Apply-->
                        <div class="card mb-12">
                            <!--begin::Body-->
                            <div class="card-body p-lg-12">
                                <div class="d-flex justify-content-end">
                                    <div class="menu-state-bg menu-rounded mb-6">
                                        <!--begin::Menu sub-->
                                        <div class="menu-sub d-flex flex-row justify-content-end w-300px">
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-1">
                                                <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}" class="menu-link d-flex align-items-center fw-bolder text-gray-600 px-5 @if($lang == 'ru') active @endif">
                                            <span class="symbol symbol-20px me-4">
                                                <img class="rounded-1" src="{{asset('assets/media/flags/russia.svg')}}" alt="" />
                                            </span>
                                                    Русский
                                                </a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-1">
                                                <a href="{{ LaravelLocalization::getLocalizedURL('ky', null, [], true) }}" class="menu-link d-flex align-items-centerfw-bolder  text-gray-600 px-5 @if($lang == 'ky') active @endif">
                                            <span class="symbol symbol-20px me-4">
                                                <img class="rounded-1" src="{{asset('assets/media/flags/kyrgyzstan.svg')}}" alt="" />
                                            </span>
                                                    Кыргызча
                                                </a>
                                            </div>
                                            <!--end::Menu item-->
                                        </div>
                                        <!--end::Menu sub-->
                                    </div>
                                </div>

                                <!--begin::Title-->
                                <h4 class="fs-2x text-gray-800 w-boldest mb-12 text-center">
                                    @if(strtolower($lang) == 'ru')
                                        Заявка на участие в Product Lab
                                    @elseif(strtolower($lang) == 'ky')
                                        Product Lab'га катышуу үчүн өтүн
                                    @endif
                                    <br>
                                </h4>
                                <!--end::Title-->

                            @if(strtolower($lang) == 'ru')

                                <!--begin::Text-->
                                    <p class="fs-4 text-gray-700 mb-3">
                                        Привет! Была ли у тебя мечта обучиться новым знаниям ли же запустить свой стартап? Фонд “Тэкайым” дает тебе эту возможность! Мы ищем 500 человек для участия в бесплатной лаборатории стартапов для молодежи от 14 до 24 лет по всему Кыргызстану!
                                        Лаборатория стартапов даст тебе качественное обучение новым навыкам и поможет создать свой проект!
                                    </p>
                                    <p class="fs-4 text-gray-700 mb-3">
                                        Кто сможет принимать участие?
                                        Если тебе от 14 до 24 лет и ты проживаешь в одной из этих локаций: Бишкек, Беловодск, Нарын, Угут, Талас, Кара-Буура, Джалал-Абад, Сузак, Баткен, Кызыл-Кия, Ош, Гулистан, Каракол, Кашкар Кыштак.
                                        Ты сможешь выделять время от 2 до 5 часов онлайн и офлайн 3 раза в неделю на протяжении 8 недель
                                        Есть мотивация обучаться новому и желание реализовывать свои идеи, создавая инновации!
                                    </p>
                                    <p class="fs-4 text-gray-700 mb-3">
                                        На обучение попадут участие 500 человек -  350 девочек\девушек и 150 мальчиков!
                                        мальчики. Все обучение будет проходить на русском и кыргызском языках. Ты будешь обучаться со всеми с нуля, поэтому нечего бояться! Подавай заявку - шансы есть у многих!
                                    </p>
                                    <p class="fs-4 fw-boldest text-gray-900 my-6">
                                        Дедлайн подачи заявок: до 23:59 21 сентября
                                    </p>
                                    <!--end::Text-->

                            @elseif(strtolower($lang) == 'ky')

                                <!--begin::Text-->
                                    <p class="fs-4 text-gray-700 mb-3">
                                        Салам!  Сизде инновациялык нерсе жаратуу же өзүңүздүн стартапыңызды баштоо кыялыңыз бар беле? Идеям ишке ашпай калат же ал үчүн көп акча керек деп өзүңүздөн күмөн санайсызбы?
                                    </p>
                                    <p class="fs-4 text-gray-700 mb-3">
                                        Эгерде бул суроолорго сиз 'Ооба' деп жооп берсеңиз, анда Стартаптар лабораториясына катышуу үчүн биздин өтүнмөнү сөзсүз толтуруңуз.  Бул жерде сиз кыялыңызды жана идеяңызды ишке ашыруу мүмкүнчүлүгүнө ээ боло аласыз!  Эң жакшысы, сиз командаңыз менен өз продуктуңузду ишке киргизип, инновациялык бизнесиңиз үчүн жабдуулар түрүндө 500 долларга чейин  утуп алсаңыз болот!
                                    </p>
                                    <p class="fs-4 text-gray-700 mb-3">
                                        Кимдер катыша алат?
                                        Эгерде сиз 14 жаштан 24 жашка чейин болсоңуз жана төмөндө көрсөтүлгөн жерлердин биринде жашасаңыз: Бишкек, Беловодск, Нарын, Үгүт, Талас, Кара-Буура, Жалал-Абад, Сузак, Баткен, Кызыл-Кыя, Ош, Гулистан, Каракол, Кашкар Кыштак.
                                        8 жума бою, жумасына 2 сааттан жогору онлайн түрүндө жана 3 саатан жогору оффлайн түрүндо  убакыт бөлө алсаңыз.
                                        Жаңы нерселерди үйрөнүүгө мотивацияңыз жана инновацияларды түзүп, идеяларыңызды ишке ашырууга каалооңуз бар болсо.
                                    </p>
                                    <p class="fs-4 text-gray-700 mb-3">
                                        Стартаптар Лабораториясына 500 адам катышат, алардын 70% кыздар жана 30% балдар.  Бардык тренингдер орус жана кыргыз тилдеринде жүргүзүлөт.  Баардыгын нөлдөн баштап үйрөнөсүз, андыктан коркпой эле койсоңуз болот.  Мүмкүнчүлүк баарында бар, андыктан өтүнмөнү толтуруңуздар!
                                    </p>
                                    <p class="fs-4 fw-boldest text-gray-900 my-6">
                                        Өтүнмөнү тапшыруунун акыркы мөөнөтү: 21-сентябрь саат 23:59га чейин
                                    </p>
                                    <!--end::Text-->
                                @endif

                                <div class="separator separator-dashed border-gray-300 my-12"></div>

                                <h3 class="fw-boldest my-9 text-center">
                                    @if(strtolower($lang) == 'ru') Форма заявки:
                                    @elseif(strtolower($lang) == 'ky') Өтүнмө формасы:
                                    @endif
                                </h3>

                                <!--begin::Form-->
                            {!! Form::open(['route' => 'participant.apply.submit', 'method' => 'POST', 'class' => 'form', 'id' => 'participant_apply']) !!}

                            <!--begin::Input group-->
                                <div class="fv-row row mb-9">
                                    <!--begin::Col-->
                                    <div class="col-md-4 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required fs-5 fw-bold mb-2">
                                            @if(strtolower($lang) == 'ru') Ф.И.О:
                                            @elseif(strtolower($lang) == 'ky') Аты-жөнүңүз:
                                            @endif
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Фамилия']) !!}
                                    <!--end::Input-->
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-md-4 fv-row fv-plugins-icon-container">
                                        <!--end::Label-->
                                        <label class="fs-5 fw-bold mb-2">&nbsp;</label>
                                        <!--end::Label-->
                                        <!--end::Input-->
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                                    <!--end::Input-->
                                    </div>
                                    <!--end::Col-->
                                    <div class="col-md-4 fv-row fv-plugins-icon-container">
                                        <!--end::Label-->
                                        <label class="fs-5 fw-bold mb-2">&nbsp;</label>
                                        <!--end::Label-->
                                        <!--end::Input-->
                                    {!! Form::text('patronymic', null, ['class' => 'form-control', 'placeholder' => 'Отчество']) !!}
                                    <!--end::Input-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Дата вашего рождения:
                                        @elseif(strtolower($lang) == 'ky') Туулган күнүңүз:
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                {!! Form::text('birth_date', null, ['class' => 'form-control', 'placeholder' => 'Выберите дату', 'id' => 'datepicker_birth_date']) !!}
                                <!--end::Input-->
                                </div>

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Ваш пол:
                                        @elseif(strtolower($lang) == 'ky') Жынысыңыз:
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--end::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="gender" type="radio" value="0">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Женский
                                                @elseif(strtolower($lang) == 'ky') Аял
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline">
                                            <input class="form-check-input" name="gender" type="radio" value="1">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Мужской
                                                @elseif(strtolower($lang) == 'ky') Эркек
                                                @endif
                                            </span>
                                        </label>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Ваш номер телефона:
                                        @elseif(strtolower($lang) == 'ky') Телефон номериңиз:
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Адрес вашей электронной почты:
                                        @elseif(strtolower($lang) == 'ky') Электрондук почтаңыздын дареги:
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Ваша деятельность:
                                        @elseif(strtolower($lang) == 'ky') Сиздин ишмердүүлүгүңүз:
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Школьник (ца) / Окуучу 9 класса">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Школьник(-ца) 9-класса
                                                @elseif(strtolower($lang) == 'ky') 9-класстын окуучусу
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Школьник(ца) / Окуучу 10 класса">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Школьник(-ца) 10-класса
                                                @elseif(strtolower($lang) == 'ky') 10-класстын окуучусу
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Школьник(ца) / Окуучу 11 класса">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Школьник(-ца) 11-класса
                                                @elseif(strtolower($lang) == 'ky') 11-класстын окуучусу
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Учусь в колледже / Колледжде окуйм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Учусь в колледже
                                                @elseif(strtolower($lang) == 'ky') Колледжде окуйм
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Учусь в университете / Университетте окуйм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Учусь в университете
                                                @elseif(strtolower($lang) == 'ky') Университетте окуйм
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Выпускник(ца) ВУЗа / ЖОЖдун бүтүрүүчүсү">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Выпускник(-ца) ВУЗа
                                                @elseif(strtolower($lang) == 'ky') ЖОЖдун бүтүрүүчүсү
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Не учусь в школе или университете / Университетте же мектепте окубайм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Не учусь в школе или университете
                                                @elseif(strtolower($lang) == 'ky') Университетте же мектепте окубайм
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="activity" type="radio" value="Работаю или подрабатываю / Иштейм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Работаю или подрабатываю
                                                @elseif(strtolower($lang) == 'ky') Иштейм
                                                @endif
                                            </span>
                                        </label>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru')  Область/Город/Село вашего проживания:
                                        @elseif(strtolower($lang) == 'ky') Сиз жашаган аймак област/шаар/айыл:
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        @foreach($regions as $region)
                                            <label class="form-check form-check-inline mb-4">
                                                <input class="form-check-input" name="region_id" type="radio" value="{{$region->id}}">
                                                <span class="fw-bold ps-1 fs-6">@if($lang == 'ru') {{$region->name_ru}} @else {{$region->name}} @endif</span>
                                            </label>
                                        @endforeach
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru')
                                            Укажите ваше социально-экономическое положение, можно указать несколько. Если хотите добавить свой ответ отдельно, можете написать внизу в разделе "Другое":
                                        @elseif(strtolower($lang) == 'ky')
                                            Социалдык-экономикалык абалыңызды көрсөтүңуз, бир нечесин тандасаңыз болот. Эгерде сиз өзүңүздүн жообуңуз менен бөлүшкүңүз келсе, төмөндө "Другое" бөлүмүнө калтырсаңыз болот.
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="У меня многодетная семья (в семье больше 5 детей) / Менин үй-бүлөм көп балалуу (5 баладан көп)">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')
                                                    У меня многодетная семья (в семье больше 5 детей)
                                                @elseif(strtolower($lang) == 'ky')
                                                    Менин үй-бүлөм көп балалуу (5 баладан көп)
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="Хочу учиться новому, но у меня пока нету компьютера / Мен жаңы нерселерди үйрөнгүм келет, бирок менде азырынча компьютер жок">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Хочу учиться новому, но у меня пока нету компьютера
                                                @elseif(strtolower($lang) == 'ky') Мен жаңы нерселерди үйрөнгүм келет, бирок менде азырынча компьютер жок
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="В семье есть только 1 родитель / Үй-бүлөдө бир гана ата-эне ( атам же энем)">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  В семье есть только 1 родитель
                                                @elseif(strtolower($lang) == 'ky') Үй-бүлөдө бир гана ата-эне ( атам же энем)
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="Я живу в интернате/в детском доме / Мен балдар үйүндө жашайм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Я живу в интернате/в детском доме
                                                @elseif(strtolower($lang) == 'ky') Мен балдар үйүндө жашайм
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="Сейчас живу с родственниками / Азыр туугандарым менен жашап жатам">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Сейчас живу с родственниками
                                                @elseif(strtolower($lang) == 'ky') Азыр туугандарым менен жашап жатам
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="По обстоятельствам не хожу в школу/колледж/университет / Турмуш-шартка байланыштуу мектепке/колледжге/университетке барбай элем">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  По обстоятельствам не хожу в школу/колледж/университет
                                                @elseif(strtolower($lang) == 'ky') Турмуш-шартка байланыштуу мектепке/колледжге/университетке барбай элем
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="Мои родители сейчас работают заграницей / Менин ата-энем азыр чет өлкөдо иштеп жатышат">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Мои родители сейчас работают заграницей
                                                @elseif(strtolower($lang) == 'ky') Менин ата-энем азыр чет өлкөдо иштеп жатышат
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="Я хочу обучаться, но пока в моем городе/селе нету таких курсов и центров / Мен окугум келет, бирок азырынча менин шаарымда/айылымда мындай курстар жана борборлор жок">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Я хочу обучаться, но пока в моем городе/селе нету таких курсов и центров
                                                @elseif(strtolower($lang) == 'ky') Мен окугум келет, бирок азырынча менин шаарымда/айылымда мындай курстар жана борборлор жок
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="socio_economic_status[]" type="checkbox" value="Мои родители меня не поддерживают / Ата-энем мени колдобойт">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Мои родители меня не поддерживают
                                                @elseif(strtolower($lang) == 'ky') Ата-энем мени колдобойт
                                                @endif
                                            </span>
                                        </label>
                                        <div class="d-flex align-items-center mb-4">
                                            <label class="form-check form-check-inline">
                                                <input class="form-check-input" name="socio_economic_status_other_checked" type="checkbox" value="1">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Другое:
                                                </span>
                                            </label>
                                            {!! Form::text('socio_economic_status_other', null, ['class' => 'flex-grow-1 form-control form-control-sm form-control-flush border-bottom border-2 border-secondary p-0 fs-6 lh-1', 'autocomplete' => 'off']) !!}
                                        </div>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Укажите к какому типу вы относитесь больше всего? Вы можете выбрать несколько, а если хотите написать свой ответ - то выберите "Другое" и напишите свой ответ туда
                                        @elseif(strtolower($lang) == 'ky') Сиз көбүнчө кайсы типтүүсүз? Сиз бир нечени тандай аласыз, эгерде өз жообуңузду жазгыңыз келсе, анда "Другое" тандап, жообуңузду ошол жерге калтырыңыз.
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="У меня многодетная семья (в семье больше 5 детей) / Менин үй-бүлөм көп балалуу (5 баладан көп)">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Я супер активная и всегда беру лидерство команды на себя
                                                @elseif(strtolower($lang) == 'ky') Мен супер активдүүмүн жана ар дайым команданын лидерлигин колго алам.
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="Хочу учиться новому, но у меня пока нету компьютера / Мен жаңы нерселерди үйрөнгүм келет, бирок менде азырынча компьютер жок">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Я иногда беру на себя роль лидера
                                                @elseif(strtolower($lang) == 'ky') Мен кээде лидерлик ролду аткарам.
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="В семье есть только 1 родитель / Үй-бүлөдө бир гана ата-эне ( атам же энем)">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Я не люблю брать на себя роль лидера, но помогаю команде изо всех сил
                                                @elseif(strtolower($lang) == 'ky') Мен лидердин ролун алгым келбейт, бирок колумдан келишинче командага жардам берем.
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="Я живу в интернате/в детском доме / Мен балдар үйүндө жашайм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')  Я стесняюсь быть лидером
                                                @elseif(strtolower($lang) == 'ky') Мен лидер болгондон уялам
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="Сейчас живу с родственниками / Азыр туугандарым менен жашап жатам">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru')   Я больше люблю помогать команде
                                                @elseif(strtolower($lang) == 'ky') Мага командага жардам берген көбүрөөк жагат
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="По обстоятельствам не хожу в школу/колледж/университет / Турмуш-шартка байланыштуу мектепке/колледжге/университетке барбай элем">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Я хочу запустить свою идею, но пока не верю в себя
                                                @elseif(strtolower($lang) == 'ky') Мен өз идеямды ишке киргизгим келет, бирок азырынча өзүмө ишенбейм
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="type[]" type="checkbox" value="Мои родители сейчас работают заграницей / Менин ата-энем азыр чет өлкөдо иштеп жатышат">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Я не знаю с чего начать чтобы реализовать свою идею
                                                @elseif(strtolower($lang) == 'ky') Оюмду ишке ашыруу үчүн эмнеден баштаарымды билбеймт
                                                @endif
                                            </span>
                                        </label>
                                        <div class="d-flex align-items-center">
                                            <label class="form-check form-check-inline">
                                                <input class="form-check-input" name="type_other_checked" type="checkbox" value="1">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Другое:
                                                </span>
                                            </label>
                                            {!! Form::text('type_other', null, ['class' => 'flex-grow-1 form-control form-control-sm form-control-flush border-bottom border-2 border-secondary p-0 fs-6 lh-1', 'autocomplete' => 'off']) !!}
                                        </div>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <!--begin::Input group-->
                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru')
                                            Ваша цель участия в проекте UpSkill? Можете выбрать несколько и в разделе "другое" написать свой ответ
                                        @elseif(strtolower($lang) == 'ky')
                                            UpSkill долбооруна катышуунун максаты кандай? Сиз бир нечесин тандасаңыз болот, эгерде өз жообуңуз болсо "другое" бөлүмүнө жазыңыз.
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="goal[]" type="checkbox" value="Я хочу научиться работать в команде / Командада иштөөгө үйрөнгүм келет">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Я хочу научиться работать в команде
                                                @elseif(strtolower($lang) == 'ky') Командада иштөөгө үйрөнгүм келет
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу просто попробовать поучаствовать, почему бы и нет? / Мен жөн эле катышып көргүм келип жатат">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Хочу просто попробовать поучаствовать, почему бы и нет?
                                                @elseif(strtolower($lang) == 'ky') Мен жөн эле катышып көргүм келип жатат
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            @if(strtolower($lang) == 'ru')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу разработать свой социальный стартап / Өзүмдүн жеке социалдык стартапымды иштеп чыккым келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Хочу разработать свой социальный стартап
                                                </span>
                                            @elseif(strtolower($lang) == 'ky')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу разработать свой социальный стартап / Өзүмдүн жеке социалдык стартапымды иштеп чыккым келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Өзүмдүн жеке социалдык стартапымды иштеп чыккым келет
                                                </span>
                                            @endif
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            @if(strtolower($lang) == 'ru')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Найти хорошую работу/ Жакшы жумуш табуу үчүн">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Найти хорошую работу
                                                </span>
                                            @elseif(strtolower($lang) == 'ky')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Найти хорошую работу/ Жакшы жумуш табуу үчүн">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Жакшы жумуш табуу үчүн
                                                </span>
                                            @endif
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            @if(strtolower($lang) == 'ru')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу научиться цифровым и гибким навыкам / Санариптик жана жумшак көндүмдөргө үйрөнгүм келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Хочу научиться цифровым и гибким навыкам
                                                </span>
                                            @elseif(strtolower($lang) == 'ky')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу научиться цифровым и гибким навыкам / Санариптик жана жумшак көндүмдөргө үйрөнгүм келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Санариптик жана жумшак көндүмдөргө үйрөнгүм келет
                                                </span>
                                            @endif
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            @if(strtolower($lang) == 'ru')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Начать саморазвиваться, расти и начать разрабатывать свои продукты / Өзумдү өнүктүрүүнү баштап, өз продуктуларымды иштеп чыккым келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Начать саморазвиваться, расти и начать разрабатывать свои продукты
                                                </span>
                                            @elseif(strtolower($lang) == 'ky')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Начать саморазвиваться, расти и начать разрабатывать свои продукты / Өзумдү өнүктүрүүнү баштап, өз продуктуларымды иштеп чыккым келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Өзумдү өнүктүрүүнү баштап, өз продуктуларымды иштеп чыккым келет
                                                </span>
                                            @endif
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            @if(strtolower($lang) == 'ru')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу поменять ситуацию, там где я живу через инновации / Мен жашаган аймактагы кырдаалды инновация аркылуу өзгөрткүм келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Хочу поменять ситуацию, там где я живу через инновации
                                                </span>
                                            @elseif(strtolower($lang) == 'ky')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу поменять ситуацию, там где я живу через инновации / Мен жашаган аймактагы кырдаалды инновация аркылуу өзгөрткүм келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Мен жашаган аймактагы кырдаалды инновация аркылуу өзгөрткүм келет
                                                </span>
                                            @endif
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            @if(strtolower($lang) == 'ru')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу понять, как разрабатывать и внедрять инновации / Инновацияларды кантип иштеп чыгуу жана ишке ашыруу керек экендигин түшүнгүм келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Хочу понять, как разрабатывать и внедрять инновации
                                                </span>
                                            @elseif(strtolower($lang) == 'ky')
                                                <input class="form-check-input" name="goal[]" type="checkbox" value="Хочу понять, как разрабатывать и внедрять инновации / Инновацияларды кантип иштеп чыгуу жана ишке ашыруу керек экендигин түшүнгүм келет">
                                                <span class="fw-bold ps-1 fs-6">
                                                    Инновацияларды кантип иштеп чыгуу жана ишке ашыруу керек экендигин түшүнгүм келет
                                                </span>
                                            @endif
                                        </label>
                                        <div class="d-flex align-items-center">
                                            <label class="form-check form-check-inline">
                                                <input class="form-check-input" name="goal_other_checked" type="checkbox" value="1">
                                                <span class="fw-bold ps-1 fs-6">
                                                        Другое:
                                                    </span>
                                            </label>
                                            {!! Form::text('goal_other', null, ['class' => 'flex-grow-1 form-control form-control-sm form-control-flush border-bottom border-2 border-secondary p-0 fs-6 lh-1', 'autocomplete' => 'off']) !!}
                                        </div>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Участвовали ли вы уже в других проектах?
                                        @elseif(strtolower($lang) == 'ky') Мурун башка проекттерге катыштыңыз беле?
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--end::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="participated" type="radio" value="1">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Да
                                                @elseif(strtolower($lang) == 'ky') Ооба
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline">
                                            <input class="form-check-input" name="participated" type="radio" value="0">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Нет
                                                @elseif(strtolower($lang) == 'ky') Жок
                                                @endif
                                            </span>
                                        </label>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru')
                                            Готовы ли вы на протяжении 8 недель принимать участие в обучении 3 раза в неделю от 1,5 часа - онлайн и от 3-х часов оффлайн?
                                        @elseif(strtolower($lang) == 'ky')
                                            Сиз 8 жума бою жумасына 3 жолу 1,5 сааттан онлайн жана 3 сааттан оффлайн окууга катышууга даярсызбы?
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--end::Input-->
                                    <div class="d-flex flex-column pt-4">
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="ready" type="radio" value="Да / Ооба">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Да
                                                @elseif(strtolower($lang) == 'ky') Ооба
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline mb-4">
                                            <input class="form-check-input" name="ready" type="radio" value="Нет / Жок">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Нет
                                                @elseif(strtolower($lang) == 'ky') Жок
                                                @endif
                                            </span>
                                        </label>
                                        <label class="form-check form-check-inline">
                                            <input class="form-check-input" name="ready" type="radio" value="Не знаю / Билбейм">
                                            <span class="fw-bold ps-1 fs-6">
                                                @if(strtolower($lang) == 'ru') Не знаю
                                                @elseif(strtolower($lang) == 'ky') Билбейм
                                                @endif
                                            </span>
                                        </label>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="separator separator-dashed border-gray-300 my-9"></div>

                                <div class="fv-row mb-9">
                                    <!--begin::Label-->
                                    <label class="required fs-5 fw-bold mb-2">
                                        @if(strtolower($lang) == 'ru') Напишите про вашу мотивацию, почему вы хотите принять участие в Лаборатории стартапов?
                                        @elseif(strtolower($lang) == 'ky') Эмне учун биздин Стартаптар Лабораториясына катышкыныз келип жатат, кыскача баяндама жазыныз?
                                        @endif
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                {!! Form::textarea('motivation', null, ['class' => 'form-control', 'rows' => '3']) !!}
                                <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <div class="d-flex justify-content-end align-items-center mt-12">
                                    <!--begin::Button-->
                                    <button type="reset" class="btn btn-light me-5">
                                        @if(strtolower($lang) == 'ru')  Очистить форму
                                        @elseif(strtolower($lang) == 'ky') Форманы тазалоо
                                        @endif
                                    </button>
                                    <!--end::Button-->
                                    <!--begin::Button-->
                                    <button type="submit" class="btn btn-primary">
                                        @if(strtolower($lang) == 'ru') Отправить
                                        @elseif(strtolower($lang) == 'ky') Жөнөтүү
                                        @endif

                                    </button>
                                    <!--end::Button-->
                                </div>

                            {!! Form::close() !!}
                            <!--end::Form-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end:: Apply-->
                    </div>
                </div>

            </div>

        </div>
        <!--end::Content-->
    </div>
    <!--end::Authentication - Sign-in-->
</div>
<!--end::Root-->
<!--end::Main-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('assets/js/scripts.bundle.js')}}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{{asset('assets/plugins/custom/form-validation/AutoFocus.min.js')}}"></script>
<script>

    $("#datepicker_birth_date").flatpickr({
        dateFormat: "d-m-Y",
    });

    // Define form element
    const form = document.getElementById('participant_apply');

    // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                'lastname': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'name': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'birth_date': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'gender': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'phone': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'email': {validators: {notEmpty: {message: 'Обязательное поле'}, emailAddress: {message: 'Неверный формат эл.адреса',},}},
                'activity': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'region_id': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'socio_economic_status[]': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'type[]': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'participated': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'ready': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'motivation': {validators: {notEmpty: {message: 'Обязательное поле'}}},
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap5({
                    rowSelector: '.fv-row',
                    eleInvalidClass: '',
                    eleValidClass: ''
                }),
                autoFocus: new FormValidation.plugins.AutoFocus(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );

    @if(session('applied'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'><p>Вы успешно подали заявку на участие в проекте!</p><p>В ближайшее время мы с Вами свяжемся.</p></div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Ok!",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
    @endif
</script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
</body>


<!--end::Body-->
</html>
