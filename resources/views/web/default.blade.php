<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('beta/fonts/pequena/stylesheet.css')}}"/>

    <link rel="stylesheet" href="{{asset('beta/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('beta/css/bootstrap-reboot.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('beta/css/bootstrap-grid.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('beta/css/bootstrap-icons.css')}}"/>

    <link rel="stylesheet" type="text/css" href="{{asset('beta/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('beta/slick/slick-theme.css')}}"/>

    <link rel="stylesheet" href="{{asset('beta/css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('beta/css/owl.theme.default.min.css')}}"/>

    <link rel="stylesheet" href="{{asset('beta/css/app.css')}}"/>

    <!-- Favicons -->
    <link rel="icon" href="{{asset('favicon.ico')}}">

    <title>UStart</title>
</head>

<body>
<!-- header -->
<header class="header @yield('header-class')">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header__content">
                    <!-- btn -->
                    <button class="header__btn" type="button" aria-label="header__nav">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <!-- end btn -->

                    <!-- logo -->
                    <a data-scroll href="{{route('web.index')}}" class="header__logo">
                        <img class="header__logo-white" src="{{asset('beta/img/logo.png')}}" alt="">
                        <img class="header__logo-dark" src="{{asset('beta/img/logo--dark.png')}}" alt="">
                    </a>
                    <!-- end logo -->

                    <!-- navigation -->
                    <ul class="header__nav" id="header__nav">
                        <li>
                            <a data-scroll href="#news">
                                @if($lang == 'ru')
                                    Новости
                                @else
                                    Жаңылыктар
                                @endif
                            </a>
                        </li>
                        <li>
                            <a data-scroll href="#about">
                                @if($lang == 'ru')
                                    О проекте
                                @else
                                    Долбоор жөнүндө
                                @endif
                            </a>
                        </li>
                        <li>
                            <a data-scroll href="#timeline">Таймлайн</a>
                        </li>
                        <li>
                            <a data-scroll href="#mentors">
                                @if($lang == 'ru')
                                    Тренеры/Менторы
                                @else
                                    Тренерлер/менторлор
                                @endif
                            </a>
                        </li>
                        <li>
                            <a data-scroll href="#faq">ЧаВо</a>
                        </li>
                        <li>
                            <a data-scroll href="#testimonial">
                                @if($lang == 'ru')
                                    Для родителей
                                @else
                                    Ата-энелер үчүн
                                @endif
                            </a>
                        </li>
                        <li>
                            <a data-scroll href="#contacts">
                                @if($lang == 'ru')
                                    Контакты
                                @else
                                    Байланыштар
                                @endif
                            </a>
                        </li>
                    </ul>
                    <!-- end navigation -->

                    <!-- sign in -->
                    <div class="d-flex">
                        <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}" class="btn btn-lang @if($lang == 'ru') active @endif">
                            <img class="img-fluid" src="{{asset('assets/media/flags/russia.svg')}}" alt="" />
                        </a>
                        <a href="{{ LaravelLocalization::getLocalizedURL('ky', null, [], true) }}" class="btn btn-lang @if($lang == 'ky') active @endif">
                            <img class="img-fluid" src="{{asset('assets/media/flags/kyrgyzstan.svg')}}" alt="" />
                        </a>
                    </div>
                    <div class="d-flex align-items-center">
                        <a data-scroll href="{{route('web.login')}}" class="btn btn-light">
                            <span>
                                @if($lang == 'ru')
                                    Вход
                                @else
                                    Кирүү
                                @endif
                            </span>
                        </a>
                    </div>
                    <!-- end sign in -->
                </div>
            </div>
        </div>
    </div>
</header>
<!-- end header -->

<div id="main-content">

    @yield('content')

    <!-- brand start -->
    <section class="brand-slider">
        <div class="container">
            <div class="owl-carousel" id="brands-owl">
                <div class="brand-item">
                    <div class="brand-content">
                        <a href="https://www.unicef.org/kyrgyzstan/ru" target="_blank">
                            @if($lang == 'ru')
                                <img src="{{asset('beta/img/brand/1.png')}}" alt="" title="" />
                            @else
                                <img src="{{asset('beta/img/brand/1_ky.png')}}" alt="" title="" />
                            @endif
                        </a>
                    </div>
                </div>
                <div class="brand-item">
                    <div class="brand-content">
                        <img src="{{asset('beta/img/brand/3.jpg')}}" alt="" title="" />
                    </div>
                </div>
                <div class="brand-item">
                    <div class="brand-content">
                        <img src="{{asset('beta/img/brand/3.png')}}" alt="" title="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- brand end -->

    <!-- footer start -->
    <div class="footer-wrapper" id="contacts">
        <div class="container">
            <div class="footer-content-wrapper">
                <div class="row">
                    <div class="col-lg-3">
                        <!-- footer logo -->
                        <div class="footer-logo-wrapper">
                            <img src="{{asset('beta/img/logo--dark.png')}}" class="footer-logo" style="height: 80px;" alt="" title="" />
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="footer-contact-us">
                            <div class="footer-link-title" data-target="#footer-address-wrapper" data-toggle="collapse">
                                <div class="footer-title">
                                    @if($lang == 'ru')
                                        Контакты
                                    @else
                                        Контактылар
                                    @endif
                                </div>
                                <span class="footer-toggle-icon-wrapper">
                                 <span class="collapse-icons footer-toggle-icon">
                                    <i class="fa fa-angle-down add"></i>
                                 </span>
                              </span>
                            </div>
                            <div class="footer-details-link collapse" id="footer-address-wrapper">
                                <ul>
                                    <li>
                                        <a href="https://www.google.com/maps/place/High+Technology+Park+of+the+Kyrgyz+Republic/@42.8780078,74.5810091,15z/data=!4m5!3m4!1s0x0:0xa3add42d4faf4a33!8m2!3d42.8780078!4d74.5810091?shorturl=1" target="_blank">
                                            <span>
                                                <i class="bi bi-geo-alt gradient-color"></i>
                                            </span>
                                            <h5 class="footer-link">
                                                @if($lang == 'ru')
                                                    Чуй, 265/1 (Парк Высоких Технологий Кыргызской Республики)
                                                @else
                                                    Дарек: Чуй, 265/1 ( Кыргыз Республикасындагы Жогорку Технологиялар Паркы)
                                                @endif
                                            </h5>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailto:techaim@techaim.org">
                                            <span>
                                                <i class="bi bi-envelope gradient-color"></i>
                                            </span>
                                            <h5 class="footer-link footer-email">techaim@techaim.org</h5>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="footer-about-us">
                            <div class="footer-about-outer">
                                <div class="footer-link-title" data-target="#footer-about-wrapper" data-toggle="collapse">
                                    <div class="footer-title">
                                        @if($lang == 'ru')
                                            О нас
                                        @else
                                            Биз тууралуу
                                        @endif
                                    </div>
                                    <span class="footer-toggle-icon-wrapper">
                                    <span class="collapse-icons footer-toggle-icon">
                                       <i class="fa fa-angle-down add"></i>
                                    </span>
                                 </span>
                                </div>
                                <div class="footer-details-link collapse" id="footer-about-wrapper">
                                    <ul>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">
                                                    @if($lang == 'ru')
                                                        Новости
                                                    @else
                                                        Жанылыктар
                                                    @endif
                                                </h5>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">
                                                    @if($lang == 'ru')
                                                        О проекте
                                                    @else
                                                        Долбоор тууралуу
                                                    @endif
                                                </h5>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">Таймлайн</h5>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">
                                                    @if($lang == 'ru')
                                                        Тренеры/Менторы
                                                    @else
                                                        Тренерлер/Менторлор
                                                    @endif
                                                </h5>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">ЧаВо</h5>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">
                                                    @if($lang == 'ru')
                                                        Для родителей
                                                    @else
                                                        Ата-энелер үчүн
                                                    @endif
                                                </h5>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <h5 class="footer-link footer-arrow">
                                                    @if($lang == 'ru')
                                                        Контакты
                                                    @else
                                                        Контактылар
                                                    @endif
                                                </h5>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="footer-newsletter">
                            <div class="footer-link-title" data-target="#footer-social-wrapper" data-toggle="collapse">
                                <div class="footer-title">
                                    @if($lang == 'ru')
                                        Мы в соцсетях
                                    @else
                                        Биз соцтармактарда
                                    @endif
                                </div>
                                <span class="footer-toggle-icon-wrapper">
                                 <span class="collapse-icons footer-toggle-icon">
                                    <i class="fa fa-angle-down add"></i>
                                 </span>
                              </span>
                            </div>
                            <div class="footer-newsletter-social collapse" id="footer-social-wrapper">
                                <ul class="social-wrapper">
                                    <li>
                                        <a href="https://www.instagram.com/techaim.kg/" target="_blank"><i class="bi bi-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/techaim.kg" target="_blank"><i class="bi bi-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.linkedin.com/company/techaim/" target="_blank"><i class="bi bi-linkedin"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/channel/UCU2EN3DqnNLFSKYqbAsLx_A" target="_blank"><i class="bi bi-youtube"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://vm.tiktok.com/ZSep7FH93/" target="_blank"><i class="bi bi-tiktok"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer end -->

    <!-- copytext start -->
    <div class="footer-wrapper-copytext">
        <div class="container">
            <div class="footer-copyright-text">
                <div class="copyright-link"><span>© Copyright {{$current_year}}. All Rights Reserved Term & Condition /
                        Privacy & Policy</span>
                </div>
                <div class="scroll-to-top">
                    <div id="bottom-to-top" title="Back to top" class="show">
                        <div class="scroll-icon-link gradient-color"><i class="bi bi-chevron-up"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- copytext end -->

</div>

<!-- JS -->
<script type="text/javascript" src="{{asset('beta/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('beta/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('beta/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('beta/js/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('beta/slick/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('beta/js/parallax.min.js')}}"></script>

<script src="{{asset('beta/js/owl.carousel.min.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/goodshare.js@6/goodshare.min.js"></script>

<script src="{{asset('beta/js/main.js')}}"></script>

<script>
    $(document).ready(function(){
        $('.slick-hero').slick({
            dots: false,
            // arrows: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<svg class="arrow-next" x="0px" y="0px" width="37px" height="20px" viewBox="0 0 37 20" enable-background="new 0 0 37 20" xml:space="preserve">  <line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="36.448" y1="10" x2="8.198" y2="10"></line>  <polyline fill="none" stroke="currentColor" stroke-linejoin="bevel" stroke-miterlimit="10" points="9.198,1 0.198,10 9.198,19"></polyline></svg>',
            prevArrow: '<svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="37px" height="20px" viewBox="0 0 37 20" enable-background="new 0 0 37 20" xml:space="preserve">  <line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="36.448" y1="10" x2="8.198" y2="10"></line>  <polyline fill="none" stroke="currentColor" stroke-linejoin="bevel" stroke-miterlimit="10" points="9.198,1 0.198,10 9.198,19"></polyline></svg>'
        });

        if ($(window).width() > 1200) {
            if ($('#hero__dates').length) {
                var scene1 = document.getElementById('hero__dates');
                var parallaxInstance1 = new Parallax(scene1);
            }
        }

        $('.slick-mentors').slick({
            // dots: true,
            // arrows: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<svg class="arrow-next" x="0px" y="0px" width="37px" height="20px" viewBox="0 0 37 20" enable-background="new 0 0 37 20" xml:space="preserve">  <line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="36.448" y1="10" x2="8.198" y2="10"></line>  <polyline fill="none" stroke="currentColor" stroke-linejoin="bevel" stroke-miterlimit="10" points="9.198,1 0.198,10 9.198,19"></polyline></svg>',
            prevArrow: '<svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="37px" height="20px" viewBox="0 0 37 20" enable-background="new 0 0 37 20" xml:space="preserve">  <line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="36.448" y1="10" x2="8.198" y2="10"></line>  <polyline fill="none" stroke="currentColor" stroke-linejoin="bevel" stroke-miterlimit="10" points="9.198,1 0.198,10 9.198,19"></polyline></svg>',
            responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToScroll: 1,
					slidesToShow: 1,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToScroll: 1,
					slidesToShow: 1,
				}
			}
		]
        });

        $('#testimonial-owl').owlCarousel({
            nav: true,
            dots: true,
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $('#brands-owl').owlCarousel({
            nav: false,
            dots: false,
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });

        /****************************scroll-to-top************************* */
        if ($('#bottom-to-top').length) {
            var scrollTrigger = 100, // px
                bottomToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#bottom-to-top').addClass('show');
                    } else {
                        $('#bottom-to-top').removeClass('show');
                    }
                };
            bottomToTop();
            $(window).on('scroll', function () {
                bottomToTop();
            });
            $('#bottom-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 100);
            });
        }
    });
</script>
</body>
</html>
