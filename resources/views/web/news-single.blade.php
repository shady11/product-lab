@extends('web.default')

@section('header-class', 'header--news')

@section('content')

    <!-- blog start -->
    <section class="blog-wrapper" id="news">
        <div class="container">
            <div class="main-title-wrapper mb-0 d-flex align-items-center">
                <h4 class="sitemain-subtitle">
                    @if($lang == 'ru') Новости
                    @else Жаңылыктар
                    @endif
                </h4>
            </div>
            <div class="row">
                <div class="blog-post col-md-12">
                    <div class="blog-content">
                        <div class="blog-first-block">
                            <img src="{{asset($news->img)}}" alt="{{$news->img}}" title="{{$news->img}}" />
                        </div>
                        <div class="blog-second-block">
                            <div class="blog-right-content pl-0">
                                <div class="blog-date">{{$news->getPublishedDate($lang)}}</div>
                                <p class="blog-dec">
                                    {{$news->getTitle($lang)}}
                                </p>

                                <article class="mt-5">
                                  {!! $news->getContent($lang) !!}
                                </article>

                                <div class="blog-footer mt-5">
                                    <ul class="share">
                                        <li class="share-link">
                                            <a href="#" data-social="facebook" class="fb">
                                                <svg class="icon" viewBox="0 0 64 64">
                                                    <g id="Layer_2">
                                                        <path class="st0" d="M20.1,36h3.4c0.3,0,0.6,0.3,0.6,0.6V58c0,1.1,0.9,2,2,2h7.8c1.1,0,2-0.9,2-2V36.6c0-0.3,0.3-0.6,0.6-0.6h5.6
		c1,0,1.9-0.7,2-1.7l1.3-7.8c0.2-1.2-0.8-2.4-2-2.4h-6.6c-0.5,0-0.9-0.4-0.9-0.9v-5c0-1.3,0.7-2,2-2h5.9c1.1,0,2-0.9,2-2v-8
		c0-1.1-0.9-2-2-2h-7.1c-13,0-12.7,10.5-12.7,12v7.3c0,0.3-0.3,0.6-0.6,0.6H20c-1.1,0-2,0.9-2,2v7.8C18.1,35.1,19,36,20.1,36z"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                        <li class="share-link">
                                            <a href="#" data-social="twitter" class="tw">
                                                <svg class="icon" viewBox="0 0 64 64">
                                                    <path d="M63,11.6c-1.4,0.6-2.9,1.1-4.4,1.5c1.7-1.6,2.9-3.6,3.6-5.8l0,0c0.2-0.6-0.5-1.2-1.1-0.8l0,0c-2.1,1.2-4.4,2.2-6.7,2.8
	c-0.1,0-0.3,0.1-0.4,0.1c-0.4,0-0.9-0.2-1.2-0.5c-2.5-2.2-5.8-3.4-9.1-3.4c-1.5,0-2.9,0.2-4.4,0.7c-4.5,1.4-7.9,5.1-9,9.6
	c-0.4,1.7-0.5,3.4-0.3,5.1c0,0.2-0.1,0.3-0.1,0.4c-0.1,0.1-0.2,0.2-0.4,0.2c0,0,0,0,0,0C19.6,20.4,10.8,15.7,4.5,8l0,0
	C4.2,7.7,3.6,7.7,3.4,8.1l0,0c-1.2,2.1-1.9,4.5-1.9,6.9c0,3.7,1.5,7.3,4.1,9.8c-1.1-0.3-2.2-0.7-3.1-1.2l0,0
	c-0.5-0.3-1.1,0.1-1.1,0.6l0,0C1.3,29.9,4.6,34.8,9.4,37c-0.1,0-0.2,0-0.3,0c-0.8,0-1.6-0.1-2.3-0.2l0,0c-0.5-0.1-1,0.4-0.8,0.9l0,0
	c1.6,4.9,5.8,8.6,10.9,9.4c-4.2,2.8-9.1,4.3-14.3,4.3l-1.6,0c-0.5,0-0.9,0.3-1,0.8c-0.1,0.5,0.1,1,0.5,1.2c5.8,3.4,12.3,5.1,19,5.1
	c5.8,0,11.3-1.2,16.3-3.4c4.5-2.1,8.5-5.1,11.9-8.9c3.1-3.6,5.6-7.7,7.3-12.2c1.6-4.3,2.5-8.9,2.5-13.4v-0.2c0-0.7,0.3-1.4,0.9-1.8
	c2.1-1.7,4-3.7,5.5-6l0,0C64.3,12,63.6,11.3,63,11.6L63,11.6z"></path>
                                                </svg>                                </a>
                                        </li>
                                        <li class="share-link">
                                            <a href="#" data-social="linkedin" class="vk">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <circle cx="4.983" cy="5.009" r="2.188"></circle>
                                                    <path d="M9.237 8.855v12.139h3.769v-6.003c0-1.584.298-3.118 2.262-3.118 1.937 0 1.961 1.811 1.961 3.218v5.904H21v-6.657c0-3.27-.704-5.783-4.526-5.783-1.835 0-3.065 1.007-3.568 1.96h-.051v-1.66H9.237zm-6.142 0H6.87v12.139H3.095z"></path>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="d-flex mt-5">
                                        <a href="{{route('web.news')}}" class="btn btn-primary">
                                            <span>
                                                @if($lang == 'ru') Все новости
                                                @else Бардык Жаңылыктар
                                                @endif
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog end -->

@endsection
