@extends('web.default')

@section('header-class', 'position-fixed')

@section('content')
    <!-- banner start -->
    <section class="banner-wrapper" id="home">
        <div class="banner-outer">
            <div class="slider-animation-images">
                <span class="image1"><img src="{{asset('beta/img/banner/animation/1.png')}}" alt="" title=""></span>
                <span class="image2"><img src="{{asset('beta/img/banner/animation/2.png')}}" alt="" title=""></span>
                <span class="image3"><img src="{{asset('beta/img/banner/animation/3.png')}}" alt="" title=""></span>
                <span class="image4"><img src="{{asset('beta/img/banner/animation/4.png')}}" alt="" title=""></span>
                <span class="image5"><img src="{{asset('beta/img/banner/animation/5.png')}}" alt="" title=""></span>
                <span class="image6"><img src="{{asset('beta/img/banner/animation/6.png')}}" alt="" title=""></span>
                <span class="image7"><img src="{{asset('beta/img/banner/animation/7.png')}}" alt="" title=""></span>
                <span class="image8"><img src="{{asset('beta/img/banner/animation/8.png')}}" alt="" title=""></span>
                <span class="image9"><img src="{{asset('beta/img/banner/animation/9.png')}}" alt="" title=""></span>
            </div>
            <div class="container">
                <div class="slick-hero">

                    <div class="slick-hero__slide">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="banner-content">
                                    <h4 class="banner-main-title">
                                        @if($lang == 'ru')
                                            Молодежный конкурс социальных инноваций <br>“UStart”
                                        @else
                                            “UStart” жаштардын социалдык инновацияларынын конкурсу
                                        @endif

                                    </h4>
                                    <p class="banner-dec">
                                        @if($lang == 'ru')
                                            Очень хочешь создать свой социальный стартап?
                                            <br>
                                            Сможешь уделять от 3 до 6 часов в неделю на обучение?
                                            <br>
                                            Тогда наша лаборатория стартапов ждет твоей заявки!
                                        @else
                                            Сиз чындап эле өзүңүздүн социалдык стартабыңызды түзгүңүз келеби?
                                            <br>
                                            Сиз жумасына 3-6 саатты окууга арнай аласызбы?
                                            <br>
                                            Анда биздин стартап лаборатория сиздин анкетаңызды күтөт!
                                        @endif
                                    </p>
                                    <div class="banner-btn-wrapper d-flex align-items-center">
                                        <a href="{{route('participant.apply')}}" target="_blank" class="btn btn-secondary mr-3">
                                            <span class="gradient-color title">
                                                @if($lang == 'ru')
                                                    ПОДАТЬ ЗАЯВКУ
                                                @else
                                                    Анкета жөнөтүү
                                                @endif
                                            </span>
                                            <span class="btn-icon gradient-color1">
                                                <i class="flaticon-right-arrow gradient-color"></i>
                                             </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="banner-image">
                                    <img src="{{asset('beta/img/banner/bannerimg.png')}}" class="banner-img img-fluid" alt="" title="">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- banner end -->

    <!-- service start -->
    <section class="service-wrapper pb-0" id="about">
        <div class="container">
            <div class="service-content-1 main-title-wrapper">
                <h4 class="sitemain-subtitle">
                    @if($lang == 'ru')
                        О проекте
                    @else
                        Долбоор тууралуу
                    @endif
                </h4>
                <p class="service-dec">
                    @if($lang == 'ru')
                        Республиканский конкурс по всему Кыргызстану, который призывает молодых людей от 14 до 24 лет предложить идеи и создать социальные инновации и решения в поддержку основных направлений Целей Устойчивого Развития.
                    @else
                        14 жаштан 24 жашка чейинки жаштарды Туруктуу Ѳнүгүү Максаттарынын негизги агымын колдоо үчүн идеяларды жана социалдык инновацияларды жана чечимдерди түзүүгө үндөгөн Кыргызстан боюнча республикалык сынак.
                    @endif
                </p>
                <p class="service-dec">
                    @if($lang == 'ru')
                        В рамках конкурса участники пройдут отбор и затем обучение в формате онлайн и офлайн по созданию цифровых и технологичных решений для социальных и инженерных инноваций, в частности запуска своего социального стартапа по подходам Upshift, лучшие молодежные стартапы дошедшие до конца и выполнившие все условия получат шанс начального финансирование в виде оборудования (equipment seed-funding) и 1 месяц менторства по реализации проекта.
                    @else
                        Сынактын алкагында катышуучулар тандалып алынат. Андан соң, социалдык жана инженердик инновациялар үчүн санариптик жана технологиялык чечимдерди түзүү үчүн, атап айтканда, Upshift ыкмаларын колдонуу менен социалдык стартапты ишке киргизүү боюнча онлайн жана оффлайн режиминде сабактар окутулат. Аягына жеткен жана бардык шарттарды аткарган эң мыкты жаштар стартаптары жабдуулар түрүндө баштапкы каржылоого (equipment seed-funding) жана долбоорду ишке ашыруу боюнча 1 айлык менторлукка мүмкүнчүлүк алышат.
                    @endif
                </p>
                {{--                <div class="section-actions d-flex">--}}
                {{--                    <a href="#" class="btn btn-primary">--}}
                {{--                        <span>Подробнее</span>--}}
                {{--                    </a>--}}
                {{--                </div>--}}
            </div>
            <div class="features-list row">
                <div class="col-md-6">
                    <div class="features features--first">
                        <div class="features__inner"></div>
                        <h3 class="features__title">
                            @if($lang == 'ru')
                                Цель конкурса
                            @else
                                Сынактын максаты
                            @endif
                        </h3>
                        <p class="features__text">
                            @if($lang == 'ru')
                                Развитие навыков трудоустройства и предпринимательства в социальных инновациях, а также повышение знаний и навыков 21 века - цифровые навыки, коммуникация, командная работа, креативность, умение решать проблемы, персональное лидерство и навыки предпринимательства. Также проект предполагает создание движения молодежи с активной гражданской позицией по внедрению социальных инноваций в своих сообществах.
                            @else
                                Социалдык инновацияларды түзүү аркылуу ишке орноштуруу жана ишкердүүлүк көндүмдөрүн өнүктүрүү, ошондой эле 21-кылымдагы  билим жана көндүмдөрдү жогорулатуу – санариптик көндүмдөр, коммуникация, командада иштөө, креативдүүлүк , көйгөйлөрдү чече билүү, жеке лидерлик жана ишкердик көндүмдөрү. Долбоор ошондой эле өз жамааттарына социалдык инновацияларды киргизүү үчүн активдүү жарандык позициясы бар жаштар кыймылын түзүүнү көздөйт.
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="features features--second">
                        <h3 class="features__title">
                            @if($lang == 'ru')
                                Миссия
                            @else
                                Миссиясы
                            @endif
                        </h3>
                        <p class="features__text">
                            @if($lang == 'ru')
                                Обеспечить уязвимой молодежи от 14 до 24 лет экологичную среду для обучения, развития навыков 21 века и содействие реализации молодежных и женских инновационных стартапов на базе ЦУР.
                            @else
                                14 жаштан 24 жашка чейинки аярлуу жаштарды билим алуу, 21-кылымдын көндүмдөрүн өнүктүрүү жана ТӨМ негизинде жаштардын жана аялдардын инновациялык стартаптарын ишке ашырууга көмөктөшүү үчүн экологиялык чөйрө менен камсыз кылуу.
                            @endif
                        </p>
                    </div>
                </div>
            </div>
            <div class="features-list row">
                <div class="col-md-6">
                    <div class="features features--third">
                        <h3 class="features__title">
                            @if($lang == 'ru')
                                Кто может принять участие?
                            @else
                                Кимдер катыша алат?
                            @endif
                        </h3>
                        <p class="features__text">
                            @if($lang == 'ru')
                                Школьники старших классов, выпускники колледжей со средним высшим образованием; студенты и выпускники университетов в возрасте от 14 до 24 лет со всех регионов Кыргызстана.
                                <br>
                                Приоритет будет отдан участницам из сельской местности и регионов, из семей мигрантов, многодетных семей, либо необеспеченных, живущих на обеспечении 1-го родителя, либо под попечительством опекунов или государства, а также лиц с инвалидностью.
                            @else
                                Мектептин жогорку классынын  окуучулары, орто жогорку билими бар колледждердин бүтүрүүчүлөрү; Кыргызстандын бардык аймактарынан келген 14 жаштан 24 жашка чейинки жогорку окуу жайлардын студенттери жана бүтүрүүчүлөрү.
                                Катышуучуларга  айыл жеринен жана аймактардан келген, мигранттардын үй-бүлөлөрүнөн, көп балалуу үй-бүлөлөрдөн же жакыр жашаган, ата-энесинин биринин багуусунда же камкорчуларынын же мамлекеттин камкордугунда жашаган жарандарга, ошондой эле ден соолугунун мүмкүнчүлүктөрү чектелүү адамдарга артыкчылык берилет.
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="features features--fourth">
                        <h3 class="features__title">
                            @if($lang == 'ru')
                                Кого и что мы ищем?
                            @else
                                Кимди жана эмнени издеп жатабыз?
                            @endif
                        </h3>
                        <p class="features__text">
                            @if($lang == 'ru')
                                500 участников, готовых пройти обучение в лаборатории стартапов путем смешанного обучения в формате онлайн и офлайн по методике UpSHIFT (Дизайн-мышление).
                                <br>
                                Молодых инноваторов, которые готовы от проблемы и идеи найти и создать прототипы решений и представить их на итоговом питчинге.
                                <br>
                                Команды, готовые воплощать и реализовывать решения при поддержке менторов и посевного финансирования.
                            @else
                                UpSHIFT (Дизайн ой жүгүртүү) методологиясы боюнча онлайн жана оффлайн аралаш окутуу  жолуу аркылуу Startup лабораториясында окууга даяр 500 катышуучуну.
                                <br>
                                Көйгөйлөрдүн жана идеялардын негизинде чыккан чечимдердин прототиптерин табууга, түзүүгө жана аларды жыйынтыктоочу  питчингде көрсөтүүгө даяр жаш инноваторлорду.
                                <br>
                                Менторлордун жардамы астында жана каржылоонун колдоосу менен чечимдерди ишке ашыра турган жана командаларда иштөөгө даяр катышуучуларды.
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service end -->

    <!-- timeline start -->
    <section class="timeline-wrapper pb-0" id="timeline">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 service-block-content">
                    <div class="service-content-1 main-title-wrapper">
                        <h4 class="sitemain-subtitle">Таймлайн</h4>
                    </div>
                    <div class="seofy_module_time_line_vertical appear_anim">
                        <div class="time_line-item item_show">
                            <div class="time_line-date_wrap">
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="seofy_hexagon">
                                    <svg style="filter: drop-shadow(4px 5px 4px rgba(226,0,122,0.3));fill: #e2007a;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="time_line-date">
                                    <img src="{{asset('beta/img/timeline/apply-bg.png')}}" alt="">
                                    <span>
                                        @if($lang == 'ru')
                                            Подача заявки
                                        @else
                                            Өтүнмө берүү
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="time_line-content">
                                <h5 class="time_line-title">
                                    @if($lang == 'ru')
                                        Мечтал создать что-то инновационное или же запустить собственный стартап?
                                    @else
                                        Инновациялык нерсени жаратууну же жеке стартабыңызды ачууну кыялдандыңыз беле?
                                    @endif
                                </h5>
                                <div class="time_line-descr">
                                    @if($lang == 'ru')
                                        Но сомневаешься в себе и переживаешь, что не справишься и для запуска нужно много денег?
                                    @else
                                        Бирок сиз өзүңүздөн күмөн санап, туруштук бере албайм жана баштоо үчүн көп акча керек деп кооптонуп жатасызбы?
                                    @endif
                                    @if($lang == 'ru')
                                        Ответил на первые два вопроса - да, то заполняй нашу заявку для участия в Лаборатории стартапов UStart!
                                    @else
                                        Алгачкы эки суроого жооп бердинизби - ооба, анда UStart Startup лабораториясына катышуу үчүн биздин анкетабызды толтуруңуз!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="time_line-item item_show">
                            <div class="time_line-date_wrap">
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="time_line-date">
                                    <img src="{{asset('beta/img/timeline/deadline-bg.png')}}" alt="">
                                    <span>
                                        @if($lang == 'ru')
                                            Крайний срок
                                        @else
                                            Тапшыруу мөөнөтү
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="time_line-content">
                                <h5 class="time_line-title">
                                    @if($lang == 'ru')
                                        Дата: 15 сентября
                                    @else
                                        Дата: 15-сентябрь
                                    @endif
                                </h5>
                                <div class="time_line-descr">
                                    @if($lang == 'ru')
                                        Рассмотрев все заявки, мы отберем 500 участников для обучения.
                                    @else
                                        Бардык арыздарды карап чыккандан кийин биз окутууга 500 катышуучуну тандап алабыз.
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="time_line-item item_show">
                            <div class="time_line-date_wrap">
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="time_line-date">
                                    <img src="{{asset('beta/img/timeline/study-bg.png')}}" alt="">
                                    <span>
                                        @if($lang == 'ru')
                                            Обучение
                                        @else
                                            Окутуу
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="time_line-content">
                                <h5 class="time_line-title">
                                    @if($lang == 'ru')
                                        Обучение в Лаборатории стартапов
                                    @else
                                        Стартаптар лабораториясында окутуу
                                    @endif
                                </h5>
                                <div class="time_line-descr">
                                    @if($lang == 'ru')
                                        Самые крутые тренеры и эксперты в области создания стартапов в Кыргызстане помогут тебе научиться креативно мыслить, находить и понимать проблемы, а также их причины и, конечно же, работать в команде, реализовывая свою мечту в реальность!
                                    @else
                                        Кыргызстандагы стартаптарды түзүү чөйрөсүндөгү эң укмуш тренерлер жана эксперттер сизге креативдүү ой жүгүртүүгө, көйгөйлөрдү, ошондой эле алардын себептерин табууга жана түшүнүүгө, жана, албетте, командада иштөөгө, кыялыңызды ишке ашырууга үйрөнүүгө жардам берет!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="time_line-item item_show">
                            <div class="time_line-date_wrap">
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="time_line-date">
                                    <img src="{{asset('beta/img/timeline/pitching-bg.png')}}" alt="">
                                    <span>Питчинг</span>
                                </div>
                            </div>
                            <div class="time_line-content">
                                <h5 class="time_line-title">
                                    Питчинг
                                </h5>
                                <div class="time_line-descr">
                                    @if($lang == 'ru')
                                        Лучшие команды выступят на питчинге, где смогут выиграть финансирование на 500$ в виде оборудования.
                                    @else
                                        Мыкты командалар питчинг сессиясына катышат, анда алар жабдуулар түрүндө 500$ утуп алышат.
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="time_line-item item_show">
                            <div class="time_line-date_wrap">
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="seofy_hexagon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4">
                                        <path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path>
                                    </svg>
                                </div>
                                <div class="time_line-date">
                                    <img src="{{asset('beta/img/timeline/mentor-bg.png')}}" alt="">
                                    <span>
                                        @if($lang == 'ru')
                                            Менторство
                                        @else
                                            Насаатчылык
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="time_line-content">
                                <h5 class="time_line-title">
                                    @if($lang == 'ru')
                                        Менторство и реализация
                                    @else
                                        Ментордук программа жана ишке ашыруу
                                    @endif
                                </h5>
                                <div class="time_line-descr">
                                    @if($lang == 'ru')
                                        Победившие команды пройдут через менторство наших крутых наставников длиною в 1 месяц.
                                    @else
                                        Жеңүүчү командалар 1 ай бою биздин  укмуш насаатчылардын менторлугунан  өтүшөт.
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- timeline end -->

    <!-- stats start -->
    <section class="stats-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="service-content-1 main-title-wrapper">
                        <h4 class="sitemain-subtitle">
                            @if($lang == 'ru')
                                Цифры
                            @else
                                Цифралар
                            @endif
                        </h4>
                    </div>
                    <div class="client-list">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-person"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">512 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Участников
                                            @else
                                                катышуучу
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-people"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">20 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Команд на питчинге
                                            @else
                                                команда питчингде
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-diagram-3"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">14 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Стартапов команд-победителей
                                            @else
                                                женүүчү командалардын стартаптары
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-mortarboard"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">20 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Менторов
                                            @else
                                                менторлор
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-calendar4-week"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">10 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Недель обучения
                                            @else
                                                жума окутуу
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-megaphone"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">11 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Тренеров
                                            @else
                                                тренерлер
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="client-item d-flex align-items-center">
                                    <div class="client-icon">
                                        <i class="bi bi-layers"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="client-item-title">15 </div>
                                        <div class="client-item-descr slide-descr">
                                            @if($lang == 'ru')
                                                Ролевых моделей
                                            @else
                                                ролдук моделдер
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="clients-photo">
                        <div class="clients-photo-item clients-photo-item1"><div class="inside"><img src="{{asset('beta/img/people/1.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item2"><div class="inside"><img src="{{asset('beta/img/people/8.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item3"><div class="inside"><img src="{{asset('beta/img/people/7.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item4"><div class="inside"><img src="{{asset('beta/img/people/4.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item5"><div class="inside"><img src="{{asset('beta/img/people/5.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item6"><div class="inside"><img src="{{asset('beta/img/people/6.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item7"><div class="inside"><img src="{{asset('beta/img/people/3.png')}}" alt=""></div></div>
                        <div class="clients-photo-item clients-photo-item8"><div class="inside"><img src="{{asset('beta/img/people/2.png')}}" alt=""></div></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- stats end -->

    <!-- mentors start -->
    <section class="mentors-wrapper" id="mentors">
        <div class="slick-mentors">
            <div class="mentor-item text-white" style="background-image: url({{asset('beta/img/mentors/2.png')}});">
                <div class="slide-container">
                    <div class="container">
                        <div class="">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="slide-title-sub slide-title-sub-md">
                                        @if($lang == 'ru')
                                            Тренера/Менторы
                                        @else
                                            Тренерлер/Менторлор
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <h3 class="slide-title text-white">
                                        Досмамбетова Бермет
                                    </h3>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="slide-descr slide-descr-projects">
                                                @if($lang == 'ru')
                                                    Тренер по UpSHIFT на русском языке.
                                                    <br>
                                                    Customer Success Manager в британском стартапе Hypha, ex-Product Owner в компании Beeline.
                                                    <br>
                                                    C 2018 года работает в сфере IT в качестве продакт/проджект менеджера, предыдущий опыт работы в маркетинге и телекоммуникациях. С 2021 года начала вести собственный курс “Войти в IT”, рассказывающий об основах сферы технологий для новичков.
                                                @else
                                                    UpSHIFT боюнча орус тилиндеги тренер.
                                                    <br>
                                                    Hypha, ex-Product Owner британдык стартабында жана Beeline да Customer Success Manager болуп иштеген.
                                                    <br>
                                                    2018-жылдан баштап IT тармагында продакт/проджект менеджер болуп иштеген. Ошондой эле маркетинг жана телекоммуникация тармакатрында иштөө тажрыйбасы бар. 2021- жылдан тарта жаңы баштап жаткандар үчүн технология сферасынын негиздери тууралуу “Войти в IT” деген курсту алып барат.
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mentor-item text-white" style="background-image: url({{asset('beta/img/mentors/1.png')}});">
                <div class="slide-container">
                    <div class="container">
                        <div class="">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="slide-title-sub slide-title-sub-md">
                                        @if($lang == 'ru')
                                            Тренера/Менторы
                                        @else
                                            Тренерлер/Менторлор
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <h3 class="slide-title text-white">
                                        Утюшева Лилия
                                    </h3>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="slide-descr slide-descr-projects">
                                                @if($lang == 'ru')
                                                    Тренер по гибким навыкам на русском языке.
                                                    <br>
                                                    Международный Бизнес тренер с опытом 25+ лет, профессиональный коуч-ментор ACC ICF, НЛП мастер, основатель Coach Master Academy, мастер интеллект-тренер Всемирной Ассоциации Тренеров.
                                                    <br>
                                                    Провела более 4500 коуч-сессий, 1000 тренингов для ведущих местных и международных организаций в Кыргызстане и зарубежом.
                                                @else
                                                    ийкемдүү кɵндүм (гибкие навыки) боюнча орус тилиндеги тренер.
                                                    <br>
                                                    25 жылддан ашык тажрыйбасы бар Эл аралык Бизнес Тренер, ACC ICF Чебер Коуч Ментор, NLP Мастер, Коуч Мастер Академиясынын негиздөөчүсү, Дүйнөлүк Тренерлер Ассоциациясынын Мастер Интеллект Тренери.
                                                    <br>
                                                    Кыргызстанда жана чет өлкөлөрдө алдыңкы жергиликтүү жана эл аралык уюмдар үчүн 4500дөн ашык коучинг сессияларын, 1000 тренингдерди өткөргөн.
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mentor-item text-white" style="background-image: url({{asset('beta/img/mentors/4.png')}});">
                <div class="slide-container">
                    <div class="container">
                        <div class="">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="slide-title-sub slide-title-sub-md">
                                        @if($lang == 'ru')
                                            Тренера/Менторы
                                        @else
                                            Тренерлер/Менторлор
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <h3 class="slide-title text-white">
                                        Абдраимова Асель
                                    </h3>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="slide-descr slide-descr-projects">
                                                @if($lang == 'ru')
                                                    Тренер по гибким навыкам на кыргызском языке.
                                                    <br>
                                                    Является тренером по гражданскому образованию  и жизненным навыкам,  специалист по подготовке социальных педагогов, менторов и тренеров для эффективной работы с  молодежью.
                                                    <br>
                                                    Окончила Ошский государственный университет по специальности – специалист по социальной работе, специализируется на социальной работе с молодежью и подростками, проработала больше 10 лет в молодежных проектах.
                                                @else
                                                    Ийкемдүү кɵндүм боюнча кыргыз тилиндеги тренер.
                                                    <br>
                                                    Жарандык тарбия жана турмуштук көндүмдөр боюнча тренер, жаштар менен эффективдүү иштөө үчүн социалдык педагогдорду, насаатчыларды жана тренерлерди даярдоо боюнча адис.
                                                    <br>
                                                    Ош Мамлекеттик Университетинин социалдык кызматкер адистигин  бүтүргөн, жаштар жана өспүрүмдөр менен социалдык иш боюнча адистиги бар, жаштар долбоорлорунда 10 жылдан ашык эмгектенген.
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mentor-item text-white" style="background-image: url({{asset('beta/img/mentors/3.png')}});">
                <div class="slide-container">
                    <div class="container">
                        <div class="">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="slide-title-sub slide-title-sub-md">
                                        @if($lang == 'ru')
                                            Тренера/Менторы
                                        @else
                                            Тренерлер/Менторлор
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <h3 class="slide-title text-white">
                                        Кубанычбекова Асел
                                    </h3>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="slide-descr slide-descr-projects">
                                                @if($lang == 'ru')
                                                    Тренер по UpSHIFT на кыргызском языке.
                                                    <br>
                                                    Основательница Фонда развития женского предпринимательства в Кыргызстане (SheStartsKG), Национальный молодежный гендерный активист глобальной гендерной молодежной группы ООН, Член наблюдательного совета «Гражданское Участие».
                                                    <br>
                                                    Региональный координатор проекта WAGE в Центральной Азии, Центр Международного частного предпринимательства (CIPE), а также выпускница программы Women Deliver Young Leaders Program.
                                                @else
                                                    UpSHIFT боюнча  кыргыз тилиндеги тренер.
                                                    <br>
                                                    Кыргызстандагы Аялдардын ишкердигин өнүктүрүү фондунун (SheStartsKG) негиздөөчүсү, БУУнун Глобалдык гендердик жаштар тобунун Улуттук жаштар гендердик активисти, “Жарандык катышуу” Байкоочу кеңешинин мүчөсү.
                                                    <br>
                                                    Борбордук Азиядагы  WAGE долбоордун, Эл аралык жеке ишкердик борбору (CIPE) аймактык координатору жана  Women Deliver Young Leaders Program программасынын бүтүрүүчүсү.
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- mentors end -->

    <!-- testimonial start -->
    <section class="testimonial-wrapper gradient-color" id="testimonial">
        <div class="container">
            <div class="main-title-wrapper">
                <h4 class="sitemain-subtitle text-white">
                    @if($lang == 'ru')
                        Для Родителей/Отзывы участников
                    @else
                        Ата-энелер үчүн/Катышуучулардын пикирлери
                    @endif
                </h4>
            </div>
        </div>
        <div class="position-relative">
            <div class="container">
                <div class="testimonial-slider">
                    <div class="testimonial-inner">
                        <div class="testimonial-outer-box">
                            <div class="owl-carousel" id="testimonial-owl">
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <h5 class="testimonial-title text-reset mb-4">
                                            @if($lang == 'ru')
                                                Почему подростку нужно участвовать в подобных конкурсах?
                                            @else
                                                Эмне үчүн өспүрүмгө мындай сынактарга катышуу маанилүү?
                                            @endif
                                        </h5>
                                        <p class="testimonial-dec">
                                            @if($lang == 'ru')
                                                Данный конкурс направлен на формирование навыков, которые помогают подросткам и студентам в профессиональной ориентации и дальнейшем трудоустройстве.
                                            @else
                                                Бул сынак өспүрүмдөрдүн жана студенттердин кесипкөйлүгүнө жана андан ары жумушка орношуусуна жардам берүүчү көндүмдөрдү калыптандырууга багытталган.
                                            @endif
                                        </p>
                                        <div class="d-flex justify-content-center">
                                            <a class="btn btn-primary">
                                            <span>
                                                1/4
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <h5 class="testimonial-title text-reset mb-4">
                                            @if($lang == 'ru')
                                                Почему подростку нужно участвовать в подобных конкурсах?
                                            @else
                                                Эмне үчүн өспүрүмгө мындай сынактарга катышуу маанилүү?
                                            @endif
                                        </h5>
                                        <p class="testimonial-dec">
                                            @if($lang == 'ru')
                                                Обучающая программа проекта сформирована так, чтобы работая в командах участники расширяли свой кругозор и работали в дружелюбной атмосфере.
                                            @else
                                                Долбоордун билим берүү программасы командаларда иштөө менен катышуучулардын кругозорун кеңейтип, достук атмосферада иштеше тургандай түзүлгɵн.
                                            @endif
                                        </p>
                                        <div class="d-flex justify-content-center">
                                            <a class="btn btn-primary">
                                            <span>
                                                2/4
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <h5 class="testimonial-title text-reset mb-4">
                                            @if($lang == 'ru')
                                                Почему подростку нужно участвовать в подобных конкурсах?
                                            @else
                                                Эмне үчүн өспүрүмгө мындай сынактарга катышуу маанилүү?
                                            @endif
                                        </h5>
                                        <p class="testimonial-dec">
                                            @if($lang == 'ru')
                                                Участие в конкурсе помогает раскрыть способности и таланты подростков. Девушки и парни в проекте учатся решать реальные проблемы, развивая свои гибкие навыки и креативность, тем самым происходит правильная адаптация в социуме, общение со сверстниками.
                                            @else
                                                Сынакка катышуу өспүрүмдөрдүн жөндөмүн жана талантын ачууга жардам берет. Долбоордогу кыздар жана жигиттер ийкемдүү жөндөмдөрүн жана креативдүүлүгүн өнүктүрүүү менен чыныгы көйгөйлөрдү чечүүнү үйрөнүшɵт,  , ошону менен коомго туура адаптациялануу жүрɵт, теңтуштар менен баарлашуу ишке ашат.
                                            @endif
                                        </p>
                                        <div class="d-flex justify-content-center">
                                            <a class="btn btn-primary">
                                            <span>
                                                3/4
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <h5 class="testimonial-title text-reset mb-4">
                                            @if($lang == 'ru')
                                                Почему подростку нужно участвовать в подобных конкурсах?
                                            @else
                                                Эмне үчүн өспүрүмгө мындай сынактарга катышуу маанилүү?
                                            @endif
                                        </h5>
                                        <p class="testimonial-dec">
                                            @if($lang == 'ru')
                                                Формирование активной позиции и учение защищать и реализовывать свои проекты, работа в команде дают большое преимущество при поступлении в университет, трудоустройстве, запуске своего стартапа.
                                            @else
                                                Активдүү позицияны калыптандыруу жана долбоорлоруңузду коргоого жана ишке ашырууга үйрөтүү, командада иштөө университетке тапшырууда, жумуш орношуу, өзүңүздүн стартапыңызды баштоодо чоң артыкчылык берет.
                                            @endif
                                        </p>
                                        <div class="d-flex justify-content-center">
                                            <a class="btn btn-primary">
                                            <span>
                                                4/4
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <p class="testimonial-dec py-5">
                                            После участия в проекте стала более уверенна в себе, стала не бояться высказывать свое мнение, делать презентации и не бояться публики
                                        </p>
{{--                                        <h5 class="testimonial-title">John five</h5>--}}
{{--                                        <h5 class="testimonial-subtitle">Client of example Project</h5>--}}
                                        <div class="testimonial-images">
                                            <img src="{{asset('beta/img/testimonial/3.png')}}" alt="" title="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <p class="testimonial-dec py-5">
                                            Мага долбоор аябай жакты. Кыскача айтканда мен жашоого болгон көз карашымды өзгөртүм жана өзүмдү таанып билдим.
                                        </p>
{{--                                        <h5 class="testimonial-title">John five</h5>--}}
{{--                                        <h5 class="testimonial-subtitle">Client of example Project</h5>--}}
                                        <div class="testimonial-images">
                                            <img src="{{asset('beta/img/testimonial/4.png')}}" alt="" title="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <p class="testimonial-dec py-5">
                                            Это был удивительный опыт…Я не просто получила какие-то знания, а нечто большое, больше чем могла представить… Безумно рада и благодарна, что стала участницей такого невероятного проекта. Он надолго останется в моей памяти.
                                        </p>
{{--                                        <h5 class="testimonial-title">John five</h5>--}}
{{--                                        <h5 class="testimonial-subtitle">Client of example Project</h5>--}}
                                        <div class="testimonial-images">
                                            <img src="{{asset('beta/img/testimonial/5.png')}}" alt="" title="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-item">
                                    <div class="testimonial-content">
                                        <div class="animated-bg"><i class=""></i><i class=""></i><i class=""></i>
                                        </div>
                                        <p class="testimonial-dec py-5">
                                            Мен өзумө жаны досторду таптым жана көп төрөгөн жаңы маалыматтарды, информацияларды алдым десем болот. Ошондой эле бул долбоорго келгениме кубанычтамын.
                                        </p>
{{--                                        <h5 class="testimonial-title">John five</h5>--}}
{{--                                        <h5 class="testimonial-subtitle">Client of example Project</h5>--}}
                                        <div class="testimonial-images">
                                            <img src="{{asset('beta/img/testimonial/6.png')}}" alt="" title="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial end -->

    <!-- faq start -->
    <section class="faq-wrapper" id="faq">
        <div class="container">
            <div class="service-content-1 main-title-wrapper">
                <h4 class="sitemain-subtitle">
                    FAQ - ЧаВо
                </h4>
            </div>
            <div class="row">
                <!-- accordion -->
                <div class="col-12">
                    <div class="accordion" id="accordion">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="accordion__card">
                                    <button class="" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                        @if($lang == 'ru')
                                            Кто может принять участие?
                                        @else
                                            Ким катыша алат?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse1" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Если тебе от 14 до 24 лет, и ты проживаешь в одной из этих локаций: Бишкек, Беловодск, Нарын, Угут, Талас, Кара-Буура, Сузак, Жалалабад, Каракол, Кызыл-Кия, Гулистан, Ош, Кашкар-Кыштак, Баткен. Ты горишь желанием создать свой стартап и сможешь уделять время от 6 до 9 часов в неделю на онлайн и офлайн обучение на местах, то наша лаборатория стартапов UStart ждет твоей заявки!
                                            @else
                                                Эгерде сиз 14 жаштан 24 жашка чейин болсоңуз жана сиз  бул локациялардын биринде Бишкек, Беловодск, Нарын, Үгүт, Талас, Кара-Буура, Сузак, Жалал-Абад, Каракол, Кызыл-Кыя, Гүлистан, Ош, Кашкар-Кыштак, Баткенде жашасаңыз.
                                                Эгерде сиз өзүңүздүн стартапыңызды түзүүнү каалап жатсаңыз жана жумасына 6 дан 9 саатка чейин онлайн жана оффлайн форматында окууга убактыңызды бөлө алсаңыз, анда биздин USstart Startup Lab сиздин анкетаңызды күтөт!
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                        @if($lang == 'ru')
                                            Что делать если у меня нет команды?
                                        @else
                                            Эгерде менин командам жок болсо эмне кылам?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse2" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Не волнуйся, благодаря нашим тренерам и фасилитаторам, ты будешь проходить супер интерактивное обучение, в ходе которого мы поможем тебе сформироваться в команду мечты с остальными участниками проекта! К тому же ты можешь пригласить своих друзей, чтобы вы вместе участвовали и разрабатывали свой стартап.
                                            @else
                                                Кабатыр болбоңуз, биздин тренерлер менен фасилитаторлорубуздун жардамы менен, сиз супер интерактивдүү тренингге ээ болосуз, анын жүрүшүндө биз сизге долбоордун башка катышуучулары менен кыялданган команданы түзүүгө жардам беребиз! Мындан тышкары, сиз стартапыңызды чогуу өнүктүрүүгө жана катышууга досторуңузду чакыра аласыз.
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        @if($lang == 'ru')
                                            Как подать заявку на участие?
                                        @else
                                            Кантип анкета тапшырсак берсе болот?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse3" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Очень легко! В наших социальных сетях @techaim.kg в инстаграме и фейсбуке есть анкета прикрепленная в шапке профиля. Просто нажимаешь на нее и заполняешь саму анкету, а если у тебя возникнут вопросы, то мы всегда готовы помочь тебе, напиши нам сообщение в директ.
                                            @else
                                                Абдан оңой! Арыз берүү үчүн, сиз UStart веб-сайтына кирип, негизги беттеги "Колдонуу" опциясын басыңыз. Анкета өзү чыккандан кийин, толтуруп, арызыңызды жөнөтүңүз. Жана кандайдыр бир суроолоруңуз болсо, бизге түз кат жөнөтүңүз. Биз сизге жардам берүүгө дайым даярбыз!
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                        @if($lang == 'ru')
                                            Когда будут известны результаты?
                                        @else
                                            Жыйынтыгы качан белгилүү болот?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse4" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Результаты заявки будут известны в течение 2-3 недель. Наша команда отправит письмо о приглашении в проект на почтовый адрес который ты указал в анкете. Кандидаты которые не прошли также будут оповещены через почту.
                                            @else
                                                Арыздын жыйынтыгы 2 жуманын ичинде белгилүү болот. Биздин команда сиз анкетада көрсөткөн почта дарегине долбоорго чакыруу катын жөнөтөт. Андан өтпөй калган талапкерлерге өз алдынча окууга чакыруу каты менен почта аркылуу да кабарланат.
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                        @if($lang == 'ru')
                                            Платное ли обучение?
                                        @else
                                            Окутууга акы төлөнөбү?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse9" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Обучение у нас абсолютно бесплатное! За 8 недель обучения ты получишь целый багаж знаний, начиная с того как придумать идею и заканчивая тем что ты запустишь свой стартап!
                                            @else
                                                Биздин окутуу таптакыр бекер! Тренингдин 8 жумасында сиз идеяны кантип ойлоп табуудан баштап, өзүңүздүн стартабыңызды ишке киргизүүгө чейин толук билим багажын  аласыз!
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-6">
                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                        @if($lang == 'ru')
                                            Будет ли деление по возрастной категории?
                                        @else
                                            Курактык категориясы боюнча чектөөлөр болобу?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse5" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Обучение у нас проходит в смешанных группах от 14 до 24 лет. Деления по возрастной категории нет.
                                            @else
                                                Биздин окуу 14 жаштан 24 жашка чейинки аралаш топтордо өтөт. Жаш боюнча бөлүү жок.
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                        @if($lang == 'ru')
                                            Можно ли пройти обучение онлайн?
                                        @else
                                            Мен онлайн окусам болобу?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse6" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Обучение в UStart супер интерактивное! Главный акцент сделан на то чтобы было больше практики и меньше теории. Именно поэтому формат обучения смешанный: онлайн занятия 2 раза в неделю и 1 занятие офлайн на местах. Посещение офлайн занятий обязательное, так как именно на этих встречах собираются команды. Если у тебя нет возможности посещать офлайн занятия, но ты горишь желанием учиться, то у нас есть решение! Самостоятельная группа обучения создана специально для формата онлайн уроков. У тебя будет доступ на посещение онлайн занятий без участия на офлайн встречах, но в таком случае не будет возможности участвовать в социальном конкурсе UStart.
                                            @else
                                                USstartта үйрөнүү супер интерактивдүү! Негизги басым көбүрɵɵк практикага жана азыраак теорияга жасалат. Ошондуктан окутуу форматы аралаш: жумасына 2 жолу онлайн сабактар ​​жана  1 офлайн сабак жеринде болот. Оффлайн сабактарга катышуу милдеттүү, анткени командалар дал ушул жолугушууларда чогулушат. Эгерде сизде оффлайн сабактарга катышуу мүмкүнчүлүгүңуз жок болсо, бирок сиз үйрөнүүгө ынтызар болсоңуз, анда бизде чечим бар – ɵз алдынча окутуу тобу атайын онлайн сабактар ​​форматы үчүн түзүлгөн. Сиз оффлайн жолугушууларга катышпастан онлайн сабактарга катышуу мүмкүнчүлүгүнө ээ болосуз, бирок бул учурда сиз UStart социалдык сынагына катыша албай каласыз.
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                        @if($lang == 'ru')
                                            Как будут отбираться команды\участники для участия в социальном конкурсе UStart?
                                        @else
                                            UStart социалдык сынагына катышуу үчүн командалар/катышуучулар кантип тандалат?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse7" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Самый главные пункт по которому мы будем отбирать участников это - сильная мотивация и желание учиться! Во-вторых мы будем смотреть на локацию в которых вы проживаете и на возрастные критерии. Также приемная комиссия будет оценивать вашу готовность обязательного посещения онлайн и офлайн занятий во время проекта.
                                            @else
                                                Катышуучуларды тандай турган эң негизги нерсе – бул күчтүү мотивация жана өнүгүүгө болгон каалоо! Экинчиден, жашаган жериңизди жана жаш курагыңызга карайбыз. Ошондой эле, атайын комиссия долбоордун жүрүшүндө милдеттүү онлайн жана оффлайн сабактарына катышууга даярдыгыңызды баалайт.
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                        @if($lang == 'ru')
                                            Что получат победители питчинга?
                                        @else
                                            Питчингдин жеңүүчүлөрү эмне алышат?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse8" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Победители питчинга получат начальное финансирование в сумме 560$ в виде оборудования, материалов или релевантных курсов для успешного запуска стартапа. К тому же каждой команде будет предоставлятся помощь ментора на 1 месяц. Благодаря ментору вы сможете вместе пройти путь реализации стартапа!
                                            @else
                                                Питчингдин жеңүүчүлөрү стартапты ийгиликтүү баштоо үчүн жабдуулар, материалдар же тиешелүү курстар түрүндө 500 долларлык баштапкы каржылоону алышат. Мындан тышкары ар бир командага 1 ай ментордун жардамы берилет. Ментордун жардамынын аркасында сиз стартапты ишке ашыруу жолун чогуу баса аласыз!
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                        @if($lang == 'ru')
                                            Как проходит обучение и что для этого нужно?
                                        @else
                                            Окутуу кандай жүргүзүлɵт  жана бул үчүн эмне керек?
                                        @endif
                                        <span>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,11H5a1,1,0,0,0,0,2H19a1,1,0,0,0,0-2Z"/></svg>
										</span>
                                    </button>

                                    <div id="collapse10" class="collapse" data-parent="#accordion">
                                        <p>
                                            @if($lang == 'ru')
                                                Обучение будет с нуля, поэтому не бойся! Единственное что тебе будет необходимо это интернет и телефон для участия в онлайн занятиях. Помимо этого, вам самим будет необходимо покрывать транспортные расходы при посещении офлайн встреч на местах.
                                            @else
                                                Эң башында эч нерсени билүүнүн же аткара  билүүнүн кажети жок, окутуу нөлдөн баштап жүргүзүлөт, ошондуктан эч нерседен коркпостон, өзүмө бул туура келбейт деп ойлобошуңуз керек! Дал ушундай деп ойлосоңуз, бизге анкетаңызды  берүүңүз керек! Онлайн сабактарга катышуу үчүн сизге бир гана нерсе интернет жана телефон керек.
                                                Мындан тышкары, сиз оффлайн жолугушууларга катышуу үчүн жол чыгымдарыңызды ɵзүнүз тɵлɵшүнүз керек.
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end accordion -->
            </div>
        </div>
    </section>
    <!-- faq end -->

    <!-- blog start -->
    <section class="blog-wrapper pt-0" id="news">
        <div class="container">

            <div class="main-title-wrapper d-flex align-items-center justify-content-between">
                <h4 class="sitemain-subtitle mb-0">
                    @if($lang == 'ru') Новости
                    @else Жаңылыктар
                    @endif

                </h4>
                <div class="section-actions d-flex">
                    <a href="{{route('web.news')}}" class="btn btn-primary">
                        <span>
                            @if($lang == 'ru') Все новости
                            @else Бардык жаңылыктар
                            @endif
                        </span>
                    </a>
                </div>
            </div>


            <div class="row">
                @foreach($news as $item)
                    <div class="blog-{{$loop->index}} col-sm-6 col-lg-4">
                        <div class="blog-content">
                            <div class="blog-first-block">
                                <img src="@if($item->img_thumbnail) {{asset($item->img_thumbnail)}} @else {{asset('beta/img/blog/default.png')}} @endif" alt="{{$item->getTitle($lang)}}" title="{{$item->getTitle($lang)}}" />
                            </div>
                            <div class="blog-second-block">
                                <div class="blog-right-content pl-0">
                                    <div class="blog-date">{{$item->getPublishedDate($lang)}}</div>
                                    <a href="{{route('web.news.single', $item->id)}}" class="blog-dec stretched-link text-dark">
                                        {{$item->getTitle($lang)}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- blog end -->
@endsection
