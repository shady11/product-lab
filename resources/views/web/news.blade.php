@extends('web.default')

@section('header-class', 'header--news')

@section('content')

    <!-- blog start -->
    <section class="blog-wrapper" id="news">
        <div class="container">

            <div class="main-title-wrapper d-flex align-items-center justify-content-between">
                <h4 class="sitemain-subtitle mb-0">
                    @if($lang == 'ru') Новости
                    @else Жаңылыктар
                    @endif
                </h4>
            </div>


            <div class="row">
                @foreach($news as $item)
                    <div class="blog-{{$loop->index}} col-sm-6 col-lg-4">
                        <div class="blog-content">
                            <div class="blog-first-block">
                                <img src="{{asset($item->img_thumbnail)}}" alt="{{$item->img}}" title="{{$item->img}}" />
                            </div>
                            <div class="blog-second-block">
                                <div class="blog-right-content pl-0">
                                    <div class="blog-date">{{$item->getPublishedDate()}}</div>
                                    <a href="{{route('web.news.single', $item->id)}}" class="blog-dec stretched-link text-dark">
                                        {{$item->name_ru}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- blog end -->

@endsection
