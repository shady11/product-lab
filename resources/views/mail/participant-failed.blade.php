<html><head><style>html,body { padding: 0; margin:0; }</style>
</head><body><div style="font-family:Arial,Helvetica,sans-serif; line-height: 1.5; font-weight: normal; font-size: 15px; color: #2F3044; min-height: 100%; margin:0; padding:0; width:100%; background-color:#edf2f7">
    <br><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0 auto; padding:0; max-width:600px">
        <tbody>
        <tr>
            <td align="left" valign="center">
                <div style="text-align:left; margin: 0 20px; padding: 40px; background-color:#ffffff; border-radius: 6px">
                    <!--begin:Email content-->
                    <div style="padding-bottom: 30px; font-size: 17px;">
                        <strong>{{$header}}</strong>
                    </div>
                    <div style="padding-bottom: 30px">
                        {{$content}}
                    </div>
                    <!--end:Email content-->
                    <div style="padding-bottom: 10px">
                        {{$footer}}
                    </div></div></td></tr><tr>
            <td align="center" valign="center" style="font-size: 13px; text-align:center;padding: 20px; color: #6d6e7c;">
                <p>Copyright ©
                    <a href="{{route('web.index')}}" rel="noopener" target="_blank">Product Lab</a>.</p>
            </td>
        </tr>



        </tbody>
    </table>
</div></body></html>
