@extends('admin.default')

@section('title', 'Отчет по приложению ishtapp')

@section('content')

<div class="row">
    <div class="col-md-4">
        <div class="card bg-primary shadow-sm card-md-stretch-50 mb-md-8">
            <!--begin::Body-->
            <div class="card-body d-flex flex-column justify-content-center">
                <!--begin::Svg Icon | path: icons/duotune/graphs/gra007.svg-->
                <span class="svg-icon svg-icon-white svg-icon-5x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black"/>
                            <rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black"/>
                            <path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black"/>
                            <rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black"/>
                        </svg>
                    </span>
                <!--end::Svg Icon-->
                <div class="text-white fw-boldest fs-3x mb-2 mt-5">
                    {{$ishtapp_users->count()}}
                </div>
                <div class="fw-bold text-white fs-4">Пользователи</div>
                <a href="{{route('admin.report.ishtapp.users')}}" class="stretched-link"></a>
            </div>
            <!--end::Body-->
        </div>
    </div>
    <div class="col-md-4">
        <div class="card bg-primary shadow-sm card-md-stretch-50 mb-md-8">
            <!--begin::Body-->
            <div class="card-body d-flex flex-column justify-content-center">
                <!--begin::Svg Icon | path: icons/duotune/graphs/gra007.svg-->
                <span class="svg-icon svg-icon-white svg-icon-5x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M3 2H10C10.6 2 11 2.4 11 3V10C11 10.6 10.6 11 10 11H3C2.4 11 2 10.6 2 10V3C2 2.4 2.4 2 3 2Z" fill="black"></path>
                            <path opacity="0.3" d="M14 2H21C21.6 2 22 2.4 22 3V10C22 10.6 21.6 11 21 11H14C13.4 11 13 10.6 13 10V3C13 2.4 13.4 2 14 2Z" fill="black"></path>
                            <path opacity="0.3" d="M3 13H10C10.6 13 11 13.4 11 14V21C11 21.6 10.6 22 10 22H3C2.4 22 2 21.6 2 21V14C2 13.4 2.4 13 3 13Z" fill="black"></path>
                            <path opacity="0.3" d="M14 13H21C21.6 13 22 13.4 22 14V21C22 21.6 21.6 22 21 22H14C13.4 22 13 21.6 13 21V14C13 13.4 13.4 13 14 13Z" fill="black"></path>
                        </svg>
                    </span>
                <!--end::Svg Icon-->
                <div class="text-white fw-boldest fs-3x mb-2 mt-5">
                    {{$ishtapp_companies->count()}}
                </div>
                <div class="fw-bold text-white fs-4">Компании</div>
                <a href="{{route('admin.report.ishtapp.companies')}}" class="stretched-link"></a>
            </div>
            <!--end::Body-->
        </div>
    </div>
    <div class="col-md-4">
        <div class="card bg-primary shadow-sm card-md-stretch-50 mb-md-8">
            <!--begin::Body-->
            <div class="card-body d-flex flex-column justify-content-center">
                <!--begin::Svg Icon | path: icons/duotune/graphs/gra007.svg-->
                <span class="svg-icon svg-icon-white svg-icon-5x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="black"/>
                        </svg>
                    </span>
                <!--end::Svg Icon-->
                <div class="text-white fw-boldest fs-3x mb-2 mt-5">
                    {{$ishtapp_vacancies->count()}}
                </div>
                <div class="fw-bold text-white fs-4">Возможности</div>
                <a href="{{route('admin.report.ishtapp.opportunities')}}" class="stretched-link"></a>
            </div>
            <!--end::Body-->
        </div>
    </div>
</div>

@endsection

@section('scripts')
@endsection
