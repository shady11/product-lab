@extends('admin.default')

@section('title', 'Отчет')

@section('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .table-responsive table thead th {
            position: sticky;
            top: 0;
            z-index: 1;
            background: white;
        }
        .table-responsive table thead th:first-child {
            position: sticky;
            left: 0;
            z-index: 2;
            background: #fff !important;
        }
        .table-responsive table tbody th {
            position: sticky;
            left: 0;
            background: #fff !important;
            z-index: 1;
        }

        .table-responsive table {
            border-collapse: collapse !important;
            border: solid #000;
        }

        .table-responsive table td, .table-responsive table th {
            border: 2px solid #dddde4 !important;
        }

        .table-responsive .table>:not(:first-child) {
            border-color: #dddde4;
            border-width: 2px;
            border-style: inherit;
        }

    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h2 class="card-title fw-bolder">Отчет</h2>
    </div>
    <div class="card-body">
        <form id="filter">
            <div class="row mb-7">
                <div class="col-md-3 fv-row mb-3">
                    {!! Form::label('region_id', 'По регионам') !!}
                    {!! Form::select('region_id', $regions, request()->query('region'), ['class' => 'form-select region_id', 'data-control' => 'select2',
                    'data-placeholder' => '--Выберите--', 'data-hide-search' => 'false', 'data-allow-clear'=>'true', 'placeholder' => '']) !!}
                </div>
                <div class="col-md-3 fv-row mb-3">
                    {!! Form::label('participant_id', 'По участникам') !!}
                    {!! Form::select('participant_id', $participants, request()->query('participant'), ['class' => 'form-select participant_id', 'data-control' => 'select2',
                    'data-placeholder' => '--Выберите--', 'data-hide-search' => 'false', 'data-allow-clear'=>'true', 'placeholder' => '']) !!}
                </div>
                <div class="col-sm-3 fv-row mb-3">
                    {!! Form::label('team_id', 'По командам') !!}
                    {!! Form::select('team_id', $teams, request()->query('team'), ['class' => 'form-select team_id', 'data-control' => 'select2',
                    'data-placeholder' => '--Выберите--', 'data-hide-search' => 'false', 'data-allow-clear'=>'true', 'placeholder' => '']) !!}
                </div>
                <div class="col-sm-3 fv-row mb-3">
                    {!! Form::label('period', 'Выбрать период') !!}
                    {!! Form::text('period', request()->query('period'), ['class' => 'form-control', 'id'=>'period']) !!}
                </div>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table table-striped table-rounded border border-gray-300 table-row-bordered table-row-gray-300 gy-7 gs-7 ">
                <thead>
                    <tr class="fw-bolder">
                        <th class="min-w-150px">ФИО</th>
                        @foreach ($exercises as $exercise)
                        <th class="min-w-200px">
                            <div class="text-gray-600">{{$exercise->deadline}}</div>
                            <div class="fs-5 fw-bolder text-gray-800 text-hover-primary text-active-primary active">{{$exercise->name}}</div>
                            <div class="text-gray-600">из {{$exercise->grade}}</div>
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody class="text-gray-600 fw-bold">
                    @foreach ($data as $item)
                    <tr>
                        <th scope="row">{{$item->getName()}}</th>
                        @foreach ($item->exercises as $exec)
                            <td>{{$exec->grade}}</td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{ asset('assets/js/moment.ru.min.js')}}"></script>
<script>
    $(".table-responsive").mousewheel(function(event, delta) {
        this.scrollLeft -= (delta * 30);
        event.preventDefault();
    });

    $("#period").daterangepicker({
        locale: {
            applyLabel: 'Применить',
            cancelLabel: 'Отменить',
            format: 'YYYY-MM-DD'
        },
        startDate: moment().clone().startOf('month').format('YYYY-MM-DD')
    });

    var url = new URL('{{route('admin.report.index')}}');
    var search_params = url.searchParams;

    $("#filter input, select").change(function() {
        let region_id = $('select[name=region_id]').val();
        let participant_id = $('select[name=participant_id]').val();
        let team_id = $('select[name=team_id]').val();
        let period = $('input[name=period]').val();

        if(region_id){
            search_params.set('region', region_id);
        }

        if(participant_id){
            search_params.set('participant', participant_id);
        }

        if(team_id){
            search_params.set('team', team_id);
        }

        if(period){
            search_params.set('period', period);
        }

        url.search = search_params.toString();
        window.location = url.toString();
    });

</script>
@endsection
