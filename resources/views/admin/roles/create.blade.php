@extends('admin.default')

@section('title', 'Новая роль')

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <h3>Новая роль</h3>
                </div>
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger d-flex align-items-center m-8 mb-0">
                <span class="svg-icon svg-icon-2hx svg-icon-danger me-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z" fill="black"></path>
                        <path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z" fill="black"></path>
                    </svg>
                </span>
                <div class="d-flex flex-column">
                    <ul class="list-unstyled mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                    <i class="bi bi-x fs-1 text-danger"></i>
                </button>
            </div>
        @endif

        {!! Form::open(array('route' => 'admin.roles.store','method'=>'POST')) !!}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-2 col-form-label">
                    Название
                </label>
                <div class="col-6">
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group row align-items-center my-4">
                <label class="col-2 col-form-label">
                    Права
                </label>
                <div class="col-10">
                    <div class="row">
                        @foreach($permission as $value)
                            <div class="col-3 mb-3">
                                <div class="form-check form-check-custom form-check-solid">
                                    {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'form-check-input')) }}

                                    <label class="form-check-label" for="flexCheckDefault">
                                        {{ $value->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                    <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection