@extends('admin.default')

@section('title', $role->name)

@section('content')

    <div class="card card-custom">
        <div class="card-header cursor-pointer">
            <div class="card-title m-0">
                <h4 class="card-title">{{$role->name}}</h4>
            </div>
            <div class="d-flex">
                <a href="{{route('admin.roles.index')}}" class="me-3  btn btn-sm btn-light-dark btn-hover-scale align-self-center">Назад</a>
                <a href="{{route('admin.roles.edit', $role)}}" class="me-3 btn btn-sm btn-light-primary btn-hover-scale align-self-center">Редактировать</a>
                <a href="{{route('admin.roles.delete', $role)}}" class="btn btn-sm btn-light-danger btn-hover-scale align-self-center">Удалить</a>
            </div>
        </div>
        <div class="card-body p-9">
            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">Название</label>
                <div class="col-lg-8">
                    <span class="fw-bolder fs-6 text-gray-800">{{$role->name}}</span>
                </div>
            </div>
            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">Права</label>
                <div class="col-lg-8">
                    @if(!empty($rolePermissions))
                    <div class="row">
                        @foreach($rolePermissions as $value)
                            <div class="col-3">
                                <div class="form-check form-check-custom form-check-solid">
                                    {{ Form::checkbox('permission[]', $value->id, ['class' => 'form-check-input', 'disabled' ]) }}
                                    <label class="form-check-label" for="flexCheckDefault">
                                        {{ $value->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection