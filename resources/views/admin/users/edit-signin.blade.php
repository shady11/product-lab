<div id="kt_account_settings_signin_method" class="collapse show">


    <!--begin::Card body-->
    <div class="card-body p-9">


    @if ($errors->any())
        <!--begin::Alert-->
            <div class="alert alert-danger d-flex align-items-center p-5 mb-10">
                <!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
                <span class="svg-icon svg-icon-2hx svg-icon-danger me-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z" fill="black"></path>
                        <path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z" fill="black"></path>
                    </svg>
                </span>
                <!--end::Svg Icon-->
                <div class="d-flex flex-column">
                    <ul class="list-unstyled mb-0">
                        @foreach ($errors->all() as $error)
                            <li class="mt-1">- {{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!--end::Alert-->
        @endif


    <!--begin::Login-->
        <div class="d-flex flex-wrap align-items-center border-top p-5">
            <!--begin::Label-->
            <div id="kt_signin_login">
                <div class="fs-6 fw-bolder mb-1">Логин</div>
                <div class="fw-bold text-gray-600">{{$user->login}}</div>
            </div>
            <!--end::Label-->
            <!--begin::Edit-->
            <div id="kt_signin_login_edit" class="flex-row-fluid d-none">
                <!--begin::Form-->
                {!! Form::model($user, ['route' => ['admin.users.changeLogin', $user], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'id' => 'kt_signin_change_login', 'class' => 'form fv-plugins-bootstrap5 fv-plugins-framework']) !!}
                <div class="row mb-6">
                    <div class="col-lg-6 mb-4 mb-lg-0">
                        <div class="fv-row mb-0 fv-plugins-icon-container">
                            <label for="login" class="form-label fs-6 fw-bolder mb-3">Новый логин <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-solid" name="login" id="login" placeholder="Логин">
                            <div class="fv-plugins-message-container invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="fv-row mb-0 fv-plugins-icon-container">
                            <label for="confirmloginpassword" class="form-label fs-6 fw-bolder mb-3">Пароль<span class="text-danger">*</span></label>
                            <input type="password" class="form-control form-control-solid" name="confirmloginpassword" id="confirmloginpassword" placeholder="Пароль">
                            <div class="fv-plugins-message-container invalid-feedback"></div>
                        </div>
                    </div>
                    </div>
                    <div class="d-flex">
                        <button id="kt_signin_submit" type="submit" class="btn btn-primary me-2 px-6">Обновить Логин</button>
                        <button id="kt_signin_cancel" type="button" class="btn btn-color-gray-400 btn-active-light-primary px-6">Отменить</button>
                    </div>
                {!! Form::close() !!}
                <!--end::Form-->
            </div>
            <!--end::Edit-->
            <!--begin::Action-->
            <div id="kt_signin_login_button" class="ms-auto">
                <button class="btn btn-light btn-active-light-primary">Изменить Логин</button>
            </div>
            <!--end::Action-->
        </div>


        <div class="d-flex flex-wrap align-items-center border-top p-5">
            <!--begin::Label-->
            <div id="kt_signin_password" class="">
                <div class="fs-6 fw-bolder mb-1">Пароль</div>
                <div class="fw-bold text-gray-600">************</div>
            </div>
            <!--end::Label-->


            <!--begin::Edit-->
            <div id="kt_signin_password_edit" class="flex-row-fluid d-none">

            <!--begin::Form-->
                {!! Form::model($user, ['route' => ['admin.users.changePassword', $user], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'id' => 'kt_signin_change_password', 'class' => 'form fv-plugins-bootstrap5 fv-plugins-framework']) !!}
                    <div class="row mb-1">
                        <div class="col-lg-4">
                            <div class="fv-row mb-0 fv-plugins-icon-container">
                                <label for="currentpassword" class="form-label fs-6 fw-bolder mb-3">Текущий пароль<span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-lg form-control-solid required" name="currentpassword" id="currentpassword">
                                <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            </div>
                        <div class="col-lg-4">
                            <div class="fv-row mb-0 fv-plugins-icon-container">
                                <label for="newpassword" class="form-label fs-6 fw-bolder mb-3">Новый пароль <span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-lg form-control-solid required" name="newpassword" id="newpassword">
                                <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            </div>
                        <div class="col-lg-4">
                            <div class="fv-row mb-0 fv-plugins-icon-container">
                                <label for="confirmpassword" class="form-label fs-6 fw-bolder mb-3">Новый пароль ещё раз <span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-lg form-control-solid required" name="confirmpassword" id="confirmpassword">
                                <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            </div>
                        </div>
                        <div class="form-text mb-5">
    {{--                        Password must be at least 8 character and contain symbols--}}
                        </div>
                        <div class="d-flex">
                            <button id="kt_password_submit" type="submit" class="btn btn-primary me-2 px-6">Обновить Пароль</button>
                            <button id="kt_password_cancel" type="button" class="btn btn-color-gray-400 btn-active-light-primary px-6">Отменить</button>
                        </div>
                {!! Form::close() !!}
                <!--end::Form-->
            </div>
            <!--end::Edit-->

            <!--begin::Action-->
            <div id="kt_signin_password_button" class="ms-auto">
                <button class="btn btn-light btn-active-light-primary">Изменить Пароль</button>
            </div>
            <!--end::Action-->
        </div>
        <!--end::Password-->
    </div>
    <!--end::Card body-->
</div>



@push('scripts')
    <script>
        // begin::script
        $( document ).ready(function() {

            // begin::sign-in toggle
            var KTAccountSettingsSigninMethods = {
                init: function() {
                    var t, e;
                    ! function() {
                        var t = document.getElementById("kt_signin_login"),
                            e = document.getElementById("kt_signin_login_edit"),
                            n = document.getElementById("kt_signin_password"),
                            o = document.getElementById("kt_signin_password_edit"),
                            i = document.getElementById("kt_signin_login_button"),
                            s = document.getElementById("kt_signin_cancel"),
                            r = document.getElementById("kt_signin_password_button"),
                            a = document.getElementById("kt_password_cancel");

                        i.querySelector("button").addEventListener("click", () => l());
                        s.addEventListener("click",() => l());
                        r.querySelector("button").addEventListener("click", () => d());
                        a.addEventListener("click", () => d());

                        var l = function() {
                            t.classList.toggle("d-none"), i.classList.toggle("d-none"), e.classList.toggle("d-none")
                        };
                        var d = function() {
                            n.classList.toggle("d-none"), r.classList.toggle("d-none"), o.classList.toggle("d-none")
                        }
                    }();
                }
            };
            KTUtil.onDOMContentLoaded(function() {
                KTAccountSettingsSigninMethods.init()
            });
            // end::sign-in toggle
        });
        // end::script
    </script>
@endpush





