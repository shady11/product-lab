@extends('admin.default')

@section('title', $title)

@section('content')
    <!--begin-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <h4 class="card-title">Редактировать</h4>
                </div>

                {!! Form::model($user, ['route' => ['admin.users.update', $user], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                    @include('admin.users.form', $user)
                {!! Form::close() !!}
            </div>
            <!--end::Card-->

            <!--begin::Card-->
            <div class="card mt-7 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_signin_method">
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Логин и Пароль</h3>
                    </div>
                </div>
                <!--end::Card header-->

                <!--begin::Content-->
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        @include('admin.users.edit-signin', $user)
                    </div>
                </div>
                <!--end::Content-->

            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end-->
@endsection
