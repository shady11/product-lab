<div class="card-body border-top p-9" data-select2-id="select2-data-151-4n1m">

    <!--begin::Alert-->
    <div class="row">
        <div class="col-lg-8 offset-lg-1">
            <div class="alert alert-dismissible bg-primary d-flex flex-column flex-sm-row w-100 p-5 mb-10">
                <!--begin::Icon-->
                <!--begin::Svg Icon | path: icons/duotune/general/gen007.svg-->
                <span class="svg-icon svg-icon-2hx svg-icon-light me-4 mb-5 mb-sm-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M12 22C13.6569 22 15 20.6569 15 19C15 17.3431 13.6569 16 12 16C10.3431 16 9 17.3431 9 19C9 20.6569 10.3431 22 12 22Z" fill="black"></path>
                        <path d="M19 15V18C19 18.6 18.6 19 18 19H6C5.4 19 5 18.6 5 18V15C6.1 15 7 14.1 7 13V10C7 7.6 8.7 5.6 11 5.1V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V5.1C15.3 5.6 17 7.6 17 10V13C17 14.1 17.9 15 19 15ZM11 10C11 9.4 11.4 9 12 9C12.6 9 13 8.6 13 8C13 7.4 12.6 7 12 7C10.3 7 9 8.3 9 10C9 10.6 9.4 11 10 11C10.6 11 11 10.6 11 10Z" fill="black"></path>
                    </svg>
                </span>
                <!--end::Svg Icon-->
                <!--end::Icon-->
                <!--begin::Content-->
                <div class="d-flex flex-column text-light pe-0 pe-sm-10">
                    <h4 class="mb-2 text-light">Предупреждение</h4>
                    <span>Поля с меткой <span class="text-danger font-weight-bold">*</span> является объзательными к заполнению</span>
                </div>
                <!--end::Content-->
                <!--begin::Close-->
                <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-2x svg-icon-light">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </button>
                <!--end::Close-->
            </div>
        </div>
    </div>
    <!--end::Alert-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 text-end col-form-label fw-bold fs-6" for="lastname">Фамилия</label>
        <!--end::Label-->
        <!--begin::Row-->
        <div class="col-lg-6">
            <input type="text" name="lastname" id="lastname" class="form-control" value="{{old('lastname') ?: $user->lastname}}">
        </div>
        <!--end::Row-->
    </div>

    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 text-end col-form-label fw-bold fs-6" for="name">Имя</label>
        <!--end::Label-->
        <!--begin::Row-->
        <div class="col-lg-6">
            <input type="text" name="name" id="name" class="form-control" value="{{old('name') ?: $user->name}}">
        </div>
        <!--end::Row-->
    </div>

    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 text-end col-form-label fw-bold fs-6" for="patronymic">Отчество</label>
        <!--end::Label-->
        <!--begin::Row-->
        <div class="col-lg-6">
            <input type="text" name="patronymic" id="patronymic" class="form-control" value="{{old('patronymic') ?: $user->patronymic}}">
            <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('lastname')}}</div>
        </div>
        <!--end::Row-->
    </div>

    <!--end::Input group-->


    @if(isset($mode) && $mode === 'create')
        <!--begin::Input group-->
        <div class="row mb-6">
            <!--begin::Label-->
            <label class="col-lg-3 text-end col-form-label fw-bold fs-6 required" for="login">Логин</label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-6">
                <input type="text" name="login" id="login" class="form-control" value="{{old('login') ?: $user->login}}">
                @if($errors->has('login'))
                    <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('lastname')}}</div>
                @endif
            </div>

            <!--end::Col-->
        </div>
        <!--end::Input group-->

        <!--begin::Input group-->
        <div class="row mb-6">
            <!--begin::Label-->
            <label class="col-lg-3 text-end col-form-label fw-bold fs-6 required" for="password" >Пароль</label>

            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-6 fv-row fv-plugins-icon-container">
                <input type="password" name="password" id="password" class="form-control" placeholder="Пароль">
                @if($errors->has('password'))
                    <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('password')}}</div>
                @endif
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
    @endif


    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 text-end col-form-label fw-bold fs-6" for="email">Email</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{old('email') ?: $user->email}}">
            @if($errors->has('email'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('email')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 text-end col-form-label fw-bold fs-6">Роль</label>
        <!--begin::Label-->
        <!--begin::Label-->
        <div class="col-lg-6 d-flex align-items-center">
            {!! Form::select('roles', $roles, null,['class' => 'form-select', 'data-control'=>'select2', 'placeholder'=>'-- Выберите --']) !!}
        </div>
        <!--begin::Label-->
    </div>

    <!--begin::Input group-->
    <div class="row">
        <!--begin::Label-->
        <label class="col-lg-3 text-end col-form-label fw-bold fs-6" id="status-display">Статуc</label>
        <!--begin::Label-->
        <!--begin::Label-->
        <div class="col-lg-6 d-flex align-items-center">
            <div class="form-check form-check-solid form-switch fv-row align-items-center">
                <label class="form-check-label" for="status"></label>
                <input class="form-check-input w-45px h-30px" type="checkbox" name="status-checkbox" id="status-checkbox" @if($user->status == 1) checked="checked" @endif>
            </div>
        </div>
        <!--begin::Label-->
    </div>

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success" id="kt_account_profile_details_submit">Сохранить</button>
            <button type="reset" class="btn btn-secondary btn-active-secondary-primary me-2" onclick="window.history.back()">Назад</button>
        </div>
    </div>
</div>


@push('scripts')
    <script>
        $( document ).ready(function() {
            // begin::monitoring status update
            function statusMonitoring(checkbox, msg) {
                if (checkbox.checked) {
                    msg.innerHTML = "Статус <span> (активный)</span>";
                } else {
                    msg.innerHTML = "Статус <span class='text-muted'> (неактивный)</span>";
                }
            }

            var status = document.getElementById('status-checkbox'),
                statusMsg = document.getElementById('status-display');

            statusMonitoring(status, statusMsg);   // run in start
            status.onchange = () => statusMonitoring(status, statusMsg); // run in changes checkbox
            // end::monitoring status update
        });
    </script>
@endpush




