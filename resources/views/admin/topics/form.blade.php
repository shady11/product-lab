<div class="card-body">
    <div class="form-group row">
        {!! Form::label('name', 'Название', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if($errors->has('name'))
                <div class="error-message">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <br>
    <div class="form-group row">
        {!! Form::label('description', 'Описание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '6']) !!}
        </div>
    </div>
    <br>

    <div class="form-group row">
        {!! Form::label('dateactive', 'Дата', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('dateactive', null, ['class' => 'form-control', 'placeholder' => 'Выберите дату', 'id' => 'datepicker_date']) !!}
            @if($errors->has('dateactive'))
                <div class="error-message">{{ $errors->first('dateactive') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-md-6">
            <button type="submit" class="btn btn-success mr-2">Сохранить</button>
            <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад
            </button>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $("#datepicker_date").flatpickr({
            dateFormat: "Y-m-d",
        });
    </script>
@endsection
