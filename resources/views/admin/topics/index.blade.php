@extends('admin.default')

@section('title', $title)

@section('pre_styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!--begin::Row-->
    <div class="row gy-0 mb-6 mb-xl-12">
        <!--begin::Col-->
        @foreach($topics as $key=>$topic)
            <div class="col-md-6 mb-6">
                <div class="card h-xl-100">
                    <!--begin::Header-->
                    <div class="card-header py-6">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-boldest text-uppercase text-dark fs-4">{{ $topic->name }}</span>
                        </h3>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <button type="button" class="btn btn-sm btn-icon btn-color-dark btn-active-light-dark"
                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                         viewBox="0 0 24 24">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="5" y="5" width="5" height="5" rx="1"
                                                  fill="#000000"></rect>
                                            <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                  opacity="0.3"></rect>
                                            <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                  opacity="0.3"></rect>
                                            <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                  opacity="0.3"></rect>
                                        </g>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                            <!--begin::Menu 3-->
                            <div
                                class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3"
                                data-kt-menu="true" style="">
                                <!--begin::Heading-->
                                <div class="menu-item px-3">
                                    <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Действия</div>
                                </div>
                                <!--end::Heading-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="{{ route('admin.topics.edit', $topic) }}" class="menu-link px-3">Редактировать</a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="{{ route('admin.topics.delete', $topic) }}"
                                       class="menu-link px-3">Удалить</a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                            <!--end::Menu 3-->
                            <!--end::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                    @foreach($topic->materials as $material)
                        @if(!$loop->first) <div class="separator separator-dashed mb-4"></div> @endif
                        <!--begin::Item-->
                        <div class="d-flex align-items-sm-center mb-7">
                            <!--begin::Symbol-->
                            <div class="symbol symbol-50px me-3">
                                <div class="symbol-label bg-light-info">
                                    <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm010.svg-->
                                    <span class="svg-icon svg-icon-2x svg-icon-info">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M16.0077 19.2901L12.9293 17.5311C12.3487 17.1993 11.6407 17.1796 11.0426 17.4787L6.89443 19.5528C5.56462 20.2177 4 19.2507 4 17.7639V5C4 3.89543 4.89543 3 6 3H17C18.1046 3 19 3.89543 19 5V17.5536C19 19.0893 17.341 20.052 16.0077 19.2901Z"
                                                fill="black"/>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Symbol-->
                            <!--begin::Section-->
                            <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                                <div class="flex-grow-1 me-3">
                                    <a href="{{ route('admin.materials.show', $material) }}" class="text-gray-800 text-hover-info fs-6 fw-bolder">
                                        {{ $material->name }}
                                    </a>
                                </div>
                                <div>
                                    <!--begin::Menu-->
                                    <button type="button"
                                            class="btn btn-icon btn-color-info btn-active-light-info"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <span class="svg-icon svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                     viewBox="0 0 24 24">
                                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                          fill="black"/>
                                                    <rect x="11" y="11" width="2" height="2" rx="1" fill="black"/>
                                                    <rect x="11" y="15" width="2" height="2" rx="1" fill="black"/>
                                                    <rect x="11" y="7" width="2" height="2" rx="1" fill="black"/>
                                                </svg>
                                            </span>
                                        <!--end::Svg Icon-->
                                    </button>
                                    <!--begin::Menu 3-->
                                    <div
                                        class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-info fw-bold w-200px py-3"
                                        data-kt-menu="true" style="">
                                        <!--begin::Heading-->
                                        <div class="menu-item px-3">
                                            <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">
                                                Действия
                                            </div>
                                        </div>
                                        <!--end::Heading-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="{{ route('admin.materials.edit', $material) }}" class="menu-link px-3">Редактировать</a>
                                        </div>
                                        <!--end::Menu item-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="{{ route('admin.materials.delete', $material) }}" class="menu-link px-3">Удалить</a>
                                        </div>
                                        <!--end::Menu item-->
                                    </div>
                                    <!--end::Menu 3-->
                                    <!--end::Menu-->
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Item-->
                    @endforeach
                    @foreach($topic->exercises as $exercise)
                        <div class="separator separator-dashed mb-4"></div>
                        <!--begin::Item-->
                        <div class="d-flex align-items-sm-center mb-7">
                            <!--begin::Symbol-->
                            <div class="symbol symbol-50px me-3">
                                <div class="symbol-label bg-light-primary">
                                    <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm010.svg-->
                                    <span class="svg-icon svg-icon-2x svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3"
                                                  d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                                  fill="black"/>
                                            <rect x="7" y="17" width="6" height="2" rx="1" fill="black"/>
                                            <rect x="7" y="12" width="10" height="2" rx="1" fill="black"/>
                                            <rect x="7" y="7" width="6" height="2" rx="1" fill="black"/>
                                            <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Symbol-->
                            <!--begin::Section-->
                            <div class="d-flex align-items-center flex-row-fluid flex-nowrap">
                                <div class="flex-grow-1 me-3">
                                    <a href="{{ route('admin.exercises.show', $exercise) }}"
                                       class="text-gray-800 text-hover-primary fs-6 fw-bolder">{{ $exercise->name }}</a>
                                </div>
                                <div class="d-flex align-items-center">
                                    @if($exercise->type == 1)
                                        <span class="badge badge-light-primary fw-bolder my-2 ">Индивидуальное</span>
                                    @else
                                        <span class="badge badge-light-primary fw-bolder my-2 ">Командное</span>
                                    @endif
                                    <div class="ms-3">
                                        <!--begin::Menu-->
                                        <button type="button"
                                                class="btn btn-icon btn-color-primary btn-active-light-primary"
                                                data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                            <span class="svg-icon svg-icon-2x">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                         viewBox="0 0 24 24">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                              fill="black"/>
                                                        <rect x="11" y="11" width="2" height="2" rx="1" fill="black"/>
                                                        <rect x="11" y="15" width="2" height="2" rx="1" fill="black"/>
                                                        <rect x="11" y="7" width="2" height="2" rx="1" fill="black"/>
                                                    </svg>
                                                </span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <!--begin::Menu 3-->
                                        <div
                                            class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3"
                                            data-kt-menu="true" style="">
                                            <!--begin::Heading-->
                                            <div class="menu-item px-3">
                                                <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">
                                                    Действия
                                                </div>
                                            </div>
                                            <!--end::Heading-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="{{ route('admin.exercises.edit', $exercise) }}"
                                                   class="menu-link px-3">Редактировать</a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="{{ route('admin.exercises.delete', $exercise) }}"
                                                   class="menu-link px-3">Удалить</a>
                                            </div>
                                            <!--end::Menu item-->
                                        </div>
                                        <!--end::Menu 3-->
                                        <!--end::Menu-->
                                    </div>
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Item-->
                    @endforeach
                    </div>
                    <!--end::Body-->
                </div>
            </div>
            <!--end::Col-->
        @endforeach
    </div>
    <!--end::Row-->

@endsection

@section('scripts')

    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

    <script>
        let table = $('#dataTable').DataTable({
            // dom: 'Btrlip',
            dom: 'tr' +
                '<"row"<"col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"li><"col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end dataTables_pager"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.topics.index') }}',
                data: function (d) {
                }
            },
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'actions', className: 'text-end'},
            ],
            order: [[0, "desc"]],
            pageLength: 20,
            lengthMenu: ['20', '30', '40', '50', '100'],
            language: {
                "url": "{{asset('assets/js/russian.json')}}"
            },
        });

        // Search Datatable
        const filterSearch = $('#search_handler');

        filterSearch.keyup(function (e) {
            table.search(e.target.value).draw();
        });

    </script>
@endsection
