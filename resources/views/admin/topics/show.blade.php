@extends('admin.default')

@section('title', $title)

@section('content')

    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-0 pt-6">
            <!--begin::Card title-->
            <div class="card-title">
            </div>
            <!--begin::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <h2>Тема - {{ $row->name }}</h2>
                </div>
                <!--end::Toolbar-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body py-4">
            <div class="card-header">
                <h3 class="card-title">Все материалы</h3>
                <div class="card-toolbar">
                    <a href="{{ route('admin.material.create_', $row) }}" class="btn btn-sm btn-success">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
                        <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none">
                                    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1"
                                          transform="rotate(-90 11.364 20.364)" fill="black"></rect>
                                    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>
                                </svg>
                            </span>
                        <!--end::Svg Icon-->
                        Добавить материал
                    </a>
                </div>
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body pb-5">
                <!--begin::Table wrapper-->
                <div class="table-responsive">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                        <!--begin::Table head-->
                        <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                        <!--begin::Table row-->
                        <tr class="text-start text-muted text-uppercase gs-0">
                            <th class="w-40px">№</th>
                            <th>Тема</th>
                            <th>Название</th>
                            <th>описание</th>
                            <th class="min-w-125px text-end">Действия</th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fs-6 fw-bold text-gray-600">
                        @foreach($materials as $key->$m)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $p->getTopic() }}</td>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->description }}</td>
                                <td class="text-end">
                                    <a href="{{ route('admin.materials.delete', $row) }}"
                                       class="btn btn-sm btn-light-danger" title="Удалить">
                                        Удалить
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <!--end::Table body-->
                    </table>

                    <!--end::Table-->
                </div>
                <!--end::Table wrapper-->
            </div>
        </div>


        <div class="card-body py-4">
            <div class="card-header">
                <h3 class="card-title">Все задание</h3>
                <div class="card-toolbar">
                    <a href="{{ route('admin.exercise.create_', $row) }}" class="btn btn-sm btn-success">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
                        <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none">
                                    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1"
                                          transform="rotate(-90 11.364 20.364)" fill="black"></rect>
                                    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>
                                </svg>
                            </span>
                        <!--end::Svg Icon-->
                        Добавить задание
                    </a>
                </div>
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body pb-5">
                <!--begin::Table wrapper-->
                <div class="table-responsive">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                        <!--begin::Table head-->
                        <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                        <!--begin::Table row-->
                        <tr class="text-start text-muted text-uppercase gs-0">
                            <th class="w-40px">№</th>
                            <th>Тема</th>
                            <th>Наименование задания</th>
                            <th>Срок выполнения</th>
                            <th>Оценка</th>
                            <th>Ссылка на задание</th>
                            <th class="min-w-125px text-end">Действия</th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fs-6 fw-bold text-gray-600">
                        @foreach($exercises as $key=>$p)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $p->getTopic() }}</td>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->deadline }}</td>
                                <td>{{ $p->grade }}</td>
                                <td>{{ $p->link }}</td>
                                <td class="text-end">
                                    <a href="{{route('admin.exercises.edit', $p) }}" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Редактировать">
                                        <i class="las la-pen fs-4 text-success"></i>
                                    </a>
                                    <a href="{{route('admin.exercises.delete', $p) }}" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Удалить">
                                        <i class="las la-times fs-4 text-danger"></i>
                                    </a>
                                    <a href="{{route('admin.exercises.show', $p) }}" class="btn btn-sm btn-icon btn-light btn-active-light-danger" title="Посмотреть">
                                        <i class="las la-eye fs-4 text-warning"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <!--end::Table body-->
                    </table>

                    <!--end::Table-->
                </div>
                <!--end::Table wrapper-->
            </div>
        </div>
        <!--end::Card body-->
    </div>

@endsection

@section('scripts')

@endsection
