@extends('admin.default')
@section('title', $title)
@section('content')


    {!! Form::model($row, ['route' => 'admin.exercises.store','enctype' => 'multipart/form-data']) !!}

        <div class="row">
            <div class="col-md-5">
                <!--begin::Card-->
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <div class="d-flex align-items-center position-relative my-1">
                                <h3>{{ $title }}</h3>
                            </div>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card title-->
                    </div>
                    <!--end::Card header-->

                    @include('admin.exercises.form_estimate', $row)

                </div>
            </div>

            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-9">
                            <div class="col-xl-12">

                                <h3>Участники</h3>

                                <div class="table-responsive">
                                    <!--begin::Table-->
                                    <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                                        <!--begin::Table head-->
                                        <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                                        <!--begin::Table row-->
                                        <tr class="text-start text-muted text-uppercase gs-0">
                                            <th class="min-w-100px">№</th>
                                            <th>ФИО Участников</th>
                                            <th>Email адресс</th>
                                            <th>Оценка</th>
                                            <th class="min-w-125px text-end">Оценить</th>
                                        </tr>
                                        <!--end::Table row-->
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody class="fs-6 fw-bold text-gray-600">
                                        @foreach($participants as $key=>$p)
                                            <tr>
                                                <!--begin::Invoice=-->
                                                <td>{{$key+1}}</td>
                                                <!--end::Invoice=-->
                                                <!--begin::Status=-->
                                                <td>{{ $p->name }} {{ $p->lastname }}</td>
                                                <!--end::Status=-->
                                                <!--begin::Amount=-->
                                                <td>{{ $p->email }}</td>
                                                <!--end::Amount=-->
                                                <!--begin::Amount=-->
                                                <td>{{ $p->exercise($row->id) ? $p->exercise($row->id)->grade : '-' }}</td>
                                                <!--end::Amount=-->
                                                <!--begin::Date=-->
                                                <td class="text-end">
                                                    @if($p->exercise($row->id) && $p->exercise($row->id)->file)
                                                        @if($row->status($p->id))
                                                            <a href="#" class="btn btn-sm btn-success btn-estimate" data-participant="{{$p->id}}" data-bs-toggle="modal" data-bs-target="#kt_modal_update_details">
                                                                Изменить
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-sm btn-primary btn-estimate" data-participant="{{$p->id}}" data-bs-toggle="modal" data-bs-target="#kt_modal_update_details">
                                                                Оценить
                                                            </a>
                                                        @endif
                                                    @else
                                                        <a class="btn btn-sm btn-danger" disabled>
                                                            Не выполнено
                                                        </a>
                                                    @endif
                                                </td>
                                                <!--end::Date=-->
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <!--end::Table body-->
                                    </table>

                                </div>
                            </div>
                            <div class="col-xl-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <h3>Команды</h3>

                                <div class="table-responsive">
                                    <!--begin::Table-->
                                    <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                                        <!--begin::Table head-->
                                        <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                                        <!--begin::Table row-->
                                        <tr class="text-start text-muted text-uppercase gs-0">
                                            <th class="min-w-100px">№</th>
                                            <th>Наименование</th>
                                            <th>Количество участников</th>
                                            <th>Оценка</th>
                                            <th class="min-w-125px text-end">Действия</th>
                                        </tr>
                                        <!--end::Table row-->
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody class="fs-6 fw-bold text-gray-600">
                                        @foreach($teams as $key => $p)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{ $p->name }}</td>
                                                <td>{{ $p->participants->count() }}</td>
                                                <td>{{ $p->exercise($row->id) ? $p->exercise($row->id)->grade : '-' }}</td>
                                                <td class="text-end">
                                                    @if($p->exercise($row->id) && $p->exercise($row->id)->file)
                                                        @if($row->status_team($p->id))
                                                            <a href="#" class="btn btn-sm btn-success btn-estimate-team" data-team="{{$p->id}}" data-bs-toggle="modal" data-bs-target="#kt_modal_update_details_team">
                                                                Изменить
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-sm btn-primary btn-estimate-team" data-team="{{$p->id}}" data-bs-toggle="modal" data-bs-target="#kt_modal_update_details_team">
                                                                Оценить
                                                            </a>
                                                        @endif
                                                    @else
                                                        <a class="btn btn-sm btn-danger" disabled>
                                                            Не выполнено
                                                        </a>
                                                    @endif
                                                </td>
                                                <!--end::Date=-->
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <!--end::Table body-->
                                    </table>

                                </div>
                            </div>
                            <div class="col-xl-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}

{{--  Participant  --}}
    <div class="modal fade" id="kt_modal_update_details" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Form-->
            {!! Form::open(['route' => ['admin.exercise.participant.grade', $row], 'method'=>'POST', 'class'=>'form', 'id'=>'modal_exercise_participant_grade', 'enctype'=>'multipart/form-data']) !!}
            <!--begin::Modal header-->
                <div class="modal-header px-lg-12" id="kt_modal_update_user_header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder">Оценить задание</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                    <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                </svg>
                            </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body py-10 px-lg-12">
                    <!--begin::Scroll-->
                    <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_update_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_user_header" data-kt-scroll-wrappers="#kt_modal_update_user_scroll" data-kt-scroll-offset="300px">
                        <!--begin::User form-->
                        <div id="kt_modal_update_user_user_info">
                            <!--begin::Input group-->
                            <div class="row align-items-center mb-7">
                                <!--begin::Label-->
                                <label class="col-md-3 fs-6 fw-bolder">Ф.И.О. участника</label>
                                <!--end::Label-->
                                <div class="col-md-9">
                                    <div id="participantFullName" class="fs-5"></div>
                                </div>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="row align-items-center mb-7">
                                <!--begin::Label-->
                                <label class="col-md-3 fs-6 fw-bolder">Загруженные данные</label>
                                <!--end::Label-->
                                <div class="col-md-9">
                                    <div id="participantExerciseFile" class="fs-5"></div>
                                </div>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="row align-items-center mb-7">
                                <!--begin::Label-->
                                <label class="col-md-3 fs-6 fw-bolder">Оценка</label>

                                <div class="col-md-9">
                                    {!! Form::text('grade', null, ['id'=>'autograde', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::User form-->
                    </div>
                    <!--end::Scroll-->
                </div>
                <!--end::Modal body-->
                <!--begin::Modal footer-->
                <div class="modal-footer flex-center">
                    <!--begin::Button-->
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Отмена</button>
                    <!--end::Button-->
                    <!--begin::Button-->
                    <button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
                        <span class="indicator-label">Сохранить</span>
                    </button>
                    <!--end::Button-->
                </div>
                <!--end::Modal footer-->
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
        </div>
    </div>

{{--  Team  --}}
    <div class="modal fade" id="kt_modal_update_details_team" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Form-->
            {!! Form::open(['route' => ['admin.exercise.team.grade', $row], 'method'=>'POST', 'class'=>'form', 'id'=>'modal_exercise_team_grade', 'enctype'=>'multipart/form-data']) !!}
            <!--begin::Modal header-->
                <div class="modal-header px-lg-12" id="kt_modal_update_user_header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder">Оценить задание</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                    <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                </svg>
                            </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body py-10 px-lg-12">
                    <!--begin::Scroll-->
                    <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_update_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_user_header" data-kt-scroll-wrappers="#kt_modal_update_user_scroll" data-kt-scroll-offset="300px">
                        <!--begin::User form-->
                        <div id="kt_modal_update_user_user_info">
                            <!--begin::Input group-->
                            <div class="row align-items-center mb-7">
                                <!--begin::Label-->
                                <label class="col-md-3 fs-6 fw-bolder">Название</label>
                                <!--end::Label-->
                                <div class="col-md-9">
                                    <div id="teamName" class="fs-5"></div>
                                </div>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="row align-items-center mb-7">
                                <!--begin::Label-->
                                <label class="col-md-3 fs-6 fw-bolder">Загруженные данные</label>
                                <!--end::Label-->
                                <div class="col-md-9">
                                    <div id="teamExerciseFile" class="fs-5"></div>
                                </div>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="row align-items-center mb-7">
                                <!--begin::Label-->
                                <label class="col-md-3 fs-6 fw-bolder">Оценка</label>

                                <div class="col-md-9">
                                    {!! Form::text('grade', null, ['id'=>'autograde_team', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::User form-->
                    </div>
                    <!--end::Scroll-->
                </div>
                <!--end::Modal body-->
                <!--begin::Modal footer-->
                <div class="modal-footer flex-center">
                    <!--begin::Button-->
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Отмена</button>
                    <!--end::Button-->
                    <!--begin::Button-->
                    <button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
                        <span class="indicator-label">Сохранить</span>
                    </button>
                    <!--end::Button-->
                </div>
                <!--end::Modal footer-->
            {!! Form::close() !!}
            <!--end::Form-->
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
         $('.btn-estimate').on('click', function () {
            let id = $(this).data('participant');
            $.ajax({
                url: "{{route('admin.exercise.participant.info', $row)}}",
                cache: false,
                method: 'POST',
                data: {
                    'participant_id': id
                }
            }).done(function(data) {
                console.log(data);

                $('#participantFullName').html(data['full_name']);
                if (data['grade'] != null){
                    $('#autograde').val(data['grade']);
                }else {
                    if (data['autograde'] != null){
                        $('#autograde').val(data['autograde']);
                    }
                }
                if(data['file']){
                    $('#participantExerciseFile').html('<a href="'+data['file']+'" target="_blank">Ссылка на файл</a>');
                }
                $('#modal_exercise_participant_grade').append('<input type="hidden" name="participant_id" value="'+data['id']+'">')
            });

        });

         $('.btn-estimate-team').on('click', function () {
             let id = $(this).data('team');
             $.ajax({
                 url: "{{route('admin.exercise.team.info', $row)}}",
                 cache: false,
                 method: 'POST',
                 data: {
                     'team_id': id
                 }
             }).done(function(data) {
                 console.log(data);

                 $('#teamName').html(data['teamName']);
                 if (data['grade'] != null){
                     $('#autograde_team').val(data['grade']);
                 }else {
                     if (data['autograde'] != null){
                         $('#autograde_team').val(data['autograde']);
                     }
                 }
                 if(data['file']){
                     $('#teamExerciseFile').html('<a href="'+data['file']+'" target="_blank">Ссылка на файл</a>');
                 }
                 $('#modal_exercise_team_grade').append('<input type="hidden" name="team_id" value="'+data['id']+'">')
             });

         });

        @if(session('exercise-graded'))
            Swal.fire({
                html: "<div class='fw-bold fs-4'>Оценка на задание добавлена!</div>",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Продолжить",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        @elseif(session('exercise-not-graded'))
            Swal.fire({
                html: "<div class='fw-bold fs-4'>Задание еще не выполнено!</div>",
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Продолжить",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        @endif

{{--        Team  --}}
        @if(session('exercise-graded-team'))
            Swal.fire({
                html: "<div class='fw-bold fs-4'>Оценка на задание добавлена!</div>",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Продолжить",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        @elseif(session('exercise-not-graded-team'))
            Swal.fire({
                html: "<div class='fw-bold fs-4'>Задание еще не выполнено!</div>",
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Продолжить",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        @endif

    </script>

@endsection
