<div class="card-body">
    <div class="row mb-6">
        {!! Form::label('topic', 'Тема', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            {{$row->topic ? $row->topic->name : '-'}}
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('name', 'Наименование задания', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            {{$row->name}}
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('description', 'Описание задания', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            {!! $row->description ? $row->description : '-' !!}
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('link', 'Ссылка на задание', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12">
            <div class="col-lg-12 fs-5">
                {{ $row->link ? $row->link : '-' }}
            </div>
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('deadline', 'Срок выполнения', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            {{$row->deadline ? $row->deadline : '-'}}
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('type', 'Тип задания', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            @if($row->type == 1)
                <span class="badge badge-primary fw-bolder my-2 ">Индивидуальное</span>
            @else
                <span class="badge badge-primary fw-bolder my-2 ">Командное</span>
            @endif
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('type', 'Тип начисления баллов', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            @if($row->autograde == 1)
                <span class="badge badge-primary fw-bolder my-2 ">Ручной</span>
            @else
                <span class="badge badge-primary fw-bolder my-2 ">Автоматический</span>
            @endif
        </div>
    </div>
    <div class="row mb-6">
        {!! Form::label('grade', 'Баллы за выполнение задания', ['class' => 'col-lg-12 col-form-label fw-boldest text-gray-500 text-uppercase']) !!}
        <div class="col-lg-12 fs-5">
            {{$row->grade ? $row->grade : '-'}}
        </div>
    </div>
    <!--end::Table-->
</div>
<div class="card-footer d-flex justify-content-end">
    <button type="reset" onclick="window.history.back();" class="btn btn-secondary me-2">Назад</button>
    <a href="{{route('admin.exercises.edit', $row)}}" class="btn btn-primary">Редактировать</a>
</div>
