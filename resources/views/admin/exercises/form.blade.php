<div class="card-body">
    <div class="form-group row mb-9">
        {!! Form::label('name', 'Наименование задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if($errors->has('name'))
                <div class="error-message">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row mb-9">
        <!--begin::Label-->
        {!! Form::label('description', 'Описание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row">
            {!! Form::text('description', null, ['id' => 'description', 'class' => 'form-control']) !!}
            @if($errors->has('description'))
                <div class="error-message">{{ $errors->first('description') }}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <div class="form-group row mb-9">
        {!! Form::label('link', 'Ссылка на задание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('link', null, ['class' => 'form-control']) !!}
            @if($errors->has('link'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row mb-9">
        {!! Form::label('deadline', 'Срок выполнения', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('deadline', null, ['class' => 'form-control', 'placeholder' => 'Выберите дату', 'id' => 'datepicker_date']) !!}
            @if($errors->has('deadline'))
                <div class="error-message">{{ $errors->first('deadline') }}</div>
            @endif
        </div>
    </div>
    <div class="radio">

        <div class="form-radio row mb-9">
            {!! Form::label('type', 'Тип задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9 py-3">
                <div class="form-check form-check-inline" id="individual">
                    <input class="form-check-input" type="radio" name="type" id="type" value="1" @if($row->type == 1 or old('type') == 1) checked @endif>
                    <label class="form-check-label" for="type1">
                        Индивидуальное задание
                    </label>
                </div>
                <div class="form-check form-check-inline" id="team">
                    <input class="form-check-input" type="radio" name="type" id="type2" value="2" @if($row->type == 2 or old('type') == 2) checked @endif>
                    <label class="form-check-label" for="type2">
                        Командное задание
                    </label>
                </div>
                @if($errors->has('type'))
                    <div class="error-message">{{ $errors->first('type') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="radio">
        <div class="form-radio row mb-9">
            {!! Form::label('autograde', 'Тип начисления баллов', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9 py-3">
                @if($row->autograde==1 or old('autograde') == 1)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="autograde" id="grade_type" value="1" checked onclick="hideGrade()">
                        <label class="form-check-label" for="grade_type">
                            Ручной
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="autograde" id="grade_type2" value="2" onclick="showGrade()">
                        <label class="form-check-label" for="grade_type2">
                            Автоматический
                        </label>
                    </div>
                @elseif($row->autograde==2 or old('autograde') == 2)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="autograde" id="grade_type" value="1" onclick="hideGrade()">
                        <label class="form-check-label" for="grade_type">
                            Ручной
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="autograde" id="grade_type2" value="2" checked onclick="showGrade()">
                        <label class="form-check-label" for="grade_type2">
                            Автоматический
                        </label>
                    </div>
                @else
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="autograde" id="grade_type" value="1" onclick="hideGrade()">
                        <label class="form-check-label" for="grade_type">
                            Ручной
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="autograde" id="grade_type2" value="2" onclick="showGrade()">
                        <label class="form-check-label" for="grade_type2">
                            Автоматический
                        </label>
                    </div>
                @endif
                @if($errors->has('autograde'))
                    <div class="error-message">{{ $errors->first('autograde') }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row mb-9" id="grade" @if(old('autograde') == 2) style="display: flex" @elseif($row->autograde!=2) style="display: none" @endif>
        {!! Form::label('grade', 'Баллы за выполнение задания', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-3">
            {!! Form::number('grade', null, ['class' => 'form-control']) !!}
            @if($errors->has('grade'))
                <div class="error-message">{{ $errors->first('grade') }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row mb-9" id="percent" @if(old('autograde') == 2) style="display: flex" @elseif($row->autograde!=2) style="display: none" @endif>
        {!! Form::label('percent', 'Процент (%)', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-3">
            {!! Form::number('percent', null, ['class' => 'form-control']) !!}
            @if($errors->has('percent'))
                <div class="error-message">{{ $errors->first('percent') }}</div>
            @endif
        </div>
    </div>
    @if(isset($topic))
        <div class="form-group row">
            {!! Form::label('topic_id', 'Тема', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9">
                {!! Form::select('topic_id', [null=>'- выбрать -']+$topics, [$topic->id],['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
                @if($errors->has('topic_id'))
                    <div class="error-message">{{ $errors->first('topic_id') }}</div>
                @endif
            </div>
        </div>
    @else
        <div class="form-group row">
            {!! Form::label('topic_id', 'Тема', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9">
                {!! Form::select('topic_id', [null=>'- выбрать -']+$topics, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
            @if($errors->has('topic_id'))
                    <div class="error-message">{{ $errors->first('topic_id') }}</div>
            @endif
            </div>
        </div>
    @endif

</div>
<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success mr-2">Сохранить</button>
            <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад
            </button>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $("#datepicker_date").flatpickr({
            dateFormat: "d-m-Y",
        });
        function showGrade(){
            $('#grade').show();
            $('#percent').show();
        }
        function hideGrade(){
            $('#grade').hide();
            $('#percent').hide();
        }
    </script>

    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
    <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>

    <script>
        $(document).ready(function () {
            // tinyMCE editor
            tinyMCE.init({
                selector: '#description',
                plugins: "autoresize link lists media image  table textcolor lists code quickbars paste",
                menubar: false,
                toolbar: [
                    'undo redo | styleselect | bold italic link | alignleft aligncenter alignright alignjustify | outdent indent | code',
                ]
            });

            // date pick plugin
            $("#published_at").flatpickr({
                locale: "ru",
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                time_24hr: true
            });
        });
    </script>
@endsection
