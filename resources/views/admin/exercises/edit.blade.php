@extends('admin.default')
@section('title', $title)
@section('content')
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <h3>{{ $title }}</h3>
                </div>
                <!--end::Search-->
            </div>
            <!--begin::Card title-->
        </div>
        <!--end::Card header-->
    {!! Form::model($row, ['route' => ['admin.exercises.update', $row], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
    @include('admin.exercises.form', $row)
    {!! Form::close() !!}
    <!--end::Datatable-->
    </div>
@endsection
@section('scripts')
@endsection
