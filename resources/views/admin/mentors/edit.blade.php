@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <div class="card card-custom">
                <div class="card-header">
                    <h4 class="card-title">Редактировать</h4>
                </div>

                {!! Form::model($mentor, ['route' => ['admin.landing.mentors.update', $mentor], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                    @include('admin.mentors.form', $mentor)
                {!! Form::close() !!}

            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection



