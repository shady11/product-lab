@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <div class="card card-custom">

                <div class="card-header cursor-pointer">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h4 class="card-title">Посмотреть</h4>
                    </div>
                    <!--end::Card title-->

                    <!--begin::Action-->
                    <div class="d-flex">
                        <a href="{{route('admin.regions.index')}}" class="me-3  btn btn-sm btn-light-dark btn-hover-scale align-self-center">Назад</a>
                        <a href="{{route('admin.regions.edit', $region)}}" class="me-3 btn btn-sm btn-light-primary btn-hover-scale align-self-center">Редактировать</a>
                        <a href="{{route('admin.regions.delete', $region)}}" class="btn btn-sm btn-light-danger btn-hover-scale align-self-center">Удалить</a>
                    </div>
                    <!--end::Action-->
                </div>


                <div class="card-body p-9">
                    <!--begin::Row-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Имя на русском</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$region->name_ru}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Имя на кыргызском</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$region->name}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                </div>

            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection













