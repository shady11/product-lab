<div class="card-body border-top p-9" data-select2-id="select2-data-151-4n1m">
    <!--begin::Alert-->
    <div class="row">
        <div class="col-lg-8 offset-lg-1">
            <div class="alert alert-dismissible bg-primary d-flex flex-column flex-sm-row w-100 p-5 mb-10">
                <!--begin::Icon-->
                <!--begin::Svg Icon | path: icons/duotuner/general/gen007.svg-->
                <span class="svg-icon svg-icon-2hx svg-icon-light me-4 mb-5 mb-sm-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M12 22C13.6569 22 15 20.6569 15 19C15 17.3431 13.6569 16 12 16C10.3431 16 9 17.3431 9 19C9 20.6569 10.3431 22 12 22Z" fill="black"></path>
                        <path d="M19 15V18C19 18.6 18.6 19 18 19H6C5.4 19 5 18.6 5 18V15C6.1 15 7 14.1 7 13V10C7 7.6 8.7 5.6 11 5.1V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V5.1C15.3 5.6 17 7.6 17 10V13C17 14.1 17.9 15 19 15ZM11 10C11 9.4 11.4 9 12 9C12.6 9 13 8.6 13 8C13 7.4 12.6 7 12 7C10.3 7 9 8.3 9 10C9 10.6 9.4 11 10 11C10.6 11 11 10.6 11 10Z" fill="black"></path>
                    </svg>
                </span>
                <!--end::Svg Icon-->
                <!--end::Icon-->
                <!--begin::Content-->
                <div class="d-flex flex-column text-light pe-0 pe-sm-10">
                    <h4 class="mb-2 text-light">Предупреждение</h4>
                    <span>Поля с меткой <span class="text-danger font-weight-bold">*</span> является объзательными к заполнению</span>
                </div>
                <!--end::Content-->
                <!--begin::Close-->
                <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                <span class="svg-icon svg-icon-2x svg-icon-light">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                    </svg>
                </span>
                <!--end::Svg Icon-->
                </button>
                <!--end::Close-->
            </div>
        </div>
    </div>
    <!--end::Alert-->

    <!--begin::Input group-->
    <div class="row mb-3">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="name_ru">Имя на русском</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            <input type="text" name="name_ru" class="form-control" value="{{ old('name_ru') ?: $region->name_ru}}">
            @if($errors->has('name_ru'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('name_ru')}}</div>
            @endif
        </div>
        <!--end::Col-->

    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-3">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="name">Имя на кыргызском</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            <input type="text" name="name" class="form-control" value="{{ old('name') ?: $region->name}}">
            @if($errors->has('name'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('name')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success" id="kt_account_profile_details_submit">Сохранить</button>
            <button type="reset" class="btn btn-secondary btn-active-secondary-primary me-2" onclick="window.history.back()">Назад</button>
        </div>
    </div>
</div>




@push('scripts')
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

    <script>
        $( document ).ready(function() {
            // custom format style
            tinyMCE.init({
                selector: '#content,#content_ru',
                plugins: "autoresize link lists media image  table textcolor lists code quickbars paste",
                menubar: false,
                toolbar: [
                    'undo redo | styleselect | bold italic link | alignleft aligncenter alignright alignjustify | outdent indent | code',
                ]
            });
        });
    </script>
@endpush

