@extends('admin.default')

@section('title', 'Мероприятия')

@push('styles')
    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar4.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar_theme.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="card">
    <div class="card-header">
        <h2 class="card-title fw-bolder">Мероприятия</h2>
        <div class="card-toolbar">
            <button class="btn btn-flex btn-success" data-bs-toggle="modal" data-bs-target="#kt_modal_add_event">
                <span class="svg-icon svg-icon-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>
                        <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>
                    </svg>
                </span>
                Добавить мероприятие
            </button>
        </div>
    </div>
    <div class="card-body">
        <div id="kt_calendar_app"></div>
    </div>
</div>

<div class="modal fade" id="kt_modal_add_event" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">
            {!! Form::open(['route' => 'admin.events.data.add', 'method' => 'POST', 'class' => 'form', 'id' => 'add_event_form']) !!}
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder">Добавить мероприятие</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"/>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"/>
                            </svg>
                        </span>
                    </div>
                    <!--end::Close-->
                </div>
                <div class="modal-body">
                    <div class="fv-row mb-9">
                        <label class="required fs-5 fw-bold mb-2">Наименование мероприятия:</label>
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="fv-row mb-9">
                        <label class="fs-5 fw-bold mb-2">Описание мероприятия:</label>
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'rows'=>'8']) !!}
                    </div>
                    <div class="fv-row mb-9">
                        <label class="fs-6 fw-bold mb-2">Тип мероприятия</label>
                        <div class="d-flex pt-4">
                            <label class="form-check form-check-inline mb-4">
                                <input class="form-check-input" name="type" type="radio" checked value="1">
                                <span class="fw-bold ps-1 fs-6">Онлайн мероприятие</span>
                            </label>
                            <label class="form-check form-check-inline mb-4">
                                <input class="form-check-input" name="type" type="radio" value="0">
                                <span class="fw-bold ps-1 fs-6">Оффлайн мероприятие</span>
                            </label>
                        </div>
                    </div>
                    <div class="fv-row mb-9">
                        <label class="fs-5 fw-bold mb-2">Регион:</label>
                        {!! Form::select('region_id', [null=>'- выбрать -']+$regions, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
                    </div>
                    <div class="fv-row mb-9">
                        <label class="fs-5 fw-bold mb-2">Адрес проведения мероприятия <br>(При онлайн мероприятии указывается ссылка):</label>
                        {!! Form::text('location', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="fv-row row mb-9">
                        <div class="col-lg-6 mb-4 mb-lg-0">
                            <label class="fs-6 fw-bold mb-2 required">Дата мероприятия</label>
                            <input class="form-control form-control-solid date" name="date" id="date" />
                        </div>
                    </div>
                    <div class="fv-row row mb-9">
                        <div class="col-lg-6 mb-4 mb-lg-0">
                            <label class="fs-6 fw-bold mb-2 required">Время начала</label>
                            <input class="form-control form-control-solid" name="start_time" id="start_time" autocomplete="off" />
                        </div>
                        <div class="col-lg-6 mb-4 mb-lg-0">
                            <label class="fs-6 fw-bold mb-2 required">Время завершения</label>
                            <input class="form-control form-control-solid" name="end_time" id="end_time" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Отмена</button>
                    <button type="submit" id="kt_modal_add_event_submit" class="btn btn-primary">
                        <span class="indicator-label">Сохранить</span>
                    </button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="event" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">

        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar4.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar_ru.js') }}"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let calendarEl = document.getElementById('kt_calendar_app');
            let calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                themeSystem: 'bootstrap',
                header: {
                    left: 'prev, next today, printButton',
                    center: 'title',
                    right: 'dayGridMonth, timeGridWeek, listWeek'
                },
                height: 800,
                aspectRatio: 3,
                nowIndicator: true,
                defaultView: 'timeGridWeek',
                displayEventTime: true,
                locale: 'ru',
                timezone: 'Asia/Bishkek',
                timeFormat: 'HH:mm',
                axisFormat: 'hh:mm',
                contentHeight: "auto",
                editable: false,
                eventLimit: 4,
                navLinks: true,
                forceEventDuration: true,
                eventDurationEditable: false,
                events: {
                    url: "{!! route('admin.events.data') !!}",
                    method: 'GET',
                    error: function() {
                        alert('Ошибка!');
                    }
                },
                eventTimeFormat: {
                    hour12: false,
                    hour: '2-digit',
                    minute: '2-digit'
                },
                eventRender: function(info) {
                    let element = $(info.el);
                    element.find('.fc-content').append('<div class="fc-description">'+ info.event.extendedProps.description + '</div>');
                    element.attr('event_id', info.event.id);

                    console.log(info.event);
                    $(info.el).popover({
                        title: info.event.title,
                        trigger : 'hover',
                        container: "body",
                        boundary: "window",
                        placement: "auto",
                        dismiss: !0,
                        html: !0,
                        content: '<div class="fw-bolder mb-2">' + info.event.extendedProps.description + '</div><div class="fs-7"><span class="fw-bold">Время начала:</span> ' + info.event.extendedProps.start_time + '</div><div class="fs-7 mb-4"><span class="fw-bold">Время завершения:</span> ' + info.event.extendedProps.end_time + '</div>'
                    }).popover('show');
                },
                eventClick:  function(info) {
                    let url = '/admin/events/data/view/'+info.event.id;
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        cache: false,
                        type: 'GET',
                        url: url,
                        success: function (data) {
                            $('#event .modal-content').html(data);
                            $('#event').modal('show');
                        },
                    });
                }
            });
            calendar.render();

        });
    </script>

<script src="{{asset('assets/plugins/custom/form-validation/AutoFocus.min.js')}}"></script>"
<script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>"
<script>

    $("#date").flatpickr({
        dateFormat: "Y-m-d",
        defaultDate: "{{date('Y-m-d')}}",
        mode: "range",
        locale: "ru",
    });

    $("#start_time, #end_time").flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        allowInput: true
    });

    const form = document.getElementById('add_event_form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                'title': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'type': {validators: {notEmpty: {message: 'Обязательное поле'}}},
                'date': {validators: {notEmpty: {message: 'Обязательное поле'}}}
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap5({
                    rowSelector: '.fv-row',
                    eleInvalidClass: '',
                    eleValidClass: ''
                }),
                autoFocus: new FormValidation.plugins.AutoFocus(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>
@endsection
