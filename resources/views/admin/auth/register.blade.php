<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Регистрация | {{ config('app.name', 'Site Name') }}</title>

    <!--begin::Fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
    <!--end::Fonts-->
    <!--begin::Page Custom Styles(used by this page)-->
    <link href="{{asset('assets/css/pages/login/classic/login-4.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles-->
    <!--end::Layout Themes-->

    <link href="{{asset('assets/css/app.css')}}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" />

    <style>
        body{
            background: #EEF0F8;
        }
    </style>
</head>

<!--begin::Body-->
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-4 login-signup-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6 p-7 position-relative">
                        <!--begin::Login Header-->
                        <div class="d-flex flex-center mb-15">
                            <a href="#">
                                <svg class="logo-dark h-100px" viewBox="0 0 454.8 188.3">
                                    <g id="Слой_x0020_1">
                                        <g id="_2466012256944">
                                            <path class="st0" d="M262.8,56.9l-0.1-47.2h0l0-5.5c-5.5-1.7-12-2.9-18.9-3.6l-0.3,55.9c4.8,2.7,12,7,19.3,12.2L262.8,56.9
			L262.8,56.9L262.8,56.9z M270.4,74.5c13.6,11,25.1,24.6,20,36.3c-2,4.5-3.1,4.6-7.4,7.6c19.1,0.1,18.9-19.8,10.2-32.7
			c-1.3-2-2.6-3.7-3.8-5.2V15.4c-5.6-4.2-12-7.1-19-9.3v26v30.4C270.4,62.5,270.4,74.5,270.4,74.5z M207.9,34.5l1.5,1v-9.7l-0.1,0
			l0.1-11.7V2.4c-4.5,1-8.5,2-11.6,3.4l0,0l0,0c-1.7,0.8-3.3,1.9-4.9,3.3v0c-2.1,2.3-2.9,5.5-1.9,10C192.3,25.2,201.9,31,207.9,34.5
			L207.9,34.5z M216.3,40.1l0.8,0.5l0,0c4.4,2.6,17.4,10.5,19.7,10.8l0.2-51.3c-6.7-0.2-13.7,0.1-20.5,1l-0.1,21.4L216.3,40.1
			L216.3,40.1z"></path>
                                            <path class="st1" d="M190.3,77l0.1,47.2h0l0,5.5c5.5,1.7,12,2.9,18.9,3.6l0.3-55.9c-4.8-2.7-12-7-19.3-12.2L190.3,77L190.3,77
			L190.3,77z M182.6,59.4c-13.6-11-25.1-24.6-20-36.3c2-4.5,3.1-4.6,7.4-7.6c-19.1-0.1-18.9,19.8-10.2,32.7c1.3,2,2.6,3.7,3.8,5.2
			v65.1c5.6,4.2,12,7.1,19,9.3v-26V71.4V59.4L182.6,59.4z M245.2,99.4l-1.5-1v9.7l0.1,0l-0.1,11.7v11.7c4.5-1,8.5-2,11.6-3.4l0,0
			l0,0c1.7-0.8,3.3-1.9,4.9-3.3v0c2.1-2.3,2.9-5.5,1.9-10C260.7,108.7,251.1,102.8,245.2,99.4C245.2,99.4,245.2,99.4,245.2,99.4z
			 M236.8,93.8l-0.8-0.5l0,0c-4.4-2.6-17.4-10.5-19.7-10.8l-0.2,51.3c6.7,0.2,13.7-0.1,20.5-1l0.1-21.4
			C236.8,111.4,236.8,93.8,236.8,93.8z"></path>
                                            <path class="st2" d="M0,186.6c1.5,0.8,4.4,1.6,7.4,1.6c7.2,0,10.5-3.7,10.5-8.1c0-3.7-2.1-6.1-6.8-7.8c-3.4-1.3-4.9-2-4.9-3.7
			c0-1.3,1.2-2.5,3.8-2.5c2.5,0,4.4,0.7,5.4,1.2l1.3-4.7c-1.6-0.7-3.7-1.3-6.7-1.3c-6.1,0-9.9,3.4-9.9,7.9c0,3.8,2.9,6.2,7.2,7.8
			c3.2,1.1,4.4,2,4.4,3.7c0,1.7-1.4,2.8-4.2,2.8c-2.5,0-5-0.8-6.6-1.6L0,186.6L0,186.6z"></path>
                                            <path class="st2" d="M52.9,181.1l2,6.7h6.4l-8.2-26.2h-7.8l-8,26.2h6.1l1.9-6.7H52.9L52.9,181.1z M46.3,176.7l1.6-5.6
			c0.4-1.5,0.8-3.5,1.2-5.1h0.1c0.4,1.6,0.9,3.5,1.3,5.1l1.6,5.6C52.1,176.7,46.3,176.7,46.3,176.7z"></path>
                                            <path class="st2" d="M82.2,187.8h5.9v-10.5h9.4v-4.8h-9.4v-6h10.1v-4.9h-16V187.8L82.2,187.8z"></path>
                                            <path class="st2" d="M135.6,171.9h-9.6v-5.4h10.2v-4.9H120v26.2h16.7V183h-10.8v-6.2h9.6V171.9L135.6,171.9z"></path>
                                            <path class="st2" d="M165.9,187.7c1.3,0.2,3.5,0.4,6.4,0.4c5.2,0,8.4-0.9,10.3-2.4c1.6-1.3,2.7-3.2,2.7-5.6c0-3.3-2.2-5.6-5.2-6.4
			v-0.1c3-1.1,4.3-3.3,4.3-5.6c0-2.4-1.3-4.2-3.1-5.2c-1.9-1.1-4.2-1.5-7.8-1.5c-3,0-6.1,0.3-7.7,0.5L165.9,187.7L165.9,187.7z
			 M171.8,165.9c0.5-0.1,1.2-0.2,2.5-0.2c2.7,0,4.2,1.1,4.2,3c0,1.9-1.6,3.2-4.8,3.2h-1.9V165.9L171.8,165.9z M171.8,176.2h2
			c3,0,5.3,1.1,5.3,3.7c0,2.8-2.4,3.8-5.1,3.8c-1,0-1.7,0-2.3-0.1V176.2z"></path>
                                            <path class="st2" d="M206.7,161.6v14.6c0,8.3,3.9,12,10.5,12c6.8,0,10.9-3.9,10.9-11.9v-14.7h-5.9v15.1c0,4.7-1.7,6.8-4.8,6.8
			c-3,0-4.7-2.3-4.7-6.8v-15.1H206.7L206.7,161.6z"></path>
                                            <path class="st2" d="M249.5,186.6c1.5,0.8,4.4,1.6,7.4,1.6c7.2,0,10.5-3.7,10.5-8.1c0-3.7-2.1-6.1-6.8-7.8c-3.4-1.3-4.9-2-4.9-3.7
			c0-1.3,1.2-2.5,3.8-2.5c2.5,0,4.4,0.7,5.4,1.2l1.3-4.7c-1.6-0.7-3.7-1.3-6.7-1.3c-6.1,0-9.9,3.4-9.9,7.9c0,3.8,2.9,6.2,7.2,7.8
			c3.2,1.1,4.4,2,4.4,3.7c0,1.7-1.4,2.8-4.2,2.8c-2.5,0-5-0.8-6.6-1.6L249.5,186.6L249.5,186.6z"></path>
                                            <path class="st2" d="M288.9,161.6v26.2h5.9v-26.2H288.9z"></path>
                                            <path class="st2" d="M322.7,187.8v-7.9c0-4.3-0.1-8-0.3-11.5h0.2c1.2,3.1,3,6.5,4.6,9.3l5.6,10.1h6.2v-26.2h-5.4v7.6
			c0,4,0.1,7.5,0.5,11h-0.1c-1.2-3-2.7-6.2-4.3-9l-5.4-9.6h-6.9v26.2L322.7,187.8L322.7,187.8z"></path>
                                            <path class="st2" d="M377,171.9h-9.6v-5.4h10.2v-4.9h-16.1v26.2h16.7V183h-10.8v-6.2h9.6L377,171.9L377,171.9z"></path>
                                            <path class="st2" d="M398.5,186.6c1.5,0.8,4.4,1.6,7.4,1.6c7.2,0,10.5-3.7,10.5-8.1c0-3.7-2.1-6.1-6.8-7.8c-3.4-1.3-4.9-2-4.9-3.7
			c0-1.3,1.2-2.5,3.8-2.5c2.5,0,4.4,0.7,5.4,1.2l1.3-4.7c-1.6-0.7-3.7-1.3-6.7-1.3c-6.1,0-9.9,3.4-9.9,7.9c0,3.8,2.9,6.2,7.2,7.8
			c3.2,1.1,4.4,2,4.4,3.7c0,1.7-1.4,2.8-4.2,2.8c-2.5,0-5-0.8-6.6-1.6L398.5,186.6L398.5,186.6z"></path>
                                            <path class="st2" d="M436.8,186.6c1.5,0.8,4.4,1.6,7.4,1.6c7.2,0,10.5-3.7,10.5-8.1c0-3.7-2.1-6.1-6.8-7.8c-3.4-1.3-4.9-2-4.9-3.7
			c0-1.3,1.2-2.5,3.8-2.5c2.5,0,4.4,0.7,5.4,1.2l1.3-4.7c-1.6-0.7-3.7-1.3-6.7-1.3c-6.1,0-9.9,3.4-9.9,7.9c0,3.8,2.9,6.2,7.2,7.8
			c3.2,1.1,4.4,2,4.4,3.7c0,1.7-1.4,2.8-4.2,2.8c-2.5,0-5-0.8-6.6-1.6L436.8,186.6L436.8,186.6z"></path>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <!--end::Login Header-->

                        <!--begin::Login Sign up form-->
                        <div class="login-signup">
                            <div class="mb-20 text-center">
                                <h3>Регистрация</h3>
                            </div>
                            {!! Form::open(['route' => 'admin.register.submit', 'class' => 'form', 'method' => 'POST']) !!}
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="recommender">Рекомендатель</label>
                                    <div class="col-lg-9">
                                        {!! Form::select('recommender', $participants, null, ['class' => 'selectpicker form-control form-control-solid', 'title' => 'Выбрать', 'data-width' => '100%', 'data-live-search' => 'true', 'data-size' => '6']) !!}
                                        @error('recommender') <span class="form-text text-danger">Выберите рекомендателя</span> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="mentor">Наставник</label>
                                    <div class="col-lg-9">
                                        {!! Form::select('mentor', $participants, null, ['class' => 'selectpicker form-control form-control-solid', 'title' => 'Выбрать', 'data-width' => '100%', 'data-live-search' => 'true', 'data-size' => '6']) !!}
                                        @error('mentor') <span class="form-text text-danger">Выберите наставника</span> @enderror
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <label class="col-lg-3 mt-2 font-weight-bold">Персональный №</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                {!! Form::select('branch', $branches, null, ['class' => 'selectpicker form-control form-control-solid', 'title' => 'Филиал', 'data-width' => '100%', 'data-live-search' => 'true', 'data-size' => '6']) !!}
                                                @error('branch') <span class="form-text text-danger">Выберите филиал</span> @enderror
                                            </div>
                                            <div class="col-lg-4">
                                                {!! Form::text('mentor_code', null, ['class' => 'form-control']) !!}
                                            </div>
                                            <div class="col-lg-4">
                                                {!! Form::text('number', $number, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <label class="col-lg-3 mt-2 font-weight-bold">ФИО</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Фамилия']) !!}
                                                @error('lastname') <span class="form-text text-danger">Введите фамилию</span> @enderror
                                            </div>
                                            <div class="col-lg-4">
                                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                                                @error('name') <span class="form-text text-danger">Введите имя</span> @enderror
                                            </div>
                                            <div class="col-lg-4">
                                                {!! Form::text('patronymic', null, ['class' => 'form-control', 'placeholder' => 'Отчество']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="passport_id">Паспорт №</label>
                                    <div class="col-lg-9">
                                        {!! Form::text('passport_id', null, ['class' => 'form-control']) !!}
                                        @error('passport_id') <span class="form-text text-danger">Введите номер документа</span> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="email">Эл.адрес / Логин</label>
                                    <div class="col-lg-9">
                                        {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                        @error('email') <span class="form-text text-danger">Введите верный эл.адрес</span> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="password">Пароль</label>
                                    <div class="col-lg-9">
                                        {!! Form::password('password', ['class' => 'form-control']) !!}
                                        @error('password') <span class="form-text text-danger">Длина пароля не должна быть менее 6 символов</span> @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="phone">Телефон</label>
                                    <div class="col-lg-9">
                                        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold" for="address">Адрес</label>
                                    <div class="col-lg-9">
                                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 mt-2 font-weight-bold">&nbsp;</label>
                                    <div class="col-lg-9">
                                        <button type="submit" class="btn btn-danger font-weight-bold">Потвердить</button>
                                        <button class="btn btn-light-danger font-weight-bold ml-3">Авторизация</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Login Sign up form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Login-->
</div>
<!--end::Main-->

<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->

<!--begin::Global Theme Bundle(used by all pages)-->
<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
<script src="{{asset('assets/js/scripts.bundle.js')}}"></script>
<!--end::Global Theme Bundle-->

<script>
    $.ajaxSetup({
        headers: { 'X-CSRF-Token': $('meta[name=csrf-token]').attr('content') }
    });
    $('select[name=recommender]').on('change', function() {
        $.ajax({
            url: "{{route('admin.register.recommender')}}",
            cache: false,
            method: 'POST',
            data: {
                'recommender': $(this).val()
            }
        }).done(function(data) {
            $('input[name=mentor_code]').val(data);
        });
    });
</script>

</body>
<!--end::Body-->

</html>
