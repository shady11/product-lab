@extends('admin.default')

@section('title', $permission->name)

@section('content')

    <div class="card card-custom">
        <div class="card-header cursor-pointer">
            <div class="card-title m-0">
                <h4 class="card-title">{{$permission->name}}</h4>
            </div>
            <div class="d-flex">
                <a href="{{route('admin.permissions.index')}}" class="me-3  btn btn-sm btn-light-dark btn-hover-scale align-self-center">Назад</a>
                <a href="{{route('admin.permissions.edit', $permission)}}" class="me-3 btn btn-sm btn-light-primary btn-hover-scale align-self-center">Редактировать</a>
                <a href="{{route('admin.permissions.delete', $permission)}}" class="btn btn-sm btn-light-danger btn-hover-scale align-self-center">Удалить</a>
            </div>
        </div>
        <div class="card-body p-9">
            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">Название</label>
                <div class="col-lg-8">
                    <span class="fw-bolder fs-6 text-gray-800">{{$permission->name}}</span>
                </div>
            </div>
        </div>
    </div>

@endsection