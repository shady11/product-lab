@extends('admin.default')

@section('title', $participant->full_name)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <div class="card card-custom">

                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h4 class="card-title">{{$participant->full_name}}</h4>
                    </div>
                    <!--end::Card title-->

                    <!--begin::Action-->
                    <div class="d-flex">
                        <a href="{{route('admin.participants.index')}}" class="me-3 btn btn-light-dark align-self-center">Назад</a>
                        <a href="#" class="me-3 btn btn-light-primary align-self-center" data-bs-toggle="modal" data-bs-target="#kt_modal_participant_grade">Оценить участника</a>
                        <a href="{{route('admin.participants.delete', $participant)}}" class="btn btn-light-danger align-self-center">Удалить</a>
                    </div>
                    <!--end::Action-->

                    <div class="modal fade" id="kt_modal_participant_grade" tabindex="-1" aria-hidden="true">
                        <!--begin::Modal dialog-->
                        <div class="modal-dialog modal-dialog-centered mw-650px">
                            <!--begin::Modal content-->
                            <div class="modal-content">
                                <!--begin::Modal header-->
                                <div class="modal-header" id="kt_modal_add_user_header">
                                    <!--begin::Modal title-->
                                    <h2 class="fw-bolder">Оценить участника</h2>
                                    <!--end::Modal title-->

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                        <span class="svg-icon svg-icon-2x">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"/>
                                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"/>
                                            </svg>
                                        </span>
                                    </div>
                                    <!--end::Close-->
                                </div>
                                <!--end::Modal header-->
                                <!--begin::Modal body-->
                                <div class="modal-body scroll-y mx-5 mx-xl-12 my-7">
                                    <!--begin::Form-->
                                    <form class="form" action="{{route('admin.participants.grade', $participant)}}" method="POST">
                                        @csrf
                                        <!--begin::Input group-->
                                        <div class="fv-row">
                                            <!--begin::Label-->
                                            <label class="required fw-bold fs-6 mb-2">Оценка</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" name="grade" class="form-control" value="{{$participant->grade}}">
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Actions-->
                                        <div class="text-center pt-12">
                                            <button type="button" class="btn btn-light me-3" data-bs-dismiss="modal">Отмена</button>
                                            <button type="submit" class="btn btn-primary">
                                                Отправить
                                            </button>
                                        </div>
                                        <!--end::Actions-->
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <!--end::Modal body-->
                            </div>
                            <!--end::Modal content-->
                        </div>
                        <!--end::Modal dialog-->
                    </div>
                </div>

                <div class="card-body p-9">

                    <!--begin::Row-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Ф.И.О.</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->full_name}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Дата рождения</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->getBirthDate()}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Пол</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->getGender()}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Номер телефона</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->phone}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Email</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->email}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Деятельность</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->activity}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Область/Город/Село проживания</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$participant->getRegion->getName(app()->getLocale())}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Укажите ваше социально-экономическое положение
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {!! $participant->getSocioEconomicStatus() !!}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Укажите к какому типу вы относитесь больше всего?
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {!! $participant->getType() !!}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Ваша цель участия в проекте UpSkill?
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {!! $participant->getGoal() !!}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Участвовали ли вы уже в других проектах?
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {{$participant->getParticipated()}}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Готовы ли вы принимать участие в обучении?
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {{$participant->getReady()}}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Готовы ли вы принимать участие в обучении?
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {{$participant->ready}}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">
                            Напишите про вашу мотивацию, почему вы хотите принять участие в Лаборатории стартапов?
                        </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">
                                {{$participant->motivation}}
                            </span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection

@section('scripts')

    <script>
        @if(session('status') == 'passed')
            Swal.fire({
                html: "<div class='fw-bold fs-4'>Участник успешно прошел отбор!</div>",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Продолжить",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        @elseif(session('status') == 'failed')
            Swal.fire({
                html: "<div class='fw-bold fs-4'>К сожалению, участник не прошел отбор!</div>",
                icon: "infox",
                buttonsStyling: false,
                confirmButtonText: "Продолжить",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        @endif
    </script>

@endsection














