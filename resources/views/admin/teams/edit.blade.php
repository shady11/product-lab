@extends('admin.default')

@section('title', $title)

@section('content')

    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-0 pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <h3>{{ $title }}</h3>
                </div>
                <!--end::Search-->
            </div>
            <!--begin::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                </div>
                <!--end::Toolbar-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body py-4">
            <!--begin::Datatable-->
        {!! Form::model($row, ['route' => ['admin.teams.update', $row], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
            @include('admin.teams.form', $row)
        {!! Form::close() !!}
        <!--end::Datatable-->
        </div>
        <!--end::Card body-->
    </div>

@endsection

@section('scripts')

@endsection
