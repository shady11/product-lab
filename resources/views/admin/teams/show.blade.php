@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column flex-lg-row">
        <!--begin::Sidebar-->
        <div class="flex-column flex-lg-row-auto w-lg-300px w-xl-400px mb-10">
            <!--begin::Card-->
            <div class="card mb-5 mb-xl-8">
                <!--begin::Card body-->
                <div class="card-body">
                    <!--begin::Summary-->
                    <!--begin::User Info-->
                    <div class="d-flex flex-center flex-column py-5">
                        <!--begin::Avatar-->
                        <div class="symbol symbol-100px symbol-circle mb-7">
                            <img src="{{asset('assets/media/avatars/blank.png')}}" alt="image">
                        </div>
                        <!--end::Avatar-->
                        <!--begin::Name-->
                        <a href="#" class="fs-3 text-gray-800 text-hover-primary fw-bolder mb-3">{{ $row->name }}</a>
                        <!--end::Name-->
                        <!--begin::Position-->
                        <div class="mb-9">
                            <!--begin::Badge-->
                            <!--begin::Badge-->
                        </div>
                        <!--end::Position-->
                    </div>
                    <!--end::User Info-->
                    <!--end::Summary-->
                    <!--begin::Details toggle-->
                    <div class="d-flex flex-stack fs-4 py-3">
                        <div class="fw-bolder rotate collapsible" data-bs-toggle="collapse" href="#kt_user_view_details" role="button" aria-expanded="false" aria-controls="kt_user_view_details">
                            Информация
                        </div>
                        <span>
                            <a href="{{ route('admin.teams.edit', $row) }}" class="btn btn-sm btn-light-primary">
                                Редактировать
                            </a>
                        </span>
                    </div>
                    <!--end::Details toggle-->
                    <div class="separator"></div>
                    <!--begin::Details content-->
                    <div id="kt_user_view_details" class="collapse show">
                        <div class="pb-5 fs-6">
                            <!--begin::Details item-->
                            <div class="fw-boldest mt-5">Название команды</div>
                            <div class="text-gray-600">{{ $row->name }}</div>
                            <!--begin::Details item-->
                            <div class="fw-boldest mt-5">Описание команды</div>
                            <div class="text-gray-600">{{ $row->description }}</div>
                            <!--begin::Details item-->
                            <div class="fw-boldest mt-5">Регион</div>
                            <div class="text-gray-600">{{ $row->region ? $row->region->getName(app()->getLocale()) : '-' }}</div>
                            <!--begin::Details item-->
                            <div class="fw-boldest mt-5">Ментор</div>
                            <div class="text-gray-600">{{ $row->mentor ? $row->mentor->full_name : '-' }}</div>
                        </div>
                    </div>
                    <!--end::Details content-->
                    <div class="separator"></div>
                    <!--begin::Details toggle-->
                    <div class="d-flex flex-stack fs-4 py-6">
                        <span>
                            <a href="{{ route('admin.teams.delete', $row) }}" class="btn btn-sm btn-danger">
                                Удалить
                            </a>
                        </span>
                    </div>
                    <!--end::Details toggle-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Sidebar-->
        <!--begin::Content-->
        <div class="flex-lg-row-fluid ms-lg-15">

            <div class="card mb-6 mb-xl-9">
                <!--begin::Card header-->
                <div class="card-header">
                    <h3 class="card-title">Состав команды</h3>
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#kt_modal_update_details">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1"  transform="rotate(-90 11.364 20.364)" fill="black"></rect>
                                    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                            Добавить участников
                        </a>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pb-5">
                    <!--begin::Table wrapper-->
                    <div class="table-responsive">
                        <!--begin::Table-->
                        <table class="table align-middle table-row-dashed gy-5" id="kt_table_users_login_session">
                            <!--begin::Table head-->
                            <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
                            <!--begin::Table row-->
                            <tr class="text-start text-muted text-uppercase gs-0">
                                <th class="w-40px">№</th>
                                <th>ФИО</th>
                                <th>Телефон</th>
                                <th>Email адресс</th>
                                <th class="min-w-125px text-end">Действия</th>
                            </tr>
                            <!--end::Table row-->
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                            <tbody class="fs-6 fw-bold text-gray-600">
                            @foreach($participants as $p)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $p->name }} {{ $p->lastname }}</td>
                                    <td>{{ $p->phone }}</td>
                                    <td>{{ $p->email }}</td>
                                    <td class="text-end">
                                        <a href="{{ route('admin.teams.delete_user', [$row, $p->id]) }}" class="btn btn-sm btn-light-danger" title="Удалить">
                                            Удалить
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <!--end::Table body-->
                        </table>

                        <!--end::Table-->
                    </div>
                    <!--end::Table wrapper-->
                </div>

                <!--end::Card body-->
            </div>

        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal-->
    <div class="modal fade" id="kt_modal_update_details" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Form-->
                <form class="form" action="{{route('admin.teams.particiantAdd', $row)}}" id="kt_modal_update_user_form"
                      method="POST" enctype="multipart/form-data">
                @csrf
                <!--begin::Modal header-->
                    <div class="modal-header px-lg-12" id="kt_modal_update_user_header">
                        <!--begin::Modal title-->
                        <h2 class="fw-bolder">Добавить участников</h2>
                        <!--end::Modal title-->
                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none">
                                    <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                          transform="rotate(-45 6 17.3137)" fill="black"/>
                                    <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                          transform="rotate(45 7.41422 6)" fill="black"/>
                                </svg>
                            </span>
                        </div>
                        <!--end::Close-->
                    </div>
                    <!--end::Modal header-->
                    <!--begin::Modal body-->
                    <div class="modal-body py-10 px-lg-12">
                        <!--begin::Scroll-->
                        <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_update_user_scroll"
                             data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}"
                             data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_user_header"
                             data-kt-scroll-wrappers="#kt_modal_update_user_scroll" data-kt-scroll-offset="300px">
                            <!--begin::User form-->
                            <div id="kt_modal_update_user_user_info">
                                <!--begin::Input group-->
                                <div class="fv-row mb-7">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-bold mb-2">
                                        <span>Участники</span>
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    {!! Form::select('participants', $participants_select, $participants_selected,['class' => 'form-select', 'data-control'=>'select2', 'multiple'=>'multiple','name'=>'participants[]', 'data-placeholder'=>'Выбрать']) !!}
                                    @if($errors->has('participants'))
                                        <div class="error-message">{{ $errors->first('participants') }}</div>
                                @endif
                                <!--end::Input-->
                                </div>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::User form-->
                    </div>
                    <!--end::Scroll-->
                    <!--end::Modal body-->
                    <!--begin::Modal footer-->
                    <div class="modal-footer flex-center">
                        <!--begin::Button-->
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Отмена</button>
                        <!--end::Button-->
                        <!--begin::Button-->
                        <button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">Сохранить</button>
                        <!--end::Button-->
                    </div>
                    <!--end::Modal footer-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Modal - Update user details-->
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        $("#datepicker_date").flatpickr({
            dateFormat: "d-m-Y",
        });

        @if(session('profile-updated'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Изменения сохранены!</div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif

    </script>

@endsection
