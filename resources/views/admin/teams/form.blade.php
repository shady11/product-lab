<div class="card-body">
    <div class="row fv-row mb-9">
        {!! Form::label('name', 'Наименование команды', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if($errors->has('name'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('description', 'Описание команды', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '3']) !!}
            @if($errors->has('description'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('description') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('sdgs', 'Цель устойчивого развития', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('sdgs[]', $sdg, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -', 'multiple']) !!}
            @if($errors->has('sdgs'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('sdgs') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('region_id', 'Регион', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('region_id', [null=>'- выбрать -']+$regions, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
            @if($errors->has('region_id'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('region_id') }}</div>
            @endif
        </div>
    </div>
    <div class="row mb-9">
        {!! Form::label('participants', 'Участники', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('participants', $participants, $participants_selected,['class' => 'form-select', 'data-control'=>'select2', 'multiple'=>'multiple','name'=>'participants[]', 'data-placeholder'=>'Select an option']) !!}
            @if($errors->has('participants'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('participants') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('mentor_id', 'Ментор', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-5">
            {!! Form::select('mentor_id', [null=>'- выбрать -']+$mentors, $row->mentor ? $row->mentor->id : null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
            @if($errors->has('mentor_id'))
                <div class="error-message fs-7 mt-1 text-danger">{{ $errors->first('mentor') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success mr-2">Сохранить</button>
            <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад</button>
        </div>
    </div>
</div>
