@extends('admin.default')

@section('title', $title)

@section('content')

    <!--begin::Row-->
    <div class="row gy-5 g-xl-10">
        <!--begin::Col-->
        <div class="col-xl-4 mb-xl-10">
            <!--begin::List widget 18-->
            <div class="card h-md-100">
                <!--begin::Header-->
                <div class="card-header py-6">
                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder text-dark fs-3">Участники</span>
                        <span class="text-gray-400 mt-1 fw-bold fs-6">рейтинг по успеваемости</span>
                    </h3>
                    <!--end::Title-->
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    @if($participants)
                        @foreach($participants as $participant)
                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    @if($participant->image)
                                        <!--begin::Icon-->
                                        <img src="{{asset($participant->image)}}" class="me-3 w-30px" alt="" />
                                        <!--end::Icon-->
                                    @else
                                        <span class="symbol symbol-50px me-3">
                                            <span class="symbol-label bg-light-primary">
                                                <!--begin::Svg Icon | path: icons/duotune/finance/fin001.svg-->
                                                <span class="svg-icon svg-icon-2x svg-icon-primary">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 7C10.3 7 9 8.3 9 10C9 11.7 10.3 13 12 13C13.7 13 15 11.7 15 10C15 8.3 13.7 7 12 7Z" fill="black"/>
                                                        <path d="M12 22C14.6 22 17 21 18.7 19.4C17.9 16.9 15.2 15 12 15C8.8 15 6.09999 16.9 5.29999 19.4C6.99999 21 9.4 22 12 22Z" fill="black"/>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    @endif
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{$participant->name . ' ' . $participant->lastname}}</a>
                                        @if($participant->team)
                                            <span class="mt-2 text-gray-400 fw-bold d-block fs-7 lh-1">
                                                Команда:
                                                <span class="d-block mt-1 text-primary">
                                                     {{$participant->team->name}}
                                                </span>
                                            </span>
                                        @endif
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                                @if($participants_exercises)
                                    <!--begin::Statistics-->
                                    <div class="d-flex align-items-center w-100 mw-125px">
                                        <!--begin::Progress-->
                                        <div class="progress h-6px w-100 me-2 bg-light-success">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$participant->exercises->whereNotNull('grade')->count() / $participants_exercises->count() * 100}}%" aria-valuenow="{{$participant->exercises->whereNotNull('grade')->count() / $participants_exercises->count() * 100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <!--end::Progress-->
                                        <!--begin::Value-->
                                        <span class="text-gray-400 fw-bold">{{$participant->exercises->whereNotNull('grade')->count() / $participants_exercises->count() * 100}}%</span>
                                        <!--end::Value-->
                                    </div>
                                    <!--end::Statistics-->
                                @endif
                            </div>
                            <!--end::Item-->
                            @if(!$loop->last)
                                <!--begin::Separator-->
                                <div class="separator separator-dashed my-4"></div>
                                <!--end::Separator-->
                            @endif
                        @endforeach
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <!--end::List widget 18-->
        </div>
        <!--end::Col-->
        <!--begin::Col-->
        <div class="col-xl-8 mb-5 mb-xl-10">
            <!--begin::Chart widget 17-->
            <div class="card h-md-100">
                <!--begin::Header-->
                <div class="card-header py-6">
                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder text-dark">Количество участников по регионам</span>
                        <span class="text-gray-400 mt-1 fw-bold fs-6">в процентном соотношении</span>
                    </h3>
                    <!--end::Title-->
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-5">
                    @if($regions)
                        <!--begin::Chart container-->
                        <div id="kt_charts_widget_17_chart" class="w-100 h-400px"></div>
                        <!--end::Chart container-->
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <!--end::Chart widget 17-->
        </div>
        <!--end::Col-->
    </div>
    <!--end::Row-->

    <!--begin::Row-->
    <div class="row gy-5 g-xl-10">
        <!--begin::Col-->
        <div class="col-xl-4 mb-xl-10">
            <!--begin::List widget 18-->
            <div class="card h-md-100">
                <!--begin::Header-->
                <div class="card-header py-6">
                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder text-dark fs-3">Питчинг</span>
                        <span class="text-gray-400 mt-1 fw-bold fs-6">квалификация команд</span>
                    </h3>
                    <!--end::Title-->
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    @if($teams)
                        @foreach($teams as $team)
                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    @if($team->image)
                                        <!--begin::Icon-->
                                        <img src="{{asset($team->image)}}" class="me-3 w-30px" alt="" />
                                        <!--end::Icon-->
                                    @else
                                        <span class="symbol symbol-50px me-3">
                                            <span class="symbol-label bg-light-primary">
                                                <!--begin::Svg Icon | path: icons/duotune/finance/fin001.svg-->
                                                <span class="svg-icon svg-icon-2x svg-icon-primary">
                                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3" d="M8.70001 6C8.10001 5.7 7.39999 5.40001 6.79999 5.10001C7.79999 4.00001 8.90001 3 10.1 2.2C10.7 2.1 11.4 2 12 2C12.7 2 13.3 2.1 13.9 2.2C12 3.2 10.2 4.5 8.70001 6ZM12 8.39999C13.9 6.59999 16.2 5.30001 18.7 4.60001C18.1 4.00001 17.4 3.6 16.7 3.2C14.4 4.1 12.2 5.40001 10.5 7.10001C11 7.50001 11.5 7.89999 12 8.39999Z" fill="black"></path>
                                                        <path d="M7 20C7 20.2 7 20.4 7 20.6C6.2 20.1 5.49999 19.6 4.89999 19C4.59999 18 4.00001 17.2 3.20001 16.6C2.80001 15.8 2.49999 15 2.29999 14.1C4.99999 14.7 7 17.1 7 20ZM10.6 9.89995C8.70001 8.09995 6.39999 6.89996 3.79999 6.29996C3.39999 6.89996 2.99999 7.49995 2.79999 8.19995C5.39999 8.59995 7.7 9.79996 9.5 11.6C9.8 10.9 10.2 10.3999 10.6 9.89995ZM2.20001 10.1C2.10001 10.7 2 11.4 2 12C2 12 2 12 2 12.1C4.3 12.4 6.40001 13.7 7.60001 15.6C7.80001 14.8 8.09999 14.0999 8.39999 13.3999C6.89999 11.5999 4.70001 10.4 2.20001 10.1ZM11 20C11 14 15.4 8.99996 21.2 8.09996C20.9 7.39996 20.6 6.79995 20.2 6.19995C13.8 7.49995 9 13.0999 9 19.8999C9 20.3999 9.00001 21 9.10001 21.5C9.80001 21.7 10.5 21.7999 11.2 21.8999C11.1 21.2999 11 20.7 11 20ZM19.1 19C19.4 18 20 17.2 20.8 16.6C21.2 15.8 21.5 15 21.7 14.1C19 14.7 16.9 17.1 16.9 20C16.9 20.2 16.9 20.4 16.9 20.6C17.8 20.2 18.5 19.6 19.1 19ZM15 20C15 15.9 18.1 12.6 22 12.1C22 12.1 22 12.1 22 12C22 11.3 21.9 10.7 21.8 10.1C16.8 10.7 13 14.9 13 20C13 20.7 13.1 21.2999 13.2 21.8999C13.9 21.7999 14.5 21.7 15.2 21.5C15.1 21 15 20.5 15 20Z" fill="black"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    @endif
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{$team->name}}</a>
                                        @if($team->participants)
                                            <span class="mt-2 text-gray-400 fw-bold d-block fs-7 lh-1">
                                                Количество участников:
                                                <span class="text-primary">
                                                     {{$team->participants->count()}}
                                                </span>
                                            </span>
                                        @endif
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                                @if($teams_exercises->count() > 0)
                                    <!--begin::Statistics-->
                                    <div class="d-flex align-items-center w-100 mw-125px">
                                        <!--begin::Progress-->
                                        <div class="progress h-6px w-100 me-2 bg-light-success">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$team->exercises->whereNotNull('grade')->count() / $teams_exercises->count() * 100}}%" aria-valuenow="{{$team->exercises->whereNotNull('grade')->count() / $teams_exercises->count() * 100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <!--end::Progress-->
                                        <!--begin::Value-->
                                        <span class="text-gray-400 fw-bold">{{$team->exercises->whereNotNull('grade')->count() / $teams_exercises->count() * 100}}%</span>
                                        <!--end::Value-->
                                    </div>
                                    <!--end::Statistics-->
                                @endif
                            </div>
                            <!--end::Item-->
                            @if(!$loop->last)
                                <!--begin::Separator-->
                                <div class="separator separator-dashed my-4"></div>
                                <!--end::Separator-->
                            @endif
                        @endforeach
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <!--end::List widget 18-->
        </div>
        <!--end::Col-->
        <!--begin::Col-->
        <div class="col-xl-8 mb-5 mb-xl-10">
            <!--begin::Chart widget 17-->
            <div class="card h-md-100">
                <!--begin::Header-->
                <div class="card-header py-6">
                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder text-dark">Количество команд по регионам</span>
                        <span class="text-gray-400 mt-1 fw-bold fs-6">в процентном соотношении</span>
                    </h3>
                    <!--end::Title-->
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-5">
                    @if($regions)
                        <!--begin::Chart container-->
                        <div id="chart_teams_by_region" class="w-100 h-400px"></div>
                        <!--end::Chart container-->
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <!--end::Chart widget 17-->
        </div>
        <!--end::Col-->
    </div>
    <!--end::Row-->

@endsection

@section('scripts')
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <script>

        // Private methods
        var initChart = function () {
            // Check if amchart library is included
            if (typeof am5 === "undefined") {
                return;
            }

            var element = document.getElementById("kt_charts_widget_17_chart");

            if (!element) {
                return;
            }

            am5.ready(function () {
                // Create root element
                // https://www.amcharts.com/docs/v5/getting-started/#Root_element
                var root = am5.Root.new(element);

                // Set themes
                // https://www.amcharts.com/docs/v5/concepts/themes/
                root.setThemes([am5themes_Animated.new(root)]);

                // Create chart
                // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
                // start and end angle must be set both for chart and series
                var chart = root.container.children.push(
                    am5percent.PieChart.new(root, {
                        startAngle: 180,
                        endAngle: 360,
                        layout: root.verticalLayout,
                        innerRadius: am5.percent(50),
                    })
                );

                // Create series
                // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
                // start and end angle must be set both for chart and series
                var series = chart.series.push(
                    am5percent.PieSeries.new(root, {
                        startAngle: 180,
                        endAngle: 360,
                        valueField: "value",
                        categoryField: "category",
                        alignLabels: false,
                    })
                );

                series.labels.template.setAll({
                    fontWeight: "400",
                    fontSize: 13,
                    fill: am5.color(KTUtil.getCssVariableValue('--bs-gray-500'))
                });

                series.states.create("hidden", {
                    startAngle: 180,
                    endAngle: 180,
                });

                series.slices.template.setAll({
                    cornerRadius: 5,
                });

                series.ticks.template.setAll({
                    forceHidden: true,
                });

                // Set data
                // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
                series.data.setAll([
                    @foreach($regions as $region)
                        @if($region->participants->count() > 0)
                            { value: {{$region->participants->count()}}, category: "{{$region->name}}"},
                        @endif
                    @endforeach
                ]);

                series.appear(1000, 100);
            });
        };

        // Private methods
        var teamsByRegionChart = function () {
            // Check if amchart library is included
            if (typeof am5 === "undefined") {
                return;
            }

            var element = document.getElementById("chart_teams_by_region");

            if (!element) {
                return;
            }

            am5.ready(function () {
                // Create root element
                // https://www.amcharts.com/docs/v5/getting-started/#Root_element
                var root = am5.Root.new(element);

                // Set themes
                // https://www.amcharts.com/docs/v5/concepts/themes/
                root.setThemes([am5themes_Animated.new(root)]);

                // Create chart
                // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
                // start and end angle must be set both for chart and series
                var chart = root.container.children.push(
                    am5percent.PieChart.new(root, {
                        startAngle: 180,
                        endAngle: 360,
                        layout: root.verticalLayout,
                        innerRadius: am5.percent(50),
                    })
                );

                // Create series
                // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
                // start and end angle must be set both for chart and series
                var series = chart.series.push(
                    am5percent.PieSeries.new(root, {
                        startAngle: 180,
                        endAngle: 360,
                        valueField: "value",
                        categoryField: "category",
                        alignLabels: false,
                    })
                );

                series.labels.template.setAll({
                    fontWeight: "400",
                    fontSize: 13,
                    fill: am5.color(KTUtil.getCssVariableValue('--bs-gray-500'))
                });

                series.states.create("hidden", {
                    startAngle: 180,
                    endAngle: 180,
                });

                series.slices.template.setAll({
                    cornerRadius: 5,
                });

                series.ticks.template.setAll({
                    forceHidden: true,
                });

                // Set data
                // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
                series.data.setAll([
                    @foreach($regions as $region)
                        @if($region->teams->count() > 0)
                            { value: {{$region->teams->count()}}, category: "{{$region->name}}"},
                        @endif
                    @endforeach
                ]);

                series.appear(1000, 100);
            });
        };

        initChart();
        teamsByRegionChart();

    </script>
@endsection
