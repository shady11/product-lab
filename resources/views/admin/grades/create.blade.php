@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">
            <div class="card card-custom">
                <div class="card-header">
                    <h4 class="card-title">Добавить</h4>
                </div>

                {!! Form::model($grade, ['route' => 'admin.grades.store', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                    @include('admin.grades.form')
                {!! Form::close() !!}
            </div>
        </div>
        <!--end::Container-->
    </div>
@endsection








