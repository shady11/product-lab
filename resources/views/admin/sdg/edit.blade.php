@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <div class="card card-custom">
                <div class="card-header">
                    <h4 class="card-title">Редактировать</h4>
                </div>

                {!! Form::model($sdg, ['route' => ['admin.sdg.update', $sdg], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                    @include('admin.sdg.form', $sdg)
                {!! Form::close() !!}

            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection



