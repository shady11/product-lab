@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <div class="card card-custom">

                <div class="card-header cursor-pointer">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h4 class="card-title">Посмотреть</h4>
                    </div>
                    <!--end::Card title-->

                    <!--begin::Action-->
                    <div class="d-flex">
                        <a href="{{route('admin.mail_messages.index')}}" class="me-3 btn btn-light-dark align-self-center">Назад</a>
                        <a href="{{route('admin.mail_messages.edit', $mail_message)}}" class="me-3 btn btn-light-primary align-self-center">Редактировать</a>
                        <a href="{{route('admin.mail_messages.delete', $mail_message)}}" class="btn btn-light-danger align-self-center">Удалить</a>
                    </div>
                    <!--end::Action-->
                </div>


                <div class="card-body p-9">

                    <!--begin::Row-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Код</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$mail_message->code}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->

                    <!--begin::Row-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Название</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$mail_message->name}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->

                    <div class="separator separator-dashed my-9"></div>

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Header</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$mail_message->header}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Row-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Body</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$mail_message->content}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->

                    <!--begin::Row-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Link Text</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$mail_message->link}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->

                    <!--begin::Input group-->
                    <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-3 text-end fw-bold text-muted">Footer</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <span class="fw-bolder fs-6 text-gray-800">{{$mail_message->footer}}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                </div>

            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection













