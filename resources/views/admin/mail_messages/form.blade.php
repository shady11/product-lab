<div class="card-body border-top p-9" data-select2-id="select2-data-151-4n1m">
    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="name_ru">Код</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            {!! Form::text('code', null, ['class' => 'form-control']) !!}
            @if($errors->has('code'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('code')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="name">Название</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if($errors->has('name'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('name')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <div class="separator separator-dashed my-9"></div>
    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="header">Header</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            {!! Form::text('header', null, ['class' => 'form-control']) !!}
            @if($errors->has('header'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('header')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="content">Body</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => 6]) !!}
            @if($errors->has('content'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('content')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="link">Link Text</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            {!! Form::textarea('link', null, ['class' => 'form-control', 'rows' => 6]) !!}
            @if($errors->has('link'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('link')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="footer">Footer</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row fv-plugins-icon-container">
            {!! Form::text('footer', null, ['class' => 'form-control']) !!}
            @if($errors->has('footer'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('footer')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success" id="kt_account_profile_details_submit">Сохранить</button>
            <button type="reset" class="btn btn-secondary btn-active-secondary-primary me-2" onclick="window.history.back()">Назад</button>
        </div>
    </div>
</div>




@push('scripts')
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

    <script>
        $( document ).ready(function() {
            // custom format style
            tinyMCE.init({
                selector: '#content,#content_ru',
                plugins: "autoresize link lists media image  table textcolor lists code quickbars paste",
                menubar: false,
                toolbar: [
                    'undo redo | styleselect | bold italic link | alignleft aligncenter alignright alignjustify | outdent indent | code',
                ]
            });
        });
    </script>
@endpush

