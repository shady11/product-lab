@extends('admin.default')

@section('title', $title)

@section('content')

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="w-100">

            <div class="card card-custom">
                <div class="card-header">
                    <h4 class="card-title">Добавить</h4>
                </div>

                {!! Form::model($feedback, ['route' => 'admin.landing.feedbacks.store', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                    @include('admin.feedbacks.form')
                {!! Form::close() !!}
            </div>
        </div>
        <!--end::Container-->
    </div>
@endsection








