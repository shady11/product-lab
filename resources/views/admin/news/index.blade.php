@extends('admin.default')

@section('title', $title)

@section('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-0 pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <input type="text" id="search_handler" class="form-control w-250px ps-14" placeholder="Поиск" />
                </div>
                <!--end::Search-->
            </div>
            <!--end::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <a href="{{route('admin.landing.news.create')}}" class="btn btn-success ms-3">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>
                                <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        Создать
                    </a>
                </div>
                <!--end::Toolbar-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body py-4 position-relative">
            <!--begin: Datatable-->
            <table id="dataTable" class="table align-middle table-row-dashed table-row-dashed fs-6 gy-5">
                <thead>
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th class="w-50px">№</th>
                    <th>имя (на русском)</th>
                    <th>имя (на кыргызском)</th>
                    <th>дата создание</th>
                    <th class="text-end min-w-100px">Действия</th>
                </tr>
                </thead>
                <tbody class="text-gray-600 fw-bold"></tbody>
            </table>
            <!--end: Datatable-->
        </div>
        <!--end::Card body-->
    </div>

@endsection

@section('scripts')

    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

    <script>
        let table = $('#dataTable').DataTable({
            dom: 'tr'+
                '<"row"<"col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"li><"col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end dataTables_pager"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.landing.news.index') }}',
                data: function(d) {
                }
            },
            columns: [
                { data: 'id'},
                { data: 'name_ru'},
                { data: 'name'},
                { data: 'created_at'},
                { data: 'actions', className: 'text-end'},
            ],
            order: [[ 0, "desc" ]],
            pageLength: 20,
            lengthMenu: ['20', '30', '40', '50', '100'],
            language: {
                "url": "{{asset('assets/js/russian.json')}}"
            },
        });

        // Search Datatable
        const filterSearch = $('#search_handler');

        filterSearch.keyup(function (e) {
            table.search(e.target.value).draw();
        });

    </script>
@endsection
