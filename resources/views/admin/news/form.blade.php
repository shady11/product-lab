<div class="card-body border-top p-9" data-select2-id="select2-data-151-4n1m">

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="name_ru">Имя на русском</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row fv-plugins-icon-container">
            <input type="text" name="name_ru" class="form-control" placeholder="Заголовок" value="{{ old('name_ru') ?: $news->name_ru}}">
            @if($errors->has('name_ru'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('name_ru')}}</div>
            @endif
        </div>
        <!--end::Col-->

    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="name">Имя на кыргызском</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row fv-plugins-icon-container">
            <input type="text" name="name" class="form-control" placeholder="Аталышы" value="{{ old('name') ?: $news->name}}">
            @if($errors->has('name'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('name')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6 fv-plugins-icon-container">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label required fw-bold fs-6 text-end" for="published_at">Дата публикации</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row fv-plugins-icon-container">
            <input class="form-control flatpickr-input" placeholder="Выберите дату" id="published_at" value="{{ old('published_at') ?: $news->published_at}}" name="published_at" type="text" readonly="readonly">
            @if($errors->has('published_at'))
                <div class="fv-plugins-message-container invalid-feedback ms-2">{{$errors->first('published_at')}}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label fw-bold fs-6 text-end">Изображение</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9">
            <!--begin::Image input-->
            <div class="image-input image-input-outline" data-kt-image-input="true" style="@if ($news->img) background-image: url({{asset($news->img)}}); background-position: center center; @endif">

                <!--begin::Preview existing avatar-->
                <div class="image-input-wrapper w-400px h-300px" style="
                    background-image: url( {{old('img') ?: asset($news->img_thumbnail)}} );
                    background-position: center center;">
                </div>
                <!--end::Preview existing avatar-->

                <!--begin::Label-->
                <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="" data-bs-original-title="Изменить изображению">
                    <i class="bi bi-pencil-fill fs-7"></i>
                    <!--begin::Inputs-->
                    <input type="file" name="img" accept=".png, .jpg, .jpeg" value="{{old('img') ?: $news->img_thumbnail}}">
                    <input type="hidden" name="img_remove">
                    <!--end::Inputs-->
                </label>
                <!--end::Label-->
                <!--begin::Cancel-->
                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="" data-bs-original-title="Удалить изображению">
                    <i class="bi bi-x fs-2"></i>
                </span>
                <!--end::Cancel-->
                <!--begin::Remove-->
                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="" data-bs-original-title="Удалить изображение">
                    <i class="bi bi-x fs-2"></i>
                </span>
                <!--end::Remove-->
            </div>
            <!--end::Image input-->
            <!--begin::Hint-->
            <div class="form-text">Допустимые разрешения: png, jpg, jpeg.</div>
                <!--end::Hint-->
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label fw-bold fs-6 text-end" for="content_ru">Контент на русском</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row">
            <textarea class="form-control" name="content_ru" id="content_ru">
                {{old('content_ru') ?: $news->content_ru}}
            </textarea>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-3 col-form-label fw-bold fs-6 text-end" for="content">Контент на кыргызском</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row">
            <textarea class="form-control" name="content" id="content">
                {{old('content') ?: $news->content}}
            </textarea>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
</div>


<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-9">
            <button type="submit" class="btn btn-success" id="kt_account_profile_details_submit">Сохранить</button>
            <button type="reset" class="btn btn-secondary btn-active-secondary-primary me-2" onclick="window.history.back()">Назад</button>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
    <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>

    <script>
        $( document ).ready(function() {
            // tinyMCE editor
            tinyMCE.init({
                height: 400,
                selector: '#content,#content_ru',
                plugins: "link lists media image table textcolor code quickbars paste",
                menubar: false,
                toolbar: [
                    'undo redo | styleselect | bold italic link | numlist bullist |  alignleft aligncenter alignright alignjustify | outdent indent | code',
                ]
            });

            // date pick plugin
            $("#published_at").flatpickr({
                locale: "ru",
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                time_24hr: true
            });
        });
    </script>
@endpush

