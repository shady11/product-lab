<!--begin::Header-->
<div id="kt_header" class="header">
    <!--begin::Header top-->
    <div class="header-top d-flex align-items-stretch flex-grow-1">
        <!--begin::Container-->
        <div class="d-flex container-xxl w-100">
            <!--begin::Wrapper-->
            <div class="d-flex flex-stack align-items-stretch w-100">
                <!--begin::Brand-->
                <div class="d-flex align-items-center align-items-lg-stretch me-5">
                    <!--begin::Heaeder navs toggle-->
                    <button class="d-lg-none btn btn-icon btn-color-white bg-hover-white bg-hover-opacity-10 w-35px h-35px h-md-40px w-md-40px ms-n2 me-2" id="kt_header_navs_toggle">
                        <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                                <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </button>
                    <!--end::Heaeder navs toggle-->
                    <!--begin::Logo-->
                    <a href="{{route('admin.index')}}" class="d-flex align-items-center">
                        <img alt="Logo" src="{{asset('assets/logo.png')}}" class="h-25px h-lg-50px" />
                    </a>
                    <!--end::Logo-->
                    <div class="align-self-end" id="kt_brand_tabs">
                        <!--begin::Header tabs-->
                        <div class="header-tabs overflow-auto mx-4 ms-lg-10 mb-5 mb-lg-0" id="kt_header_tabs" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_header_navs_wrapper', lg: '#kt_brand_tabs'}">
                            <ul class="nav flex-nowrap">
                                @can('teams-index')
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.team') ? 'active' : ''}}" href="{{route('admin.teams.index')}}">Команды</a>
                                </li>
                                @endcan
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.exercises') ? 'active' : ''}} {{str_contains(Route::currentRouteName(), 'admin.materials') ? 'active' : ''}} {{str_contains(Route::currentRouteName(), 'admin.topics') ? 'active' : ''}}" href="{{route('admin.topics.index')}}">Обучение</a>
                                </li>
                                @can('mentors-index')
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.events') ? 'active' : ''}}" href="{{route('admin.events.index')}}">Мероприятия</a>
                                </li>
                                @endcan
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.participants') ? 'active' : ''}}" href="{{route('admin.participants.index')}}">Заявки</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.report') ? 'active' : ''}}" href="{{route('admin.report.index')}}">Отчеты</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.landing.') ? 'active' : ''}}" data-bs-toggle="tab" href="#kt_header_navs_tab_6">Лендинг</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{str_contains(Route::currentRouteName(), 'admin.users') || str_contains(Route::currentRouteName(), 'admin.permissions') || str_contains(Route::currentRouteName(), 'admin.roles') || str_contains(Route::currentRouteName(), 'admin.regions') || str_contains(Route::currentRouteName(), 'admin.sdg') || str_contains(Route::currentRouteName(), 'admin.grades') || str_contains(Route::currentRouteName(), 'admin.mail_messages') ? 'active' : ''}}" data-bs-toggle="tab" href="#kt_header_navs_tab_7">Система</a>
                                </li>
                            </ul>
                        </div>
                        <!--end::Header tabs-->
                    </div>
                </div>
                <!--end::Brand-->
                <!--begin::Topbar-->
                <div class="d-flex align-items-center flex-shrink-0">
                    <!--begin::User-->
                    <div class="d-flex align-items-center ms-1" id="kt_header_user_menu_toggle">
                        <!--begin::User info-->
                        <div class="btn btn-flex align-items-center bg-hover-white bg-hover-opacity-10 py-2 px-2 px-md-3" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                            <!--begin::Name-->
                            <div class="d-none d-md-flex flex-column align-items-end justify-content-center me-2 me-md-4">
                                <span class="text-white fs-6 fw-bolder lh-1">
                                    @if(auth()->user()->name) {{auth()->user()->getName()}} @else {{auth()->user()->login}} @endif
                                </span>
                            </div>
                            <!--end::Name-->
                            <!--begin::Symbol-->
                            <div class="symbol symbol-30px symbol-md-40px">
                                <img src="@if(auth()->user()->image) {{asset(auth()->user()->image)}} @else {{asset('assets/media/svg/avatars/blank.svg')}} @endif" alt="image" />
                            </div>
                            <!--end::Symbol-->
                        </div>
                        <!--end::User info-->
                        <!--begin::User account menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <div class="menu-content d-flex align-items-center px-3">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-50px me-5">
                                        <img alt="Logo" src="@if(auth()->user()->image) {{asset(auth()->user()->image)}} @else {{asset('assets/media/svg/avatars/blank.svg')}} @endif" />
                                    </div>
                                    <!--end::Avatar-->
                                    <!--begin::Username-->
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder d-flex align-items-center fs-5">
                                            @if(auth()->user()->name) {{auth()->user()->getName()}} @else {{auth()->user()->login}} @endif
                                        </div>
                                        <div class="fw-bold text-muted fs-7">
                                            Администратор
                                        </div>
                                    </div>
                                    <!--end::Username-->
                                </div>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu separator-->
                            <div class="separator my-2"></div>
                            <!--end::Menu separator-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5">
                                <a href="{{route('admin.users.edit', auth()->user())}}" class="menu-link px-5">Мой профиль</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu separator-->
                            <div class="separator my-2"></div>
                            <!--end::Menu separator-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start">
                                <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}" class="menu-link px-5">
                                    <span class="menu-title position-relative">
                                        Язык
                                        <span class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">
                                            @if(app()->getLocale() == 'ru')
                                                Русский
                                                <img class="w-15px h-15px rounded-1 ms-2" src="{{asset('assets/media/flags/russia.svg')}}" alt="" />
                                            @else
                                                Кыргызча
                                                <img class="w-15px h-15px rounded-1 ms-2" src="{{asset('assets/media/flags/kyrgyzstan.svg')}}" alt="" />
                                            @endif

                                        </span>
                                    </span>
                                </a>
                                <!--begin::Menu sub-->
                                <div class="menu-sub menu-sub-dropdown w-175px py-4">
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}" class="menu-link d-flex px-5 @if(app()->getLocale() == 'ru') active @endif">
                                            <span class="symbol symbol-20px me-4">
                                                <img class="rounded-1" src="{{asset('assets/media/flags/russia.svg')}}" alt="" />
                                            </span>
                                            Русский
                                        </a>
                                    </div>
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a href="{{ LaravelLocalization::getLocalizedURL('ky', null, [], true) }}" class="menu-link d-flex px-5 @if(app()->getLocale() == 'ky') active @endif">
                                            <span class="symbol symbol-20px me-4">
                                                <img class="rounded-1" src="{{asset('assets/media/flags/kyrgyzstan.svg')}}" alt="" />
                                            </span>
                                            Кыргызча
                                        </a>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu sub-->
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5 my-1">
                                <a href="#" class="menu-link px-5">Настройки</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5">
                                <a href="{{route('admin.logout')}}" class="menu-link px-5">Выйти</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::User account menu-->
                    </div>
                    <!--end::User -->
                    <!--begin::Heaeder menu toggle-->
                    <!--end::Heaeder menu toggle-->
                </div>
                <!--end::Topbar-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header top-->
    <!--begin::Header navs-->
    <div class="header-navs d-flex align-items-stretch flex-stack h-lg-70px w-100 py-5 py-lg-0" id="kt_header_navs" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_header_navs_toggle" data-kt-swapper="true" data-kt-swapper-mode="append" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header'}">
        <!--begin::Container-->
        <div class="d-lg-flex container-xxl w-100">
            <!--begin::Wrapper-->
            <div class="d-lg-flex flex-column justify-content-lg-center w-100" id="kt_header_navs_wrapper">
                <!--begin::Header tab content-->
                <div class="tab-content" data-kt-scroll="true" data-kt-scroll-activate="{default: true, lg: false}" data-kt-scroll-height="auto" data-kt-scroll-offset="70px">
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'admin.index') ? 'active show' : ''}}" id="kt_header_navs_tab_1">
                    </div>
                    <!--end::Tab panel-->
                    @can('teams-index')
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'admin.team') ? 'active show' : ''}}" id="kt_header_navs_tab_2">
                        <!--begin::Menu wrapper-->
                        <div class="header-menu flex-column align-items-stretch flex-lg-row">
                            <!--begin::Menu-->
                            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.teams.index') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.teams.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Все команды</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.teams.create') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.teams.create')}}" class="menu-link py-3">
                                        <span class="menu-title">Создать команду</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Tab panel-->
                    @endcan
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'admin.exercises') ? 'active show' : ''}} {{str_contains(Route::currentRouteName(), 'admin.materials') ? 'active show' : ''}} {{str_contains(Route::currentRouteName(), 'admin.topics') ? 'active show' : ''}}" id="kt_header_navs_tab_3">
                        <!--begin::Menu wrapper-->
                        <div class="header-menu flex-column align-items-stretch flex-lg-row">
                            <!--begin::Menu-->
                            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.topics.index') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.topics.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Темы</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.topics.create') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.topics.create')}}" class="menu-link py-3">
                                        <span class="menu-title">Создать тему</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.exercises.create') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.exercises.create')}}" class="menu-link py-3">
                                        <span class="menu-title">Создать задание</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.materials.create') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.materials.create')}}" class="menu-link py-3">
                                        <span class="menu-title">Создать материал</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.exercises.index') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.exercises.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Все задания</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Tab panel-->
                    @can('mentors-index')
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade" id="kt_header_navs_tab_4">
                    </div>
                    <!--end::Tab panel-->
                    @endcan
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'admin.report.') ? 'active show' : ''}}" id="kt_header_navs_tab_5">
                        <!--begin::Menu wrapper-->
                        <div class="header-menu flex-column align-items-stretch flex-lg-row">
                            <!--begin::Menu-->
                            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                <div class="menu-item me-lg-1">
                                    <a href="{{route('admin.report.index')}}" class="menu-link py-3">
                                        <span class="menu-title">По участникам</span>
                                    </a>
                                </div>
                                <div class="menu-item me-lg-1">
                                    <a href="{{route('admin.report.index')}}" class="menu-link py-3">
                                        <span class="menu-title">По командам</span>
                                    </a>
                                </div>
                                <div class="menu-item me-lg-1">
                                    <a href="{{route('admin.report.ishtapp')}}" class="menu-link py-3">
                                        <span class="menu-title">По приложению ishtapp</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Tab panel-->
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'admin.landing.') ? 'active show' : ''}}" id="kt_header_navs_tab_6">
                        <!--begin::Menu wrapper-->
                        <div class="header-menu flex-column align-items-stretch flex-lg-row">
                            <!--begin::Menu-->
                            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.landing.news') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.landing.news.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Новости</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.landing.mentors') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.landing.mentors.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Тренера / Менторы</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.landing.feedbacks') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.landing.feedbacks.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Отзывы</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Tab panel-->
                    <!--begin::Tab panel-->
                    <div class="tab-pane fade {{str_contains(Route::currentRouteName(), 'admin.users') || str_contains(Route::currentRouteName(), 'admin.permissions') || str_contains(Route::currentRouteName(), 'admin.roles') || str_contains(Route::currentRouteName(), 'admin.regions') || str_contains(Route::currentRouteName(), 'admin.sdg') || str_contains(Route::currentRouteName(), 'admin.grades') || str_contains(Route::currentRouteName(), 'admin.mail_messages') ? 'active show' : ''}}" id="kt_header_navs_tab_7">
                        <!--begin::Menu wrapper-->
                        <div class="header-menu flex-column align-items-stretch flex-lg-row">
                            <!--begin::Menu-->
                            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.users') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.users.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Пользователи</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.roles') ? 'here show' : ''}} me-lg-1">
                                    <a href="{{route('admin.roles.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Роли</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.permissions') ? 'here show' : ''}}  me-lg-1">
                                    <a href="{{route('admin.permissions.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Права</span>
                                    </a>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.grades') ? 'here show' : ''}}  me-lg-1">
                                    <a href="{{route('admin.grades.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Баллы</span>
                                    </a>
                                </div>
                                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1 {{str_contains(Route::currentRouteName(), 'admin.regions') || str_contains(Route::currentRouteName(), 'admin.sdg') ? 'here show' : ''}}">
                                    <span class="menu-link py-3">
                                        <span class="menu-title">Справочник</span>
                                        <span class="menu-arrow d-lg-none"></span>
                                    </span>
                                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px" data-popper-placement="bottom-start" style="z-index: 105; position: fixed; inset: 0px auto auto 0px; margin: 0px; transform: translate(343px, 125px);">
                                        <div class="menu-item">
                                            <a class="menu-link px-6 py-3" href="{{route('admin.regions.index')}}" title="" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                <span class="menu-title">Регионы</span>
                                            </a>
                                        </div>
                                        <div class="menu-item">
                                            <a class="menu-link px-6 py-3" href="{{route('admin.sdg.index')}}" title="" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                                <span class="menu-title">ЦУР (Цели Устойчивого Развития)</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="menu-item {{str_contains(Route::currentRouteName(), 'admin.mail_messages') ? 'here show' : ''}}  me-lg-1">
                                    <a href="{{route('admin.mail_messages.index')}}" class="menu-link py-3">
                                        <span class="menu-title">Почтовые сообщения</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Tab panel-->
                </div>
                <!--end::Header tab content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header navs-->
</div>
<!--end::Header-->
