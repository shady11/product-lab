@extends('admin.default')
@section('title', $title)
@section('content')

    <div class="row">
        <div class="col-md-8">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <h3>{{ $title }}</h3>
                        </div>
                        <!--end::Search-->
                    </div>


                    <div class="card">
                        <div class="card-body">
                            <div class="card-body">

                                {!! Form::model($row, ['route' => 'admin.exercises.store','enctype' => 'multipart/form-data']) !!}
                                <div class="row mb-6">
                                    {!! Form::label('name', 'Название', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                    <div class="col-lg-12">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
                                        @if($errors->has('name'))
                                            <div class="error-message">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    {!! Form::label('description', 'Описание', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                    <div class="col-lg-12">
                                       <textarea class="form-control" name="description" id="description" disabled readonly>
                                            {{old('description') ?: $row->description }}
                                        </textarea>
                                        @if($errors->has('description'))
                                            <div class="error-message">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    {!! Form::label('link', 'Ссылка на материал', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                    <div class="col-lg-12">
                                        {!! Form::text('link', null, ['class' => 'form-control', 'disabled']) !!}
                                        @if($errors->has('link'))
                                            <div class="error-message">{{ $errors->first('link') }}</div>
                                        @endif
                                    </div>
                                </div>
                                @if(isset($row->file))
                                <div class="row">
                                    {!! Form::label('deadline', 'Файл', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                    <div class="col-lg-12">
                                        <a class="current-attachment" href="{{ asset($row->file) }}">{{ $row->file }}</a>&nbsp;<a href="#" class="current-attachment"><span class="glyphicon glyphicon-remove-circle remove-attachment"></span></a>
                                    </div>
                                </div>
                                @endif
                                @if($row->type == 1)
                                    <div class="row">
                                        {!! Form::label('grade', 'Тип материала', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                        <div class="col-lg-12">
                                            {!! Form::text('grade', 'Индивидуальный материал', ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        {!! Form::label('type', 'Тип материала', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                        <div class="col-lg-12">
                                            {!! Form::text('type', 'Командный материал', ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                    </div>
                                @endif
                                <!--end::Table-->
                                <div class="row">
                                    {!! Form::label('topic', 'Тема', ['class' => 'col-lg-12 col-form-label text-right']) !!}
                                    <div class="col-lg-12">
                                        {!! Form::text('topic', $row->getTopic(), ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                </div>
                                <!--end::Table-->

                                <div class="card-footer d-flex justify-content-end">
                                    <button type="reset" onclick="window.history.back();"
                                            class="btn btn-secondary me-2">Назад
                                    </button>
                                    <a href="{{route('admin.materials.edit', $row)}}" class="btn btn-primary">Редактировать</a>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->

            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        $('.btn-estimate').on('click', function () {
            let id = $(this).data('participant');

            $.ajax({
                url: "{{route('admin.exercise.participant.info', $row)}}",
                cache: false,
                method: 'POST',
                data: {
                    'participant_id': id
                }
            }).done(function (data) {
                console.log(data);
                $('#participantFullName').html(data['full_name']);
                if (data['file']) {
                    $('#participantExerciseFile').html('<a href="' + data['file'] + '" target="_blank">Ссылка на файл</a>');
                }
                $('#modal_exercise_participant_grade').append('<input type="hidden" name="participant_id" value="' + data['id'] + '">')
            });
        });


        @if(session('exercise-graded'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Оценка на задание добавлена!</div>",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @elseif(session('exercise-not-graded'))
        Swal.fire({
            html: "<div class='fw-bold fs-4'>Задание еще не выполнено!</div>",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Продолжить",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        });
        @endif

    </script>

@endsection

@push('scripts')
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
    <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>

    <script>
        $(document).ready(function () {
            // tinyMCE editor
            tinyMCE.init({
                selector: '#description',
                plugins: "autoresize link lists media image  table textcolor lists code quickbars paste",
                menubar: false,
                toolbar: [
                    'undo redo | styleselect | bold italic link | alignleft aligncenter alignright alignjustify | outdent indent | code',
                ]
            });

            // date pick plugin
            $("#published_at").flatpickr({
                locale: "ru",
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                time_24hr: true
            });
        });
    </script>
@endpush
