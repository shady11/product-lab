<div class="card-body">
    <div class="form-group row mb-9">
        {!! Form::label('name', 'Название', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if($errors->has('name'))
                <div class="error-message">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row mb-9">
        <!--begin::Label-->
        {!! Form::label('description', 'Описание', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-9 fv-row">
            <textarea class="form-control" name="description" id="description">
                {{old('description') ?: $row->description }}
            </textarea>
            @if($errors->has('description'))
                <div class="error-message">{{ $errors->first('description') }}</div>
            @endif
        </div>
        <!--end::Col-->
    </div>
    <div class="form-group row mb-9">
        {!! Form::label('link', 'Ссылка на материал', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('link', null, ['class' => 'form-control']) !!}
            @if($errors->has('link'))
                <div class="error-message">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row mb-9">
        {!! Form::label('videourl', 'Ссылка на видео (Youtube)', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::text('videourl', null, ['class' => 'form-control']) !!}
            @if($errors->has('videourl'))
                <div class="error-message">{{ $errors->first('videourl') }}</div>
            @endif
        </div>
    </div>
    <div class="row fv-row mb-9">
        {!! Form::label('file', 'Файл', ['class' => 'col-lg-3 col-form-label text-right']) !!}
        <div class="col-lg-9">
            {!! Form::file('file', ['class' => 'form-control']) !!}
            <a class="current-attachment" href="{{ asset($row->file) }}">{{ $row->file }}</a>&nbsp;<a href="#" class="current-attachment"><span class="glyphicon glyphicon-remove-circle remove-attachment"></span></a>
        </div>
    </div>
    <div class="radio mb-9">
        <div class="form-radio row">
            {!! Form::label('type', 'Выберите язык', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9 py-3">
                @if($row->type and $row->type == 1)
                    <div class="form-check form-check-inline" id="individual">
                        <input class="form-check-input" type="radio" name="type" id="type" value="1" checked>
                        <label class="form-check-label" for="type1">
                            На русском
                        </label>
                    </div>
                    <div class="form-check form-check-inline" id="team">
                        <input class="form-check-input" type="radio" name="type" id="type2" value="2">
                        <label class="form-check-label" for="type2">
                            На кыргызском
                        </label>
                    </div>
                @elseif($row->type and $row->type == 2)
                    <div class="form-check form-check-inline" id="individual">
                        <input class="form-check-input" type="radio" name="type" id="type" value="1">
                        <label class="form-check-label" for="type1">
                            На русском
                        </label>
                    </div>
                    <div class="form-check form-check-inline" id="team">
                        <input class="form-check-input" type="radio" name="type" id="type2" value="2" checked>
                        <label class="form-check-label" for="type2">
                            На кыргызском
                        </label>
                    </div>
                @else
                    <div class="form-check form-check-inline" id="individual">
                        <input class="form-check-input" type="radio" name="type" id="type" value="1">
                        <label class="form-check-label" for="type1">
                            На русском
                        </label>
                    </div>
                    <div class="form-check form-check-inline" id="team">
                        <input class="form-check-input" type="radio" name="type" id="type2" value="2">
                        <label class="form-check-label" for="type2">
                            На кыргызском
                        </label>
                    </div>
                @endif
                @if($errors->has('type'))
                    <div class="error-message">{{ $errors->first('type') }}</div>
                @endif
            </div>
        </div>
    </div>
    @if(isset($topic))
        <div class="form-group row mb-9">
            {!! Form::label('topic_id', 'Тема', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9">
                {!! Form::select('topic_id', [null=>'- выбрать -']+$topics, [$topic->id],['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
                @if($errors->has('topic_id'))
                    <div class="error-message">{{ $errors->first('topic_id') }}</div>
                @endif
            </div>
        </div>
    @else
        <div class="form-group row mb-9">
            {!! Form::label('topic_id', 'Тема', ['class' => 'col-lg-3 col-form-label text-right']) !!}
            <div class="col-lg-9">
                {!! Form::select('topic_id', [null=>'- выбрать -']+$topics, null,['class' => 'form-select', 'data-control'=>'select2', 'data-placeholder'=>'- выбрать -']) !!}
                @if($errors->has('topic_id'))
                    <div class="error-message">{{ $errors->first('topic_id') }}</div>
                @endif
            </div>
        </div>
    @endif

</div>
<div class="card-footer">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <button type="submit" class="btn btn-success mr-2">Сохранить</button>
            <button type="reset" onclick="window.history.back();" class="btn btn-secondary">Назад
            </button>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $("#datepicker_date").flatpickr({
            dateFormat: "d-m-Y",
        });
    </script>
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
    <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>

    <script>
        $(document).ready(function () {
            // tinyMCE editor
            tinyMCE.init({
                selector: '#description',
                plugins: "autoresize link lists media image  table textcolor lists code quickbars paste",
                menubar: false,
                toolbar: [
                    'undo redo | styleselect | bold italic link | alignleft aligncenter alignright alignjustify | outdent indent | code',
                ]
            });

            // date pick plugin
            $("#published_at").flatpickr({
                locale: "ru",
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                time_24hr: true
            });
        });
    </script>
@endpush
